Ezbizi is an international deal-making platform with social networking
features. It is designed for small-medium sized companies to build
lasting business relationships more effectively. Ezbizi helps companies
generate targeted business leads and connect them in a bustling
community of buyers, sellers and traders.

Ezbizi focuses on clean design and ease of use as compared to other
deal-making sites. Our platform is adding a social networking aspect to
deal-making, with a goal of using social verification as method to
prevent scams. Social networking features also allow users to easily
build on their newly established business relationships and stay
connected for future business networking.

The focus at the moment is on developing markets in Asia Pacific and
linking these markets with developed markets such Japan, US and Europe.
Ezbizi is available in English and Japanese, with plans to add
additional languages in the near future.

Read more: [http://www.crunchbase.com/company/ezbizi](http://www.crunchbase.com/company/ezbizi)