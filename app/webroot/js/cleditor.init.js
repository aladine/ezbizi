$(document).ready(function(){    
   $('.cleditor textarea').cleditor({
      width: 690,
      height: 800,
      controls:
         "bold italic underline strikethrough subscript superscript | size " +
         "| color highlight removeformat | bullets numbering | " +
         "| alignleft center alignright justify | undo redo | " +
         "link unlink | print source", 
      useCSS: true, 
      docCSSFile: "../css/cleditorDoc.css", 
      bodyStyle: "margin:4px; font:10pt 'Trebuchet MS', Helvetica, sans-serif; cursor:text; text-transform: none;"
   });
});