$(document).ready(function(){   
   
   jQuery.fn.exists = function() {return this.length>0;}
      
   //user account menu
   $(document).click(function(){
      $('.userMenu').hide();
      $('.socPop').hide();
      $('#privWin').hide();
   });
   
   $('#usrClk').bind('click', function(e){
      $('.userMenu').toggle();
      e.stopPropagation();
   });
   
   $('#socClk').bind('click', function(e){
      $('.socPop').toggle();
      e.stopPropagation();
   });
   
   $('.priv').bind('click', function(e){
      $(this).parent().find('.privWin').toggle();
      e.stopPropagation();
   });
  
   //lightbox
   $('.gal').lightBox();
   
   //social media
   $('#socm').bind('mouseenter', function(){
      $(this).dequeue().stop().animate({right: 0+'px'}, 300);
   }).bind('mouseleave', function(){
      $(this).dequeue().stop().animate({right: -120+'px'}, 300);
   });
   
});