<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Add new sector', true));
      
      echo '<div class="form">';
         
      echo $form->create('Sector', array('url' => array('controller' => 'sectors', 'action' => 'add', $category_id)));
         echo $form->input('Sector.name_en', array('label' => __('Name En', true).':'));
         echo $form->input('Sector.name_ja', array('label' => __('Name Ja', true).':'));
         //echo $form->input('name_zh', array('label' => __('Name Zh', true).':'));
         
         echo '<div class="bblk">';
            echo $form->submit(__('add', true));
         echo '</div>';
      
      echo $form->end();

      echo '</div>';
            
   echo '</div>';

echo '</div></div>';
?>