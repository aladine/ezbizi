<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Edit sector', true).' - '.$this->data['Sector']['name_'.$lang]);
      
      echo '<div class="form">';
      
      echo $form->create('Sector', array('action' => 'edit'));
         echo $form->input('Sector.id', array('type' => 'hidden'));
         echo $form->input('Sector.name_en', array('label' => __('Name En', true).':'));
         echo $form->input('Sector.name_ja', array('label' => __('Name Ja', true).':'));
         //echo $form->input('Sector.name_zh', array('label' => __('Name Zh', true).':'));
         echo '<div class="bblk">';
            echo $form->submit(__('edit', true));
         echo '</div>';
      
      echo $form->end();

      echo '</div>';
      
      echo $html->link(__('delete', true), array('controller' => 'sectors', 'action' => 'delete', $this->data['Sector']['id']));
            
   echo '</div>';

echo '</div></div>';
?>