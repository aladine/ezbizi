<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Overview of registered companies', true));
      
      echo '<div class="form">';
         if (!empty($companies)) {
            foreach ($companies as $comp) {
               echo '<div class="list rad3">';
                  //50x50 
                  if (!empty($comp['Company']['avatar']))
                     echo $html->div('img', $image->resize('comps/'.$comp['Company']['id'].'/'.$comp['Company']['avatar'], 50, 50));
                  else         
                     echo $html->div('img', $html->image('noImage50.png'));
                  echo '<div class="body">';
                     echo '<div class="top">';
                        echo $html->link($comp['Company']['name'], array('controller' => 'companies', 'action' => 'view', $comp['Company']['id']), array('class' => 'link'));
                        $vip = '';                 
                        if($comp['Company']['account_id'] == 1)
                           $vip = 'geBg';
                        if($comp['Company']['account_id'] == 2)
                           $vip = 'blBg';
                        if($comp['Company']['account_id'] == 3)
                           $vip = 'viBg';
                        echo $html->tag('span', $comp['Account']['name_'.$lang], array('class' => 'ind rad3 iR10 '.$vip.''));
                        echo $html->tag('span', $comp['Type']['name_'.$lang], array('class' => 'ind rad3 iR10 geBg'));
                     echo '</div>';
                     echo '<div class="col">';
                        echo $html->div('it', $html->div('at', __('Country', true).':').$html->div('vl', $comp['Country']['name_'.$lang]));
                        if (empty($comp['Company']['public_sector']))
                           echo $html->div('it', $html->div('at', __('Select', true).':').$html->div('vl', $comp['Sector']['name_'.$lang]));
                        else
                           echo $html->div('it', $html->div('at', __('Select', true).':').$html->div('vl', __('Public sector', true)));
                     echo '</div>';
                     echo '<div class="col">';
                        if (!empty($comp['Company']['phone2']))
                           echo $html->div('it', $html->div('at', __('Phone', true).':').$html->div('vl', $comp['Company']['phone1'].' '.$comp['Company']['phone2']));
                        if (!empty($comp['Company']['fax2']))
                           echo $html->div('it', $html->div('at', __('Fax', true).':').$html->div('vl', $comp['Company']['fax1'].' '.$comp['Company']['fax2']));
                        if (!empty($comp['Company']['web']))
                           echo $html->div('it', $html->div('at', __('Website', true).':').$html->link($comp['Company']['web'], $comp['Company']['web'], array('class' => 'vla')));
                     echo '</div>';
                     echo '<div class="col btns">';
                        echo $html->div('it', $html->link(__('show users', true), array('controller' => 'users', 'action' => 'admin_index', $comp['Company']['id']), array('class' => 'bt')));
                        if (empty($comp['Company']['neg_rating']))
                           echo $html->div('it', $html->link(__('add negative rating', true), array('controller' => 'companies', 'action' => 'neg_rating', $comp['Company']['id']), array('class' => 'bt'), __('Are you sure?', true)));
                        else
                           echo $html->div('it', $html->link(__('remove negative rating', true), array('controller' => 'companies', 'action' => 'neg_rating', $comp['Company']['id']), array('class' => 'bt')));
                        if (empty($comp['Company']['regnum_verified']))
                            echo $html->div('it', $html->link(__('add account verified flag', true), array('controller' => 'companies', 'action' => 'acc_verified', $comp['Company']['id']), array('class' => 'bt'), __('Are you sure?', true)));
                        else
                            echo $html->div('it', $html->link(__('remove account verified flag', true), array('controller' => 'companies', 'action' => 'acc_verified', $comp['Company']['id']), array('class' => 'bt')));
                        echo $html->div('it', $html->link(__('change type of account', true), array('controller' => 'companies', 'action' => 'set_account', $comp['Company']['id']), array('class' => 'bt')));
                     echo '</div>';
                  echo '</div>';
                  echo $html->link(__('remove', true), array('controller' => 'companies', 'action' => 'delete', $comp['Company']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>';
            }  
         }
         else
            echo $html->div('noData', __('Company list is empty', true));  

      echo '</div>';
      
      if (!empty($companies)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';

echo '</div></div>';
?>