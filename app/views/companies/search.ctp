<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface"><div class="cen">';
   echo '<div class="iCen">';
   
      echo '<div class="list">';
                
         echo $html->div('headline hlB', __('Companies search results', true));
         if (!empty($companies)) {
            foreach ($companies as $company) {
               echo '<div class="bconn rad3">';

                  if (empty($company['Company']['avatar']))
                     $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                  else
                     $foto = $image->resize('comps/'.$company['Company']['id'].'/'.$company['Company']['avatar'], 130, 173, true, true);
                  echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));

                  echo $html->div('hov', $company['Company']['name']);
                  echo $html->link('', array('controller' => 'companies', 'action' => 'view', $company['Company']['id']), array('class' => 'tLink'));
                  if ($company['Company']['id'] == $session->read('Auth.User.company_id'))
                     echo $html->link('x', array('controller' => 'connections', 'action' => 'delete', $company['Company']['id']), array('class' => 'del', 'escape' => false), __('Are you sure?',true));
                  
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('no company found', true));
         
      echo '</div>';
      
      if (!empty($companies)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   echo '<div class="iRight">';
      echo $this->element('company_interface_right');
   echo '</div>';
   
echo '</div></div>';
?>
