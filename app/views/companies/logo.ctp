<?php
echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_company');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Change company logo', true));
      
      echo '<div class="form">';
      echo $form->create('Company', array('url' => array('controller' => 'companies', 'action' => 'edit', 'logo'), 'enctype' => 'multipart/form-data'));

      echo '<div>'; 
         echo '<div class="blk">';         
            echo $form->input('Company.image', array('label' => __('Select logo', true).':', 'type' => 'file'));     
         echo '</div>';
         echo '<div class="blk avatar">';
            echo $html->div('title', __('Current logo', true).':');
            echo '<div class="photo">';
            if (empty($company['Company']['avatar']))
               echo $html->image('noImage115.png');
               //160 x160
            else 
               echo $image->resize('comps/'.$company['Company']['id'].'/'.$company['Company']['avatar'], 160, 160);
            if (!empty($company['Company']['avatar']))
               echo $html->link('x | '.__('remove', true), array('controller' => 'companies', 'action' => 'delete_avatar'), array('class' => 'del', 'escape' => false));
         echo '</div>';
         echo '</div>';                                                                                              
      echo '</div>';
         
      echo '<div class="bblk">';
         echo $form->submit(__('update', true));
      echo '</div>';
                  
      echo $form->end(); 
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>
<script type="text/javascript">
$(document).ready(function(){     
   $("input[type=file]").filestyle({
      image: "/img/bg/fileBg.png",
      inputheight : 18,
      inputwidth : 142,
      imageheight : 20,
      imagewidth : 38
   });
});
</script>