<?php
echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_company');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Change the type of your company account', true));
      
      echo '<div class="form">';
      echo $form->create('Company', array('url' => array('controller' => 'companies', 'action' => 'edit', 'type')));
         echo $form->input('Company.type_id', array('type' => 'hidden'));
         
         echo '<div class="accswi">';
            echo $html->div('swi', $html->image('icons/b_buyer.png').$html->tag('span', __('buyer', true)).$html->tag('span', '1', array('class' => 'id none')));
            echo $html->div('swi', $html->image('icons/b_seller.png').$html->tag('span', __('seller', true)).$html->tag('span', '2', array('class' => 'id none')));
            echo $html->div('swi', $html->image('icons/b_trader.png').$html->tag('span', __('trader', true)).$html->tag('span', '3', array('class' => 'id none')));
         echo '</div>';
         
      echo '<div class="bblk">';
         echo $form->submit(__('change', true));
      echo '</div>';
                  
      echo $form->end(); 
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
   
   if($('#CompanyTypeId').val() != '')
      $('.accswi .swi').eq($('#CompanyTypeId').val()-1).addClass('act');
   
   $('.accswi .swi').bind('click', function(){
      $('#CompanyTypeId').val($(this).find('.id').html());
      $('.accswi .swi').removeClass('act');
      $(this).addClass('act');
   });

});

</script>