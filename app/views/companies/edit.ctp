<?php

function catmenu($cats, & $catlevel, & $html, $lang, $sectors = null) {
   $catlevel++;
   foreach ($cats as $cat) {
      if ($catlevel > 1) {
         echo '<div class="cats lev'.$catlevel.' none">'; 
         echo $html->div('cat', $html->image('icons/plusR.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      else {
         echo '<div class="cats lev'.$catlevel.'">';
         echo $html->div('cat', $html->image('icons/plus.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      if (!empty($cat['children']))
         catmenu($cat['children'], $catlevel, $html, $lang, $cat['Sector']);
      else if (!empty($cat['Sector'])) {
         echo '<div class="cats lev'.($catlevel+1).' none">';  
         foreach ($cat['Sector'] as $sector) {
            echo $html->div('cat sec', $html->image('icons/sel.png').$html->tag('span', $sector['id'], array('class' => 'id none')).$html->tag('span', $sector['name_'.$lang], array('class' => 'name')));
         } 
         echo '</div>';
      }
      echo '</div>';
   }
   $catlevel--;
}

if (!empty($this->data['Company']['region_id']))
   $reg_id = $this->data['Company']['region_id'];
else
   $reg_id = 0;

echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_company');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Update company information', true));
      
      echo '<div class="form">';
      echo $form->create('Company', array('action' => 'edit'));

      echo '<div>'; 
         echo '<div class="blk">';
            
            echo '<div class="req">';      
            echo $form->input('Company.name', array('label' => __('Name', true).':'));
            echo $form->input('Company.address1', array('label' => __('Address', true).':'));
            echo '</div>';
            echo $form->input('Company.address2', array('label' => ' ')); 
            echo $form->input('Company.address3', array('label' => ' '));
            echo $form->input('Company.zip', array('label' => __('Zip code', true).':', 'div' => 'input text short80'));
            echo '<div class="req">';
            echo $form->input('Company.city', array('label' => __('City', true).':'));
            echo $form->input('Company.country_id', array('label' => __('Country', true).':', 'type' => 'select', 'empty' => ' - '.__('Select your country', true).' - '));           
            echo '</div>';
            echo $form->input('Company.region_id', array('label' => __('County', true).':', 'type' => 'select', 'empty' => ' - '.__('Select your county', true).' - ', 'disabled' => 'disabled'));
            echo $form->input('Company.region_id_temp', array('label' => '', 'type' => 'select', 'empty' => ' - '.__('Select your county', true).' - ', 'options' => $regions, 'div' => 'input select none'));
            echo '<br />';
            echo '<div class="sectorBlk req">';
               echo $form->input('Company.sector_id', array('label' => __('Select', true).':', 'type' => 'text', 'div' => 'input select sectorInput'));        
               echo $form->input('Company.sector_text', array('label' => '', 'type' => 'text', 'default' => ' - '.__('Select your trade sector', true).' - ', 'div' => 'input text sectorText'));
            echo '</div>';
            if (!empty($categories)) {
               echo '<div id="secSel" class="none">';
                  $catlevel = 0;          
                  catmenu($categories, $catlevel, $html, $lang);
               echo '</div>';
            }
            
         echo '</div>';
         echo '<div class="blk">';
            
            echo $form->input('Company.est_date', array('label' => __('Date of establishment', true).':', 'type' => 'date', 'dateFormat' => 'DMY', 'minYear' => '1900', 'maxYear' => date('Y'), 'empty' => ' - ', 'separator' => null, 'id' => 'dateEdit'));
            echo '<div class="double req">';
               echo $form->input('Company.phone1', array('label' => __('TEL', true).':', 'div' => 'input text code'));
               echo $form->input('Company.phone2', array('label' => '', 'div' => 'input text phone'));
            echo '</div>';
            echo $form->input('Company.regnum', array('label' => __('Company registration number', true).':'));
            echo '<br />';
            echo '<div class="compSize req">';
               echo $form->input('Company.size', array('label' => __('Company size', true).':', 'type' => 'text'));
               echo $form->input('Company.TextSize', array('label' => '', 'value' => ' - '.__('Type in or select size', true).' - ', 'type' => 'text', 'div' => 'input text compSizeTextInput'));                       
               echo '<div class="options">';
                  if(!empty($sizes)) {
                     foreach ($sizes as $ind => $size)
                        echo $html->div($ind, $size);
                  }
               echo '</div>';
               echo $html->div('arr', $html->image('icons/selectorA.png'));
            echo '</div>';
            echo $form->input('Company.public_sector', array('label' => __('Public sector', true).':', 'type' => 'checkbox'));
            echo '<div class="double">';
               echo $form->input('Company.fax1', array('label' => __('Fax', true).':', 'div' => 'input text code'));
               echo $form->input('Company.fax2', array('label' => '', 'div' => 'input text phone'));
            echo '</div>'; 
            echo $form->input('Company.web', array('label' => __('Website', true).':'));
            echo '<br />';
            echo '<br />';
            echo '<br />';
         echo '</div>';
      echo '</div>';
         
      echo '<div class="bblk">';
         echo $form->submit(__('update', true));
      echo '</div>';
                  
      echo $form->end(); 
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
   
   var compSizeHov = false;
   
   setRegionSel();
   publicSecCheck();

   //region selector
   $('#CompanyCountryId').change(function(){
      setRegionSel();
   });

   //public sector checkbox
   $('#CompanyPublicSector').change(function() {
      publicSecCheck();   
   });
   
   $('#CompanySectorText').click(function() {
      $(this).val('');
      $('#secSel').show();   
   });

   //company size------>
   if($('#CompanySize').val() != '')
      $('#CompanyTextSize').val($('#CompanySize').val());
   if($('#CompanySize').val() < 0)
      $('#CompanyTextSize').val($('.compSize .options').find('.'+$('#CompanySize').val()).html());
   
   $('.compSize .options').bind('mouseenter', function(){
      compSizeHov = true;   
   }).bind('mouseleave', function(){
      compSizeHov = false;   
   });
   //show or hide options          
   $('#CompanyTextSize, .compSize .arr').bind('click', function(){
      $(this).val('');
      $('.compSize .options').toggle();      
   }).bind('focusout', function(){
      if(!compSizeHov)
         $('.compSize .options').hide();
   });
   //write size
   $('#CompanyTextSize').bind('change', function(){
      $('#CompanySize').val($(this).val());
   }) 
   //select size
   $('.compSize .options div').bind('click', function(){
      $('#CompanyTextSize').val($(this).html());
      $('.compSize .options').hide();
      $('#CompanySize').val($(this).attr('class'));
   });
          
   //<------

   //bussines sector select
   $('#secSel .cat').click(function() {
      var $parCat = $(this).parent();
      var path = '';
      //if exists child
      if($parCat.find('> .cats').exists()){
         $parCat.find('> .cats').toggle();   
      }
      else {
         if ($(this).hasClass('sec')) {
            $('#CompanySectorId').val($(this).find('.id').html());
            while ($parCat.parent().find('> .cat').exists()) {
               path = $parCat.parent().find('> .cat .name').html().replace("&amp;", "&")+' > ' + path;
               $parCat = $parCat.parent();               
            }
            $('#CompanySectorText').val(path + $(this).find('.name').html().replace("&amp;", "&"));
            $('#secSel').hide();
         }   
      }   
   });

});

function setRegionSel(refresh) {
   var cntId = $('#CompanyCountryId option:selected').attr('value');
   var regId = <?echo $reg_id;?>; 
   if (cntId != "" && $('#CompanyRegionIdTemp optgroup[label="'+cntId+'"]').exists()) {
      $('#CompanyRegionId').html('<option value><? echo ' - '.__('Select your county', true).' - '; ?></option>'+$('#CompanyRegionIdTemp optgroup[label="'+cntId+'"]').html());        
      if(regId != 0)
         $('#CompanyRegionId option[value="'+regId+'"]').attr('selected', 'selected');   
      $('#CompanyRegionId').removeAttr('disabled');
   }
   else {
      $('#CompanyRegionId').html('<option value><? echo ' - '.__('Select your county', true).' - '; ?></option>');
      $('#CompanyRegionId').attr('disabled', 'disabled');
   }  
}

function publicSecCheck() {
   if ($('#CompanyPublicSector:checked').val() == '1') {     
      $('#CompanySectorText, #CompanyRegnum').attr('disabled', 'disabled');
      $('#secSel').hide();
   }        
   else  
      $('#CompanySectorText, #CompanyRegnum').removeAttr('disabled');       
}
</script>