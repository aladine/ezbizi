<?php

function catmenu($cats, & $catlevel, & $html, $lang, $sectors = null) {
   $catlevel++;
   foreach ($cats as $cat) {
      if ($catlevel > 1) {
         echo '<div class="cats lev'.$catlevel.' none">'; 
         echo $html->div('cat', $html->image('icons/plusR.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      else {
         echo '<div class="cats lev'.$catlevel.'">';
         echo $html->div('cat', $html->image('icons/plus.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      if (!empty($cat['children']))
         catmenu($cat['children'], $catlevel, $html, $lang, $cat['Sector']);
      else if (!empty($cat['Sector'])) {
         echo '<div class="cats lev'.($catlevel+1).' none">';  
         foreach ($cat['Sector'] as $sector) {
            echo $html->div('cat sec', $html->image('icons/sel.png').$html->tag('span', $sector['id'], array('class' => 'id none')).$html->tag('span', $sector['name_'.$lang], array('class' => 'name')));
         } 
         echo '</div>';
      }
      echo '</div>';
   }
   $catlevel--;
}

echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_company');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Select area of your business interest', true));
      
      echo '<div class="form">';
      echo $form->create('Company', array('url' => array('controller' => 'companies', 'action' => 'edit', 'business-arrea')));

      echo '<div>'; 
         echo '<div class="blk">';

            echo '<div class="sectorBlk">';
               echo $form->input('Company.i_sector_id', array('label' => __('Select', true).':', 'type' => 'text', 'div' => 'input select sectorInput'));                      
               echo $form->input('Company.sector_text', array('label' => '', 'type' => 'text', 'default' => ' - '.__('all sectors', true).' - ', 'div' => 'input text sectorText'));
               echo $html->tag('span', __('select all', true), array('class' => 'resSec geBg rad3', 'id' => 'allSec'));
            echo '</div>';
            if (!empty($categories)) {
               echo '<div id="secSel" class="none">';
                  $catlevel = 0;          
                  catmenu($categories, $catlevel, $html, $lang);
               echo '</div>';
            }
            
         echo '</div>';
         echo '<div class="blk">';
            
            echo $form->input('Company.i_locality_id', array('label' => __('Location', true).': *', 'type' => 'select', 'empty' => ' - '.__('all locations', true).' - ', 'options' => $countries));           
            
         echo '</div>';
      echo '</div>';
         
      echo '<div class="bblk">';
         echo $form->submit(__('change', true));
      echo '</div>';
                  
      echo $form->end(); 
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
   
   set_sector_text();
   
   $('#allSec').click(function() {
      $('#CompanyISectorId').val('');
      $('#CompanySectorText').val('<? echo ' - '.__('all sectors', true).' - '; ?>');  
   });
   
   $('#CompanySectorText').click(function() {
      $(this).val('');
      $('#secSel').show();   
   });

   //bussines sector select
   $('#secSel .cat').click(function() {
      var $parCat = $(this).parent();
      var path = '';
      //if exists child
      if($parCat.find('> .cats').exists()){
         $parCat.find('> .cats').toggle();   
      }
      else {
         if ($(this).hasClass('sec')) {
            $('#CompanyISectorId').val($(this).find('.id').html());
            while ($parCat.parent().find('> .cat').exists()) {
               path = $parCat.parent().find('> .cat .name').html().replace("&amp;", "&")+' > ' + path;
               $parCat = $parCat.parent();               
            }
            $('#CompanySectorText').val(path + $(this).find('.name').html().replace("&amp;", "&"));
            $('#secSel').hide();
         }   
      }   
   });

});

function set_sector_text() {
   if($('#CompanyISectorId').val() != '') {
      $('#secSel .sec').each(function() {
         if ($(this).find('.id').html() == $('#CompanyISectorId').val())
            $('#CompanySectorText').val($(this).find('.name').html().replace("&amp;", "&"));   
      });
   }     
}
</script>