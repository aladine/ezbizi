<?php
echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   /*
   echo '<div class="sLeft">';
      echo $this->element('setting_company');
   echo '</div>';
   */
   echo '<div class="sFull">';
      echo $html->div('headline', __('Upgrade to premium account', true));
      
      echo '<div class="form">';
       if (!empty($order)) {
           echo '<div class="notic rad3">';
               echo $html->tag('span', __('Thank you for upgrading your account to', true)).' '.$html->tag('span', $order['Package']['name_'.$lang], array('class' => 'pck detBt')).'.<br />'.$html->tag('span', __('Your account will be upgraded as soon as we register your payment. Please note this might take few days. If your account has not been upgraded in 7 business days, please contact the site administrator.', true));
           echo '</div>';
           
           echo '<div class="pckg rad3 none" id="pckDet">';
               echo $html->div('name', $order['Package']['name_'.$lang]);
               echo '<div class="body">';
                  echo '<div class="dsc">';
                     //echo $order['Package']['description_'.$lang];
                  echo '</div>';
                  echo '<div class="pby rad3">';
                     echo $html->div('prc', $order['Package']['price'].' $ ');
                     echo $html->div('val', $order['Package']['validity'].' months ');
                  echo '</div>';
               echo '</div>';
            echo '</div>';         
        }
        else if (!empty($packages)) {
            echo '<div class="packT">';
               echo '<div class="ln hd">';
                  echo $html->div('it lf rad3', '');
                  echo $html->div('it', '');
                  echo $html->div('it blBg rad3', __('Premium account - senator', true));
                  echo $html->div('it viBg rad3', __('Premium account - ezVIP', true));
               echo '</div>';
               echo '<div class="ln iln">';
                                
                  echo '<div class="it lf invI">';
                     echo $html->image('bg/bubiMascotL.png');
                     echo $html->div('bub rad5 viBg', __('Get your premium account now! It\'s free in beta, just invite others to join us too! So what are you waiting for? Grab it now!', true).$html->image('bg/bubArrInv.png', array('class' => 'bubA')));
                     echo $html->div('curr vi', $html->div('val', $html->tag('span', __('You already sent', true)).' '.$html->tag('span', $company['Company']['invitation_count']).' '.$html->tag('span', __('invitations', true)).'.'));
                  echo '</div>';
                  
                  echo $html->div('it ge', '');
               
                  foreach ($packages as $package) {
                     if (!empty($package['Package']['invitations'])) {
                        echo '<div class="it inv">';
                           //echo $this->element('order_paypal', array('package' => $package))
                           echo $html->div('count', $html->tag('span', __('Send', true)).' '.$html->tag('span', $package['Package']['invitations']).' '.$html->tag('span', __('invitations to upgrade', true)).'.');
                           if ($package['Package']['invitations'] <= $company['Company']['invitation_count'])
                              echo $html->link('Spend it for this package', array('controller' => 'orders', 'action' => 'paid', 'invitations', $package['Package']['id']), array('class' => 'buy geBg rad3'));
                           else {
                              echo $form->create('Invitation', array('url' => array('controller' => 'companies', 'action' => 'edit', 'account')));
                              echo '<div class="invForm rad3">';
                                 echo $form->input('Invitation.email', array('label' => __('E-mail', true).':'));
                                 echo $form->submit(__('send invitation', true));
                                 echo '</div>';
                              echo $form->end();
                           }
                        echo '</div>';
                     }
                  }
               echo '</div>';
               echo $html->div('warn rad3 geBg', __('Your contacts will receive only 1 time exclusive invitation, that will not be sent repeatedly so you don\'t have to worry about "spamming" your contact\'s email. At Ezbizi we want help others not to annoy them!', true));
            echo '</div>';                                                       
        }
        else {                                                                                                                                   
            echo '<div class="invI">';
               echo $html->image('bg/bubiMascotL.png');
               echo $html->div('bub rad5 viBg', $html->tag('span', __('Your', true)).' '.$html->tag('span', $account['Account']['name_'.$lang]).' '.$html->tag('span', __('premium account is valid till', true)).' '.$html->tag('span', date('d.m.Y', strtotime($company['Company']['valid']))).$html->image('bg/bubArrInv.png', array('class' => 'bubA')));
            echo '</div>';
        }
        if($lang == 'en')
         echo $this->element('package_table_en');
        if($lang == 'ja')
         echo $this->element('package_table_ja');
           
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>
<script type="text/javascript">
$(document).ready(function(){   
   
   $('.detBt').bind('click', function(){
      $('#pckDet').toggle();
   });
   
});
</script>