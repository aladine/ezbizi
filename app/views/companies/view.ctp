<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface profile"><div class="cen">';
   echo '<div class="pLeft">';
      
      //---> LOGO
      
      echo '<div class="modP rad5 logo">';
         echo $html->div('comName rad3', $vcompany['Company']['name']);
         echo '<div class="logImg rad3">';
            //200 x 200
            if (empty($vcompany['Company']['avatar']))                                                    
               $foto = $this->webroot.IMAGES_URL.'noImage115.png';
            else
               $foto = $image->resize('comps/'.$vcompany['Company']['id'].'/'.$vcompany['Company']['avatar'], 200, 200, true, true);
            
            echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
            
            if ($vcompany['Company']['online']) {
               $last_activity = strtotime($vcompany['Company']['last_activity']);
               $now = time();
               $last_activity = $now - $last_activity;
               if ($last_activity < 1800)
                  echo $html->image('icons/online.png', array('class' => 'onf'));
               else
                  echo $html->image('icons/offline.png', array('class' => 'onf'));
            }
            else
               echo $html->image('icons/offline.png', array('class' => 'onf'));
               
         echo '</div>';
         if ($vcompany['Company']['id'] == $session->read('Auth.User.company_id'))
            echo $html->div('edtL', $html->link(__('change logo', true), array('controller' => 'companies', 'action' => 'edit', 'logo')));
      echo '</div>';
      
      //---> COMPANY INFOS
      
      echo '<div class="modP modPB rad5 cInfos">';
         $loc = $vcompany['Country']['name_'.$lang];
         if (!empty($vcompany['Region']['name_'.$lang]))
            $loc .= ', '.$vcompany['Region']['name_'.$lang];
         echo $html->div('inf loc rad3', $html->image('flags/'.$vcompany['Country']['id'].'.png').$html->tag('span', $loc));
         if (empty($vcompany['Company']['public_sector']))
            echo $html->div('inf rad3', $html->image('icons/sector.png').$html->tag('span', $vcompany['Sector']['name_'.$lang]));
         else
            echo $html->div('inf rad3', $html->image('icons/sector.png').$html->tag('span', __('Public sector', true)));
         if (!empty($vcompany['Company']['establish']))
            echo $html->div('inf rad3', $html->image('icons/estab.png').$html->tag('span', substr($vcompany['Company']['establish'], 0, 4)));
         if ($vcompany['Company']['size'] > 0)
            echo $html->div('inf rad3', $html->image('icons/size.png').$html->tag('span', $vcompany['Company']['size']));
         else if (!empty($sizes[$vcompany['Company']['size']]))
            echo $html->div('inf rad3', $html->image('icons/size.png').$html->tag('span', $sizes[$vcompany['Company']['size']]));
         if (!$vcompany['Company']['public_sector'] && !empty($vcompany['Company']['regnum']) && in_array($account['Account']['id'], array(2, 3))/* && ($vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])) */)
            echo $html->div('inf regnum rad3', $html->image('icons/reg.png').$html->tag('span', __('C.R.N.', true).':&nbsp;'.$vcompany['Company']['regnum']));
         else if (!$vcompany['Company']['public_sector'] &&  !empty($vcompany['Company']['regnum']) && $account['Account']['id'] == 1)
            echo $html->div('inf proregnum rad3', $html->image('icons/w_reg.png').$html->tag('span', __('Business registration number provided. To view upgrade to premium account.', true)));
         else
            echo $html->div('inf noregnum rad3', $html->image('icons/w_reg.png').$html->tag('span', __('Caution! Business registration number has not yet been provided.', true)));
         if ($vcompany['Company']['id'] == $session->read('Auth.User.company_id'))
            echo $html->div('edtL', $html->link(__('edit info', true), array('controller' => 'companies', 'action' => 'edit')));
      echo '</div>';
                        
      //---> INDICATORS
      
      echo '<div class="mod inds">';
         
         if ($vcompany['Company']['type_id'] == 1)
            $tp_cls = 'ge_ind';
         if ($vcompany['Company']['type_id'] == 2)
            $tp_cls = 'bl_ind';
         if ($vcompany['Company']['type_id'] == 3)
            $tp_cls = 'vi_ind';
         echo $html->div('it '.$tp_cls , $html->tag('span', '', array('class' => 'a')).$html->tag('span', '', array('class' => 'l')).$html->tag('span', '● '.$vcompany['Type']['name_'.$lang], array('class' => 't')).$html->tag('span', '', array('class' => 'r')));

         if ($vcompany['Account']['id'] == 2)
            echo $html->div('it bl_ind', $html->tag('span', '', array('class' => 'a')).$html->tag('span', '', array('class' => 'l')).$html->tag('span', '● '.$vcompany['Account']['name_'.$lang], array('class' => 't')).$html->tag('span', '', array('class' => 'r')));
         if ($vcompany['Account']['id'] == 3)
            echo $html->div('it vi_ind', $html->tag('span', '', array('class' => 'a')).$html->tag('span', '', array('class' => 'l')).$html->tag('span', '● '.$vcompany['Account']['name_'.$lang], array('class' => 't')).$html->tag('span', '', array('class' => 'r')));
         
      echo '</div>';
                      
      //---> CONNECTIONS
      
      if (true /*$vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])*/) {
         echo '<div class="mod">';       
           echo $html->div('headline hlB', __('Connections', true));      
           if (!empty($connections)) {
              foreach ($connections as $connection) {
                 echo '<div class="conn rad3">';

                    if (empty($connection['Company']['avatar']))
                       $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                    else
                       $foto = $image->resize('comps/'.$connection['Company']['id'].'/'.$connection['Company']['avatar'], 60, 80, true, true);
                    
                    echo '<div class="thumb rad3">'; 
                        echo $html->div('foto', '', array('style' => 'background-image: url('.$foto.');'));
                    echo '</div>';
                    
                    echo '<div class="info">';
                       echo $html->div('name', $connection['Company']['name']);
                       if (empty($connection['Company']['public_sector']))
                          echo $html->div('sector', $connection['Sector']['name_'.$lang]);
                       else
                          echo $html->div('sector', __('Public sector', true));
                       $loc = $connection['Country']['name_'.$lang];
                       if (!empty($connection['Region']['name_'.$lang]))
                          $loc .= ' - '.$connection['Region']['name_'.$lang];
                       echo $html->div('loc', '('.$loc.')');
                    echo '</div>';
                    echo $html->link('', array('controller' => 'companies', 'action' => 'view', $connection['Company']['id']), array('class' => 'tLink'));        
                 echo '</div>';
              }
              echo $html->div('edtL', $html->link(__('more connections', true), array('controller' => 'connections', 'action' => 'index', $vcompany['Company']['id'])));
           }
           else 
            echo $html->div('noData', __('Currently you have no connections', true));           
         echo '</div>';
      }
         
   echo '</div>';
   echo '<div class="pCen">';
      
      echo '<div class="top">'; 
      
      if ($vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($connected)) {
         //---> STATUS      
         echo '<div class="rB g_B w480 float">';
            echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
            echo '<div class="mid">';
               if(!empty($vcompany['Company']['status']))
                  echo $html->div('status comstat', $html->tag('span', $vcompany['Company']['status']));
               else
                  echo $html->div('status comstat', $html->tag('span', __('No status added', true)));   
               if($vcompany['Company']['id'] == $session->read('Auth.User.company_id') && $session->read('Auth.User.group_id') < 3) {  
                  echo '<div class="statAdd comstat">';         
                     echo $form->create('Company', array('url' => array('controller' => 'companies', 'action' => 'view', $vcompany['Company']['id'])));
                        echo $form->input('Company.status', array('label' => ''));
                        echo $html->div('def', __('Type in your new status ...', true), array('id' => 'UserStatusDef'));
                        echo $form->submit(__('submit', true));
                     echo $form->end();
                  echo '</div>';
               }
               
            echo '</div>';
            echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
         echo '</div>';
      }
      
      echo '</div>';
      
      if ($vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($connected)) {
         //---> ADVERTISE
         if (!empty($demands)) {
            echo '<div class="advert modP rad5">'; 
            
               echo $html->div('headline hlB hlIn', __('Latest demands from this company', true));
                       
                  foreach ($demands as $demand) {
                     echo '<div class="feed">';
                        echo $html->image('icons/f_demand.png', array('class' => 'iL'));
                        echo $html->tag('span', __('looking for goods', true).':', array('class' => 'text')).'&nbsp;';
                        
                        $dtitle = '';
                        if (empty($demand['Demand']['priceper_id']))
                           $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed', true);
                        else
                           $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed', true);
                        if ($lang == 'ja')
                           $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                        echo $html->link($dtitle, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id']), array('class' => 'link', 'escape' => false));
                     echo '</div>';
                  }
                    
            echo '</div>';
         }
      }
      
      if (true /*$vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])*/) {
         //---> FEEDS     
         echo '<div class="feeds modP rad5">'; 
         
            echo $html->div('headline hlB hlIn', __('Newsfeed', true));
            
            if (!empty($notifications)) {
                  echo $this->element('feeds');
               echo $html->div('edtL', $html->link(__('more feeds', true), array('controller' => 'notifications', 'action' => 'index', $vcompany['Company']['id'])));
            }
            else
               echo $html->div('noData', __('Currently there are no news to display', true)); 
         
         echo '</div>';
      }
      
      if (true /*$vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])*/) {
         //---> ALBUMS     
         echo '<div class="albums">'; 
         
            echo $html->div('headline hlB', __('Albums', true));
            
            if (!empty($albums)) {
               //150 x 150
               echo '<div class="fotos">';
               foreach ($albums as $album) {
                  echo '<div class="itm rad3">';
                     if (empty($album['Photo'][0]['file']))
                        $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                     else
                        $foto = $image->resize('comps/'.$album['Album']['company_id'].'/'.$album['Album']['id'].'/'.$album['Photo'][0]['file'], 200, 200, true, true);
                     echo $html->tag('span', $album['Album']['name'], array('class' => 'name'));
                     echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                     echo $html->link('', array('controller' => 'albums', 'action' => 'view', $album['Album']['id']), array('class' => 'tLink'));  
                  echo '</div>';
                  
               }
               echo '</div>';
               echo $html->div('edtL', $html->link(__('more albums', true), array('controller' => 'albums', 'action' => 'index', $vcompany['Company']['id'])));
            }
            else
               echo $html->div('noData', __('No album added', true));
            
         echo '</div>';
      }
      
      if (true /*$vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])*/) {
         //---> RECOMMENDATIONS      
         echo '<div class="recomms feeds modP rad5 ">'; 
         
            echo $html->div('headline hlV hlIn', __('Recommendations', true));
            if (!empty($recommendations)) {
               foreach ($recommendations as $recommendation) {
                  echo '<div class="feed">';
                     echo $html->image('icons/f_recom.png', array('class' => 'iL'));
                     echo $html->tag('span', date('d.m.Y', strtotime($recommendation['Recommendation']['created'])).'&nbsp;'.__('from', true).':', array('class' => 'date')).'&nbsp;';
                     echo $html->link($recommendation['Senderc']['name'], array('controller' => 'companies', 'action' => 'view', $recommendation['Senderc']['id']), array('class' => 'comp'));
                     echo $html->div('recom', $recommendation['Recommendation']['text']);
                  echo '</div>';
               }
               echo $html->div('edtL', $html->link(__('more recommendations', true), array('controller' => 'recommendations', 'action' => 'index', $vcompany['Company']['id'])));
            }
            else
               echo $html->div('noData', __('No recommendation added', true));
               
         echo '</div>';
      }
           
   echo '</div>';
   echo '<div class="pRight">';
      
      //---> ACTIONS
      echo '<div class="inds acts">';
         if ($company['Company']['id'] != $vcompany['Company']['id'] && $company['Company']['account_id'] == 1/* && $vcompany['Company']['account_id'] == 1*/ && empty($interested) && empty($connect))
            echo $html->div('it ge_ind', $html->tag('span', '', array('class' => 'l')).$html->link($html->image('icons/w_interest.png', array('class' => 'wi')).$html->tag('span', __('show interest', true)), array('controller' => 'interests', 'action' => 'add', $vcompany['Company']['id']),array('class' => 't', 'escape' => false)).$html->tag('span', '', array('class' => 'r')));
         if ($company['Company']['id'] != $vcompany['Company']['id'] && ($company['Company']['account_id'] != 1 || !empty($deal)) && empty($connect))
            echo $html->div('it bl_ind', $html->tag('span', '', array('class' => 'l')).$html->link($html->image('icons/w_connections.png', array('class' => 'wi')).$html->tag('span', __('add to your connections', true)), array('controller' => 'connections', 'action' => 'add', $vcompany['Company']['id']),array('class' => 't', 'escape' => false)).$html->tag('span', '', array('class' => 'r')));
         if ($company['Company']['id'] != $vcompany['Company']['id'] && empty($recommended) && (!empty($deal) || !empty($connected)))   
            echo $html->div('it vi_ind sTo', $html->tag('span', '', array('class' => 'l')).$html->div('t', $html->image('icons/w_recom.png', array('class' => 'wi')).$html->tag('span', __('Recommend', true))).$html->tag('span', '', array('class' => 'r')), array('id' => 'pUpRec'));         
  
      echo '</div>';
      
      //--> POP UP RECOMMENDATIONS
      
      echo $this->element('popup_recommendations');
               
      //---> RATING
      
      echo '<div class="mod">';
      
         echo '<div class="rB z_B w200">';
            echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
            echo '<div class="mid">';
               echo $html->div('ctit', __('Company rating', true));
               echo '<div class="ratingBlk">';
                  echo '<div class="ratBot">';
                     $px = round(($vcompany['Company']['rating'] / 5) * 100);
                     echo '<div class="line" style="width: '.$px.'px;"></div>'; //100% = 100px
                     echo '<div class="mask"></div>';
                     //vote links
                     echo '<div class="links">';
                        echo $html->link(''.$html->tag('span', __('Add 1 star', true), array('class' => 'pop')), array('controller' => 'ratings', 'action' => 'add', $vcompany['Company']['id'], 1), array('class' => 'lnk', 'escape' => false));
                        echo $html->link(''.$html->tag('span', __('Add 2 stars', true), array('class' => 'pop')), array('controller' => 'ratings', 'action' => 'add', $vcompany['Company']['id'], 2), array('class' => 'lnk', 'escape' => false));
                        echo $html->link(''.$html->tag('span', __('Add 3 stars', true), array('class' => 'pop')), array('controller' => 'ratings', 'action' => 'add', $vcompany['Company']['id'], 3), array('class' => 'lnk', 'escape' => false));
                        echo $html->link(''.$html->tag('span', __('Add 4 stars', true), array('class' => 'pop')), array('controller' => 'ratings', 'action' => 'add', $vcompany['Company']['id'], 4), array('class' => 'lnk', 'escape' => false));
                        echo $html->link(''.$html->tag('span', __('Add 5 stars', true), array('class' => 'pop')), array('controller' => 'ratings', 'action' => 'add', $vcompany['Company']['id'], 5), array('class' => 'lnk', 'escape' => false));
                     echo '</div>';                                                                                                              
                  echo '</div>';
                  echo $html->div('value', number_format($vcompany['Company']['rating'], 2));
               echo '</div>';
            echo '</div>';
            echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
         echo '</div>';
      
      if ($vcompany['Company']['regnum_verified']) {
         echo '<div class="masInfo">';
            echo '<div class="bubi rad5 geBg">';
               echo __('EZBIZI', true).'<br />'.__('VERIFIED', true);
               echo $html->image('bg/verArr.png', array('class' => 'arr'));
            echo '</div>';
            echo $html->image('bg/mascotVerified.png', array('class' => 'mas'));
         echo '</div>';
      } 
      
      if ($vcompany['Company']['neg_rating']) {
         echo '<div class="masInfo">';
            echo '<div class="bubi rad5 redBg">';
               echo __('CAUTION!', true).'<br />'.__('Negative rating.', true);
               echo $html->image('bg/negArr.png', array('class' => 'arr'));
            echo '</div>';
            echo $html->image('bg/mascotNegative.png', array('class' => 'mas'));
         echo '</div>';
      }  
   
      echo '</div>';
      
      if ($vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])) {
         //---> CONTACTS    
         echo '<div class="modP rad5">';
            
            echo '<div class="cons">';
               echo $html->div('tit', $html->image('icons/cont.png').$html->tag('span', __('Contact information', true).':'));   
               echo $html->div('con', $html->image('icons/mail.png', array('class' => 'iL')).$vcompany['Company']['address1']);
               if (!empty($vcompany['Company']['address2']))
                  echo $html->div('con', $html->image('icons/emp.png', array('class' => 'iL')).$vcompany['Company']['address2']);
               if (!empty($vcompany['Company']['address3']))
                  echo $html->div('con', $html->image('icons/emp.png', array('class' => 'iL')).$vcompany['Company']['address3']);
               if (!empty($vcompany['Company']['zip']))
                  echo $html->div('con', $html->image('icons/emp.png', array('class' => 'iL')).$vcompany['Company']['zip'].' '.$vcompany['Company']['city']);
               else
                  echo $html->div('con', $html->image('icons/emp.png', array('class' => 'iL')).$vcompany['Company']['city']);
               echo '<br />';
               echo $html->div('con', $html->image('icons/phone.png', array('class' => 'iL')).$html->tag('span', '+'.$vcompany['Company']['phone1'].' '.$vcompany['Company']['phone2']));
               if (!empty($vcompany['Company']['fax2']))
                  echo $html->div('con', $html->image('icons/fax.png', array('class' => 'iL')).$html->tag('span', '+'.$vcompany['Company']['fax1'].' '.$vcompany['Company']['fax2']));
               if (!empty($vcompany['Company']['web']))
                  echo $html->div('con', $html->image('icons/web.png', array('class' => 'iL')).$html->link($vcompany['Company']['web'], http_sanitize($vcompany['Company']['web'])));
            echo '</div>';
   
         echo '</div>';
      }
      
      //---> INDICATORS
      if ($company['Company']['id'] == $vcompany['Company']['id']) {
        echo '<div class="mod">';
           
           echo '<div class="viewInd">';
              echo $html->div('topT', __('Profile has been viewed', true));
              $person = __('times', true);
              if ($vcompany['Company']['visit_count'] == 1)
                 $person = __('time', true);
              echo $html->div('count', $vcompany['Company']['visit_count'].' '.$person);
           echo '</div>';                
     
        echo '</div>';
      }
   
      if (true /*$vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])*/) {
         //---> COLLEAGUES      
         echo '<div class="modP rad5">';
         
            echo $html->div('headline hlB hlIn', __('Staff', true));
            //50 x 67
            if (!empty($colleagues)) {
               echo '<div class="colls">';
                  foreach ($colleagues as $colleague) {
                     echo '<div class="coll">';
                        if ($colleague['User']['online']) {
                           $last_activity = strtotime($colleague['User']['last_activity']);
                           $now = time();
                           $last_activity = $now - $last_activity;
                           if ($last_activity < 1800)
                              echo $html->image('icons/online.png', array('class' => 'onf'));
                           else
                              echo $html->image('icons/offline.png', array('class' => 'onf'));
                        }
                        else
                           echo $html->image('icons/offline.png', array('class' => 'onf'));
                              
                        echo $html->div('hov', $html->tag('span', $colleague['User']['first_name'], array('class' => 'name')));
                        if (empty($colleague['User']['avatar']))
                           $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                        else
                           $foto = $image->resize('users/'.$colleague['User']['id'].'/'.$colleague['User']['avatar'], 68, 90, true, true);
                        echo $html->div('foto', '', array('style' => 'background-image: url('.$foto.');'));
                        echo $html->link('', array('controller' => 'users', 'action' => 'view', $colleague['User']['id']), array('class' => 'tLink'));
                     echo '</div>';
                  }          
               echo '</div>';
               echo $html->div('edtL', $html->link(__('more staff', true), array('controller' => 'users', 'action' => 'colleagues', $vcompany['Company']['id']))); 
            }
            else
               echo $html->div('noData', __('No staff added', true));
         
         echo '</div>';
      }
      
      if (true /*$vcompany['Company']['id'] == $company['Company']['id'] || $company['Company']['account_id'] == 3 || !empty($vcompany['Connected'])*/) {
         //---> DOCUMENTS      
         echo '<div class="modP rad5 docs">';
         
            echo $html->div('headline hlB hlIn', __('Documents', true));
            
            if (!empty($documents)) {
               foreach ($documents as $document) {
                  echo '<div class="doc">';
                     echo $html->link($html->image('icons/doc.png').$html->tag('span', $document['Document']['name']), array('controller' => 'documents', 'action' => 'view', $document['Document']['id']), array('class' => 'name', 'escape' => false));
                     echo $html->div('dsc', $document['Document']['description']);
                  echo '</div>';
               }
               echo $html->div('edtL', $html->link(__('more documents', true), array('controller' => 'documents', 'action' => 'index', $vcompany['Company']['id'])));
            }
            else
               echo $html->div('noData', __('No document added', true));
            
         echo '</div>';
      }
      
      
   echo '</div>'; 

echo '</div></div>';
?>

<script type="text/javascript">
var popUpH, popUpW, popUpLP, popUpTP;

$(document).ready(function(){   
   
   $('#CompanyStatus').val('');
   
   //status input
   if($('#CompanyStatus').val() != '')
      $('#UserStatusDef').hide();   
      
   $('#CompanyStatus').bind('focus', function(){
      $('#UserStatusDef').hide();   
   }).bind('focusout', function() {
      if($('#CompanyStatus').val() == '')
         $('#UserStatusDef').show();
   });
   
   $('#UserStatusDef').bind('click', function(){
      $(this).hide();
   });
   
   //popUp Open   
   $('.sTo').click(function(){
      $win = '#'+$(this).attr('id')+'_win';
      popUpDim($win); 

      $('.popUp:visible').hide();            
      $($win).css({top: popUpTP, left: popUpLP}).fadeIn(300);
   });
   
   //popUp close 
   $('.popUpCls').click(function(){
      $('.popUp').fadeOut(300);   
   });   

});

function popUpDim($elm) {
   popUpH = $($elm).height();
   popUpW = $($elm).width();
   popUpLP = Math.abs(wW - popUpW)/2;
   popUpTP = Math.abs(wH - popUpH)/2;  
}

</script>