<?php
echo $html->script(array('cleditor.min', 'cleditor.init'));
echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_company');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Send a newsletter to your colleagues', true));
      
      echo '<div class="form">';
      echo $form->create('Newsletter', array('url' => array('controller' => 'companies', 'action' => 'edit', 'newsletter')));
         echo $form->input('Newsletter.subject', array('label' => __('Subject', true).':'));
         echo $form->input('Newsletter.text', array('label' => '', 'type' => 'textarea',  'div' => 'input textarea newsarea cleditor'));
 
         echo '<div class="bblk">';
            echo $form->submit(__('send newsletter', true));
         echo '</div>';
                  
      echo $form->end();   
      echo '</div>'; 
         
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>