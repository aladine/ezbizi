<?php

echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Change the type of your company account', true));
      
      echo '<div class="form">';
         
         echo $form->create('Company',  array('url' => array('controller' => 'companies', 'action' => 'set_account', $comp['Company']['id'])));
         
         echo '<div>';
            echo '<div class="blk">';         
               echo $form->input('Company.account_id', array('label' => __('Company account type', true).':', 'type' => 'select'/*, 'empty' => ' - '.__('Select account type', true).' - '*/));     
            echo '</div>';
            echo '<div class="blk">';         
               echo $form->input('Company.validity_to', array('label' => __('Valid till', true).':', 'type' => 'date', 'dateFormat' => 'DMY', 'separator' => null, 'id' => 'bday'));     
            echo '</div>';   
            
         echo '</div>';        
         echo '<div class="bblk">';
            echo $form->submit(__('change', true));
         echo '</div>';
                  
         echo $form->end();
       
      echo '</div>'; 
      
   echo '</div>';

echo '</div></div>';
?>