<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag iface"><div class="cen">';
   echo '<div class="iCen">';

      echo '<div class="list">';
         
         if (!empty($new_connections)) {
         echo $html->div('headline hlB', __('Connections requests', true));
            foreach ($new_connections as $new_connection) {      
               echo '<div class="itm rad5">';
                  echo '<div class="logo">';
                     if (empty($new_connection['Company']['avatar']))
                        echo $html->image('noImage50.png');
                     else
                        echo $image->resize('comps/'.$new_connection['Company']['id'].'/'.$new_connection['Company']['avatar'], 50, 50);
                  echo '</div>';
                  echo '<div class="body">';
                     echo $html->link($new_connection['Company']['name'], array('controller' => 'companies', 'action' => 'view', $new_connection['Company']['id']), array('class' => 'name'));
                     if (empty($new_connection['Company']['public_sector']))
                        echo $html->div('sec', $new_connection['Sector']['name_'.$lang]);
                     else
                        echo $html->div('sec', __('Public sector', true));
                     $loc = $new_connection['Country']['name_'.$lang];
                     if (!empty($new_connection['Region']['name_'.$lang]))
                        $loc .= ' - '.$new_connection['Region']['name_'.$lang];
                     echo $html->div('loc', '('.$loc.')');
                  echo '</div>';
                  echo '<div class="btns">';
      
                     echo '<div class="sends">';
                        echo $html->div('send', $html->link(__('add to your connections', true), array('controller' => 'connections', 'action' => 'add', $new_connection['Company']['id']), array('class' => 'it geBg', 'escape' => false)));
                        echo $html->div('send', $html->link(__('decline', true), array('controller' => 'connections', 'action' => 'delete', $new_connection['Company']['id']), array('class' => 'it redBg', 'escape' => false)));
                     echo '</div>';
                
                     echo '<div class="rating">';
                        echo $html->image('icons/bigStar.png');
                        echo $html->div('val', number_format($new_connection['Company']['rating'], 2));
                     echo '</div>';
                  echo '</div>';            
               echo '</div>';
             }
         }
         else
            echo __('empty', true);
                  
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
         
      echo '</div>';
      
   echo '</div>';
   echo '<div class="iRight">';
      echo $this->element('company_interface_right');
   echo '</div>';
   
echo '</div></div>';
?>
