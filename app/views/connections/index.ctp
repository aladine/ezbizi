<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface"><div class="cen">';
   $cen_cls = 'iCen';

   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'iCen iCenS';
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   echo '<div class="'.$cen_cls.'">';
      if (!empty($new_con) && $pcompany['Company']['id'] == $company['Company']['id']) {
         echo '<div class="top">';                              
            echo $html->link($html->tag('span', __('You have', true)).'&nbsp;'.$html->tag('span', $new_con).'&nbsp;'.$html->tag('span', __('connection requests', true)), array('controller' => 'connections', 'action' => 'index', 'requests'), array('class' => 'conReq redBg', 'escape' => false));
         echo '</div>';
      }
      echo '<div class="list">';
                
         echo $html->div('headline hlB', __('Connections', true));
         if (!empty($connections)) {
            foreach ($connections as $connection) {
               echo '<div class="bconn rad3">';

                  if (empty($connection['Company']['avatar']))
                     $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                  else
                     $foto = $image->resize('comps/'.$connection['Company']['id'].'/'.$connection['Company']['avatar'], 130, 173, true, true);
                  echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));

                  echo $html->div('hov', $connection['Company']['name']);
                  echo $html->link('', array('controller' => 'companies', 'action' => 'view', $connection['Company']['id']), array('class' => 'tLink'));
                  if ($company['Company']['id'] == $session->read('Auth.User.company_id'))
                     echo $html->link('x', array('controller' => 'connections', 'action' => 'delete', $connection['Company']['id']), array('class' => 'del', 'escape' => false), __('Are you sure?',true));
                  
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('no connections added', true));
         
      echo '</div>';
      
      if (!empty($connections)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   
echo '</div></div>';
?>
