<?php

//WELCOE TEXT

echo '<div class="elm" id="welcome_text_bub">';
   echo $html->tag('span', 'ご登録いただきまして誠にありがとうございます。新規ご入会された皆様には新しく会社アカウントを立ち上げて頂くか、既存の会社アカウントに登録して下さい。', array('class' => 'bl bold'));
   echo '<br /><br />';
   echo $html->tag('span', '上記にございます ', array('class' => 'bl')).$html->tag('span', 'ユーザーアカウント登録 ', array('class' => 'vi bold')).$html->tag('span', '  をクリックして頂きますと、あなたの既存の会社アカウントに登録することができます。もしもまだあなたの会社アカウントがEzbiziで立ち上げられてない場合でも、あなたの個人のユーザーアカウントを立ち上げる前に、左側にございます、会社アカウント登録情報で登録することができます。', array('class' => 'bl'));
echo '</div>';

//TRADE SECTOR

echo '<div class="elm none" id="sector_text_bub">';
   echo $html->tag('span', ' ビジネスカテゴリー', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'あなたの会社のビジネスカテゴリーを選択して下さい。もしもあなたの会社がいくつかのビジネスに携わっている場合は、最も多くビジネス分野を占めているカテゴリーを選択して下さい<br /><br />ビジネスカテゴリーを順に選択 ', array('class' => 'bl')).$html->tag('span', 'ビジネスカテゴリー　＞　産業別　＞　商業別', array('class' => 'vi bold'));
echo '</div>';

//REGNUM

echo '<div class="elm none" id="regnum_text_bub">';
   echo $html->tag('span', 'ビジネス登録番号', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', '弊社Ezbiziでは、可能な限り詐欺やスキャムに対して厳しい対策をしています。Ezbiziでのサービスを悪用するユーザーを検出し防御する為のインテリジェントなメカニズムと警報システムの設定をすることにより、可能な限り明白にしていきます。', array('class' => 'bl'));
   echo '<br/><br/>';
   echo $html->tag('span', '弊社は、登録企業の全てのプロフィールに事業者登録番号 ', array('class' => 'bl bold')).$html->tag('span', '("B.R.N")', array('class' => 'lnk infoLink', 'id' => 'regnum_det_text')).$html->tag('span', ' を示す為に全登録企業を奨励します。', array('class' => 'bl bold'));
   echo '<br/><br/>';
   echo $html->tag('span', 'ご安心下さい。あなたの興味を保護するために、BRNは弊社のプレミアム会員様だけに表示されます。', array('class' => 'bl'));
echo '</div>';

//REGNUM DETAIL

echo '<div class="elm none" id="regnum_det_text_bub">';
   echo $html->tag('span', 'ビジネス登録番号', array('class' => 'ge bold'));
   echo '<br /><br />';                                                                                                                                                                                                                                                                                                                                                                                             
   echo $html->tag('span', 'あなたの会社を登記した国で使用されている実際のBRNを提供して下さい。この情報は、他のユーザーがあなたの国の公式なビジネス登記を通して、あなたの会社を識別するために参照させて頂きます。一般的に使用されているBRNの例は、', array('class' => 'bl')).$html->tag('span', 'VAT', array('class' => 'vi bold')).$html->tag('span', ' 識別番号です。Ezbiziはまたあなたの会社を登記した国の企業を識別する為に、実際の事業者登録番号だけを受け入れています。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'BRNを提供することであなたの会社プロファイルに信頼性が増すだけではなく、また企業間同士においても信頼性が出て、スムーズにビジネスができると弊社は考えております。', array('class' => 'ge'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'regnum_text'));
echo '</div>';

//ACCOUNT TYPE

echo '<div class="elm none" id="account_text_bub">';
   echo $html->tag('span', '会社のアカウントタイプ', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', '最も関連性の高いビジネスをご利用頂いて、あなたの会社を弊社Ezbiziにご登録して頂く為に、弊社は3種類の異なる会社アカウントを用いています。もしも間違って他の会社アカウントの種類を選択してしまってもご心配なさらないで下さい。後程、簡単に何度でも変更できます。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', '● ', array('class' => 'vi bold')).$html->tag('span', '買い手', array('class' => 'lnk infoLink', 'id' => 'buyer_text'));
   echo '<br /><br />';
   echo $html->tag('span', '● ', array('class' => 'vi bold')).$html->tag('span', '売り手', array('class' => 'lnk infoLink', 'id' => 'seller_text'));
   echo '<br /><br />';
   echo $html->tag('span', '● ', array('class' => 'vi bold')).$html->tag('span', '業者', array('class' => 'lnk infoLink', 'id' => 'trader_text'));
   echo '<br /><br />';
echo '</div>';

//BUYER

echo '<div class="elm none" id="buyer_text_bub">';
   echo $html->tag('span', '買い手', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', '買い手のアカウントのタイプは主に、調達したい品物やお探し品（RFQ\'s）の宣伝（投稿）したいユーザー用に設定されています。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', '買い手のアカウントタイプは、あなたが選択されたビジネスカテゴリーの中でお探し品（RFQ\'s）を投稿することができます。Ezbiziはまた大規模なネットワーキングより、ご希望のビジネス条件に一致する他の企業のプロファイルをあなたの会社に提案致します。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'account_text'));
echo '</div>';

//SELLER

echo '<div class="elm none" id="seller_text_bub">';
   echo $html->tag('span', '売り手', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', '売り手のアカウントタイプは、買い手と簡単に繋げるだけではなく、登録している買い手のお探し品（RFQ\'s）やターゲットを絞って販売する売り手を閲覧することもできます。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'メインページの右上に表示されています、あなたのビジネスに興味があるカテゴリーで、最新のお探し品リストを参照し、新たなビジネスに繋がるチャンスをお見逃しなくチェックして下さい！', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'account_text'));
echo '</div>';

//TRADER

echo '<div class="elm none" id="trader_text_bub">';
   echo $html->tag('span', '業者', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', '業者のアカウントタイプは、お探し品（RFQ\'s）の購入、投稿ができるだけではなく、買い手を見つけることに興味があるユーザー用に設定されています。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', '業者のアカウントタイプは、買い手と売り手のアカウントタイプの中で、一番便宜上がよく、業者会社にとってベストだと考えております。', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'account_text'));
echo '</div>';

//PUBLIC SECTOR

echo '<div class="elm none" id="public_text_bub">';
   echo $html->tag('span', '公共部門', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'あなたの会社が公共部門や政府機関と関連されている場合、あなたの会社のプロフィールにて、この旨を示すために項目にチェックをして下さい。', array('class' => 'bl'));
echo '</div>';
?>