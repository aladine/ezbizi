<?php
$srchTypes = array('1' => 'demands', '2' => 'companies','3'=>'offers');
   
echo '<div class="srch isrch">';
   echo $form->create('Demand', array('action' => 'search'));
      echo $form->input('Search.type_id', array('type' => 'hidden'));
      echo $form->input('Search.query', array('autocomplete' => 'off', 'after' => $html->div('lab', __('Search ...', true))));   
      echo $form->submit(__('GO', true));
      
      echo $html->div('srchT', $html->tag('span', '', array('class' => 'l')).$html->image('icons/w_switch.png', array('class' => 'wi')).$html->tag('span', '', array('class' => 'type')).$html->tag('span', '', array('class' => 'r')), array('id' => 'srchT'));
      
      echo '<div class="srchPop none" id="srchPop">';   
         foreach ($srchTypes as $ind => $type)
            echo $html->div('it', $html->tag('span', $ind, array('class' => 'id none')).$html->tag('span', $type, array('class' => 'type'))); 
      echo '</div>';
      
   echo $form->end();
echo '</div>';
?>

<script type="text/javascript">
$(document).ready(function(){   

   if($('.srch .text input').val() != '')
      $('.srch .text input').parent().find('.lab').hide();  
   
   $('.srch .text input').bind('click', function() {
      $(this).parent().find('.lab').hide('fast');
   }).bind('focusout', function() {
      if($(this).val() == '')
         $(this).parent().find('.lab').show('fast');   
   })
   
   $('.srch .text .lab').bind('click', function() {
      $(this).parent().find('input').focus();
      $(this).parent().find('.lab').hide('fast');
   });                                   
   
   searchTypes();
   
   $('#srchT').bind('click', function(e) {
      $('#srchPop').css({right: -61 + $('#srchT').width()});
      $('#srchPop').toggle();
      e.stopPropagation();   
   });
   
   $('#srchPop .it').bind('click', function() {
      $('#SearchTypeId').val($(this).find('.id').html());
      $('#srchT .type').html($(this).find('.type').html());
      $('#srchPop').hide();
      $('#srchPop .it').show(); 
      $(this).hide(); 
   });
   
   $(document).click(function(){
      $('#srchPop').hide();
   });
                  
   
});

function searchTypes() {
   if ($('#SearchTypeId').val() == '') {
      $('#SearchTypeId').val($('#srchPop .it:first .id').html());
      $('#srchT .type').html($('#srchPop .it:first .type').html());
      $('#srchPop .it:first').hide();
   }
   else {
      var itid = $('#SearchTypeId').val() - 1;
      $('#srchT .type').html($('#srchPop .it:eq('+itid+') .type').html());
      $('#srchPop .it:eq('+itid+')').hide();   
   }        
} 

</script>