<?php
echo '<div id="belt"><div class="cen setting">';
   echo '<div class="menu hom">';
      echo $html->tag('span', '', array('class' => 'l'));
      
      echo $html->link(__('home', true), array('controller' => 'users', 'action' => 'home'), array('class' => 'it'));
  
      echo $html->tag('span', '', array('class' => 'r'));
    echo '</div>';
    echo '<div class="menu">';
      echo $html->tag('span', '', array('class' => 'l'));
      
      echo $html->link(__('my settings', true), array('controller' => 'users', 'action' => 'edit'), array('class' => 'it'));
      if ($session->read('Auth.User.group_id') != 3) {
         echo $html->tag('span', '', array('class' => 'sep'));
         echo $html->link(__('company settings', true), array('controller' => 'companies', 'action' => 'edit'), array('class' => 'it'));
      }
      
      echo $html->tag('span', '', array('class' => 'r'));
    echo '</div>';
   echo $this->element('user_menu');
echo '</div></div>';
?>