<?php              
            foreach ($notifications as $notification) {   
               switch ($notification['Notification']['type']) {
                  case 'user_status' : 
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['User']['avatar']))                                                    
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('users/'.$notification['User']['id'].'/'.$notification['User']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['User']['title'].' '.$notification['User']['first_name'].' '.$notification['User']['middle_name'].' '.$notification['User']['last_name'], array('controller' => 'users', 'action' => 'view', $notification['User']['id']), array('class' => 'user')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->tag('span', __('changed profile status to', true).':', array('class' => 'text')).'&nbsp;'.$html->tag('span', $notification['Notification']['text'], array('class' => 'vi bold'));
                        echo '</div>';
                     echo '</div>';
                     break;
                  case 'company_status' : 
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->tag('span', __('changed it\'s status to', true).':', array('class' => 'text')).'&nbsp;'.$html->tag('span', $notification['Notification']['text'], array('class' => 'ge bold'));
                        echo '</div>';
                     echo '</div>';
                     break;
                  case 'rating_change' :
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->image('icons/f_rating.png', array('class' => 'iL'));
                           echo $html->tag('span', __('rating has been updated to', true).':', array('class' => 'text')).'&nbsp;';
                           echo $html->tag('span', number_format($notification['Notification']['text'], 2), array('class' => 'rating'));
                        echo '</div>';
                     echo '</div>';
                     break;
                  case 'type_change' :
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));;
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->tag('span', __('changed it\'s account type to', true).':', array('class' => 'text')).'&nbsp;';
                           $icons = array(1 => 'buyer', 2 => 'seller', 3 => 'trader');
                           echo $html->tag('span', $types[$notification['Notification']['text']], array('class' => 'bst_'.$icons[$notification['Notification']['text']]));
                           echo $html->image('icons/'.$icons[$notification['Notification']['text']].'.png', array('class' => 'iR'));
                        echo '</div>';
                     echo '</div>';
                     break;
                  case 'new_photos' :
                     echo '<div class="feed addD">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->image('icons/f_picture.png', array('class' => 'iL'));
                           echo $html->tag('span', __('uploaded new photos', true).':', array('class' => 'text')).'&nbsp;';
                           echo $html->link($notification['Album']['name'], array('controller' => 'albums', 'action' => 'view', $notification['Album']['id']), array('class' => 'link'));
                           //150 x 150
                           echo '<div class="fotos">';
                           foreach ($notification['Photo'] as $photo) {
                              echo $html->link($image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Album']['id'].'/'.$photo, 150, 150), '../img/comps/'.$notification['Company']['id'].'/'.$notification['Album']['id'].'/'.$photo, array('escape' => false, 'class' => 'foto rad3 gal'));
                           }
                           echo '</div>';
                        echo '</div>';
                     echo '</div>';
                     break; 
                  case 'new_document' :
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->image('icons/f_document.png', array('class' => 'iL'));
                           echo $html->tag('span', __('shared a new document', true).':', array('class' => 'text')).'&nbsp;';
                           echo $html->link($notification['Document']['name'], array('controller' => 'documents', 'action' => 'view', $notification['Document']['id']), array('class' => 'link'));
                       echo '</div>';
                     echo '</div>';
                     break;
                  case 'new_demand' :
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->image('icons/f_demand.png', array('class' => 'iL'));
                           echo $html->tag('span', __('looking for goods', true).':', array('class' => 'text')).'&nbsp;';
                           $ndtitle = number_format($notification['Demand']['Demand']['quantity']).' '.$notification['Demand']['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$notification['Demand']['Demand']['commodity'].'&nbsp;'.__('for', true).' '.$notification['Demand']['Demand']['price'].' '.$notification['Demand']['Currency']['name_'.$lang].'/'.$notification['Demand']['Unit']['name_'.$lang].' '.__('is needed.', true);
                           if ($lang == 'ja')
                              $ndtitle = number_format($notification['Demand']['Demand']['quantity']).' '.$notification['Demand']['Unit']['name_ja'].'  の '.$notification['Demand']['Demand']['commodity'].' を 1 '.$notification['Demand']['Unit']['name_ja'].' '.$notification['Demand']['Demand']['price'].' '.$notification['Demand']['Currency']['name_ja'].' で  探しています';
                           echo $html->link($ndtitle, array('controller' => 'demands', 'action' => 'view', $notification['Demand']['Demand']['id'], $ndtitle), array('class' => 'link', 'escape' => false));
                        echo '</div>';                                                                                                                                                    
                     echo '</div>';
                     break;
                  case 'recommendation' :
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo '<div class="bt">';
                              echo $html->image('icons/f_recom.png', array('class' => 'iL'));
                              echo $html->tag('span', __('New recommendation for', true).':', array('class' => 'text')).'&nbsp;';
                              echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp'));
                           echo '</div>';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->div('recom', $notification['Notification']['text']);
                        echo '</div>';
                     echo '</div>';
                     break;
                  case 'connection' :
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                       
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->tag('span', __('is now connected to', true).':', array('class' => 'text')).'&nbsp;';
                           echo $html->link($notification['Connection']['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Connection']['Company']['id']), array('class' => 'comp'));
                           echo $html->image('icons/f_connection.png', array('class' => 'iR'));
                        echo '</div>';
                     echo '</div>'; 
                     break;
                  case 'neg_rating' :  
                     echo '<div class="feed">';
                        echo '<div class="avat rad3">';
                           if (empty($notification['Company']['avatar']))
                              $foto = $this->webroot.IMAGES_URL.'noImage40.png';
                           else
                              $foto = $image->resize('comps/'.$notification['Company']['id'].'/'.$notification['Company']['avatar'], 55, 55, true, true);
                           echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                        echo '</div>';
                        echo '<div class="body">';
                           echo $html->link($notification['Company']['name'], array('controller' => 'companies', 'action' => 'view', $notification['Company']['id']), array('class' => 'comp')).'&nbsp;';
                           echo $html->tag('span', rel_time($notification['Notification']['created']), array('class' => 'date'));
                           echo $html->div('sep', '');
                           echo $html->tag('span', __('now has negative rating!', true), array('class' => 'negative')).'&nbsp;';
                        echo '</div>';
                     echo '</div>';
                     break;
               }
            }
?>