<?php
if (!empty($companies)) {
   foreach ($companies as $company) {
      echo '<div class="comp compI">';
         if (empty($company['Company']['avatar']))
            echo $html->div('thumb', $image->resize('noImage50.png', 50, 50));
         else
            echo $html->div('thumb', $image->resize('comps/'.$company['Company']['id'].'/'.$company['Company']['avatar'], 50, 50));
         echo '<div class="info">';
            echo $html->div('id', $company['Company']['id']);
            echo $html->div('name', $company['Company']['name']);
            if (empty($company['Company']['public_sector']))
               echo $html->div('sector', $company['Sector']['name_'.$lang]);
            else
               echo $html->div('sector', __('Public sector', true));
            $loc = $company['Country']['name_'.$lang];
            if (!empty($company['Region']['name_'.$lang]))
               $loc .= ' - '.$company['Region']['name_'.$lang];
            echo $html->div('loc', '('.$loc.')');
         echo '</div>';         
      echo '</div>';
   }
}
?>
<script type="text/javascript">
$(document).ready(function(){   
   //companies results onclick    
   $('#results .comp').bind('click', function(){
      $('#UserCompanyId').val($(this).find('.id').html());
      $('#UserFind').val($(this).find('.name').html().replace("&amp;", "&")).css({color: '#000000', fontWeight: 'bold'});
      $('#results .comp').removeClass('act');      
      $(this).addClass('act');
   });   
});
</script>