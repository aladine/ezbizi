<?php
echo '<div id="belt"><div class="cen iface">';
   if(!$session->check('Auth.User')) { 
      echo $this->element('search_home');
      echo $this->element('user_login');
   }
   else {
      echo $this->element('social_menu');
      echo $this->element('search');
      echo $this->element('user_menu');
      echo $this->element('account_indicator');
      if($account['Account']['id'] == 1 && $session->read('Auth.User.group_id') == 2)
         echo $this->element('bubble_info');
   }
echo '</div></div>';
?>