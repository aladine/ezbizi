<?php
$fp_cls = '';
if ($lang == 'ja')
   $fp_cls = 'fpja';
echo '<div class="login">';
echo $form->create('User', array('action' => 'login'));
   echo $form->input('User.login', array('autocomplete' => 'off', 'after' => $html->div('lab', __('login', true))));
   echo $form->input('User.password', array('after' => $html->div('lab', __('password', true)).$html->link(__('lost password', true), array('controller' => 'users', 'action' => 'forgot'), array('class' => 'fp '.$fp_cls))));
   echo $form->input('User.remember', array('label' => __('remember me', true), 'type' => 'checkbox', 'div' => 'input checkbox remember'));
   echo $form->submit(__('OK', true));
echo $form->end();
echo '</div>';
?>

<script type="text/javascript">
$(document).ready(function(){   

   loginEmpty();
   
   $('.login input').bind('keypress', function() {
      $(this).parent().find('.lab').hide('fast');
   }).bind('focusout', function() {
      if($(this).val() == '')
         $(this).parent().find('.lab').show('fast');   
   })
   
   $('.login .lab').bind('click', function() {
      $(this).parent().find('input').focus();
   });                
   
});

function loginEmpty() {
   if($('#UserLogin').val() !='')
      $('#UserLogin').parent().find('.lab').hide();
   if($('#UserPassword').val() !='')
      $('#UserPassword').parent().find('.lab').hide();
}
</script>