<?php
echo '<div class="userMenu">';
   echo '<div>'; 
     echo '<div class="menu">';
        echo $html->link(__('my profile', true), array('controller' => 'users', 'action' => 'view', $session->read('Auth.User.id')), array('class' => 'it'));
        echo '<br />';
        echo $html->link(__('company profile', true), array('controller' => 'companies', 'action' => 'view', $session->read('Auth.User.company_id')), array('class' => 'it'));
        echo '<br />';
        if ($session->read('Auth.User.groupid') == 3)
           echo $html->link(__('settings', true), array('controller' => 'users', 'action' => 'edit'), array('class' => 'it'));
        else
           echo $html->link(__('settings', true), array('controller' => 'companies', 'action' => 'edit'), array('class' => 'it'));
        echo '<br />';
        if ($session->read('Auth.User.group_id') == 1)
            echo $html->link(__('administration', true), array('controller' => 'users', 'action' => 'admin_index'), array('class' => 'it')); 
     echo '</div>';
     echo '<div class="avatar">';//80x100
        $avatar = $session->read('Auth.User.avatar');
        if (empty($avatar))
           echo $html->image('noImage80.png');
        else
           echo $image->resize('users/'.$session->read('Auth.User.id').'/'.$avatar, 80, 100);
     echo '</div>';
   echo '</div>';
   echo '<div class="logout">';
      echo $html->link($html->image('icons/logof.png', array('class' => 'iL')).$html->tag('span', __('logout', true)), array('controller' => 'users', 'action' => 'logout'), array('class' => 'it', 'escape' => false));
   echo '</div>';    
echo '</div>';
?>      