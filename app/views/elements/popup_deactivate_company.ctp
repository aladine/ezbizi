<?php
echo '<div class="popUp" id="pUpDeactCompany_win">';
   echo '<div class="main">';
      echo '<div class="con deact">';
         echo $html->div('title', __('Deactivate company account', true).':');
               
         echo '<div class="form">';

         echo '<div class="dscText">';
            echo __('We are really sorry to see you go!', true).'<br/><br/>';
            echo __('We strive to improve our service, therefore your honest feedback is much appreciated. Please let us know the reason why you decided to deactivate you account and the way how we can make Ezbizi a better service for you in the future, by sending us your message', true).' '.$this->Html->link(__('here', true), array('controller' => 'users', 'action' => 'contact')).'<br/><br/>';
            echo __('Thank you for using Ezbizi and hope to see you back again.', true).'<br /><br />';
            echo __('The Ezbizi Team', true);
         echo '</div>';

         echo '<div class="bblk">';
            echo $html->link(__('deactivate', true), array('controller' => 'companies', 'action' => 'delete', $session->read('Auth.User.company_id')), array('class' => 'link'), __('Are you sure?', true));
         echo '</div>';
                  
         echo '</div>';
               
         echo $html->div('bot', $html->image('icons/close.png', array('title' => __('close window', true), 'class' => 'popUpCls')));
      echo '</div>';
   echo '</div>';
echo '</div>';
?>