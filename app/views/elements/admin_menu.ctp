<?php
echo '<div class="menu">';
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('User And Comapany Management', true)));
      echo $html->link(__('registered users', true), array('controller' => 'users', 'action' => 'admin_index'), array('class' => 'mn ge_mn'));
      echo $html->link(__('registered companies', true), array('controller' => 'companies', 'action' => 'admin_index'), array('class' => 'mn ge_mn'));
      
      echo $html->link(__('users archive', true), array('controller' => 'people', 'action' => 'index'), array('class' => 'mn ge_mn'));
      echo $html->link(__('companies archive', true), array('controller' => 'firms', 'action' => 'index'), array('class' => 'mn ge_mn'));
      
      echo $html->link(__('orders', true), array('controller' => 'orders', 'action' => 'index'), array('class' => 'mn ge_mn'));
   echo '</div>';
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('Web Management', true)));
      echo $html->link(__('business categories', true), array('controller' => 'categories', 'action' => 'index'), array('class' => 'mn bl_mn'));
      echo $html->link(__('countries', true), array('controller' => 'countries', 'action' => 'index'), array('class' => 'mn bl_mn'));
      echo $html->link(__('sites', true), array('controller' => 'sites', 'action' => 'index'), array('class' => 'mn bl_mn'));
      echo $html->link(__('translation', true), array('controller' => 'translations', 'action' => 'index'), array('class' => 'mn bl_mn'));
   echo '</div>';
echo '</div>';
?>