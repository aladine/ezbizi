<?php
echo '<div class="menu">';
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('Basic Information', true)));
      echo $html->link(__('personal information', true), array('controller' => 'users', 'action' => 'edit'), array('class' => 'mn ge_mn'));
      echo $html->link(__('profile photo', true), array('controller' => 'users', 'action' => 'edit', 'avatar'), array('class' => 'mn ge_mn'));
      echo $html->link(__('interests', true), array('controller' => 'users', 'action' => 'edit', 'interests'), array('class' => 'mn ge_mn'));
   echo '</div>';
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('Deactivate My Account', true)));
      echo $html->div('mn bl_mn sTo', __('deactivate account', true), array('id' => 'pUpDeactUser'));
   echo '</div>';
   echo $this->element('popup_deactivate_user');
echo '</div>';
?>

<script type="text/javascript">
var popUpH, popUpW, popUpLP, popUpTP;

$(document).ready(function(){   
   
   //popUp Open   
   $('.sTo').click(function(){
      $win = '#'+$(this).attr('id')+'_win';
      popUpDim($win); 

      $('.popUp:visible').hide();            
      $($win).css({top: popUpTP, left: popUpLP}).fadeIn(300);
   });
   
   //popUp close 
   $('.popUpCls').click(function(){
      $('.popUp').fadeOut(300);   
   });   

});

function popUpDim($elm) {
   popUpH = $($elm).height();
   popUpW = $($elm).width();
   popUpLP = Math.abs(wW - popUpW)/2;
   popUpTP = Math.abs(wH - popUpH)/2;  
}

</script>