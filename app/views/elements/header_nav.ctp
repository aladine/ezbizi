<div id="header-nav">
<div id="header-nav-left">
	<?php echo $this->Html->link(__('Messages',true),array('controller' => 'messages', 'action' => 'index'), array('escape' => false))?>	
	<?php echo $this->Html->link(__('Interests',true),array('controller' => 'interests', 'action' => 'index'), array('escape' => false))?>	
	<?php echo $this->Html->link(__('Offers',true),array('controller' => 'offers', 'action' => 'index'), array('escape' => false))?>	
	<?php echo $this->Html->link(__('Demands',true),array('controller' => 'demands', 'action' => 'index'), array('escape' => false))?>	
	<?php echo $this->Html->link(__('Connections',true),array('controller' => 'connections', 'action' => 'index'), array('escape' => false))?>	
	<a href="#" style="border-right:1px solid #909090;">More</a>
</div> <!-- header-nav-left ends -->

<div id="header-nav-right">
	<?php echo $this->Html->link(__('Post New Demands',true),array('controller' => 'demands', 'action' => 'add'), array('escape' => false,'class'=>'postdemands'))?>
	<?php echo $this->Html->link(__('Post New Offers',true),array('controller' => 'offers', 'action' => 'add'), array('escape' => false,'class'=>'postoffers'))?>	
</div> <!-- header-nav-right ends --></div> <!-- header-nav ends -->

</div> <!-- header ends -->