<?php
echo '<div class="accInd">';
   $account_id = $html->tag('span', $account['Account']['name_'.$lang]);
   if($company['Company']['type_id'] == 1)
      echo $html->image('icons/ind_buyer.png').$account_id;
   if($company['Company']['type_id'] == 2)
      echo $html->image('icons/ind_seller.png').$account_id;
   if($company['Company']['type_id'] == 3)
      echo $html->image('icons/ind_trader.png').$account_id;
   if (in_array($session->read('Auth.User.group_id'), array(1, 2)))
      echo $html->link(' ', array('controller' => 'companies', 'action' => 'edit', 'type'), array('class' => 'tLink'));
   
echo '</div>';
?>