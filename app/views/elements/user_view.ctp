<?php
//---> ACTIONS
echo '<div class="inds acts">';
   if (!$user['User']['active'] && $session->read('Auth.User.group_id') == 2 && $user['User']['group_id'] == 3 && $user['User']['company_id'] == $company['Company']['id'])
      echo $html->div('it vi_ind', $html->tag('span', '', array('class' => 'l')).$html->link($html->image('icons/w_approve.png', array('class' => 'wi')).$html->tag('span', __('approve', true)), array('controller' => 'users', 'action' => 'approve', $user['User']['id']),array('class' => 't', 'escape' => false)).$html->tag('span', '', array('class' => 'r')));
   if ($user['User']['active'] && $session->read('Auth.User.group_id') == 2 && $user['User']['group_id'] == 3 && $user['User']['company_id'] == $company['Company']['id'])
      echo $html->div('it bl_ind', $html->tag('span', '', array('class' => 'l')).$html->link($html->image('icons/w_admin.png', array('class' => 'wi')).$html->tag('span', __('set as admin', true)), array('controller' => 'users', 'action' => 'set_admin', $user['User']['id']),array('class' => 't', 'escape' => false)).$html->tag('span', '', array('class' => 'r'))); 
   if (!empty($can_message) && $user['User']['active'])        
      echo $html->div('it ge_ind', $html->tag('span', '', array('class' => 'l')).$html->link($html->image('icons/w_messages.png', array('class' => 'wi')).$html->tag('span', __('send a message', true)), array('controller' => 'messages', 'action' => 'add', $user['User']['id']),array('class' => 't', 'escape' => false)).$html->tag('span', '', array('class' => 'r')));
   if (!empty($blocked))
      echo $html->div('bt', $html->link(__('unblock user', true), array('controller' => 'bans', 'action' => 'delete', $user['User']['id']), array('escape' => false), __('Are you sure that you want unblock receiving messages from this user?',true)));
echo '</div>';

//---> AVATAR
      
$name = '';
if (!empty($user['User']['title']))
   $name .= $user['User']['title'].' ';
if (!empty($user['User']['first_name']))
   $name .= $user['User']['first_name'].' ';
if (!empty($user['User']['middle_name']))
   $name .= $user['User']['middle_name'].' ';
if (!empty($user['User']['last_name']))
   $name .= $user['User']['last_name'];

   echo '<div class="modP rad5 uAvat">';
   echo $html->div('name', $name);
      echo '<div class="avatar rad3">';
            
      if (empty($user['User']['avatar']))
         $foto = $this->webroot.IMAGES_URL.'noImage50.png';
      else
         $foto = $image->resize('users/'.$user['User']['id'].'/'.$user['User']['avatar'], 170, 170, true, true);
            
      echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
         
      if ($user['User']['online']) {
         $last_activity = strtotime($user['User']['last_activity']);
         $now = time();
         $last_activity = $now - $last_activity;
         if ($last_activity < 1800)
            echo $html->image('icons/online.png', array('class' => 'onf'));
         else
            echo $html->image('icons/offline.png', array('class' => 'onf'));
      }
      else
         echo $html->image('icons/offline.png', array('class' => 'onf'));
   echo '</div>';            
      
if ($user['User']['id'] == $session->read('Auth.User.id'))
   echo $html->div('edtL', $html->link(__('change profile picture', true), array('controller' => 'users', 'action' => 'edit', 'avatar')));

echo '</div>';

//---> INFOS
      
echo '<div class="modP rad5 infs">';
         
   //echo $html->div('headline', __('Personal Information', true));
         
   echo $html->div('inf', $html->tag('span', __('Job title', true).': ', array('class' => 'at')).$html->tag('span', $user['User']['job_title'], array('class' => 'vl')));
   if (!empty($user['Sex']['name_'.$lang]))
      echo $html->div('inf', $html->tag('span', __('Gender', true).': ', array('class' => 'at')).$html->tag('span', $user['Sex']['name_'.$lang], array('class' => 'vl')));
   if (!empty($user['User']['birthday']))
      echo $html->div('inf', $html->tag('span', __('Birthday', true).': ', array('class' => 'at')).$html->tag('span', date('d.m.Y', strtotime($user['User']['birthday'])), array('class' => 'vl')));
   if (!empty($user['State']['name_'.$lang]))
      echo $html->div('inf', $html->tag('span', __('Marital status', true).': ', array('class' => 'at')).$html->tag('span', $user['State']['name_'.$lang], array('class' => 'vl')));
         
   echo '<br />';
   echo $html->div('inf', $html->image('icons/mail.png', array('class' => 'ic')).$html->tag('span', $user['User']['email'], array('class' => 'vl')));
   if (!empty($user['User']['phone2']))
      echo $html->div('inf', $html->image('icons/phone.png', array('class' => 'ic')).$html->tag('span', '+'.$user['User']['phone1'].' '.$user['User']['phone2'], array('class' => 'vl')));
   if (!empty($user['User']['fax2']))
      echo $html->div('inf', $html->image('icons/fax.png', array('class' => 'ic')).$html->tag('span', '+'.$user['User']['fax1'].' '.$user['User']['fax2'], array('class' => 'vl')));
   echo '<br />';
         
   if (!empty($user['User']['web']))
      echo $html->div('inf', $html->image('icons/web.png', array('class' => 'ic')).$html->tag('span', $html->link($user['User']['web'], http_sanitize($user['User']['web'])), array('class' => 'vl')));
 
   $socials = array('skype' => 'Skype', 'facebook' => 'Facebook', 'linkedin' => 'LinkedIn', 'twitter' => 'Twitter', 'google' => 'Google+', 'myspace' => 'Myspace', 'foursquare' => 'Foursquare', 'reddit' => 'Reddit', 'stumble' => 'StumbleUpon', 'quora' => 'Quora', 'tumblr' => 'Tumblr', 'flickr' => 'Flickr', 'picasa' => 'Picasa', 'etsy' => 'Etsy', 'rss' => 'RSS');
   
   echo '<div class="socm">';
   if (!empty($socials)) {
      foreach ($socials as $ind => $value) {
         if (!empty($user['User'][$ind]))
            echo $html->div('soci rad5', $html->link($html->image('socmedia/'.$ind.'.png', array('class' => 'ic')), http_sanitize($user['User'][$ind]), array('escape' => false, 'target' => '_blank')));
      }
   }
   echo '</div>';
         
   if ($user['User']['id'] == $session->read('Auth.User.id'))
      echo $html->div('edtL', $html->link(__('change info', true), array('controller' => 'users', 'action' => 'edit')));
echo '</div>';
?>