<?php
echo '<div class="inv">';
   echo $html->div('prc', $html->tag('span', __('Or you send', true)).' '.$html->tag('span', $package['Package']['invitations']).' '.$html->tag('span', __('invitations', true)).'.');
   echo $html->div('val', $html->tag('span', __('You already sent', true)).' '.$html->tag('span', $company['Company']['invitation_count']).' '.$html->tag('span', __('invitations', true)).'.');
   if ($package['Package']['invitations'] <= $company['Company']['invitation_count'])
      echo $html->link('Spend it for this package', array('controller' => 'orders', 'action' => 'paid', 'invitations', $package['Package']['id']), array('class' => 'buy geBg rad3'));
   else {
      echo $form->create('Invitation', array('url' => array('controller' => 'companies', 'action' => 'edit', 'account')));
         echo '<div class="invForm rad3">';
         echo $form->input('Invitation.email', array('label' => __('E-mail', true).':'));
            echo '<div class="bblk">';
               echo $form->submit(__('send invitation', true));
            echo '</div>';
         echo '</div>';
      echo $form->end();
   }
echo '</div>';
?>