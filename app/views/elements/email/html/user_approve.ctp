<?php
echo 'Congratulations! Your account has been approved and you are now ready to grow your business with Ezbizi!<br />';
echo '<br />';
echo 'Best of luck to you and your business!<br />';
echo '<br />'; 
echo 'Best regards,<br />';
echo 'The Ezbizi Team<br /><br />';
echo $html->link($html->image('http://'.$_SERVER['HTTP_HOST'].'/img/logoBeta.png'), 'http://www.ezbizi.com', array('escape' => false));
echo '<br /><br />';
echo $html->link('Feedback', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'contact')));
echo '<br />';
echo $html->link('www.ezbizi.com', 'http://www.ezbizi.com');
?>