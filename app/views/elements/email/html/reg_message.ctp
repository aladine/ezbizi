<?php
if ($lang == 'ja') {
   echo $mes_data['User']['last_name'].'様'; 
   echo '<br /><br />';
   echo $html->div('normal', 'ご登録ありがとうございます。そしてようこそEzbiziへ。あなたのアカウントは無事に作成されました。あなたのログインの詳細は：');
   echo '<br />';
   echo '................................................................................';
   echo '<br /><br />';
   echo 'ログイン： '.$mes_data['User']['login'];
   echo '<br />';
   echo 'パスワード： '.$mes_data['User']['passwd'];
   echo '<br /><br />';
   echo '................................................................................';
   echo '<br /><br />';
   echo 'あなたのEmailアドレスの確認とあなたのユーザーアカウントを起動させる為には、下記のリンクをクリックして下さい：';
}
else {
   echo __('Dear ', true).$mes_data['User']['title'].' '.$mes_data['User']['first_name'].' '.$mes_data['User']['middle_name'].' '.$mes_data['User']['last_name']; 
   echo '<br /><br />';
   echo $html->div('normal', __('Thank you for your registration and welcome to Ezbizi. Your account has been created and your login details are:', true));
   echo '<br />';
   echo '................................................................................';
   echo '<br /><br />';
   echo __('Login: ', true).$mes_data['User']['login'];
   echo '<br />';
   echo __('Password: ', true).$mes_data['User']['passwd'];
   echo '<br /><br />';
   echo '................................................................................';
   echo '<br /><br />';
   echo __('Click on the link below to verify your email address and activate your user account:', true);
}
echo '<br /><br />';
$link = 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'activate', $mes_data['User']['key']));
echo $html->link($link, $link);
echo '<br /><br />';
echo 'Best regards,<br />';
echo 'The Ezbizi Team<br /><br />';
echo $html->link($html->image('http://'.$_SERVER['HTTP_HOST'].'/img/logoBeta.png'), 'http://www.ezbizi.com', array('escape' => false));
echo '<br /><br />';
echo $html->link('Feedback', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'contact')));
echo '<br />';
echo $html->link('www.ezbizi.com', 'http://www.ezbizi.com');
?>