<?php 
if ($lang == 'ja') {
   echo $mes_data['User']['last_name'].'様';
   echo '<br /><br />';
   echo 'ログイン情報のリセットリクエストを承りました。あなたのログイン情報にて新しいパスワードを下記に記載しています。<br />';
   echo 'この後、下記のパスワードで一度ログインをし、アカウント設定のページで、再度パスワードの変更をするようにお願いも致します。<br />';
   echo '<br />';
   echo '................................................................................';
   echo '<br /><br />';
   echo __('Email : ', true).$mes_data['User']['login'];
   echo '<br />';
   echo __('新しいパスワード： ', true).$mes_data['User']['passwd'];
}
else {
   echo __('Dear ', true).$mes_data['User']['title'].' '.$mes_data['User']['first_name'].' '.$mes_data['User']['middle_name'].' '.$mes_data['User']['last_name'];
   echo '<br /><br />';
   echo 'We have received a request to retrieve your login details. Your login details including new password is below. Once you login, please consider changing your password in the accounts settings page.';
   echo '<br /><br />';
   echo '................................................................................';
   echo '<br /><br />';
   echo __('Email : ', true).$mes_data['User']['login'];
   echo '<br />';
   echo __('New password: ', true).$mes_data['User']['passwd'];
}
echo '<br /><br />';
echo '................................................................................';
echo '<br /><br />';
echo 'Best regards,<br />';
echo 'The Ezbizi Team<br /><br />';
echo $html->link($html->image('http://'.$_SERVER['HTTP_HOST'].'/img/logoBeta.png'), 'http://www.ezbizi.com', array('escape' => false));
echo '<br /><br />';
echo $html->link('Feedback', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'contact')));
echo '<br />';
echo $html->link('www.ezbizi.com', 'http://www.ezbizi.com');