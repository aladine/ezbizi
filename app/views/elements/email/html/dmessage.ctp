<?php
if ($lang == 'ja') {
   echo $mes_data['User']['last_name'].'様'; 
   echo '<br /><br />';
   echo 'お探し品に関する新しいメッセージを受信しました。';
   echo '<br /><br />';
   echo $html->link($dtitle = $mes_data['Demand']['Demand']['quantity'].' '.$mes_data['Demand']['Unit']['name_ja'].'  の '.$mes_data['Demand']['Demand']['commodity'].' を 1 '.$mes_data['Demand']['Unit']['name_ja'].' '.$mes_data['Demand']['Demand']['price'].' '.$mes_data['Demand']['Currency']['name_ja'].' で  探しています', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'demands', 'action' => 'view', $mes_data['Demand']['Demand']['id'])));
   //echo '<br />'.$mes_data['Dmessage']['text'];
   echo '<br /><br />';
   $oponent = $mes_data['Sender']['id'];
   echo '<br />'.$html->link('こちら', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'dmessages', 'action' => 'view', $mes_data['Demand']['Demand']['id'], $oponent))).'をクリックし、このメッセージのチェック、返信ができます。';
}
else {
   echo __('Dear ', true).$mes_data['Demand']['User']['title'].' '.$mes_data['Demand']['User']['first_name'].' '.$mes_data['Demand']['User']['middle_name'].' '.$mes_data['Demand']['User']['last_name']; 
   echo '<br /><br />';
   echo 'You have received a new demand message for your demand:';
   echo '<br /><br />';
   echo $html->link($mes_data['Demand']['Demand']['quantity'].' '.$mes_data['Demand']['Unit']['name_'.$lang].__(' of ', true).$mes_data['Demand']['Demand']['commodity'].__(' for ', true).$mes_data['Demand']['Demand']['price'].' '.$mes_data['Demand']['Currency']['name_'.$lang].__(' is needed.', true), 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'demands', 'action' => 'view', $mes_data['Demand']['Demand']['id'])));
   //echo '<br />'.$mes_data['Dmessage']['text'];
   echo '<br /><br />';
   $oponent = $mes_data['Sender']['id'];
   echo '<br />'.__('You can view and reply to this message by clicking ', true).$html->link(__('here', true), 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'dmessages', 'action' => 'view', $mes_data['Demand']['Demand']['id'], $oponent)));
}
echo '<br /><br />';
echo 'Best regards,<br />';
echo 'The Ezbizi Team<br /><br />';
echo $html->link($html->image('http://'.$_SERVER['HTTP_HOST'].'/img/logoBeta.png'), 'http://www.ezbizi.com', array('escape' => false));
echo '<br /><br />';
echo $html->link('Feedback', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'contact')));
echo '<br />';
echo $html->link('www.ezbizi.com', 'http://www.ezbizi.com');
?>