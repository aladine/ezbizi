<?php
if ($lang == 'ja') {
   echo $mes_data['User']['last_name'].'様'; 
   echo '<br /><br />';
   echo $mes_data['Sender']['title'].' '.$mes_data['Sender']['first_name'].' '.$mes_data['Sender']['middle_name'].' '.$mes_data['Sender']['last_name'].'様より新着メッセージの受信';
   echo '<br /><br />';
   echo $html->link('こちら', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'messages', 'action' => 'view', $mes_data['Sender']['id']))).'をクリックし、このメッセージのチェック、返信ができます。';
}
else {
   echo __('Dear ', true).$mes_data['User']['title'].' '.$mes_data['User']['first_name'].' '.$mes_data['User']['middle_name'].' '.$mes_data['User']['last_name']; 
   echo '<br /><br />';
   echo 'You have received a new message from: '.$mes_data['Sender']['title'].' '.$mes_data['Sender']['first_name'].' '.$mes_data['Sender']['middle_name'].' '.$mes_data['Sender']['last_name'];
   echo '<br /><br />';
   //echo '<br />'.$mes_data['Message']['text'];
   echo __('You can view and reply to this message by clicking ', true).$html->link(__('here', true), 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'messages', 'action' => 'view', $mes_data['Sender']['id'])));
}
echo '<br /><br />';
echo 'Best regards,<br />';
echo 'The Ezbizi Team<br /><br />';
echo $html->link($html->image('http://'.$_SERVER['HTTP_HOST'].'/img/logoBeta.png'), 'http://www.ezbizi.com', array('escape' => false));
echo '<br /><br />';
echo $html->link('Feedback', 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'contact')));
echo '<br />';
echo $html->link('www.ezbizi.com', 'http://www.ezbizi.com');
?>