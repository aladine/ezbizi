<?php echo __('Dear ', true).$mes_data['User']['title'].' '.$mes_data['User']['first_name'].' '.$mes_data['User']['middle_name'].' '.$mes_data['User']['last_name']; ?> 


<?php echo __('Thank you for your registration and welcome to www.ezbizi.com .', true); ?>


<?php echo __('Your Free Ezbizi.com Membership has been confirmed. Your sign in details:', true); ?>

	
................................................................................

<?php echo __('Email : ', true).$mes_data['User']['login']; ?>

<?php echo __('Password : ', true).$mes_data['User']['passwd']; ?>

................................................................................

<?php echo __('Click on this link to verify your email address and to activate your user account.', true); ?>

<?php echo 'http://'.$_SERVER['HTTP_HOST'].$html->url(array('controller' => 'users', 'action' => 'activate', $mes_data['User']['key'])); ?>



www.ezbizi.com