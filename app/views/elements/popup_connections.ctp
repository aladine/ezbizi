<?php
echo '<div class="popUp" id="pUpCon_win">';
   echo '<div class="main">';
      echo '<div class="con">';
         echo $html->div('title', __('Select from your connection\'s employees', true).':');
         echo $html->div('back none', '< '.__('show connections', true), array('id' => 'recBack'));
         echo '<div class="list">';
            
            //echo '<div class="recType none">company</div>';
                     
            if (!empty($connections)) {
               foreach ($connections as $connection) {
                   echo '<div class="comp_staf">';  
                      
                      echo '<div class="itm cmp">';
                         echo '<div class="avat">';
                            if (empty($connection['Company']['avatar']))
                               echo $html->image('noImage50.png');
                            else   
                               echo $image->resize('comps/'.$connection['Company']['id'].'/'.$connection['Company']['avatar'], 50, 50);
                         echo '</div>';
                         echo $html->div('id none', $connection['Company']['id']);
                         echo $html->div('name', $connection['Company']['name']);
                      echo '</div>';
                      
                      if (!empty($connection['User'])) {
                        foreach ($connection['User'] as $user) { 
                           if (!$user['active'])
                              continue;        
                           echo '<div class="itm staf none">';
                              echo '<div class="avat">';
                                 if (empty($user['avatar']))
                                    echo $image->resize('noImage50.png', 50, 50);
                                 else
                                    echo $image->resize('users/'.$user['id'].'/'.$user['avatar'], 50, 50);
                              echo '</div>';
                              echo $html->div('id none', $user['id']);
                              echo $html->div('name', $user['title'].' '.$user['first_name'].' '.$user['middle_name'].' '.$user['last_name']);
                           echo '</div>';
                        }
                     }
                     else
                        echo $html->div('noData', __('Company has no registered employees', true));     
                      
                   echo '</div>';
               }
            }
            else
               echo $html->div('noData', __('You have no conenctions', true));
         
         echo '</div>';
         echo $html->div('bot', $html->image('icons/close.png', array('title' => __('close window', true), 'class' => 'popUpCls')));
      echo '</div>';
   echo '</div>';
echo '</div>';
?>