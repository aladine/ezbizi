<?php
echo '<div class="menu">';
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('Basic Information', true)));
      echo $html->link(__('company information', true), array('controller' => 'companies', 'action' => 'edit'), array('class' => 'mn ge_mn'));
      echo $html->link(__('company logo', true), array('controller' => 'companies', 'action' => 'edit', 'logo'), array('class' => 'mn ge_mn'));
   echo '</div>';
   
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('Account Settings', true)));
      echo $html->link(__('account type', true), array('controller' => 'companies', 'action' => 'edit', 'type'), array('class' => 'mn vi_mn'));
      echo $html->link(__('area of business interest', true), array('controller' => 'companies', 'action' => 'edit', 'business-area'), array('class' => 'mn vi_mn'));     
      echo $html->link(__('order premium account', true), array('controller' => 'companies', 'action' => 'edit', 'account'), array('class' => 'mn vi_mn'));
      echo $html->link(__('newsletter', true), array('controller' => 'companies', 'action' => 'edit', 'newsletter'), array('class' => 'mn vi_mn'));
   echo '</div>';
   echo '<div class="sub">';
      echo $html->div('tit', $html->image('icons/wrench.png').$html->tag('span', __('Deactivate Company Account', true)));
      echo $html->div('mn bl_mn sTo', __('deactivate account', true), array('id' => 'pUpDeactCompany'));
   echo '</div>';
   echo $this->element('popup_deactivate_company');
echo '</div>';
?>

<script type="text/javascript">
var popUpH, popUpW, popUpLP, popUpTP;

$(document).ready(function(){   
   
   //popUp Open   
   $('.sTo').click(function(){
      $win = '#'+$(this).attr('id')+'_win';
      popUpDim($win); 

      $('.popUp:visible').hide();            
      $($win).css({top: popUpTP, left: popUpLP}).fadeIn(300);
   });
   
   //popUp close 
   $('.popUpCls').click(function(){
      $('.popUp').fadeOut(300);   
   });   

});

function popUpDim($elm) {
   popUpH = $($elm).height();
   popUpW = $($elm).width();
   popUpLP = Math.abs(wW - popUpW)/2;
   popUpTP = Math.abs(wH - popUpH)/2;  
}

</script>          