<?php
  echo '<div class="srch">';
   echo $form->create('Demand', array('action' => 'search'));
      echo $form->input('Search.query', array('autocomplete' => 'off', 'after' => $html->div('lab', __('Search ...', true))));   
      echo $form->submit(__('GO', true));
      
      echo $html->div('srchT', $html->tag('span', '', array('class' => 'l'))./*$html->image('icons/w_switch.png', array('class' => 'wi')).*/$html->tag('span', __('demands', true), array('class' => 'type')).$html->tag('span', '', array('class' => 'r')), array('id' => 'srchT'));
         
   echo $form->end();
echo '</div>';
?>

<script type="text/javascript">
$(document).ready(function(){   

   if($('.srch .text input').val() != '')
      $('.srch .text input').parent().find('.lab').hide();   
   
   $('.srch .text input').bind('click', function() {
      $(this).parent().find('.lab').hide('fast');
   }).bind('focusout', function() {
      if($(this).val() == '')
         $(this).parent().find('.lab').show('fast');   
   })
   
   $('.srch .text .lab').bind('click', function() {
      $(this).parent().find('input').focus();
      $(this).parent().find('.lab').hide('fast');
   });                                   
     
});

</script>