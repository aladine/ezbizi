<?php
if (!empty($companies)) {
   foreach ($companies as $company) {
      echo '<div class="comp compI">';
         $string = '';
         if (empty($company['Company']['avatar']))
            $string .= $html->div('thumb', $image->resize('noImage50.png', 50, 50));
         else
            $string .= $html->div('thumb', $image->resize('comps/'.$company['Company']['id'].'/'.$company['Company']['avatar'], 50, 50));
         $string .= '<div class="info">';
            $string .= $html->div('id', $company['Company']['id']);
            $string .= $html->div('name', $company['Company']['name']);
            if (empty($company['Company']['public_sector']))
               $string .= $html->div('sector', $company['Sector']['name_'.$lang]);
            else
               $string .= $html->div('sector', __('Public sector', true));
            $loc = $company['Country']['name_'.$lang];
            if (!empty($company['Region']['name_'.$lang]))
               $loc .= ' - '.$company['Region']['name_'.$lang];
            $string .= $html->div('loc', '('.$loc.')');
         $string .= '</div>'; 
         echo $html->link($string, array('controller' => 'companies', 'action' => 'view', $company['Company']['id']), array('escape' => false));        
      echo '</div>';
   }
}
?>