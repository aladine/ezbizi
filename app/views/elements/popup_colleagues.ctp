<?php
echo '<div class="popUp" id="pUpCol_win">';
   echo '<div class="main">';
      echo '<div class="con">';
         echo $html->div('title', __('Select from colleagues', true).':');
         echo '<div class="list">';
            
            //echo '<div class="recType none">user</div>';
            
            if (!empty($colleagues)) {
               foreach ($colleagues as $colleague) {
                  echo '<div class="itm">';
                     echo '<div class="avat">';
                        if (empty($colleague['User']['avatar']))
                           echo $html->image('noImage50.png');
                        else
                           echo $image->resize('users/'.$colleague['User']['id'].'/'.$colleague['User']['avatar'], 50, 50);     
                     echo '</div>';
                     echo $html->div('id none', $colleague['User']['id']);
                     echo $html->div('name', $colleague['User']['title'].' '.$colleague['User']['first_name'].' '.$colleague['User']['middle_name'].' '.$colleague['User']['last_name']);
                  echo '</div>';
               }
            }
            else
               echo $html->div('noData', __('You have no registered colleagues', true));
            
         echo '</div>';
         echo $html->div('bot', $html->image('icons/close.png', array('title' => __('close window', true), 'class' => 'popUpCls')));
      echo '</div>';
   echo '</div>';
echo '</div>';
?>