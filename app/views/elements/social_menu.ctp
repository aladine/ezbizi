<?php
$lng_cls = ''; if ($lang == 'ja') $lng_cls = 'ja';   
echo '<div class="soc mn '.$lng_cls.'">';
   if ($lang == 'en')
      echo $html->link($html->image('icons/w_home.png', array('class' => 'wi')).$html->image('icons/b_home.png', array('class' => 'bi')).$html->tag('span', __('home', true)), array('controller' => 'users', 'action' => 'home'), array('escape' => false, 'class' => 'it'));
   if ($lang == 'ja')
      echo $html->link($html->image('icons/w_home.png', array('class' => 'wi')).$html->image('icons/b_home.png', array('class' => 'bi')), array('controller' => 'users', 'action' => 'home'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep'));
   if (!empty($new_mess) && $new_mess != 0)
      echo $html->link($html->image('icons/r_messages.png', array('class' => 'wi')).$html->image('icons/b_messages.png', array('class' => 'bi')).$html->div('count', $new_mess).$html->tag('span', __('messages', true)), array('controller' => 'messages', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
   else
      echo $html->link($html->image('icons/w_messages.png', array('class' => 'wi')).$html->image('icons/b_messages.png', array('class' => 'bi')).$html->tag('span', __('messages', true)), array('controller' => 'messages', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep'));
   if (!empty($ints_count))
      echo $html->link($html->image('icons/r_usco.png', array('class' => 'wi')).$html->image('icons/b_usco.png', array('class' => 'bi')).$html->div('count', $ints_count).$html->tag('span', __('interests', true)), array('controller' => 'interests', 'action' => 'index'), array('escape' => false, 'class' => 'it'));  
   else
      echo $html->link($html->image('icons/w_interest.png', array('class' => 'wi')).$html->image('icons/b_interest.png', array('class' => 'bi')).$html->tag('span', __('interests', true)), array('controller' => 'interests', 'action' => 'index'), array('escape' => false, 'class' => 'it'));  
   echo $html->tag('span', '', array('class' => 'sep'));
   if (!empty($new_con))
      echo $html->link($html->image('icons/r_connections.png', array('class' => 'wi')).$html->image('icons/b2_connections.png', array('class' => 'bi')).$html->div('count', $new_con).$html->tag('span', __('connections', true)), array('controller' => 'connections', 'action' => 'index'), array('escape' => false, 'class' => 'it'));   
   else   
      echo $html->link($html->image('icons/w_connections.png', array('class' => 'wi')).$html->image('icons/b_connections.png', array('class' => 'bi')).$html->tag('span', __('connections', true)), array('controller' => 'connections', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep'));
   echo $html->link($html->image('icons/w_demands.png', array('class' => 'wi')).$html->image('icons/b_demands.png', array('class' => 'bi')).$html->tag('span', __('demands', true)), array('controller' => 'demands', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep'));
   echo $html->link($html->image('icons/w_pictures.png', array('class' => 'wi')).$html->image('icons/b_pictures.png', array('class' => 'bi')).$html->tag('span', __('albums', true)), array('controller' => 'albums', 'action' => 'index'), array('escape' => false, 'class' => 'it'));  
   echo $html->tag('span', '', array('class' => 'sep'));
   echo $html->link($html->image('icons/w_documents.png', array('class' => 'wi')).$html->image('icons/b_documents.png', array('class' => 'bi')).$html->tag('span', __('documents', true)), array('controller' => 'documents', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep'));
   echo $html->div('it', $html->image('icons/w_next.png', array('class' => 'wi')).$html->image('icons/b_next.png', array('class' => 'bi')), array('id' => 'socClk'));
echo '</div>';
echo '<div class="soc socPop">';
   echo $html->link($html->image('icons/w_recom.png', array('class' => 'wi')).$html->image('icons/b_recom.png', array('class' => 'bi')).$html->tag('span', __('recommendations', true)), array('controller' => 'recommendations', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep2'));   
   if (!empty($new_users))
      echo $html->link($html->image('icons/r_usco.png', array('class' => 'wi')).$html->image('icons/b_usco.png', array('class' => 'bi')).$html->div('count', $new_users).$html->tag('span', __('colleagues', true)), array('controller' => 'users', 'action' => 'colleagues'), array('escape' => false, 'class' => 'it'));
   else
      echo $html->link($html->image('icons/w_users.png', array('class' => 'wi')).$html->image('icons/b_users.png', array('class' => 'bi')).$html->tag('span', __('colleagues', true)), array('controller' => 'users', 'action' => 'colleagues'), array('escape' => false, 'class' => 'it'));
   echo $html->tag('span', '', array('class' => 'sep2'));
   echo $html->link($html->image('icons/w_feeds.png', array('class' => 'wi')).$html->image('icons/b_feeds.png', array('class' => 'bi')).$html->tag('span', __('feeds', true)), array('controller' => 'notifications', 'action' => 'index'), array('escape' => false, 'class' => 'it'));
echo '</div>';
?>

<script type="text/javascript">
var new_users = '<? if (!empty($new_users)) echo $new_users;?>';
//var ints_count = '<? if (!empty($ints_count)) echo $ints_count;?>';

$(document).ready(function(){   
   
   if(new_users != '' /*|| ints_count != ''*/)
      $('.socPop').show();
});   
</script>