<?php

//WELCOE TEXT

echo '<div class="elm" id="welcome_text_bub">';
   echo $html->tag('span', 'Dear visitor, <br/><br/>Thank you for signing up with us. All of the new users need to create a new company account or register with their existing company account.', array('class' => 'bl bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'You can register with your existing company account by clicking the ', array('class' => 'bl')).$html->tag('span', 'User account registration', array('class' => 'vi bold')).$html->tag('span', '  tab on the top. If your company doesn\'t have a company account with Ezbizi created yet, you can do this by filling up a company account registration details on the left, before registering your personal user account.', array('class' => 'bl'));
echo '</div>';

//TRADE SECTOR

echo '<div class="elm none" id="sector_text_bub">';
   echo $html->tag('span', 'Trade sector', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'Please select a business category of your company. If your company operates in multiple business areas, please select a category that represents the largest portion of your business activities.<br /><br />Select your business category in this order: ', array('class' => 'bl')).$html->tag('span', 'Business category > Industry group> Trade group', array('class' => 'vi bold'));
echo '</div>';

//REGNUM

echo '<div class="elm none" id="regnum_text_bub">';
   echo $html->tag('span', 'Business registration number', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'At Ezbizi we have a zero tolerance towards possible frauds and scams. We want Ezbizi to be as transparent as possible, by designing a set of intelligent mechanisms and warning systems to help detect and prevent malicious users from exploiting our service.', array('class' => 'bl'));
   echo '<br/><br/>';
   echo $html->tag('span', 'We strongly encourage all of the registered companies to indicate their genuine ', array('class' => 'bl bold')).$html->tag('span', 'Business Registration Number ("B.R.N")', array('class' => 'lnk infoLink', 'id' => 'regnum_det_text')).$html->tag('span', ' in their company profile.', array('class' => 'bl bold'));
   echo '<br/><br/>';
   echo $html->tag('span', 'Don’t worry, in order to protect your interests, B.R.N is only visible to our premium subscribers.', array('class' => 'bl'));
echo '</div>';

//REGNUM DETAIL

echo '<div class="elm none" id="regnum_det_text_bub">';
   echo $html->tag('span', 'Business registration number', array('class' => 'ge bold'));
   echo '<br /><br />';                                                                                                                                                                                                                                                                                                                                                                                             
   echo $html->tag('span', 'Please provide a genuine and unique B.R.N commonly used in the country of your company registration. This information will help other users to identify your company through the official business registries in your country. An example of a commonly used B.R.N is a ', array('class' => 'bl')).$html->tag('span', 'VAT', array('class' => 'vi bold')).$html->tag('span', ' identification number. Ezbizi can also accept any other unique and traceable business registration number commonly used to identify companies in the country of your company registration.', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'Providing a B.R.N will not only add an extra credibility to your company profile, but also gives others confidence to accept you as a genuine company and trustworthy party for their business proposals. It also helps Ezbizi users in their due diligence on new business partners found with the help of our service.', array('class' => 'ge'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'regnum_text'));
echo '</div>';

//ACCOUNT TYPE

echo '<div class="elm none" id="account_text_bub">';
   echo $html->tag('span', 'Company account type', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'In order for Ezbizi to provide your company with the most relevant business leads, we created three distinctive company account types, for you to best describe your business needs. Don’t worry if you choose a wrong company account type, as these can be easily changed later as many times as you want.', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', '● ', array('class' => 'vi bold')).$html->tag('span', 'Buyer Type', array('class' => 'lnk infoLink', 'id' => 'buyer_text'));
   echo '<br /><br />';
   echo $html->tag('span', '● ', array('class' => 'vi bold')).$html->tag('span', 'Seller Type', array('class' => 'lnk infoLink', 'id' => 'seller_text'));
   echo '<br /><br />';
   echo $html->tag('span', '● ', array('class' => 'vi bold')).$html->tag('span', 'Trader Type', array('class' => 'lnk infoLink', 'id' => 'trader_text'));
   echo '<br /><br />';
echo '</div>';

//BUYER

echo '<div class="elm none" id="buyer_text_bub">';
   echo $html->tag('span', 'Buyer account type', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'Buyer account type is designed for users primarily interested in procuring goods and advertising their demands (RFQ’s) on Ezbizi.', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'Buyer account type enables you to post your demands (RFQ’s), which will be categorized and targeted towards the suppliers that match your selected business category. Ezbizi will also suggest you the profiles of other companies that match your desired business criteria for enhanced networking possibilities.', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'account_text'));
echo '</div>';

//SELLER

echo '<div class="elm none" id="seller_text_bub">';
   echo $html->tag('span', 'Seller account type', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'Seller account type is designed for users interested in getting connected to buyers easily, browse the categorized demands (RFQ’s) from registered buyers as well as generate targeted sales leads for their business.', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'Be sure not to miss any opportunity to bid for new business by browsing the list of all recent demands in the category of your business interest, displayed right on your main page! Get connected to the new buyers, maintain and expand your network to stay competitive!', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'account_text'));
echo '</div>';

//TRADER

echo '<div class="elm none" id="trader_text_bub">';
   echo $html->tag('span', 'Trader account type', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'Trader account type is carefully designed for users that are interested in finding buyers for their goods as well as wish to purchase goods and advertise their demands (RFQ’s) on Ezbizi.', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'Trader account type combines the best out of buyer and seller account types and is a perfect choice for any trading company!', array('class' => 'bl'));
   echo '<br /><br />';
   echo $html->tag('span', 'back', array('class' => 'bck rad3 viBg infoLink', 'id' => 'account_text'));
echo '</div>';

//PUBLIC SECTOR

echo '<div class="elm none" id="public_text_bub">';
   echo $html->tag('span', 'Public sector', array('class' => 'ge bold'));
   echo '<br /><br />';
   echo $html->tag('span', 'If your company is associated with the government of your country or your organization is representing a government body, please check this box to indicate this in your company profile.', array('class' => 'bl'));
echo '</div>';
?>