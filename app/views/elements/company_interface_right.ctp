<?php
//---> LOGO

echo '<div class="modP logo rad5">';
   echo $html->link($pcompany['Company']['name'], array('controller' => 'companies', 'action' => 'view', $pcompany['Company']['id']), array('class' => 'comName rad3'));
   echo '<div class="logImg rad3">';
      //160 x 160
      if (empty($pcompany['Company']['avatar']))                                                    
         $foto = $this->webroot.IMAGES_URL.'noImage115.png';
      else
         $foto = $image->resize('comps/'.$pcompany['Company']['id'].'/'.$pcompany['Company']['avatar'], 200, 200, true, true);
      echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
   
   echo '</div>';
   if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id'))
      echo $html->div('edtL', $html->link(__('change logo', true), array('controller' => 'companies', 'action' => 'edit', 'logo')));
echo '</div>';

//---> COLLEAGUES 
      
echo '<div class="modP rad5">';
      
   if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id'))
      echo $html->link(__('Colleagues', true), array('controller' => 'users', 'action' => 'colleagues'), array('class' => 'headline hlB hlIn hlL')); 
   else
      echo $html->link(__('Staff', true), array('controller' => 'users', 'action' => 'colleagues', $pcompany['Company']['id']), array('class' => 'headline hlB hlIn hlL')); 
   //50 x 67
   if (!empty($colleagues)) {
      echo '<div class="colls">';
         foreach ($colleagues as $colleague) {
            echo '<div class="coll">';
               if ($colleague['User']['online']) {
                  $last_activity = strtotime($colleague['User']['last_activity']);
                  $now = time();
                  $last_activity = $now - $last_activity;
                  if ($last_activity < 1800)
                     echo $html->image('icons/online.png', array('class' => 'onf'));
                  else
                     echo $html->image('icons/offline.png', array('class' => 'onf'));
               }
               else
                  echo $html->image('icons/offline.png', array('class' => 'onf'));
                     
               echo $html->div('hov', $html->tag('span', $colleague['User']['first_name'], array('class' => 'name')));
               if (empty($colleague['User']['avatar']))
                  $foto = $this->webroot.IMAGES_URL.'noImage50.png';
               else
                  $foto = $image->resize('users/'.$colleague['User']['id'].'/'.$colleague['User']['avatar'], 68, 90, true, true);
               echo $html->div('foto', '', array('style' => 'background-image: url('.$foto.');'));
               echo $html->link('', array('controller' => 'users', 'action' => 'view', $colleague['User']['id']), array('class' => 'tLink'));
            echo '</div>';
         }          
      echo '</div>';
      if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id'))
         echo $html->div('edtL', $html->link(__('all colleagues', true), array('controller' => 'users', 'action' => 'colleagues'))); 
      else
         echo $html->div('edtL', $html->link(__('all staff', true), array('controller' => 'users', 'action' => 'colleagues', $pcompany['Company']['id'])));
   }
   else
      echo $html->div('noData', __('no registered colleagues', true));
      
echo '</div>';

//---> SEARCH CONNECTIONS
echo '<div class="modP rad5">';

    if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id')) {
             
    echo '<div class="mod">';      
             
          echo $html->div('headline hlB hlIn', __('Search connections', true));
                
          echo '<div class="s_conn">';         
          echo $form->create('User', array('action' => 'home'));
             echo $form->input('User.find', array('label' => '', 'default' => __('Type company name ...', true)));
             echo $form->submit('');
          echo $form->end();
          echo '</div>';   
             
       echo '</div>';
             
       echo '<div id="results" class="mod">';
             
       echo '</div>';
       echo $html->div('sep', '');
    }
          
    //---> PROPOSE CONNECTIONS
            
    if (!empty($recommended)) {
       echo '<div class="mod">';       
          echo $html->div('headline hlB hlIn', __('Recommended connections', true));
          foreach ($recommended as $connection) {
             echo '<div class="compI">';
                if (empty($connection['Company']['avatar']))
                   echo $html->div('thumb', $image->resize('noImage50.png', 50, 50));
                else
                   echo $html->div('thumb', $image->resize('comps/'.$connection['Company']['id'].'/'.$connection['Company']['avatar'], 50, 50));
                echo '<div class="info">';
                   echo $html->div('name', $connection['Company']['name']);
                   if (empty($connection['Company']['public_sector']))
                      echo $html->div('sector', $connection['Sector']['name_'.$lang]);
                   else
                      echo $html->div('sector', __('Public sector', true));
                   $loc = $connection['Country']['name_'.$lang];
                   if (!empty($connection['Region']['name_'.$lang]))
                      $loc .= ' - '.$connection['Region']['name_'.$lang];
                   echo $html->div('loc', $loc);
                echo '</div>';
                echo $html->link('', array('controller' => 'companies', 'action' => 'view', $connection['Company']['id']), array('class' => 'tLink'));        
             echo '</div>';
          }
       echo '</div>';
   }
   else if (empty($recommended) && $account['Account']['id'] == 1 && in_array($session->read('Auth.User.group_id'), array(1, 2))) {
      echo '<div class="mod recom">';       
          echo $html->div('headline hlB hlIn', __('Recommended connections', true));
          
          echo '<div class="r_bub rad3 none">';
            echo $html->image('bg/vi_bubArr.png', array('class' => 'arr nrm'));
            echo $html->image('bg/vi_bubArrH.png', array('class' => 'arr hov none'));
            echo __('In order to get companies recommended to you for connection, please upgrade to premium account.', true);
            echo '<br /><br />';
            echo __('Basic subscribers are able to search for companies and show their interest to get connected.', true);
            echo $html->link('', array('controller' => 'companies', 'action' => 'edit', 'account'), array('class' => 'tLink'));
          echo '</div>';
      echo '</div>';       
   }

echo '</div>';
?>
<script type="text/javascript">
$(document).ready(function(){   

   //company find input
   $('#UserFind').click(function() {
         $(this).val('');
   });
   
   $('#UserFind').keypress(function(event) {
      $.ajax({url: '<?php echo $html->url(array('controller' => 'companies', 'action' => 'search')); ?>/'+$(this).val()+String.fromCharCode(event.which)+'/companies_interface',
           success: function(data){$('#results').html(data);}});
   });
});

</script>