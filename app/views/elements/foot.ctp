<?php
echo '<div class="cen">';
   //echo $html->image('bg/roundBot.png', array('id' => 'rndB'));
   echo '<div class="mn">';
      if (!empty($foot_menu)) {
         foreach ($foot_menu as $ind => $foot) {      
            echo $html->link($foot, array('controller' => 'sites', 'action' => 'view', $ind));
            echo $html->tag('span', ' | ');
         }
      }
      echo $html->link(__('Feedback', true), array('controller' => 'users', 'action' => 'contact'));   
      //echo $html->link(__('Site map', true), array('controller' => '#', 'action' => '#'));
   echo '</div>';
   echo '<div class="cp">';
      echo $html->tag('span', __('Copyright © ', true).' ').$html->tag('span', 'ez', array('class' => 'ge')).$html->tag('span', 'biz', array('class' => 'bl')).$html->tag('span', 'i', array('class' => 'ge')).$html->tag('span', '.com', array('class' => 'gr'));
   echo '</div>';
   echo '<div class="cr">';
      echo $html->tag('span', __('programmed', true).' &amp; '.__('co-designed', true).': ', array('class' => 'bl')).$html->link(__('AITechnology', true), 'http://www.aitechnology.sk', array('title' => 'Tvorba web stránok | eshopov | redakčné systémy - CMS | SEO'));   
   echo '</div>';
echo '</div>';
?>