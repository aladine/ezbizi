<?php

function catmenu($cats, & $catlevel, & $html, $lang, $sectors = null) {
   $catlevel++;
   foreach ($cats as $cat) {
      if ($catlevel > 1) {
         echo '<div class="cats lev'.$catlevel.' none">'; 
         echo $html->div('cat', $html->image('icons/plusR.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      else {
         echo '<div class="cats lev'.$catlevel.'">';
         echo $html->div('cat', $html->image('icons/plus.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      if (!empty($cat['children']))
         catmenu($cat['children'], $catlevel, $html, $lang, $cat['Sector']);
      else if (!empty($cat['Sector'])) {
         echo '<div class="cats lev'.($catlevel+1).' none">';  
         foreach ($cat['Sector'] as $sector) {
            echo $html->div('cat sec', $html->image('icons/sel.png').$html->tag('span', $sector['id'], array('class' => 'id none')).$html->tag('span', $sector['name_'.$lang], array('class' => 'name')));
         } 
         echo '</div>';
      }
      echo '</div>';
   }
   $catlevel--;
}
      
   echo $form->create('Demand', array('action' => 'search'));

         echo '<div class="form uForm srchForm rad3">';    
            echo $html->div('title', __('Search demand', true).':');

            echo $form->input('Demand.sector_id', array('type' => 'hidden'));
            
            echo '<div class="line">';
               echo '<div class="input text">';
                  echo '<label>'.__('Business category', true).'</label>';
                  echo $html->tag('span', '', array('id' => 'catSet', 'class' => 'none catSet'));
                  echo $html->tag('span', __('change', true), array('id' => 'catChg', 'class' => 'secCh blBg rad3'));
                  echo $html->tag('span', __('select category', true), array('id' => 'catSel', 'class' => 'secE grBg rad3'));
               echo '</div>';
            echo '</div>';
           
            if (!empty($categories)) {
               echo '<div class="catSel none">';            
                  $catlevel = 0;          
                  catmenu($categories, $catlevel, $html, $lang);
               echo '</div>';         
            }
            else
               echo $html->div('noData', 'nodata');

            echo '<div class="line">';
               echo $form->input('Demand.commodity', array('label' => __('Commodity', true)));
            echo '</div>';
            echo '<div class="line">';
               echo $form->input('Demand.country_id', array('label' => __('Location', true), 'type' => 'select', 'empty' => ' - '.__('select location', true).' - ', 'div' => 'input select loc'));
            echo '</div>';
            echo '<div class="double line last">';
               echo $form->input('Demand.quantity_min', array('label' => __('Min. Quantity', true), 'div' => 'input text quan'));
            echo '</div>';
            echo '<div class="double line">';
               echo $form->input('Demand.quantity_max', array('label' => __('Max. Quantity', true), 'div' => 'input text quan'));
               echo $form->input('Demand.unit_id', array('label' => '', 'type' => 'select' , 'empty' => ' - ', 'div' => 'input text unit'));      
            echo '</div>';
            echo '<div class="double line last">';
               echo $form->input('Demand.price_min', array('label' => __('Min. Price', true), 'div' => 'input text quan'));
            echo '</div>';
            echo '<div class="double line last">';
               echo $form->input('Demand.price_max', array('label' => __('Max. Price', true), 'div' => 'input text quan'));
               echo $form->input('Demand.currency_id', array('label' => '', 'type' => 'select', 'empty' => ' - ', 'div' => 'input text unit'));      
            echo '</div>';
            echo $form->submit(__('search demands', true));
            
         echo '</div>'; 
     
   echo $form->end();
?>
<script type="text/javascript">
$(document).ready(function(){   
   
   set_sector()
        
   //category select
   $('.catSel .cat').click(function() {
      var $parCat = $(this).parent();

      //if exists child
      if($parCat.find('> .cats').exists()){
         $parCat.find('> .cats').toggle();   
      }
      else {
         if ($(this).hasClass('sec')) {
            $('#DemandSectorId').val($(this).find('.id').html());
            $('.catSel').hide();
            $('#catSet').show().html($(this).find('.name').html());
            $('#catChg').show();
         }   
      }   
   });
   
   $('#catChg').click(function() {
      $('.catSel').show();
      $('#catSet, .catSel .lev2, .catSel .lev3, #catChg').hide();   
   });
   
   $('#catSel').click(function() {
      $(this).hide();
      $('.catSel').show();
   });
   
});

function set_sector() {
   if($('#DemandSectorId').val() != "") {
      $('.catSel .sec').each(function() {
         if ($(this).find('.id').html() == $('#DemandSectorId').val())
            $('#catSet').show().html($(this).find('.name').html().replace("&amp;", "&"));   
      });
      $('#catSel').hide();
      $('#catChg').show();         
   }
}
</script>