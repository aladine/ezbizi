<?php
echo '<div class="pby">';
   echo $html->div('prc', $package['Package']['price'].' $ ');
   echo $html->div('val', $package['Package']['validity'].' months ');
   //echo $html->link(__('buy now', true), array('controller' => 'orders', 'action' => 'add', $package['Package']['id']), array('class' => 'buy geBg rad3'));
   echo $html->link($html->image('https://www.sandbox.paypal.com/en_GB/i/btn/btn_buynow_LG.gif', array('alt' => __('PayPal - Secure and easy way to pay online!', true))), array('controller' => 'orders', 'action' => 'add', $package['Package']['id']), array('escape' => false, 'class' => 'mt20', 'title' => __('PayPal — The safer, easier way to pay online.', true)));
echo '</div>';
?>