<?php
echo '<div class="popUp" id="pUpRec_win">';
   echo '<div class="main">';
      echo '<div class="con">';
         echo $html->div('title', __('Send Recommendation', true).':');
         
           echo $form->create('Recommendation', array('action' => 'add'));
            echo $form->input('Recommendation.company_id', array('type' => 'hidden', 'value' => $vcompany['Company']['id']));
            
            echo '<div class="form">';
               echo $form->input('Recommendation.text', array('label' => __('Your feedback', true).':', 'type' => 'textarea'));       
               echo $form->submit(__('submit', true));         
            echo '</div>';
      
            echo $form->end();
               
         echo $html->div('bot', $html->image('icons/close.png', array('title' => __('close window', true), 'class' => 'popUpCls')));
      echo '</div>';
   echo '</div>';
echo '</div>';
?>