<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Overview of archived users', true));
      
      echo '<div class="form">';
         if (!empty($peoples)) {
            foreach ($peoples as $people) {
               echo '<div class="list rad3">';
               
                  echo '<div class="body nofloat">';
                     echo '<div class="top">';
                        echo $html->div('name', $people['People']['name']);
                     echo '</div>';
                     echo '<div class="col">';
                        echo $html->div('it', $html->div('at', __('E-mail', true).':').$html->div('vl', $people['People']['email']));
                        echo $html->div('it', $html->div('at', __('Job title', true).':').$html->div('vl', $people['People']['job_title']));
                     echo '</div>';
                     echo '<div class="col">';
                        if (!empty($people['People']['phone']))
                           echo $html->div('it', $html->div('at', __('Phone', true).':').$html->div('vl', $people['People']['phone']));
                        if (!empty($people['People']['fax']))
                           echo $html->div('it', $html->div('at', __('Fax', true).':').$html->div('vl', $people['People']['fax']));
                     echo '</div>';
                     echo '<div class="col btns">';
                        echo $html->div('it', $html->div('bt moreBt', __('more info', true)));
                        if (!empty($people['People']['company_id']))
                           echo $html->div('it', $html->link(__('show company', true), array('controller' => 'companies', 'action' => 'view', $people['People']['company_id']), array('class' => 'bt')));
                        if (!empty($people['People']['firm_id']))
                           echo $html->div('it', $html->link(__('show colleagues', true), array('controller' => 'people', 'action' => 'index', $people['People']['firm_id']), array('class' => 'bt')));
                     echo '</div>';
                  echo '</div>';
                  echo $html->link(__('remove', true), array('controller' => 'people', 'action' => 'delete', $people['People']['id']), array('class' => 'del'), __('Are you sure?', true));
                  
                  echo '<div class="more rad3 none">';
                     
                     if (!empty($people['People']['birthday']))
                        echo $html->div('it', $html->tag('span', __('Birthday', true).': ', array('class' => 'at')).$html->tag('span', date('d.m.Y', strtotime($people['People']['birthday'])), array('class' => 'vl')));
                     if (!empty($people['People']['web']))
                        echo $html->div('it', $html->tag('span', __('Website', true).': ', array('class' => 'at')).$html->tag('span', $html->link($people['People']['web'], $people['People']['web']), array('class' => 'vl')));
                     if (!empty($people['People']['skype']))
                        echo $html->div('it', $html->tag('span', __('Skype', true).': ', array('class' => 'at')).$html->tag('span', $people['People']['skype'], array('class' => 'vl')));
                     if (!empty($people['People']['facebook']))
                        echo $html->div('it', $html->tag('span', __('Facebook', true).': ', array('class' => 'at')).$html->tag('span', $people['People']['facebook'], array('class' => 'vl')));
                     if (!empty($people['People']['linkedin']))
                        echo $html->div('it', $html->tag('span', __('LinkedIn', true).': ', array('class' => 'at')).$html->tag('span', $people['People']['linkedin'], array('class' => 'vl')));
                     if (!empty($people['People']['twitter']))
                        echo $html->div('it', $html->tag('span', __('Twitter', true).': ', array('class' => 'at')).$html->tag('span', $people['People']['twitter'], array('class' => 'vl')));                     
                     
                     if(!empty($people['People']['summary'])) {
                        echo '<div class="it">';
                           echo $html->div('lab',__('Personal summary', true).':');
                           echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($people['People']['summary'])));
                        echo '</div>';
                     }
                     
                     if(!empty($people['People']['specialization'])) {
                        echo '<div class="it">';
                           echo $html->div('lab',__('Professional specialization', true).':');
                           echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($people['People']['specialization'])));
                        echo '</div>';
                     }
                     
                     if(!empty($people['People']['interest'])) {
                        echo '<div class="it">';
                           echo $html->div('lab',__('Personal interests (Hobbies)', true).':');
                           echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($people['People']['interest'])));
                        echo '</div>';
                     }
                     
                     if(!empty($people['People']['experience'])) {
                        echo '<div class="it">';
                           echo $html->div('lab',__('Work experience', true).':');
                           echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($people['People']['experience'])));
                        echo '</div>';
                     }
                     
                     if(!empty($people['People']['education'])) {
                        echo '<div class="it">';
                           echo $html->div('lab',__('Education', true).':');
                           echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($people['People']['education'])));
                        echo '</div>';
                     }
                     
                     if(!empty($people['People']['awards'])) {
                        echo '<div class="it">';
                           echo $html->div('lab',__('Certificates, diplomas or other awards', true).':');
                           echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($people['People']['awards'])));
                        echo '</div>';
                     }
               
                  echo '</div>';
                            
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('Users list is empty', true)); 
      echo '</div>';
      
      if (!empty($peoples)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';

echo '</div></div>';
?>
<script type="text/javascript">
$(document).ready(function(){   
   
   $('.moreBt').bind('click', function(){
      $(this).parent().parent().parent().parent().find('.more').toggle();
   });
   
});
</script>