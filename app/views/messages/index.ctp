<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      echo $html->div('headline hlB', __('My messages', true));
      
      echo '<div class="modP rad5">';
      echo $html->div('headline hlG hlIn', __('Latest conversations', true));
      echo '<div class="list conv">';
      
         if (!empty($messages)) {
            foreach ($messages as $message) {
               echo '<div class="itm itmM">';
                  echo '<div class="avat">';
                     if (empty($message['Sender']['avatar']))   
                        echo $html->image('noImage50.png');
                     else
                        echo $image->resize('users/'.$message['Sender']['id'].'/'.$message['Sender']['avatar'], 50, 50);
                  echo '</div>';
                  $new = '';
                  if ($message['Message']['read'] == 0 && $message['Sender']['id'] != $message['Recipient']['id'])
                     $new = $html->tag('span', __('new', true), array('class' => 'new'));
                  
                  echo $html->div('top', $html->tag('span', $new.$html->link($message['Sender']['title'].' '.$message['Sender']['first_name'].' '.$message['Sender']['middle_name'].' '.$message['Sender']['last_name'], array('controller' => 'messages', 'action' => 'view', $message['Sender']['id']), array('class' => 'name')).$html->tag('span', rel_time($message['Message']['created']), array('class' => 'date')), array('class' => 'lin')));
                  
                  if ($message['Sender']['id'] == $message['Recipient']['id'])
                     $sm = $html->image('icons/mes_out.png', array('class' => 'iL'));
                  else
                     $sm = $html->image('icons/mes_in.png', array('class' => 'iL'));
                  
                  $mes_text = linked_text($message['Message']['text']);
                  
                  if(strlen($mes_text) <= 200)
                     echo $html->div('mess', $sm.$html->tag('span', $mes_text));
                  else {
                     $lst = strrpos(substr(strip_tags($mes_text), 0, 200), ' ');
                     echo $html->div('mess', $sm.$html->tag('span', substr(strip_tags($mes_text), 0, $lst).' ...'));
                  } 
                  if (empty($message['blocked']))
                     echo $html->link(__('block user', true), array('controller' => 'bans', 'action' => 'add', $message['Sender']['id']), array('class' => 'block orBg', 'escape' => false), __('Are you sure that you want to block all messages from this user?',true));  
                  else
                     echo $html->link(__('unblock user', true), array('controller' => 'bans', 'action' => 'delete', $message['Sender']['id']), array('class' => 'block geBg', 'escape' => false));  
                  echo $html->link('x | '.__('remove', true), array('controller' => 'messages', 'action' => 'delete', $message['Sender']['id']), array('class' => 'del del3', 'escape' => false), __('Are you sure?',true));    
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('Currently you have no messages', true));
         
      echo '</div>';
      echo '</div>';
      
   echo '</div>';
   echo '<div class="mRight">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>