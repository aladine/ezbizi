<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      echo $html->div('headline hlB', __('Write a new message', true));

      echo $form->create('Message', array('action' => 'add'));

      echo '<div class="form uForm mesForm rad5">';
         //echo $form->input('Message.recipient_type', array('type' => 'hidden'));
         $send_to = '';
         if (empty($user))
            echo $form->input('Message.to', array('type' => 'hidden'));
         else {
            echo $form->input('Message.to', array('type' => 'hidden', 'value' => $user['User']['id']));
            $send_to = $user['User']['title'].' '.$user['User']['first_name'].' '.$user['User']['middle_name'].' '.$user['User']['last_name']; 
         }
            
         echo '<div class="line">';
            echo '<div class="input">';
               echo '<label>'.__('Send to', true).':</label>';
               echo $html->tag('span', $send_to, array('class' => 'mRec none', 'id' => 'mRec'));
               echo $html->tag('span', __('My colleague', true), array('class' => 'sTo sTol', 'id' => 'pUpCol')).$html->tag('span', __('or', true).':', array('class' => 'ml20 gr')).$html->tag('span', __('Employee of your connection', true), array('class' => 'sTo', 'id' => 'pUpCon'));
            echo '</div>';
         echo '</div>';
         echo '<div class="line last">'; 
            echo $form->input('Message.text', array('label' => __('Message', true).':', 'type' => 'textarea', 'div' => 'input textarea'));
         echo '</div>';
         
         echo $form->submit(__('send message', true));
         
      echo '</div>';
      
      echo $form->end();
      
      echo $this->element('popup_colleagues');
      echo $this->element('popup_connections');
      
   echo '</div>';
   echo '<div class="mRight">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>
<script type="text/javascript">
var popUpH, popUpW, popUpLP, popUpTP; 

$(document).ready(function(){   
   
   //mRec show
   if($('#mRec').html() != '')
      $('#mRec').show();      
         
   //popUp Open   
   $('.sTo').click(function(){
      $win = '#'+$(this).attr('id')+'_win';
      popUpDim($win); 

      $('.popUp:visible').hide();      
      resetRec();      
      $($win).css({top: popUpTP, left: popUpLP}).fadeIn(300);
   });
   
   //popUp close 
   $('.popUpCls').click(function(){
      $('.popUp').fadeOut(300);   
   });
   
   //select recipient 
   $('.popUp .name').click(function(){
      $parr = $(this).parent();
      if($parr.hasClass('cmp')){
         $parr.parent().parent().find('.cmp').hide();   
         $parr.parent().find('.staf').show();
         popUpDim('#pUpCon_win');
         $('#pUpCon_win').css({top: popUpTP, left: popUpLP});
         $('#recBack').show();
      }
      else {      
         $('#mRec').html($(this).html()).show();
         //$('#MessageRecipientType').val($parr.parent().parent().find('.recType').html());
         $('#MessageTo').val($parr.find('.id').html());
         $('.popUp').fadeOut(300);
      }   
   });
   
   //back click
   $('#recBack').click(function(){
      resetRec();
   });
   
});

function resetRec() {
   $('.popUp').find('.staf').hide();
   $('.popUp').find('.cmp').show();
   $('#recBack').hide(); 
}

function popUpDim($elm) {
   popUpH = $($elm).height();
   popUpW = $($elm).width();
   popUpLP = Math.abs(wW - popUpW)/2;
   popUpTP = Math.abs(wH - popUpH)/2;  
}
</script>