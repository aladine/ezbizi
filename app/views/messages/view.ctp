<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      echo $html->div('headline hlB', __('Conversation with', true).'&nbsp;'.$sender['User']['title'].' '.$sender['User']['first_name'].' '.$sender['User']['middle_name'].' '.$sender['User']['last_name']);
      
      echo $form->create('Message', array('url' => array('controller' => 'messages', 'action' => 'view', $sender['User']['id'])));

      echo '<div class="form uForm mesForm replyForm">';
         echo '<div class="line last">'; 
            echo $form->input('Message.text', array('label' => __('Message', true).':', 'type' => 'textarea', 'div' => 'input textarea'));
         echo '</div>';
         
         echo $form->submit(__('reply to', true).' '.$sender['User']['first_name']);
         
      echo '</div>';
      
      echo $form->end();
      
      echo '<div class="list conv">';
      if (!empty($messages)) {
         foreach ($messages as $message) {
            $user_id = $session->read('Auth.User.id');
            if ($message['Message']['sender_id'] == $user_id) {
               echo '<div class="itm itmM itmM1 ovrf">';
                  echo '<div class="avat">';
                     $avatar = $session->read('Auth.User.avatar');                                                         
                     if (empty($avatar))   
                        echo $html->link($html->image('noImage50.png'), array('controller' => 'users', 'action' => 'view', $user_id), array('escape' => false));
                     else
                        echo $html->link($image->resize('users/'.$session->read('Auth.User.id').'/'.$session->read('Auth.User.avatar'), 50, 50), array('controller' => 'users', 'action' => 'view', $user_id), array('escape' => false));
                  echo '</div>';
               echo $html->div('top', $html->tag('span', $html->link($session->read('Auth.User.first_name').' '.$session->read('Auth.User.last_name'), array('controller' => 'users', 'action' => 'view', $user_id), array('class' => 'name')).$html->image('icons/mes_out.png', array('class' => 'iR10')).$html->tag('span', rel_time($message['Message']['created']), array('class' => 'date')), array('class' => 'lin')));
               echo $html->image('bg/mesBlA.png', array('class' => 'blA'));
            }
            else {
               echo '<div class="itm itmM itmM2 ovrf">';
                  echo '<div class="avat">';
                     if (empty($sender['User']['avatar']))   
                        echo $html->link($html->image('noImage50.png'), array('controller' => 'users', 'action' => 'view', $sender['User']['id']), array('escape' => false));
                     else
                        echo $html->link($image->resize('users/'.$sender['User']['id'].'/'.$sender['User']['avatar'], 50, 50), array('controller' => 'users', 'action' => 'view', $sender['User']['id']), array('escape' => false));
                  echo '</div>';
               echo $html->div('top', $html->tag('span', $html->link($sender['User']['first_name'].' '.$sender['User']['last_name'], array('controller' => 'users', 'action' => 'view', $sender['User']['id']), array('class' => 'name')).$html->image('icons/mes_in.png', array('class' => 'iR10')).$html->tag('span', rel_time($message['Message']['created']), array('class' => 'date')), array('class' => 'lin')));
               echo $html->image('bg/mesGeA.png', array('class' => 'geA'));
            }   
            echo $html->div('mess', str_replace(PHP_EOL, '<br />', linked_text($message['Message']['text'])));
         echo '</div>';
         }
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';         
      } 
      echo '</div>';
      
   echo '</div>';
   echo '<div class="mRight">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>