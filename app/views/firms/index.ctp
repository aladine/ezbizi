<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Overview of archived companies', true));
      
      echo '<div class="form">';
         if (!empty($firms)) {
            foreach ($firms as $firm) {
               echo '<div class="list">';
                  //debug($firm);
                  
                  echo '<div class="body nofloat">';
                     echo '<div class="top">';
                        echo $html->link($firm['Firm']['name'].' '.$firm['Firm']['address'].' '.$firm['Firm']['zip'].' '.$firm['Firm']['city'], array('controller' => 'people', 'action' => 'index', $firm['Firm']['id']), array('class' => 'link')); 
                     echo '</div>';
                  
                     echo '<div class="col">';
                        echo $html->div('it', $html->div('at', __('Country', true).':').$html->div('vl', $firm['Country']['name_'.$lang]));
                        if (empty($firm['Firm']['public_sector']))
                           echo $html->div('it', $html->div('at', __('Select', true).':').$html->div('vl', $firm['Sector']['name_'.$lang]));
                        else
                           echo $html->div('it', $html->div('at', __('Select', true).':').$html->div('vl', __('Public sector', true)));
                     echo '</div>';
                  
                     echo '<div class="col">';
                        if (!empty($firm['Firm']['phone']))
                           echo $html->div('it', $html->div('at', __('Phone', true).':').$html->div('vl', $firm['Firm']['phone']));
                        if (!empty($firm['Firm']['fax']))
                           echo $html->div('it', $html->div('at', __('Fax', true).':').$html->div('vl', $firm['Firm']['fax']));
                        if (!empty($firm['Firm']['web']))
                           echo $html->div('it', $html->div('at', __('Website', true).':').$html->link($firm['Firm']['web'], $firm['Firm']['web'], array('class' => 'vla')));
                     echo '</div>';
                  
                  echo '</div>';
                  echo $html->link('remove', array('controller' => 'firms', 'action' => 'delete', $firm['Firm']['id']), array('class' => 'del'), __('Are you sure?', true));
                  
               echo '</div>';
            }
            
            echo '<div class="pgn">';
               echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
            echo '</div>';
         }
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>