<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Add new phrase', true));
      
      echo '<div class="form">';
         
      echo $form->create('Translation', array('action' => 'add'));
         echo $form->input('Translation.text_en', array('label' => __('Phrase En', true).':', 'div' => 'input text phrs'));
         echo $form->input('Translation.text_ja', array('label' => __('Phrase Ja', true).':', 'div' => 'input text phrs'));
      
         echo '<div class="bblk">';
            echo $form->submit(__('add', true));
         echo '</div>';
         
      echo $form->end();

      echo '</div>';
            
   echo '</div>';

echo '</div></div>';
?>