<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Translation', true));
      
      echo '<div class="form">';
         
         echo $html->link($html->image('icons/ad_add.png', array('class' => 'iL')).$html->tag('span',__('Add new phrase', true)), array('controller' => 'translations', 'action' => 'add'), array('class' => 'rad3 listA geB', 'escape' => false));
         
         if (!empty($phrases)) {
            foreach ($phrases as $phrase) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';
                  
                  echo $form->create('Translation', array('action' => 'index'));
                     echo $form->input('Translation.id', array('type' => 'hidden', 'value' => $phrase['Translation']['id']));
                     echo $form->input('Translation.edit', array('label' => 'change en', 'div' => 'input checkbox changeEn', 'checked' => $phrase['Translation']['edit']));
                     if ($phrase['Translation']['edit'])
                        echo $form->input('Translation.repair', array('label' => 'fixed', 'div' => 'input checkbox changeEn', 'type' => 'checkbox'));
                     echo '<div class="input text phrs">';
                        echo $html->tag('label', __('Phrase En', true).':');
                        echo $html->tag('span', $phrase['Translation']['text_en'], array('class' => 'phrTxt'));
                     echo '</div>';
                     echo $form->input('Translation.text_ja', array('label' => __('Phrase Ja', true).':', 'div' => 'input text phrs', 'value' => $phrase['Translation']['text_ja']));
      
                     echo '<div class="bblk phrbblk">';
                        echo $form->submit(__('save', true));
                     echo '</div>';
         
                  echo $form->end();
                  
                  echo '</div>'; 
                  
                  //echo $html->link(__('remove', true), array('controller' => 'translations', 'action' => 'delete', $phrase['Translation']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->div('noData', __('List is empty', true));   

      echo '</div>';
      
      if (!empty($phrases)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      } 
      
   echo '</div>';

echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
    $('.list').each(function() {
      if ($(this).find('.changeEn input:checkbox:checked').val() == '1')
         $(this).css({background: '#ffc4c4'});
    });          
});
</script>