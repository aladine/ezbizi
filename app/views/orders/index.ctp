<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Overview of orders', true));
      
      echo '<div class="form">';
         if (!empty($orders)) {
            foreach ($orders as $order) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';
                     echo '<div class="top">';               
                        echo $html->tag('span', date('d.m.Y', strtotime($order['Order']['created'])), array('class' => 'date rad3'));
                        echo $html->tag('span', $order['Package']['name_'.$lang], array('class' => 'name')); 
                     echo '</div>';
                     echo '<div class="col">';
                        echo $html->div('it', $html->div('at', __('Company', true).':').$html->div('vl', $html->link($order['Company']['name'], array('controller' => 'companies', 'action' => 'view', $order['Company']['id']), array('class' => 'link'))));     
                     echo '</div>';
                     echo '<div class="col">';
                        echo $html->div('it', $html->div('at', __('Price', true).':').$html->div('vl', $order['Package']['price'].' $ '));
                     echo '</div>';
                     echo '<div class="col btns">';
                        if ($order['Order']['paid'])
                           echo $html->tag('span', __('paid', true), array('class' => 'ind rad3 geBg'));
                        else
                           echo $html->div('it', $html->link(__('mark as paid', true), array('controller' => 'orders', 'action' => 'paid', $order['Order']['key']), array('class' => 'bt'), __('Are you sure?', true)));
                     echo '</div>';
                  echo '</div>';
                  echo $html->link(__('remove', true), array('controller' => 'orders', 'action' => 'delete', $order['Order']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->div('noData', __('List is empty', true));   

      echo '</div>';
      
      if (!empty($orders)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      } 
      
   echo '</div>';

echo '</div></div>';
?>