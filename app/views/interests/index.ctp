<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface"><div class="cen">';
   echo '<div class="iCen">';
      echo $html->div('headline hlB', __('Interests', true));
      if (!empty($company['Company']['interest_count']) && empty($interests)) {
         echo '<div class="top intNotice">';
            $string = '';
            if ($company['Company']['interest_count'] == 1)
               $string = __('interest', true);
            else
               $string = __('interests', true);
            
            echo '<div class="bubi rad5">';                      
               echo $html->tag('span', __('Dear basic subscriber', true).',', array('class' => 'vi bold'));
               echo '<br />';
               echo $html->div('conReq redBg', $html->tag('span', __('You have', true)).'&nbsp;'.$html->tag('span', $company['Company']['interest_count']).'&nbsp;'.$html->tag('span', $string));
               echo '<br />';
               echo $html->tag('span', __(' Details on who showed interest in getting connected with your company is only available for premium account subscribers. If you would like to find out who showed interest in your company profile, please upgrade to a premium account.', true), array('class' => 'vi bold'));
               echo $html->image('bg/gr_bubbleA.png', array('class' => 'bubA'));
               echo $html->image('bg/bubiMascotR.png', array('class' => 'bubM'));
               if (in_array($session->read('Auth.User.group_id'), array(1, 2)))
                  echo $html->link('', array('controller' => 'companies', 'action' => 'edit', 'account'), array('class' => 'tLink'));
            echo '</div>';
                 
         echo '</div>';
      }
      if (empty($company['Company']['interest_count'])) {
         echo '<div class="top">';
            echo $html->div('conReq lblBg', $html->tag('span', __('Currently you have no indication of interest from other users', true)));
         echo '</div>';
      }
         
      echo '<div class="list">';
         
         if (!empty($interests)) {
         
            foreach ($interests as $interest) {
               echo '<div class="itm rad5">';
                  echo '<div class="logo">';
                     if (empty($interest['Company']['avatar']))
                        echo $html->image('noImage50.png');
                     else
                        echo $image->resize('comps/'.$interest['Company']['id'].'/'.$interest['Company']['avatar'], 50, 50);
                  echo '</div>';
                  echo '<div class="body">';
                     echo $html->link($interest['Company']['name'], array('controller' => 'companies', 'action' => 'view', $interest['Company']['id']), array('class' => 'name'));
                     if (empty($interest['Company']['public_sector']))
                        echo $html->div('sec', $interest['Sector']['name_'.$lang]);
                     else
                        echo $html->div('sec', __('Public sector', true));
                     $loc = $interest['Country']['name_'.$lang];
                     if (!empty($interest['Region']['name_'.$lang]))
                        $loc .= ' - '.$interest['Region']['name_'.$lang];
                     echo $html->div('loc', '('.$loc.')');
                  echo '</div>';
                  echo '<div class="btns">';
                  
                     echo '<div class="sends">';
                        echo $html->div('send', $html->link(__('remove interest', true), array('controller' => 'interests', 'action' => 'delete', $interest['Company']['id']), array('class' => 'it redBg', 'escape' => false)));
                     echo '</div>';
  
                     echo '<div class="rating">';
                        echo $html->image('icons/bigStar.png');
                        echo $html->div('val', number_format($interest['Company']['rating'], 2));
                     echo '</div>';
                  echo '</div>';            
               echo '</div>';
            }
         }
         
      echo '</div>';
      
      if (!empty($interests)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   echo '<div class="iRight">';
      echo $this->element('company_interface_right');
   echo '</div>';
   
echo '</div></div>';
?>
