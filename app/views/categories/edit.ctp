<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Edit category', true).' - '.$this->data['Category']['name_'.$lang]);
      
      echo '<div class="form">';
      
      echo $form->create('Category', array('action' => 'edit'));
         echo $form->input('Category.id', array('type' => 'hidden'));
         echo $form->input('Category.parent_id', array('label' => __('Parent category', true).':', 'empty' => ' - '.__('no parent category', true).' - ', 'options' => $categories));
         echo $form->input('Category.name_en', array('label' => __('Name En', true).':'));
         echo $form->input('Category.name_ja', array('label' => __('Name Ja', true).':'));
         //echo $form->input('Category.name_zh', array('label' => __('Name Zh', true).':'));
         echo $html->div('title', __('Business Sectors', true).':');
         if (!empty($sectors)) {
            foreach ($sectors as $ind => $sector) {
               $sectors[$ind] .= $this->Html->link('X', array('controller' => 'sectors', 'action' => 'delete', $ind));            
            }
         }
         echo $form->input('Sector', array('type' => 'select', 'label' => '', 'multiple' => 'checkbox', 'div' => 'mult rad3', 'options' => $sectors, 'escape' => false));
      
         echo '<div class="bblk">';
            echo $form->submit(__('edit', true));
         echo '</div>';
      
      echo $form->end();

      echo '</div>';
            
   echo '</div>';

echo '</div></div>';
?>