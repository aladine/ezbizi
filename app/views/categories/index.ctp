<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Business categories', true));
      
      $parent_id = null;
      if (!empty($parent))
         $parent_id = $parent['Category']['id'];
      
      if (!empty($path)) {
         echo '<div class="path">';
            echo $html->link(__('Root', true), array('controller' => 'categories', 'action' => 'index'), array('class' => 'it'));
            echo $html->tag('span', ' > ', array('class' => 'sep'));
            foreach ($path as $ind => $value) {
               echo $html->link($value, array('controller' => 'categories', 'action' => 'index', $ind), array('class' => 'it'));
               echo $html->tag('span', ' > ', array('class' => 'sep'));
            }
         echo '</div>'; 
      } 

      echo '<div class="form">';
         
         if (!empty($parent))
            echo $html->link($html->image('icons/ad_back.png', array('class' => 'iL')).$html->tag('span', __('A step back', true)), array('controller' => 'categories', 'action' => 'index', $parent['Category']['parent_id']), array('class' => 'rad3 listA grB', 'escape' => false));
         
         if (empty($sectors))
            echo $html->link($html->image('icons/ad_add.png', array('class' => 'iL')).$html->tag('span',__('Add new category', true)), array('controller' => 'categories', 'action' => 'add', $parent_id), array('class' => 'rad3 listA geB', 'escape' => false));
         
         if (!empty($categories)) {
            foreach ($categories as $category) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';                    
                     echo '<div class="top">';
                        echo $html->link($category['Category']['name_'.$lang], array('controller' => 'categories', 'action' => 'index', $category['Category']['id']), array('class' => 'link'));                 
                     echo '</div>';                   
                  echo '</div>';
                  
                  echo $html->link(__('edit', true), array('controller' => 'categories', 'action' => 'edit', $category['Category']['id']), array('class' => 'dsp'));
                  echo $html->link(__('remove', true), array('controller' => 'categories', 'action' => 'delete', $category['Category']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->link($html->image('icons/ad_add.png', array('class' => 'iL')).$html->tag('span',__('Add new sector', true)), array('controller' => 'sectors', 'action' => 'add', $parent_id), array('class' => 'rad3 listA geB', 'escape' => false));
            
         if (!empty($sectors)) {
            foreach ($sectors as $ind => $value) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';                    
                     echo '<div class="top">';
                        echo $value;                 
                     echo '</div>';                   
                  echo '</div>';
                  
                  echo $html->link(__('edit', true), array('controller' => 'sectors', 'action' => 'edit', $ind), array('class' => 'dsp'));
                  echo $html->link(__('remove', true), array('controller' => 'categories', 'action' => 'remove_sector', $parent_id, $ind), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         if (empty($categories) && empty($sectors))
            echo $html->div('noData', __('List is empty', true));   

      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>