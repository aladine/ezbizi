<?php
$this->set('title_for_layout', __('Registration select', true));
echo $this->element('belt_home');
echo '<div id="main"><div class="cen preg">';

   echo $html->div('tit', $html->tag('span', __('Please choose one of the following', true)).$html->image('icons/titA.png'));

   echo '<div class="its">';
      echo $this->Html->link($this->Html->image('icons/reg1ico.png', array('class' => 'ico')).$this->Html->div('txt', __('My company doesn\'t have an account registered with Ezbizi yet.', true)), array('controller' => 'users', 'action' => 'register', 'company'), array('class' => 'it reg1 rad5', 'escape' => false));
      echo $this->Html->link($this->Html->image('icons/reg2ico.png', array('class' => 'ico')).$this->Html->div('txt', __('My company already has an account registered with Ezbizi and I wish to register as a new user with an existing company account.', true)), array('controller' => 'users', 'action' => 'register', 'user'), array('class' => 'it reg2 rad5', 'escape' => false));
   echo '</div>';

echo '</div></div>';
?>