<?php
echo $this->element('belt_home');
echo '<div id="main" class="site learn"><div class="cen">';
   echo $html->div('tit', $html->tag('span', __('Why to sign up?', true), array('class' => 'ge')).$html->image('icons/titA.png'));
   
   if($lang == 'en') {

   echo $html->div('sec', '<b>Ezbizi.com</b> is an international deal-making platform with social networking features. It is designed for small-medium sized companies to build lasting business relationships more effectively. Ezbizi will help you generate targeted business leads and connect you to a bustling community of buyers, sellers and traders. Find this and more in one neat, trustworthy place!');

   echo '<div class="pack rad3">';

      echo $html->div('stit ge', 'WHY IS EZBIZI GOOD FOR YOUR BUSINESS?');
   
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Be a part of a bustling international business community'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Search and bid for thousands of demands worldwide'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Post your demands to get a fast response from thousands of suppliers and solution providers'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Generate relevant sales leads and grow your revenue by targeting the right companies'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Find new connections, create lasting business relationships and effectively manage your network'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Get the latest business updates directly from your network using our targeted news-feed'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Building a transparent and trustworthy community is our highest priority. We guarantee to be your trusted partner in detecting and preventing any possible scam!'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Effectively manage your internal communications or use Ezbizi as a subsidy for expensive intranet software'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Use Ezbizi as a convenient, cost-free marketing channel to showcase your products or services'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'No more confusion! Negotiate new business in a simple, user-friendly environment'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Join us now and benefit from all the above and much more'));      
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'Most importantly get all of this for <b class="vi">FREE!</b>'));
   
      echo $html->link(__('Sign up', true), array('controller' => 'pages', 'action' => 'display', 'register'), array('class' => 'butt2 blB2 rad5 sign')); 
   echo '</div>';
   
   }
   if($lang == 'ja') {

   echo $html->div('sec', '<b>Ezbizi.com</b> は、ソーシャルネットワーキングが特徴的なインターナショナルな取引をする場です。中小企業がより有効的に継続的にビジネス関係を築けるようにデザインしています。ビジネスをリードする事がターゲットであり、またあなたが買い手、売り手や業者と知り合えるコミュニティーにご案内いたします。スマートな簡単操作で、信頼できるこの場、Ezbiziで体験してみて下さい。');

   echo '<div class="pack rad3">';

      echo $html->div('stit ge', 'EZBIZI で出来る事とは');
   
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', '数千のサプライヤーとソリューションプロバイダーから瞬時な応答を得るためにあなたのお探し品を投稿'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'あなたのネットワークを築き、新たなコネクションを作り、そして数千もの登録ユーザー、企業との交友を維持'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'ネットワークニュースを通し、あなたのコネクションの最新のビジネスのアップデート情報を即時にゲット'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'あなたの会社の皆様のネットワークを向上させることにより、社内のコミュニケーション、チームを一丸とさせるのに効果的にマネイジメント'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'インターナショナルなビジネスコミュニティーの一部となり、無駄な時間を使わずに、関連のあるセールスに繋ぎます'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', '全会員様の安全性は弊社が最も優先していることであり、Ezbiziは優れた機能で不正行為や詐欺を検出し、全会員様の手助けを致します'));
      echo $html->div('li bl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', '今すぐEzbiziに参加し、どんどん弊社をご利用して、利益を得て下さい。これら全てのサービスは無料です！'));
      echo $html->div('li dbl', $html->image('icons/bpoint.png', array('class' => 'iL10')).$html->tag('span', 'あなたが不利益になることはなにもございません。躊躇わず、今すぐEzbiziにご登録を！'));
   
      echo $html->link(__('Sign up', true), array('controller' => 'pages', 'action' => 'display', 'register'), array('class' => 'butt2 blB2 rad5 sign')); 
   echo '</div>';
   
   }
   
   
echo '</div></div>';
?>