<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      $dtitle = $offer['Offer']['quantity'].' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      if ($lang == 'ja')
         $dtitle = $offer['Offer']['quantity'].' '.$offer['Unit']['name_ja'].'  の '.$offer['Offer']['commodity'].' を 1 '.$offer['Unit']['name_ja'].' '.$offer['Offer']['price'].' '.$offer['Currency']['name_ja'].' で  探しています';
      echo $html->div('headline hlB', __('Offer messages', true).' >> '.$dtitle);
      
      //echo '<div class="modP rad5">';
      echo '<div class="list conv">';
      
      if (!empty($omessages)) {
         $thr = 0;
         foreach ($omessages as $omessage) {
            
            $thr++; if($thr > 3) $thr = 1;
            echo '<div class="itm thr thr'.$thr.'">';
               $new = '';
               if (!$omessage['Omessage']['read'] && $omessage['Omessage']['sender_id'] != $session->read('Auth.User.id'))
                  $new = $html->tag('span', __('new', true), array('class' => 'new'));
               $user_id = $omessage['Omessage']['sender_id'];
               if ($user_id == $session->read('Auth.User.id'))
                  $user_id = $omessage['Omessage']['recipient_id'];
               echo $html->tag('top', $new.$html->link(__('Offer negotiation - latest response', true).': ', array('controller' => 'omessages', 'action' => 'view', $offer['Offer']['id'], $user_id), array('class' => 'name')).$html->tag('span', rel_time($omessage['Omessage']['created']), array('class' => 'date')));
            
               $acpt = '';
               if($omessage['Omessage']['accepted'] == 1)
                  $acpt = $html->image('icons/accept.png', array('class' => 'iL'));
               if($omessage['Omessage']['accepted'] == -2)
                  $acpt = $html->image('icons/no_accept.png', array('class' => 'iL'));
               if($omessage['Omessage']['accepted'] == 2)
                  $acpt = $html->image('icons/deal.png', array('class' => 'iL'));
               
               $mes_text = $omessage['Omessage']['text'];            
               if(strlen($mes_text) <= 200)
                  echo $html->div('mess', $acpt.$html->tag('span', $mes_text));
               else {
                  $lst = strrpos(substr(strip_tags($mes_text), 0, 200), ' ');
                  echo $html->div('mess', $acpt.$html->tag('span', substr(strip_tags($mes_text), 0, $lst).' ...'));
               }
                   
               echo $html->link('x | '.__('remove', true), array('controller' => 'omessages', 'action' => 'delete', $offer['Offer']['id'], $user_id), array('class' => 'del del3', 'escape' => false), __('Are you sure?',true));    
            echo '</div>';
         }
      }
      
      echo '</div>';
      //echo '</div>';
      
   echo '</div>';
   echo '<div class="mRight iface">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>