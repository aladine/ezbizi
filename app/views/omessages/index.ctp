<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      echo $html->div('headline hlB', __('Offer messages', true));
      
      echo '<div class="modP rad5">';
      echo $html->div('headline hlG hlIn', __('Business negotiations', true));
      echo '<div class="list conv">';
      
      if (!empty($omessages)) {
         foreach ($omessages as $omessage) {
            if ($omessage['Offer']['company_id'] != $company['Company']['id']) {
               $cls = 'itmD3';
               $own = array('id' => __('Your Bid', true), 'bg' => 'viBg');
            }               
            else {
               $cls = '';
               $own = array('id' => __('Your Offer', true), 'bg' => 'blBg');
            }
            echo '<div class="itm itmD '.$cls.'">';
               $new = '';
               if (!empty($omessage['NEW']))
                  $new = $html->tag('span', __('new', true), array('class' => 'new'));
               $sm = '';
               if ($omessage['Offer']['user_id'] == $session->read('Auth.User.id'))
                  $sm = $html->image('icons/bl_home.png', array('class' => 'iL'));
               $link = array('controller' => 'omessages', 'action' => 'index', $omessage['Offer']['id']);
               if ($omessage['Offer']['company_id'] != $company['Company']['id']) {
                  $link = array('controller' => 'omessages', 'action' => 'view', $omessage['Offer']['id'], $omessage['Offer']['user_id']);
               }
               echo $html->div('topDate', $html->tag('span', rel_time($omessage['Omessage']['created']), array('class' => 'date')).$html->tag('span', $own['id'], array('class' => 'own rad3 '.$own['bg'])));
               
               $dtitle = $omessage['Offer']['quantity'].' '.$omessage['Offer']['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$omessage['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$omessage['Offer']['price'].' '.$omessage['Offer']['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
               if ($lang == 'ja')
                  $dtitle = $offer['Offer']['quantity'].' '.$offer['Unit']['name_ja'].'  の '.$offer['Offer']['commodity'].' を 1 '.$offer['Unit']['name_ja'].' '.$offer['Offer']['price'].' '.$offer['Currency']['name_ja'].' で  探しています';
               echo $html->div('top', $new.$sm.$html->link($dtitle, $link, array('class' => 'name', 'escape' => false)));
               if ($omessage['Offer']['company_id'] != $company['Company']['id'])
                  echo $html->link('x | '.__('remove', true), array('controller' => 'omessages', 'action' => 'delete', $omessage['Offer']['id'], $omessage['Offer']['company_id']), array('class' => 'del del3', 'escape' => false), __('Are you sure?',true));    
            echo '</div>';
         }
      }
      else
         echo $html->div('noData', __('Currently you have no offer messages', true));
      
      echo '</div>';
      echo '</div>';
      
   echo '</div>';
   echo '<div class="mRight iface">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>