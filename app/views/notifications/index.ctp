<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag iface"><div class="cen">';
   $cen_cls = 'iCen';

   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'iCen iCenS';
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   echo '<div class="'.$cen_cls.'">';
   
      echo '<div class="feeds modP rad5">'; 
      
         echo $html->div('headline hlB hlIn', __('Newsfeed', true));
         
         if (!empty($notifications)) {
               echo $this->element('feeds');
         }
         else
            echo $html->div('noData', __('Currently there are no news to display', true));
         
      echo '</div>';
      
      if (!empty($notifications)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   
echo '</div></div>';
?>
