<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Add new country', true));
      
      echo '<div class="form">';
         
      echo $form->create('Country', array('action' => 'add', 'enctype' => 'multipart/form-data'));
         echo $form->input('Country.name_en', array('label' => __('Name En', true).':'));
         echo $form->input('Country.name_ja', array('label' => __('Name Ja', true).':'));
         echo $form->input('Country.image', array('label' => __('Flag image', true).':', 'type' => 'file'));
      
         echo '<div class="bblk">';
            echo $form->submit(__('add', true));
         echo '</div>';
         
      echo $form->end();

      echo '</div>';
            
   echo '</div>';

echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){     
   $("input[type=file]").filestyle({
      image: "/img/bg/fileBg.png",
      inputheight : 18,
      inputwidth : 142,
      imageheight : 20,
      imagewidth : 38
   });
});
</script>