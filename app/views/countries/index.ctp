<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Countries', true));
      
      echo '<div class="form">';
         
         echo $html->link($html->image('icons/ad_add.png', array('class' => 'iL')).$html->tag('span',__('Add new country', true)), array('controller' => 'countries', 'action' => 'add'), array('class' => 'rad3 listA geB', 'escape' => false));
         
         if (!empty($countries)) {
            foreach ($countries as $country) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';                    
                     echo '<div class="top">';
                        echo $html->link($html->image('flags/'.$country['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $country['Country']['name_'.$lang]), array('controller' => 'regions', 'action' => 'index', $country['Country']['id']), array('class' => 'link', 'escape' => false));                 
                     echo '</div>';                   
                  echo '</div>';
                  
                  echo $html->link(__('remove', true), array('controller' => 'countries', 'action' => 'delete', $country['Country']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->div('noData', __('List is empty', true));   

      echo '</div>';
      
      if (!empty($countries)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      } 
      
   echo '</div>';

echo '</div></div>';
?>