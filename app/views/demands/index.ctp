<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
    
   echo '<div class="dCen">';
   
      echo '<div class="modP rad5">';
      echo '<div class="list">';
      if (!empty($latests) || !empty($all)) {
         if (!empty($latests))
            echo $html->div('headline hlB hlIn', __('Latest demands of your business interest', true));
         else
            echo $html->div('headline hlB hlIn', __('All latest demands', true));
         
         if (!empty($demands)) {
            foreach ($demands as $demand) {             
               $cls = ''; $acc = '';
               if ($demand['Company']['Account']['id'] == 2) {
                  $cls = 'itmD1';
                  $acc = $html->div('ind blBg', $demand['Company']['Account']['name_'.$lang]);
               }
               if ($demand['Company']['Account']['id'] == 3) {
                  $cls = 'itmD2';
                  $acc = $html->div('ind viBg', $demand['Company']['Account']['name_'.$lang]);
               }
               echo '<div class="itm itmD '.$cls.'">';
                  if (empty($demand['Demand']['priceper_id']))                     
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  else
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';                 
                  echo $html->link($dtitle, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'], $dtitle), array('class' => 'name', 'escape' => false));
                  echo $html->div('cat', $demand['Sector']['name_'.$lang]);
                  echo $html->div('loc', $html->image('flags/'.$demand['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $demand['Country']['name_'.$lang], array('class' => 'ge ital')));            
                  echo '<div class="comp">';
                     echo $html->tag('span', date('d.m.Y', strtotime($demand['Demand']['valid_from'])).' - '.date('d.m.Y', strtotime($demand['Demand']['valid_to'])), array('class' => 'bl')).'&nbsp;';
                     if ($company['Company']['account_id'] == 3 || !empty($demand['Connected']))
                        echo $html->tag('span', __('from', true).':', array('class' => 'gr')).'&nbsp;'.$html->link($demand['Company']['name'], array('controller' => 'companies', 'action' => 'view', $demand['Company']['id']), array('class' => 'name'));  
                  echo '</div>';
                  echo $acc;
               echo '</div>';
            }
            if (!empty($latests)) { 
               if (in_array($session->read('Auth.User.group_id'), array(1, 2))) {
                  echo $html->div('edtL', $html->link(__('change the business interest', true), array('controller' => 'companies', 'action' => 'edit', 'business-area')));
               }
            }
         }
         else if (!empty($latests)) { 
            echo $html->div('noData', __('Unfortunately, at the moment there aren\'t any demands in our database matching your desired criteria.', true));
            if (in_array($session->read('Auth.User.group_id'), array(1, 2))) {
               echo $html->div('edtL', $html->link(__('change the business interest', true), array('controller' => 'companies', 'action' => 'edit', 'business-area')));
            }
         }
         else
            echo $html->div('noData', __('There were no demands posted recently', true));
      }
      else if (!empty($added)) {      
         echo $html->div('headline hlB hlIn', __('Your posted demands', true));
         if (!empty($demands)) {
            foreach ($demands as $demand) {
               echo '<div class="itm itmD">';
                  if (empty($demand['Demand']['priceper_id']))
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  else 
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                  echo $html->link($dtitle, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'], $dtitle), array('class' => 'name', 'escape' => false));
                  echo $html->div('cat', $demand['Sector']['name_'.$lang]);
                  echo $html->div('loc', $html->image('flags/'.$demand['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $demand['Country']['name_'.$lang], array('class' => 'ge ital')));            
                  echo '<div class="comp">';
                     echo $html->tag('span', date('d.m.Y', strtotime($demand['Demand']['valid_from'])).' - '.date('d.m.Y', strtotime($demand['Demand']['valid_to'])), array('class' => 'bl'));  
                  echo '</div>';
                  //echo $html->link('□ | '.__('edit', true), array('controller' => 'demands', 'action' => 'edit', $demand['Demand']['id']), array('class' => 'edit edit2', 'escape' => false)); 
                  echo $html->link('x | '.__('remove', true), array('controller' => 'demands', 'action' => 'delete', $demand['Demand']['id']), array('class' => 'del del2', 'escape' => false), __('Are you sure?',true));    
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('There were no demands posted recently', true));
      }   
         
      echo '</div>';
      echo '</div>';
      
      if (!empty($demands)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   echo '<div class="dRight">';
      
      if ($company['Company']['type_id'] != 2)
         echo $html->link($html->image('icons/w_new.png', array('class' => 'iL10')).$html->tag('span', __('Post a new demand', true)), array('controller' => 'demands', 'action' => 'add'), array('class' => 'addB', 'escape' => false));
      
      echo '<div class="mMn">';
         if ($company['Company']['type_id'] != 2)
            echo $html->link($html->image('icons/bl_demands.png', array('class' => 'iL10')).$html->tag('span', __('Your posted demands', true)), array('controller' => 'demands', 'action' => 'index', 'added'), array('class' => 'it', 'escape' => false));
         echo $html->link($html->image('icons/bl_demands.png', array('class' => 'iL10')).$html->tag('span', __('Latest demands of your interest', true)), array('controller' => 'demands', 'action' => 'index', 'latests'), array('class' => 'it', 'escape' => false));  
         echo $html->link($html->image('icons/bl_demands.png', array('class' => 'iL10')).$html->tag('span', __('All latest demands', true)), array('controller' => 'demands', 'action' => 'index', 'all'), array('class' => 'it', 'escape' => false));
      echo '</div>';       
           
      echo $this->element('demand_search_form');
   
   echo '</div>';

   
echo '</div></div>';
?>