<?php
function catmenu($cats, & $catlevel, & $html, $lang, $sectors = null) {
   $catlevel++;
   foreach ($cats as $cat) {
      if ($catlevel > 1) {
         echo '<div class="cats lev'.$catlevel.' none">'; 
         echo $html->div('cat', $html->image('icons/plusR.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      else {
         echo '<div class="cats lev'.$catlevel.'">';
         echo $html->div('cat', $html->image('icons/plus.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      if (!empty($cat['children']))
         catmenu($cat['children'], $catlevel, $html, $lang, $cat['Sector']);
      else if (!empty($cat['Sector'])) {
         echo '<div class="cats lev'.($catlevel+1).' none">';  
         foreach ($cat['Sector'] as $sector) {
            echo $html->div('cat sec', $html->image('icons/sel.png').$html->tag('span', $sector['id'], array('class' => 'id none')).$html->tag('span', $sector['name_'.$lang], array('class' => 'name')));
         } 
         echo '</div>';
      }
      echo '</div>';
   }
   $catlevel--;
}

echo $html->css('calendar');
echo $html->script('jquery.ui.core');
echo $html->script('jquery.ui.datepicker');

echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
   
   echo $form->create('Demand', array('action' => 'add'));
   
   echo '<div>';
   
   echo '<div class="dLeft">';
      echo $html->div('headline hlB', __('Select demand category', true));   
   
      echo $form->input('Demand.sector_id', array('type' => 'hidden'));
   
      //---> CATEGORY SELECT
   
      echo '<div class="rB z_B w300">';
         echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
         
         echo $html->div('none', '', array('id' => 'catSet'));
         
         if (!empty($categories)) {
            echo '<div class="catSel">';            
               $catlevel = 0;          
               catmenu($categories, $catlevel, $html, $lang);
            echo '</div>';         
         }
         
         echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
         
      echo '</div>';
      
      echo $html->div('edtL none', '['.__('change selection', true).']', array('id' => 'catChg'));
   
   echo '</div>';
   echo '<div class="dCen">';

      echo $html->div('headline hlB', __('Insert demand details', true));
      
      echo '<div class="form uForm addForm rad5">';

         echo '<div class="line">'; 
            echo $form->input('Demand.text', array('label' => __('Text', true), 'type' => 'textarea', 'div' => 'input textarea'));
         echo '</div>';
         echo '<div class="line">'; 
            echo $form->input('Demand.commodity', array('label' => __('Commodity', true)));
         echo '</div>';
         echo '<div class="line">'; 
            echo $form->input('Demand.country_id', array('label' => __('Location', true), 'type' => 'select', 'empty' => ' - '.__('select location', true).' - '));
         echo '</div>';
         echo '<div class="double line">';
            echo $form->input('Demand.quantity', array('label' => __('Quantity', true), 'div' => 'input text quan'));
            echo $form->input('Demand.unit_id', array('label' => '', 'type' => 'select' , 'empty' => ' - ', 'div' => 'input text unit'));      
         echo '</div>';
         echo '<div class="double line rel">';
            echo $form->input('Demand.price', array('label' => __('Price', true), 'div' => 'input text quan'));
            echo $form->input('Demand.currency_id', array('label' => '', 'type' => 'select', 'div' => 'input text unit'));
            echo $html->tag('span', '/', array('class' => 'bet'));
            echo $html->tag('span', '', array('class' => 'bet', 'id' => 'PricePerValue'));
            echo '<div class="tot">';
               echo '<label>'.__('Total value', true).'</label>';
               echo $html->tag('span', '-', array('class' => 'totSum blBg rad3 ml20', 'id' => 'TotalPriceValue'));
            echo '</div>';
            //echo $form->input('Demand.priceper_id', array('label' => '', 'type' => 'select', 'div' => 'input text ship', 'empty' => __('Shipment', true), 'options' => $units));      
         echo '</div>';
         echo '<div class="double line last">';
            echo $form->input('Demand.valid_from', array('type' => 'text', 'label' => __('Valid from', true), 'div' => 'input text date dateFrom'));
            echo $form->input('Demand.valid_to', array('type' => 'text', 'label' => __('Till', true), 'div' => 'input text date dateTo'));
         echo '</div>';
         echo $form->submit(__('submit', true));
         
      echo '</div>';
      
   echo '</div>';
   
   echo '</div>';
   
   
   echo $form->end();
   
echo '</div></div>';
?>


<script type="text/javascript">
function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

$(document).ready(function(){   
       
   //dates
   var dates = $('#DemandValidFrom, #DemandValidTo').datepicker({
      dateFormat: 'dd.mm.yy',
      minDate: 0,
      //defaultDate: "+1w",
		showOn: 'button',
		buttonImage: '/img/bg/dateMask.png',
		buttonImageOnly: true,
		buttonText: '<? echo __('select a date', true) ?>',
		onSelect: function(selectedDate) {
			var option = this.id == "DemandValidFrom" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
   
   set_sector();
   
   //category select
   $('.catSel .cat').click(function() {
      var $parCat = $(this).parent();

      //if exists child
      if($parCat.find('> .cats').exists()){
         $parCat.find('> .cats').toggle();   
      }
      else {
         if ($(this).hasClass('sec')) {
            $('#DemandSectorId').val($(this).find('.id').html());
            $('.catSel').hide();
            $('#catSet').show().html($(this).find('.name').html());
            $('#catChg').show();
         }   
      }   
   });
   
   $('#catChg').click(function() {
      $('.catSel').show();
      $('#catSet, .catSel .lev2, .catSel .lev3, #catChg').hide();   
   });
   
   $('#DemandUnitId').change(function() {
      var str = "";
      $('#DemandUnitId option:selected').each(function () {
         str = $(this).text();
      });
      $('#PricePerValue').text(str);
   });
   
   $('#DemandQuantity').change(function() {
      var str = $(this).val();
      if (!isNaN(str)) {
         var price = $('#DemandPrice').val();
         if (!isNaN(price))
            price = price * str;
         var cur = "";
         $('#DemandCurrencyId option:selected').each(function () {
            cur = $(this).text();
         });
         price = addCommas(price.toFixed(2));
         $('#TotalPriceValue').text(price + " " + cur);
      }
   });
   
   $('#DemandPrice').change(function() {
      var str = $(this).val();
      if (!isNaN(str)) {
         var price = $('#DemandQuantity').val();
         if (!isNaN(price))
            price = price * str;         
         var cur = "";
         $('#DemandCurrencyId option:selected').each(function () {
            cur = $(this).text();
         });
         price = addCommas(price.toFixed(2));        
         $('#TotalPriceValue').text(price + " " + cur);
      }
   });
   
});

function set_sector() {
   if($('#DemandSectorId').val() != "") {
      $('.catSel .sec').each(function() {
         if ($(this).find('.id').html() == $('#DemandSectorId').val())
            $('#catSet').show().html($(this).find('.name').html().replace("&amp;", "&"));   
      });
      $('.catSel').hide();
      $('#catChg').show();         
   }
}
</script>