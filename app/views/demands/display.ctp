<?php
$argums = '';

echo $this->element('belt_home');
echo '<div id="main" class="pag dem"><div class="cen">';
   
   echo '<div class="pth">';
      echo $html->link(__('Home', true), array('controller' => 'users', 'action' => 'home'), array('class' => 'it hm'));
           
      if (!empty($path)) {
         foreach ($path as $element) {
            echo $html->tag('span', ' > ', array('class' => 'bl'));
            if ($argums != '')
               $argums .= '/';
            $argums .= normalize($element['Category']['name_'.$lang]);
            if (empty($element['last']))
               echo $html->link($element['Category']['name_'.$lang], array('controller' => 'demands', 'action' => 'display', $argums, 'c', $element['Category']['id']), array('class' => 'it lnk'));
            else
               echo $html->tag('span', $element['Category']['name_'.$lang], array('class' => 'it lst'));
         }
      }
      
      if (!empty($sector)) {
         echo $html->tag('span', ' > ', array('class' => 'bl'));
         echo $html->tag('span', $sector['Sector']['name_'.$lang], array('class' => 'it lst'));
      }
   echo '</div>';
   
   echo '<div class="dLeft">';  
      
      //---> SUBCATEGORY SELECT
   
      echo '<div class="lMn">';
         $count = 0;
         foreach ($subs as $sub) {
            $count++;
            if ($count == 3) $count = 1; 
            echo $html->link($sub[$model[0]]['name_'.$lang], array('controller' => 'demands', 'action' => 'display', $argums, normalize($sub[$model[0]]['name_'.$lang]), $model[1], $sub[$model[0]]['id']), array('class' => 'it rad3 it'.$count));  
         }
      echo '</div>';
      
   
   echo '</div>';
   echo '<div class="dCen">';
      
      echo '<div class="modP rad5">';
      echo $html->div('headline hlB hlIn', $html->tag('span',__('Results', true)));

      echo '<div class="list">';
         
         if (!empty($demands)) {
            foreach ($demands as $demand) {             
               $cls = ''; $acc = '';
               if ($demand['Company']['Account']['id'] == 2) {
                  $cls = 'itmD1';
                  $acc = $html->div('ind blBg', $demand['Company']['Account']['name_'.$lang]);
               }
               if ($demand['Company']['Account']['id'] == 3) {
                  $cls = 'itmD2';
                  $acc = $html->div('ind viBg', $demand['Company']['Account']['name_'.$lang]);
               }
               echo '<div class="itm itmD '.$cls.'">';
                  if (empty($demand['Demand']['priceper_id'])) 
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  else
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                  echo $html->link($dtitle, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'], $dtitle), array('class' => 'name', 'escape' => false));
                  echo $html->div('cat', $demand['Sector']['name_'.$lang]);
                  echo $html->div('loc', $html->image('flags/'.$demand['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $demand['Country']['name_'.$lang], array('class' => 'ge ital')));            
                  echo '<div class="comp">';
                      echo $html->tag('span', date('d.m.Y', strtotime($demand['Demand']['valid_from'])).' - '.date('d.m.Y', strtotime($demand['Demand']['valid_to'])), array('class' => 'bl'));  
                  echo '</div>';
                  echo $acc;
               echo '</div>';
            }     
         }
         else
            echo $html->div('noData', __('No demands matching your selected criteria were found', true));
      
      echo '</div>';
      echo '</div>';
      
      if (!empty($demands)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   
   
echo '</div></div>';
?>


<script type="text/javascript">
$(document).ready(function(){   
          
   //category select
   $('.catSel .cat').click(function() {
      var $parCat = $(this).parent();

      //if exists child
      if($parCat.find('> .cats').exists()){
         $parCat.find('> .cats').toggle();   
      }  
   });
      
});
</script>