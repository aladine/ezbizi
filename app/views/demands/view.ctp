<?php
echo $html->css('calendar');
echo $html->script(array('jquery.ui.core', 'jquery.ui.datepicker'));

echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
 
   echo '<div class="dCen">';
      $dtitle = '';
      if (empty($demand['Demand']['priceper_id']))
         $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      else
         $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      if ($lang == 'ja')
         $dtitle = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
      
      echo $html->div('headline hlB', $dtitle);    
         
      if ($this->Session->read('Auth.User.group_id') == 1)
         echo '<div class="delB">'.$html->link(__('remove', true), array('controller' => 'demands', 'action' => 'delete', $demand['Demand']['id']), array('class' => 'del')).'</div>';

      echo '<div class="detail rad5t">';
         echo $html->div('cat', '('.$demand['Sector']['name_'.$lang].')');
         echo '<div class="info">';
            echo $html->div('blk dat', $html->tag('span', __('Date', true).': ', array('class' => 'gr')).$html->tag('span', rel_time($demand['Demand']['created']), array('class' => 'gr')));
            echo $html->div('blk loc', $html->tag('span', __('Location', true).': ', array('class' => 'gr')).$html->image('flags/'.$demand['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $demand['Country']['name_'.$lang], array('class' => 'ge')));
            if (empty($demand['Demand']['priceper_id']))
               echo $html->div('blk prc', $html->tag('span', __('Price', true).': ', array('class' => 'gr')).$html->tag('span', $demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang], array('class' => 'bl')));
            else
               echo $html->div('blk prc', $html->tag('span', __('Price', true).': ', array('class' => 'gr')).$html->tag('span', $demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang], array('class' => 'bl')));  
         echo '</div>';
         echo '<div class="info">';
            echo $html->div('tot rad3', $html->tag('span', __('Total demand value', true).': ').$html->tag('span', number_format($demand['Demand']['norm_price']).' '.$demand['Currency']['name_'.$lang], array('class' => 'sum blBg rad3')));         
         echo '</div>';
         echo $html->div('sep', '');
         echo $html->para('dsc', str_replace(PHP_EOL, '<br />', linked_text($demand['Demand']['text'])));
         //100px == 100%
         $px = round(((time() - strtotime($demand['Demand']['valid_from']) + 86400) / (strtotime($demand['Demand']['valid_to']) - strtotime($demand['Demand']['valid_from']) + 86400))*100);
         $cls = '';
         if($px > 33) $cls = 'or_ind';
         if($px > 66) $cls = 'red_ind';
         echo '<div class="val '.$cls.'">';
            echo $html->div('gr float', __('Validity', true).': ');
            echo $html->div('ind rad3', $html->div('rat rad3', '', array('style' => 'width: '.$px.'px;')).$html->div('mask', ''));
            echo $html->div('dat none', date('d.m.Y', strtotime($demand['Demand']['valid_from'])).' - '.date('d.m.Y', strtotime($demand['Demand']['valid_to'])));
         echo '</div>';             
      echo '</div>';
      
      if ($this->Session->check('Auth.User.id') && $demand['Company']['id'] != $this->Session->read('Auth.User.company_id')) {
         $reaction = false;
         if (!empty($demand['Dmessage'])) {
            foreach ($demand['Dmessage'] as $message) {
               if ($message['sender_id'] == $session->read('Auth.User.id')) {
                  echo '<div class="reaBlk rad5b">';
                     echo $html->link(__('Show messages associated with this demand', true), array('controller' => 'dmessages', 'action' => 'view', $demand['Demand']['id'], $message['recipient_id']), array('class' => 'viewCon rad3'));
                  echo '</div>'; 
                  $reaction = true;
                  break;
               }
            }
         }
         if (!$reaction) {
            echo $form->create('Demand', array('url' => array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'])));
      
            echo '<div class="form uForm detForm rad5b">';
               echo $html->div('title', __('Respond to demand', true).':');
               echo '<div class="line">'; 
                  echo $form->input('Dmessage.text', array('label' => __('Text', true).':', 'type' => 'textarea', 'div' => 'input textarea'));
               echo '</div>';
               echo '<div class="double line">';
                  echo $form->input('Dmessage.price', array('label' => __('Bid price', true).':', 'div' => 'input text quan')).$html->div('bet', $demand['Currency']['name_'.$lang].' / '.$demand['Priceper']['name_'.$lang]);
                  //echo $form->input('Demand.currency_id', array('label' => '', 'type' => 'select', 'div' => 'input text unit'));      
               echo '</div>';
               echo '<div class="double line last">';
                  echo $form->input('Dmessage.valid_to', array('type' => 'text', 'label' => __('Valid till', true).':', 'div' => 'input text date dateFrom'));
               echo '</div>';
               echo $form->submit(__('send offer', true));
               
            echo '</div>';
            
            echo $form->end();
         }
      }
      if (!$this->Session->check('Auth.User.id')) {
         echo '<div class="regBlk rad5b">';
            echo $html->link(__('Sign-up to respond to this demand!', true), array('controller' => 'pages', 'action' => 'display', 'register'), array('class' => 'btn rad3'));
         echo '</div>';
      }
      
      
   echo '</div>';
   echo '<div class="dRight">';      
      if (!empty($company) && ($company['Company']['account_id'] == 3 || $company['Company']['id'] == $demand['Demand']['company_id'] || !empty($demand['Connected']))) {
     
      echo '<div class="demCon iface rad5">';           
            echo $html->link($demand['Company']['name'], array('controller' => 'companies', 'action' => 'view', $demand['Company']['id']), array('class' => 'comName rad3'));  
            
            echo '<br />';
            echo $html->div('headlineP', __('Contact information', true).':'); 
            
            echo '<div class="conBlk">';
               echo $html->div('it', $html->image('icons/addr.png', array('class' => 'iL')).$html->tag('span', $demand['Company']['address1']));
               if (!empty($demand['Company']['address2']))
                  echo $html->div('it', $html->div('at', '').$html->div('vl', $demand['Company']['address2']));
               if (!empty($demand['Company']['address3']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $demand['Company']['address3']));
               if (!empty($demand['Company']['zip']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $demand['Company']['zip'].' '.$demand['Company']['city']));
               else
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $demand['Company']['city']));
               echo $html->div('it', $html->image('icons/phone.png', array('class' => 'iL')).$html->tag('span', '+'.$demand['Company']['phone1'].' '.$demand['Company']['phone2']));
               if (!empty($demand['Company']['fax2']))
                  echo $html->div('it', $html->image('icons/fax.png', array('class' => 'iL')).$html->tag('span', '+'.$demand['Company']['fax1'].' '.$demand['Company']['fax2']));
               if (!empty($demand['Company']['web']))
                  echo $html->div('it', $html->image('icons/web.png', array('class' => 'iL')).$html->tag('span', $html->link($demand['Company']['web'], http_sanitize($demand['Company']['web']))));
               
               $con_name = '';
               if (!empty($demand['User']['title']))
                  $con_name .= $demand['User']['title'].' ';
               if (!empty($demand['User']['first_name']))
                  $con_name .= $demand['User']['first_name'].' ';
               if (!empty($demand['User']['middle_name']))
                  $con_name .= $demand['User']['middle_name'].' ';
               if (!empty($demand['User']['last_name']))
                  $con_name .= $demand['User']['last_name'];
               echo $html->div('it', $html->image('icons/pers.png', array('class' => 'iL')).$html->tag('span', $html->link($con_name, array('controller' => 'users', 'action' => 'view', $demand['User']['id']))));
               //echo $html->div('it', $html->div('at', __('Job title', true).':').$html->div('vl', $vcompany['User']['job_title']));
               echo $html->div('it', $html->image('icons/mail.png', array('class' => 'iL')).$html->tag('span', $demand['User']['email']));
                  
                 
            echo '</div>';
         
            echo '<br />';
            echo $html->div('headlineP', __('Company rating', true).':');   
            
            echo '<div class="rating">';              
            echo '<div class="rB z_B w200">';
               echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
               echo '<div class="mid">';
                  echo '<div class="ratingBlk">';
                     echo '<div class="ratBot">';
                        $px = round(($demand['Company']['rating'] / 5) * 100);
                        echo '<div class="line" style="width: '.$px.'px;"></div>'; //100% = 100px
                        echo '<div class="mask"></div>';                                                                                                            
                     echo '</div>';
                     echo $html->div('value', number_format($demand['Company']['rating'], 2));
                  echo '</div>';
               echo '</div>';
               echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
            echo '</div>';
         
         if ($demand['Company']['neg_rating'])
            echo $html->div('negativeInd', '! '.__('Warning: Negative rating by other users!', true)); 
      
      echo '</div>';
      }
      echo '</div>';
      
   echo '</div>';
      
echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
       
   //dates
   var dates = $('#DmessageValidTo').datepicker({
      dateFormat: 'dd.mm.yy',
      minDate: 0,
      //defaultDate: "+1w",
		showOn: 'button',
		buttonImage: '/img/bg/dateMask.png',
		buttonImageOnly: true,
		buttonText: '<? echo __('select a date', true) ?>',
		onSelect: function(selectedDate) {
			var option = this.id == "DmessageValidTo" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
   
});
</script>