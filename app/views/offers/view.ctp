<?php
echo $html->css('calendar');
echo $html->script(array('jquery.ui.core', 'jquery.ui.datepicker'));

echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
 
   echo '<div class="dCen">';
      $dtitle = '';
      if (empty($offer['Offer']['priceper_id']))
         $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      else
         $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'/'.$offer['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      if ($lang == 'ja')
         $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_ja'].'  の '.$offer['Offer']['commodity'].' を 1 '.$offer['Unit']['name_ja'].' '.$offer['Offer']['price'].' '.$offer['Currency']['name_ja'].' で  探しています';
      
      echo $html->div('headline hlB', $dtitle);    
         
      if ($this->Session->read('Auth.User.group_id') == 1)
         echo '<div class="delB">'.$html->link(__('remove', true), array('controller' => 'offers', 'action' => 'delete', $offer['Offer']['id']), array('class' => 'del')).'</div>';

      echo '<div class="detail rad5t">';
         echo $html->div('cat', '('.$offer['Sector']['name_'.$lang].')');
         echo '<div class="info">';
            echo $html->div('blk dat', $html->tag('span', __('Date', true).': ', array('class' => 'gr')).$html->tag('span', rel_time($offer['Offer']['created']), array('class' => 'gr')));
            echo $html->div('blk loc', $html->tag('span', __('Location', true).': ', array('class' => 'gr')).$html->image('flags/'.$offer['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $offer['Country']['name_'.$lang], array('class' => 'ge')));
            if (empty($offer['Offer']['priceper_id']))
               echo $html->div('blk prc', $html->tag('span', __('Price', true).': ', array('class' => 'gr')).$html->tag('span', $offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang], array('class' => 'bl')));
            else
               echo $html->div('blk prc', $html->tag('span', __('Price', true).': ', array('class' => 'gr')).$html->tag('span', $offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'/'.$offer['Priceper']['name_'.$lang], array('class' => 'bl')));  
         echo '</div>';
         echo '<div class="info">';
            echo $html->div('tot rad3', $html->tag('span', __('Total offer value', true).': ').$html->tag('span', number_format($offer['Offer']['norm_price']).' '.$offer['Currency']['name_'.$lang], array('class' => 'sum blBg rad3')));         
         echo '</div>';
         echo $html->div('sep', '');
         echo $html->para('dsc', str_replace(PHP_EOL, '<br />', linked_text($offer['Offer']['text'])));
         //100px == 100%
         $px = round(((time() - strtotime($offer['Offer']['valid_from']) + 86400) / (strtotime($offer['Offer']['valid_to']) - strtotime($offer['Offer']['valid_from']) + 86400))*100);
         $cls = '';
         if($px > 33) $cls = 'or_ind';
         if($px > 66) $cls = 'red_ind';
         echo '<div class="val '.$cls.'">';
            echo $html->div('gr float', __('Validity', true).': ');
            echo $html->div('ind rad3', $html->div('rat rad3', '', array('style' => 'width: '.$px.'px;')).$html->div('mask', ''));
            echo $html->div('dat none', date('d.m.Y', strtotime($offer['Offer']['valid_from'])).' - '.date('d.m.Y', strtotime($offer['Offer']['valid_to'])));
         echo '</div>';             
      echo '</div>';
      
      if ($this->Session->check('Auth.User.id') && $offer['Company']['id'] != $this->Session->read('Auth.User.company_id')) {
         $reaction = false;
         if (!empty($offer['Dmessage'])) {
            foreach ($offer['Dmessage'] as $message) {
               if ($message['sender_id'] == $session->read('Auth.User.id')) {
                  echo '<div class="reaBlk rad5b">';
                     echo $html->link(__('Show messages associated with this offer', true), array('controller' => 'dmessages', 'action' => 'view', $offer['Offer']['id'], $message['recipient_id']), array('class' => 'viewCon rad3'));
                  echo '</div>'; 
                  $reaction = true;
                  break;
               }
            }
         }
         if (!$reaction) {
            echo $form->create('Offer', array('url' => array('controller' => 'offers', 'action' => 'view', $offer['Offer']['id'])));
      
            echo '<div class="form uForm detForm rad5b">';
               echo $html->div('title', __('Respond to offer', true).':');
               echo '<div class="line">'; 
                  echo $form->input('Dmessage.text', array('label' => __('Text', true).':', 'type' => 'textarea', 'div' => 'input textarea'));
               echo '</div>';
               echo '<div class="double line">';
                  echo $form->input('Dmessage.price', array('label' => __('Bid price', true).':', 'div' => 'input text quan')).$html->div('bet', $offer['Currency']['name_'.$lang].' / '.$offer['Priceper']['name_'.$lang]);
                  //echo $form->input('Offer.currency_id', array('label' => '', 'type' => 'select', 'div' => 'input text unit'));      
               echo '</div>';
               echo '<div class="double line last">';
                  echo $form->input('Dmessage.valid_to', array('type' => 'text', 'label' => __('Valid till', true).':', 'div' => 'input text date dateFrom'));
               echo '</div>';
               echo $form->submit(__('send offer', true));
               
            echo '</div>';
            
            echo $form->end();
         }
      }
      if (!$this->Session->check('Auth.User.id')) {
         echo '<div class="regBlk rad5b">';
            echo $html->link(__('Sign-up to respond to this offer!', true), array('controller' => 'pages', 'action' => 'display', 'register'), array('class' => 'btn rad3'));
         echo '</div>';
      }
      
      
   echo '</div>';
   echo '<div class="dRight">';      
      if (!empty($company) && ($company['Company']['account_id'] == 3 || $company['Company']['id'] == $offer['Offer']['company_id'] || !empty($offer['Connected']))) {
     
      echo '<div class="demCon iface rad5">';           
            echo $html->link($offer['Company']['name'], array('controller' => 'companies', 'action' => 'view', $offer['Company']['id']), array('class' => 'comName rad3'));  
            
            echo '<br />';
            echo $html->div('headlineP', __('Contact information', true).':'); 
            
            echo '<div class="conBlk">';
               echo $html->div('it', $html->image('icons/addr.png', array('class' => 'iL')).$html->tag('span', $offer['Company']['address1']));
               if (!empty($offer['Company']['address2']))
                  echo $html->div('it', $html->div('at', '').$html->div('vl', $offer['Company']['address2']));
               if (!empty($offer['Company']['address3']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $offer['Company']['address3']));
               if (!empty($offer['Company']['zip']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $offer['Company']['zip'].' '.$offer['Company']['city']));
               else
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $offer['Company']['city']));
               echo $html->div('it', $html->image('icons/phone.png', array('class' => 'iL')).$html->tag('span', '+'.$offer['Company']['phone1'].' '.$offer['Company']['phone2']));
               if (!empty($offer['Company']['fax2']))
                  echo $html->div('it', $html->image('icons/fax.png', array('class' => 'iL')).$html->tag('span', '+'.$offer['Company']['fax1'].' '.$offer['Company']['fax2']));
               if (!empty($offer['Company']['web']))
                  echo $html->div('it', $html->image('icons/web.png', array('class' => 'iL')).$html->tag('span', $html->link($offer['Company']['web'], http_sanitize($offer['Company']['web']))));
               
               $con_name = '';
               if (!empty($offer['User']['title']))
                  $con_name .= $offer['User']['title'].' ';
               if (!empty($offer['User']['first_name']))
                  $con_name .= $offer['User']['first_name'].' ';
               if (!empty($offer['User']['middle_name']))
                  $con_name .= $offer['User']['middle_name'].' ';
               if (!empty($offer['User']['last_name']))
                  $con_name .= $offer['User']['last_name'];
               echo $html->div('it', $html->image('icons/pers.png', array('class' => 'iL')).$html->tag('span', $html->link($con_name, array('controller' => 'users', 'action' => 'view', $offer['User']['id']))));
               //echo $html->div('it', $html->div('at', __('Job title', true).':').$html->div('vl', $vcompany['User']['job_title']));
               echo $html->div('it', $html->image('icons/mail.png', array('class' => 'iL')).$html->tag('span', $offer['User']['email']));
                  
                 
            echo '</div>';
         
            echo '<br />';
            echo $html->div('headlineP', __('Company rating', true).':');   
            
            echo '<div class="rating">';              
            echo '<div class="rB z_B w200">';
               echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
               echo '<div class="mid">';
                  echo '<div class="ratingBlk">';
                     echo '<div class="ratBot">';
                        $px = round(($offer['Company']['rating'] / 5) * 100);
                        echo '<div class="line" style="width: '.$px.'px;"></div>'; //100% = 100px
                        echo '<div class="mask"></div>';                                                                                                            
                     echo '</div>';
                     echo $html->div('value', number_format($offer['Company']['rating'], 2));
                  echo '</div>';
               echo '</div>';
               echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
            echo '</div>';
         
         if ($offer['Company']['neg_rating'])
            echo $html->div('negativeInd', '! '.__('Warning: Negative rating by other users!', true)); 
      
      echo '</div>';
      }
      echo '</div>';
      
   echo '</div>';
      
echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
       
   //dates
   var dates = $('#DmessageValidTo').datepicker({
      dateFormat: 'dd.mm.yy',
      minDate: 0,
      //defaultDate: "+1w",
		showOn: 'button',
		buttonImage: '/img/bg/dateMask.png',
		buttonImageOnly: true,
		buttonText: '<? echo __('select a date', true) ?>',
		onSelect: function(selectedDate) {
			var option = this.id == "DmessageValidTo" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
   
});
</script>