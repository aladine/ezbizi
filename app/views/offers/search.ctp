<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
    
   echo '<div class="dCen">';
   
   echo '<div class="modP rad5">';
      echo '<div class="list">';
         echo $html->div('headline hlB hlIn', __('Search results', true));
         
         if (!empty($offers)) {
            foreach ($offers as $offer) {             
               $cls = ''; $acc = '';
               if ($offer['Company']['Account']['id'] == 2) {
                  $cls = 'itmD1';
                  $acc = $html->div('ind blBg', $offer['Company']['Account']['name_'.$lang]);
               }
               if ($offer['Company']['Account']['id'] == 3) {
                  $cls = 'itmD2';
                  $acc = $html->div('ind viBg', $offer['Company']['Account']['name_'.$lang]);
               }
               echo '<div class="itm itmD '.$cls.'">';
                  if (empty($offer['Offer']['priceper_id']))                     
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  else
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'/'.$offer['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_ja'].'  の '.$offer['Offer']['commodity'].' を 1 '.$offer['Unit']['name_ja'].' '.$offer['Offer']['price'].' '.$offer['Currency']['name_ja'].' で  探しています';                 
                  echo $html->link($dtitle, array('controller' => 'offers', 'action' => 'view', $offer['Offer']['id'], $dtitle), array('class' => 'name', 'escape' => false));
                  echo $html->div('cat', $offer['Sector']['name_'.$lang]);
                  echo $html->div('loc', $html->image('flags/'.$offer['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $offer['Country']['name_'.$lang], array('class' => 'ge ital')));            
                  echo '<div class="comp">';
                     echo $html->tag('span', date('d.m.Y', strtotime($offer['Offer']['valid_from'])).' - '.date('d.m.Y', strtotime($offer['Offer']['valid_to'])), array('class' => 'bl')).'&nbsp;';
                     if (!empty($company) && $company['Company']['account_id'] == 3 || !empty($offer['Connected']))
                        echo $html->tag('span', __('by', true).':', array('class' => 'gr')).'&nbsp;'.$html->link($offer['Company']['name'], array('controller' => 'companies', 'action' => 'view', $offer['Company']['id']), array('class' => 'name'));  
                  echo '</div>';
                  echo $acc;
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('No offers matching your selected criteria were found', true));
      
         
      echo '</div>';
      echo '</div>';
      
      if (!empty($offers)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   echo '<div class="dRight">';
           
     if($session->check('Auth.User')) { 
        if ($company['Company']['type_id'] != 2)
           echo $html->link($html->image('icons/w_new.png', array('class' => 'iL10')).$html->tag('span', __('Post a new offer', true)), array('controller' => 'offers', 'action' => 'add'), array('class' => 'addB', 'escape' => false));
        
        echo '<div class="mMn">';
           if ($company['Company']['type_id'] != 2)
                echo $html->link($html->image('icons/bl_offers.png', array('class' => 'iL10')).$html->tag('span', __('Your posted offers', true)), array('controller' => 'offers', 'action' => 'index', 'added'), array('class' => 'it', 'escape' => false));
            echo $html->link($html->image('icons/bl_offers.png', array('class' => 'iL10')).$html->tag('span', __('Latest offers of your interest', true)), array('controller' => 'offers', 'action' => 'index', 'latests'), array('class' => 'it', 'escape' => false));  
            echo $html->link($html->image('icons/bl_offers.png', array('class' => 'iL10')).$html->tag('span', __('All latest offers', true)), array('controller' => 'offers', 'action' => 'index', 'all'), array('class' => 'it', 'escape' => false));
        echo '</div>';       
      }
                 
      echo $this->element('offer_search_form');
   
   echo '</div>';

   
echo '</div></div>';
?>