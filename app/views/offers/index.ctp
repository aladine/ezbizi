<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
    
   echo '<div class="dCen">';
   
      echo '<div class="modP rad5">';
      echo '<div class="list">';
      if (!empty($latests) || !empty($all)) {
         if (!empty($latests))
            echo $html->div('headline hlB hlIn', __('Latest offers of your business interest', true));
         else
            echo $html->div('headline hlB hlIn', __('All latest offers', true));
         
         if (!empty($offers)) {
            foreach ($offers as $offer) {             
               $cls = ''; $acc = '';
               if ($offer['Company']['Account']['id'] == 2) {
                  $cls = 'itmD1';
                  $acc = $html->div('ind blBg', $offer['Company']['Account']['name_'.$lang]);
               }
               if ($offer['Company']['Account']['id'] == 3) {
                  $cls = 'itmD2';
                  $acc = $html->div('ind viBg', $offer['Company']['Account']['name_'.$lang]);
               }
               echo '<div class="itm itmD '.$cls.'">';
                  if (empty($offer['Offer']['priceper_id']))                     
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  else
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'/'.$offer['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_ja'].'  の '.$offer['Offer']['commodity'].' を 1 '.$offer['Unit']['name_ja'].' '.$offer['Offer']['price'].' '.$offer['Currency']['name_ja'].' で  探しています';                 
                  echo $html->link($dtitle, array('controller' => 'offers', 'action' => 'view', $offer['Offer']['id'], $dtitle), array('class' => 'name', 'escape' => false));
                  echo $html->div('cat', $offer['Sector']['name_'.$lang]);
                  echo $html->div('loc', $html->image('flags/'.$offer['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $offer['Country']['name_'.$lang], array('class' => 'ge ital')));            
                  echo '<div class="comp">';
                     echo $html->tag('span', date('d.m.Y', strtotime($offer['Offer']['valid_from'])).' - '.date('d.m.Y', strtotime($offer['Offer']['valid_to'])), array('class' => 'bl')).'&nbsp;';
                     if ($company['Company']['account_id'] == 3 || !empty($offer['Connected']))
                        echo $html->tag('span', __('from', true).':', array('class' => 'gr')).'&nbsp;'.$html->link($offer['Company']['name'], array('controller' => 'companies', 'action' => 'view', $offer['Company']['id']), array('class' => 'name'));  
                  echo '</div>';
                  echo $acc;
               echo '</div>';
            }
            if (!empty($latests)) { 
               if (in_array($session->read('Auth.User.group_id'), array(1, 2))) {
                  echo $html->div('edtL', $html->link(__('change the business interest', true), array('controller' => 'companies', 'action' => 'edit', 'business-area')));
               }
            }
         }
         else if (!empty($latests)) { 
            echo $html->div('noData', __('Unfortunately, at the moment there aren\'t any offers in our database matching your desired criteria.', true));
            if (in_array($session->read('Auth.User.group_id'), array(1, 2))) {
               echo $html->div('edtL', $html->link(__('change the business interest', true), array('controller' => 'companies', 'action' => 'edit', 'business-area')));
            }
         }
         else
            echo $html->div('noData', __('There were no offers posted recently', true));
      }
      else if (!empty($added)) {      
         echo $html->div('headline hlB hlIn', __('Your posted offers', true));
         if (!empty($offers)) {
            foreach ($offers as $offer) {
               echo '<div class="itm itmD">';
                  if (empty($offer['Offer']['priceper_id']))
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  else 
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$offer['Offer']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$lang].'/'.$offer['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $dtitle = number_format($offer['Offer']['quantity']).' '.$offer['Unit']['name_ja'].'  の '.$offer['Offer']['commodity'].' を 1 '.$offer['Unit']['name_ja'].' '.$offer['Offer']['price'].' '.$offer['Currency']['name_ja'].' で  探しています';
                  echo $html->link($dtitle, array('controller' => 'offers', 'action' => 'view', $offer['Offer']['id'], $dtitle), array('class' => 'name', 'escape' => false));
                  echo $html->div('cat', $offer['Sector']['name_'.$lang]);
                  echo $html->div('loc', $html->image('flags/'.$offer['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $offer['Country']['name_'.$lang], array('class' => 'ge ital')));            
                  echo '<div class="comp">';
                     echo $html->tag('span', date('d.m.Y', strtotime($offer['Offer']['valid_from'])).' - '.date('d.m.Y', strtotime($offer['Offer']['valid_to'])), array('class' => 'bl'));  
                  echo '</div>';
                  //echo $html->link('□ | '.__('edit', true), array('controller' => 'offers', 'action' => 'edit', $offer['Offer']['id']), array('class' => 'edit edit2', 'escape' => false)); 
                  echo $html->link('x | '.__('remove', true), array('controller' => 'offers', 'action' => 'delete', $offer['Offer']['id']), array('class' => 'del del2', 'escape' => false), __('Are you sure?',true));    
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('There were no offers posted recently', true));
      }   
         
      echo '</div>';
      echo '</div>';
      
      if (!empty($offers)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   echo '<div class="dRight">';
      
      if ($company['Company']['type_id'] != 2)
         echo $html->link($html->image('icons/w_new.png', array('class' => 'iL10')).$html->tag('span', __('Post a new offer', true)), array('controller' => 'offers', 'action' => 'add'), array('class' => 'addB', 'escape' => false));
      
      echo '<div class="mMn">';
         if ($company['Company']['type_id'] != 2)
            echo $html->link($html->image('icons/bl_offers.png', array('class' => 'iL10')).$html->tag('span', __('Your posted offers', true)), array('controller' => 'offers', 'action' => 'index', 'added'), array('class' => 'it', 'escape' => false));
         echo $html->link($html->image('icons/bl_offers.png', array('class' => 'iL10')).$html->tag('span', __('Latest offers of your interest', true)), array('controller' => 'offers', 'action' => 'index', 'latests'), array('class' => 'it', 'escape' => false));  
         echo $html->link($html->image('icons/bl_offers.png', array('class' => 'iL10')).$html->tag('span', __('All latest offers', true)), array('controller' => 'offers', 'action' => 'index', 'all'), array('class' => 'it', 'escape' => false));
      echo '</div>';       
           
      echo $this->element('offer_search_form');
   
   echo '</div>';

   
echo '</div></div>';
?>