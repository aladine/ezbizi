<?php
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
echo '<html xmlns="http://www.w3.org/1999/xhtml" lang="'.$lang.'">';
echo '<head>';
	
	echo '<title>';
		__('Ezbizi | ');
		echo $title_for_layout;
	echo '</title>';
	                                                                      
   echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
   echo '<meta name="keywords" content="" />';
   if (!empty($description_for_layout))
      echo '<meta name="description" content="'.$description_for_layout.'" />';
   else
      echo '<meta name="description" content="" />';
   echo '<meta name="robots" content="all, follow" />';
   echo '<meta name="googlebot" content="snippet,archive" />';
   echo '<meta name="revisit-after" content="1 days"/>';
   echo '<meta name="author" content="AITechnology.sk" />';
   echo '<meta http-equiv="Content-Language" content="'.$lang.'"/>';    
   echo '<meta name="language" content="'.$lang.'"/>';
   echo '<meta name="copyright" content="(c)2011, AITechnology"/>';
                                                                      
	echo '<meta property="og:title" content="Ezbizi"/>';
   echo '<meta property="og:description" content="Ezbizi is an international deal-making platform with social networking features. It is designed for small-medium sized companies to build lasting business relationships more effectively. Ezbizi will help you generate targeted business leads and connect you to a bustling community of buyers, sellers and traders. Find this and more in one neat, trustworthy place!"/>';  
   echo '<meta property="og:image" content="http://'.$_SERVER["SERVER_NAME"].DS.'img'.DS.'logoBeta.png" />';    
	echo '<meta property="og:url" content="http://'.$_SERVER["SERVER_NAME"].DS.$lang.'" />';
   echo '<meta property="og:type" content="website" />';
   echo '<meta property="og:locale" content="en_US" />';
   echo '<meta property="og:site_name" content="Ezbizi.com" />';
   echo '<meta property="fb:admins" content="100002067500115" />';
   
   echo $html->meta('icon');
	echo $html->css(array('cake', 'layout', 'lightbox', 'cleditor'));
	echo $html->script(array('jquery.min', 'password', 'flash', 'fileInput', 'lightbox-0.5.min', 'setup'));
   echo '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>';

   echo $scripts_for_layout;
   
   echo '<!--[if lt IE 7]>';
	   echo $html->script(array('ie6alert'));
   echo '<![endif]-->';
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26879979-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php
echo '</head>';                                            
echo '<body>';
   echo '<div id="top">';
      echo $this->element('top');    
   echo '</div>';
                 
   echo '<div id="cen">';
      echo $content_for_layout;
   echo '</div>';  
      
   //echo '<div id="round"><div class="cen">';    
   //echo '</div></div>';
   
   echo '<div id="foot">';
      echo $this->element('foot');      
   echo '</div>';
   
   echo $html->link($html->image('bg/feedback.png', array('class' => 'vI')).$html->image('bg/bl_feedback.png', array('class' => 'bI none')), array('controller' => 'users', 'action' => 'contact'), array('id' => 'feedback', 'escape' => false));
   
   echo '<div id="socm">';
      echo $html->image('bg/sharethis.png', array('class' => 'tab'));
      echo '<div class="med">';
         echo '<div class="blk rad3">';
            echo '<div class="fb"><iframe src="//www.facebook.com/plugins/like.php?href=http://'.$_SERVER["SERVER_NAME"].DS.$lang.'&amp;locale=en_US&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=trebuchet+ms&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe></div>';      
            echo '<div class="gp"><g:plusone size="medium"></g:plusone></div>';
            echo '<div class="tw"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.ezbizi.com" data-text="Ezbizi | A new generation online business network" data-count="horizontal">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script></div>';
         echo '</div>';
      echo '</div>';
   echo '</div>';   
   
   echo $session->flash();
   echo $session->flash('auth');
      
   echo $this->element('sql_dump');                                     
   
   echo '<!--[if lt IE 7]>';
      echo '<div id="ie6">';
         echo $this->element('ie6'); 
      echo '</div>';
   echo '<![endif]-->';
   
   echo $js->writeBuffer();

echo '</body>';
echo '</html>';
?>