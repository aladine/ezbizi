<?=$this->element('header_nav')?>
<div id="content">


<div id="contentleft">
<div id="contentleft-inner">
<h1>News Feed</h1>

<!-- individual feed -->

<div class="newsfeed-company">
<div class="newsfeed-company-logo">
<?php echo $html->image("companyx.jpg")?>
</div> <!-- newsfeed-company-logo ends -->

<div class="newsfeed-company-details">

<div class="newsfeed-company-date">
Apil 10, 2012
</div> <!-- newsfeed-company-date ends -->

<div class="newsfeed-company-name">
Company X
</div> <!-- newsfeed-company-name ends -->

<div class="newsfeed-company-status">
Looking for a new brand manager for our Singapore branch.
</div> <!-- newsfeed-company-status ends -->

<div class="newsfeed-company-demand">
Demanding
<div class="newsfeed-company-demand-row">
<div class="newsfeed-company-demand-item">
<a href="#"><?php echo $html->image("product1.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-demand-item ends -->
<div class="newsfeed-company-demand-item">
<a href="#"><?php echo $html->image("product2.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-demand-item ends -->
<div class="newsfeed-company-demand-item">
<a href="#"><?php echo $html->image("product3.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-demand-item ends -->
<div class="newsfeed-company-demand-item-more">
<a href="#" class="newsfeed-company-demand-item-more-link">more</a>
</div> <!-- newsfeed-company-demand-item ends -->
</div> <!-- newsfeed-company-demand-row ends -->
</div> <!-- newsfeed-company-demand ends -->

<div class="newsfeed-company-offer">
Offering
<div class="newsfeed-company-offer-row">
<div class="newsfeed-company-offer-item">
<a href="#"><?php echo $html->image("product5.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
<div class="newsfeed-company-offer-item">
<a href="#"><?php echo $html->image("product4.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
<div class="newsfeed-company-offer-item">
<a href="#"><?php echo $html->image("product2.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
<div class="newsfeed-company-offer-item-more">
<a href="#" class="newsfeed-company-offer-item-more-link">more</a>
</div> <!-- newsfeed-company-offer-item ends -->
</div> <!-- newsfeed-company-offer-row ends -->
</div> <!-- newsfeed-company-offer ends -->

</div> <!-- newsfeed-company-details ends -->
</div> <!-- newsfeed-company ends -->



<!-- individual feed -->

<div class="newsfeed-company">
<div class="newsfeed-company-logo">
<?php echo $html->image("companyy.jpg")?>
</div> <!-- newsfeed-company-logo ends -->

<div class="newsfeed-company-details">

<div class="newsfeed-company-date">
Apil 10, 2012
</div> <!-- newsfeed-company-date ends -->

<div class="newsfeed-company-name">
Company Y
</div> <!-- newsfeed-company-name ends -->

<div class="newsfeed-company-demand">
Demanding
<div class="newsfeed-company-demand-row">
<div class="newsfeed-company-demand-item"><a href="#"><?php echo $html->image("product2.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-demand-item ends -->
<div class="newsfeed-company-demand-item"><a href="#"><?php echo $html->image("product3.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-demand-item ends -->
</div> <!-- newsfeed-company-demand-row ends -->
</div> <!-- newsfeed-company-demand ends -->


<div class="newsfeed-company-offer">
Offering
<div class="newsfeed-company-offer-row">
<div class="newsfeed-company-offer-item"><a href="#"><?php echo $html->image("product5.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
<div class="newsfeed-company-offer-item"><a href="#"><?php echo $html->image("product4.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
</div> <!-- newsfeed-company-offer-row ends -->
</div> <!-- newsfeed-company-offer ends -->

</div> <!-- newsfeed-company-details ends -->
</div> <!-- newsfeed-company ends -->


<!-- individual feed -->

<div class="newsfeed-company">
<div class="newsfeed-company-logo">
<?php echo $html->image("companyz.jpg")?>
</div> <!-- newsfeed-company-logo ends -->

<div class="newsfeed-company-details">

<div class="newsfeed-company-date">
Apil 10, 2012
</div> <!-- newsfeed-company-date ends -->

<div class="newsfeed-company-name">
Company Z
</div> <!-- newsfeed-company-name ends -->

<div class="newsfeed-company-offer">
Offering
<div class="newsfeed-company-offer-row">
<div class="newsfeed-company-offer-item"><a href="#"><?php echo $html->image("product1.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
<div class="newsfeed-company-offer-item"><a href="#"><?php echo $html->image("product2.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-offer-item ends -->
</div> <!-- newsfeed-company-offer-row ends -->
</div> <!-- newsfeed-company-offer ends -->

</div> <!-- newsfeed-company-details ends -->
</div> <!-- newsfeed-company ends -->



<!-- individual feed -->

<div class="newsfeed-company">
<div class="newsfeed-company-logo">
<?php echo $html->image("companya.jpg")?>
</div> <!-- newsfeed-company-logo ends -->

<div class="newsfeed-company-details">

<div class="newsfeed-company-date">
Apil 10, 2012
</div> <!-- newsfeed-company-date ends -->

<div class="newsfeed-company-name">
Company A
</div> <!-- newsfeed-company-name ends -->

<div class="newsfeed-company-demand">
Demanding
<div class="newsfeed-company-demand-row">
<div class="newsfeed-company-demand-item"><a href="#"><?php echo $html->image("product2.jpg")?> Lorem ipsum dolor sit amet</a>
</div> <!-- newsfeed-company-demand-item ends -->
</div> <!-- newsfeed-company-demand-row ends -->
</div> <!-- newsfeed-company-demand ends -->

</div> <!-- newsfeed-company-details ends -->
</div> <!-- newsfeed-company ends -->


<!-- individual feed -->

<div class="newsfeed-company">
<div class="newsfeed-company-logo">
<?php echo $html->image("companyb.jpg")?>
</div> <!-- newsfeed-company-logo ends -->

<div class="newsfeed-company-details">

<div class="newsfeed-company-date">
Apil 10, 2012
</div> <!-- newsfeed-company-date ends -->

<div class="newsfeed-company-name">
Company B
</div> <!-- newsfeed-company-name ends -->

<div class="newsfeed-company-status">
It is a good day to trade.
</div> <!-- newsfeed-company-status ends -->

</div> <!-- newsfeed-company-details ends -->
</div> <!-- newsfeed-company ends -->



</div> <!-- contentleft-inner ends -->
</div> <!-- contentleft ends -->


<div id="contentright">
<div id="contentright-inner">
<h1>Recommendations</h1>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam purus lorem, malesuada vel tincidunt in, ullamcorper nec lacus. Proin rhoncus, quam id mollis vehicula, augue tellus tempus neque, ac dictum diam orci vel tellus. Fusce at mi at nibh vulputate tincidunt sit amet nec mauris. Mauris ut nisi augue. Ut luctus velit nec orci imperdiet facilisis vel vitae neque. Cras bibendum ante non orci blandit sed pharetra est convallis. Nullam dapibus, arcu vitae rutrum congue, metus ligula eleifend diam, et ullamcorper justo purus ut urna. Cras mi neque, ornare eget placerat sed, vulputate ac erat. Nulla magna elit, eleifend vel hendrerit a, aliquam eget neque. Donec lobortis orci sed diam eleifend at scelerisque sapien gravida. Nullam porta vehicula est, eget vehicula lacus vulputate eu. Sed rutrum facilisis tellus, non hendrerit felis dapibus quis. Nunc consectetur, lectus non condimentum pulvinar, lectus mi rutrum leo, sed porttitor orci urna sit amet leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed vel elit et purus pulvinar sollicitudin.
<br /><br />
Pellentesque elit lectus, malesuada eget lobortis feugiat, lobortis eget erat. Proin eleifend tortor nec sapien laoreet mollis. Donec placerat vestibulum fermentum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam erat volutpat. Nam lacus nibh, posuere vitae placerat at, hendrerit eget erat. Vivamus placerat, arcu in feugiat scelerisque, lacus massa vulputate nisi, consectetur vehicula lectus dui eu dolor.
<br /><br />
Donec convallis mauris ac elit adipiscing ac cursus ante sagittis. Donec a ante felis. Proin id magna nunc, quis cursus odio. Ut molestie, massa sed rutrum viverra, magna velit cursus ligula, at eleifend urna purus ut nunc. Quisque vel massa tortor, id egestas orci. Sed ac diam id ligula sodales commodo in sit amet nulla. Sed ut mi a quam scelerisque consequat. Etiam nec nisi leo. Phasellus venenatis augue ac tortor elementum sit amet pharetra quam accumsan. Nulla faucibus condimentum elementum. Vestibulum iaculis turpis a urna auctor ultricies. 
</div> <!-- contentright-inner ends -->
</div> <!-- contentright ends -->



</div> <!-- content ends -->