<?php
echo $this->element('belt_home');
echo '<div id="promo"><div class="cen forgot">';
   echo $html->div('tit', __('We really value your feedback!', true));
   echo '<br />';
   echo $html->div('txt', __('For any questions, complaints, suggestions, technical feedback or any other comments, please write us your message below.', true));
echo '</div></div>';

echo '<div id="main">';
   echo '<div class="cen regForm">';
      echo $form->create('User', array('url' => array('controller' => 'users', 'action' => 'contact')));
                 
         //echo '<div class="blk swiBlk">';
         
         echo $html->div('tit', $html->tag('span',__('Please write your message here', true)).$html->image('icons/titA.png'));
         
         $write_time = encrypt(time());
         echo $form->input('User.time', array('type' => 'hidden', 'value' => $write_time));
         echo $form->input('User.e_name', array('label' => '', 'type' => 'text', 'div' => array('style' => 'display:none')));
         echo $form->input('User.for_email', array('label' => __('Your email', true)));
         echo $form->input('User.text', array('label' => __('Your message', true), 'type' => 'textarea'));
        
         //echo '</div>';
         echo '<div class="bblk">';
            echo $form->submit(__('send', true));
         echo '</div>';
                  
      echo $form->end();  
   echo '</div>';

echo '</div>';
?>