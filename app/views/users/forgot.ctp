<?php
echo $this->element('belt_home');
echo '<div id="promo"><div class="cen forgot">';
   echo $html->div('tit', __('lost password', true));
   echo '<br />';
   echo $html->div('txt', __('Oups! Did you forget your password? Don\'t worry, we got you covered! Just enter your registered email address below.', true));
echo '</div></div>';

echo '<div id="main">';
   echo '<div class="cen regForm">';
      echo $form->create('User', array('action' => 'forgot'));
         
         echo '<div class="blk swiBlk">';
         
         echo $html->div('tit', $html->tag('span',__('Enter your e-mail', true)).$html->image('icons/titA.png'));
         
         echo $form->input('User.for_email', array('label' => __('E-mail', true).':'));
        
         echo '</div>';
         echo '<div class="bblk">';
            echo $form->submit(__('send login', true));
         echo '</div>';
                  
      echo $form->end();  
   echo '</div>';

echo '</div>';
?>