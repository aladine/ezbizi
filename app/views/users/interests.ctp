<?php
echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_my');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Update my personal and professional interests', true));
      
      echo '<div class="form">';
      echo $form->create('User', array('url' => array('controller' => 'users', 'action' => 'edit', 'interests')));
       
         echo $form->input('User.summary', array('label' => __('Personal summary', true).':', 'type' => 'textarea', 'div' => 'input textarea bigarea'));                           
         echo $form->input('User.specialization', array('label' => __('Professional specialization', true).':', 'type' => 'textarea', 'div' => 'input textarea bigarea'));                                                                                                 
         echo $form->input('User.interest', array('label' => __('Personal interests (Hobbies)', true).':', 'type' => 'textarea', 'div' => 'input textarea bigarea'));
         echo $form->input('User.experience', array('label' => __('Work experience', true).':', 'type' => 'textarea', 'div' => 'input textarea bigarea'));
         echo $form->input('User.education', array('label' => __('Education', true).':', 'type' => 'textarea', 'div' => 'input textarea bigarea'));
         echo $form->input('User.awards', array('label' => __('Certificates, diplomas or other awards', true).':', 'type' => 'textarea', 'div' => 'input textarea bigarea'));

      echo '<div class="bblk">';
         echo $form->submit(__('update', true));
      echo '</div>';
                  
      echo $form->end(); 
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>