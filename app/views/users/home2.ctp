<div id="header-intro">
   <div id="header-intro-left">
      <div id="header-intro-left-text">ezbizi is an international deal-making platform with social networking features. </div> <!-- header-intro-text ends -->
   </div> <!-- header-intro-left ends -->
   <div id="header-intro-right">
      
      <?=$html->link(__('Sign Up Free', true), array('controller' => 'pages','action'=>'display','register'), array('class'=>'header-intro-signupfree'))?>
      <?=$html->link(__('Features & Benefits', true), array('controller' => 'pages','action'=>'display','learn_more'), array('class'=>'header-intro-features'))?>
      
   </div> <!-- header-intro-right ends --></div> <!-- header-intro ends -->
</div> <!-- header ends -->



<div id="content" class="content-homepage">
   <div id="homepage-offers"> 
      <div id="homepage-offers-header">
         Latest Offers
      </div> <!-- homepage-offers-header ends -->
      <div id="homepage-demands-items">
         <?php 
         $variable=array(1,2,3,4,5,6,7,8);
         foreach ($variable as $key => $value) {?>
            <a href="#" class="tt"><?php echo $html->image('product'.($value>4? $value-4 : $value).'.jpg', array('class' => ''));?></a>
            <!-- tooltip element -->
            <div class="tooltip"><?php echo $html->image('product'.($value>4? $value-4 : $value).'.jpg', array('class' => ''));?><div class="tt-text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id lacus in nisl lacinia tempor. Praesent congue ipsum et tellus placerat eget luctus quam egestas. Quisque euismod tincidunt turpis, nec faucibus lorem aliquam sed. Morbi in urna et odio bibendum hendrerit non id ligula.
            </div></div>   
         <?}?>
      </div> <!-- homepage-demands-items ends -->
   </div> <!-- homepage-offers ends -->


   <div id="homepage-demands">
      <div id="homepage-demands-header">
         Latest Demands
      </div> <!-- homepage-demands-header ends -->
      <div id="homepage-demands-items">
<?php 
         $variable=array(1,2,3,4,5,6,7,8);
         foreach ($variable as $key => $value) {?>
            <a href="#" class="tt"><?php echo $html->image('product'.($value>4? $value-4 : $value).'.jpg', array('class' => ''));?></a>
            <!-- tooltip element -->
            <div class="tooltip"><?php echo $html->image('product'.($value>4? $value-4 : $value).'.jpg', array('class' => ''));?><div class="tt-text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id lacus in nisl lacinia tempor. Praesent congue ipsum et tellus placerat eget luctus quam egestas. Quisque euismod tincidunt turpis, nec faucibus lorem aliquam sed. Morbi in urna et odio bibendum hendrerit non id ligula.
            </div></div>   
         <?}?>

      </div> <!-- homepage-demands-items ends -->
   </div> <!-- homepage-demands ends -->


</div> <!-- content ends -->




<br /><br />
<script>
  $(document).ready(function() {
      $(".tt").tooltip({ effect: 'slide'});
    });
   
// initialize tooltip
$(".tt").tooltip({
 
   // tweak the position
   offset: [10, 2],
 
   // use the "slide" effect
   effect: 'slide'
 
// add dynamic plugin with optional configuration for bottom edge
}).dynamic({ bottom: { direction: 'down', bounce: true } });
</script>