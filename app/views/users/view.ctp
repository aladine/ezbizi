<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface uProf"><div class="cen">';
   
   $cen_cls = 'uCen';
   $lft_cls = 'uLeft';
   
   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'uCen uCenS';
      $lft_cls = 'uLeft uLeftS';
      echo '<div class="uRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   else {   
      echo '<div class="'.$lft_cls.'">';   
         echo $this->element('user_view');    
      echo '</div>';
   }
   
   echo '<div class="'.$cen_cls.'">';
   
      echo '<div class="top">';
           
      //---> PERSONAL STATUS
   
      if(!empty($user['User']['status'])) {
      echo '<div class="rB g_B w480">';
         echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
         echo '<div class="mid">';
            echo $html->div('status', $html->tag('span', $user['User']['status']));
         echo '</div>';
         echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
      echo '</div>';
      }
          
      echo '</div>';
      
      //---> PERSONAL INTEREST
   
      echo '<div class="modP rad5 perI">';
         
         if ($user['User']['id'] == $session->read('Auth.User.id') || !empty($user['User']['interest']))
            echo $html->div('headline hlB hlIn', __('Personal and professional Interests', true));
         
         if(!empty($user['User']['summary'])) {
            echo '<div class="it rad3">';
               echo $html->div('lab',__('Personal summary', true));
               echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($user['User']['summary'])));
            echo '</div>';
         }
         
         if(!empty($user['User']['specialization'])) {
            echo '<div class="it rad3">';
               echo $html->div('lab',__('Professional specialization', true));
               echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($user['User']['specialization'])));
            echo '</div>';
         }
         
         if(!empty($user['User']['interest'])) {
            echo '<div class="it rad3">';
               echo $html->div('lab',__('Personal interests (Hobbies)', true));
               echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($user['User']['interest'])));
            echo '</div>';
         }
         
         if(!empty($user['User']['experience'])) {
            echo '<div class="it rad3">';
               echo $html->div('lab',__('Work experience', true));
               echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($user['User']['experience'])));
            echo '</div>';
         }
         
         if(!empty($user['User']['education'])) {
            echo '<div class="it rad3">';
               echo $html->div('lab',__('Education', true));
               echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($user['User']['education'])));
            echo '</div>';
         }
         
         if(!empty($user['User']['awards'])) {
            echo '<div class="it rad3">';
               echo $html->div('lab',__('Certificates, diplomas or other awards', true));
               echo $html->div('int', str_replace(PHP_EOL, '<br />', linked_text($user['User']['awards'])));
            echo '</div>';
         }
         
         if ($user['User']['id'] == $session->read('Auth.User.id'))
            echo $html->div('edtL', $html->link(__('update profile information', true), array('controller' => 'users', 'action' => 'edit', 'interests')));
            
      echo '</div>';
   
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="uRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   else {
      echo '<div class="'.$lft_cls.'">';   
         echo $this->element('user_view');    
      echo '</div>';
   }

echo '</div></div>';
?>