<?php
function catmenu($cats, & $catlevel, & $html, $lang, $sectors = null) {
   $catlevel++;
   foreach ($cats as $cat) {
      if ($catlevel > 1) {
         echo '<div class="cats lev'.$catlevel.' none">'; 
         echo $html->div('cat', $html->image('icons/plusR.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      else {
         echo '<div class="cats lev'.$catlevel.'">';
         echo $html->div('cat', $html->image('icons/plus.png').$html->tag('span', $cat['Category']['name_'.$lang], array('class' => 'name')));
      }
      if (!empty($cat['children']))
         catmenu($cat['children'], $catlevel, $html, $lang, $cat['Sector']);
      else if (!empty($cat['Sector'])) {
         echo '<div class="cats lev'.($catlevel+1).' none">';  
         foreach ($cat['Sector'] as $sector) {
            echo $html->div('cat sec', $html->image('icons/sel.png').$html->tag('span', $sector['id'], array('class' => 'id none')).$html->tag('span', $sector['name_'.$lang], array('class' => 'name')));
         } 
         echo '</div>';
      }
      echo '</div>';
   }
   $catlevel--;
}

if (!empty($this->data['Company']['region_id']))
   $reg_id = $this->data['Company']['region_id'];
else
   $reg_id = 0;


echo $this->element('belt_register');

echo '<div id="main">';
   echo '<div class="cen regForm">';
      echo $form->create('User', array('action' => 'register'));
         
         echo '<div>';

         if (isset($user_card))
            echo '<div class="blk swiBlk none">';
         else
            echo '<div class="blk swiBlk">';
         
         echo $html->div('tit', $html->tag('span',__('Company information', true)).$html->image('icons/titA.png'));
         
         echo '<div class="req">';
         echo $form->input('Company.name', array('label' => __('Company name', true).':'));
         
        /// echo $form->input('Company.address1', array('label' => __('Company address', true).':'));
        /// echo $form->input('Company.address2', array('label' => ' ')); 
         //echo $form->input('Company.address3', array('label' => ' '));
         echo '</div>';
         /*
         echo $form->input('Company.zip', array('label' => __('Zip code', true).':', 'div' => 'input text short80'));
         */
         echo '<div class="req">';
         echo $form->input('Company.city', array('label' => __('City', true).':'));
         echo $form->input('Company.country_id', array('label' => __('Country', true).':', 'type' => 'select', 'empty' => ' - '.__('Select your country', true).' - '));           
         echo '</div>';
         /*
         echo $form->input('Company.region_id_temp', array('label' => '', 'type' => 'select', 'empty' => ' - '.__('Select your county', true).' - ', 'options' => $regions, 'div' => 'input select none'));
         
         echo '<br /><br />';
         */
         echo '<div class="sectorBlk req">';
            echo $form->input('Company.sector_id', array('label' => $html->tag('span', __('Business category', true).':').$html->image('icons/info.png', array('class' => 'iR infoIcon', 'id' => 'sector_text')), 'type' => 'text', 'div' => 'input select sectorInput'));        
            echo $form->input('Company.sector_text', array('label' => '', 'type' => 'text', 'default' => ' - '.__('Select your business category', true).' - ', 'div' => 'input text sectorText'));
         echo '</div>';
         if (!empty($categories)) {
            echo '<div id="secSel" class="none">';
               $catlevel = 0;          
               catmenu($categories, $catlevel, $html, $lang);
            echo '</div>';
         }
         /*
         echo '<div class="compSize req">';
            echo $form->input('Company.size', array('label' => __('Company size', true).':', 'type' => 'text'));
            echo $form->input('Company.TextSize', array('autocomplete' => 'off', 'label' => '', 'value' => ' - '.__('Type in or select a size', true).' - ', 'type' => 'text', 'div' => 'input text compSizeTextInput'));                       
            echo '<div class="options">';
               if(!empty($sizes)) {
                  foreach ($sizes as $ind => $size)
                     echo $html->div($ind, $size);
               }
            echo '</div>';
            echo $html->div('arr', $html->image('icons/selectorA.png'));
         echo '</div>';*/
         /*
         echo '<div class="req line2">';
         echo $form->input('Company.type_id', array('label' => $html->tag('span',__('Company account type', true).':').$html->image('icons/info.png', array('class' => 'iR infoIcon', 'id' => 'account_text')), 'type' => 'select', 'empty' => ' - '.__('Select', true).' - '));
         echo '</div>';
         echo '<div class="public">';
            echo $form->input('Company.public_sector', array('label' => $html->tag('span',__('Public sector', true)), 'type' => 'checkbox', 'after' => $html->image('icons/info.png', array('class' => 'iR infoIcon', 'id' => 'public_text'))));
         echo '</div>';
         
         echo '<br /><br />';
         
         echo $form->input('Company.est_date', array('label' => __('Date of establishment', true).':', 'type' => 'date', 'dateFormat' => 'DMY', 'monthNames' => false, 'minYear' => '1900', 'maxYear' => date('Y'), 'separator' => $html->tag('span', ' - ', array('class' => 'sep')))); 
         echo '<div class="line2">';
            echo $form->input('Company.regnum', array('label' => $html->tag('span', __('Business registration number', true).':', array('class' => 'vi bold')).$html->image('icons/info.png', array('class' => 'iR infoIcon', 'id' => 'regnum_text'))));
         echo '</div>';
         echo '<div class="double req">';
            echo $form->input('Company.phone1', array('label' => __('TEL', true).':', 'div' => 'input text code'));
            echo $form->input('Company.phone2', array('label' => '', 'div' => 'input text phone'));
         echo '</div>';         
         echo '<div class="double">';
            echo $form->input('Company.fax1', array('label' => __('FAX', true).':', 'div' => 'input text code'));
            echo $form->input('Company.fax2', array('label' => '', 'div' => 'input text phone'));
         echo '</div>'; 
         echo $form->input('Company.web', array('label' => __('Company website', true).':'));
         */

         echo '</div>';

         if (isset($user_card))
            echo '<div class="blk swiBlk">';
         else
            echo '<div class="blk swiBlk none">';
         echo $html->div('tit', $html->tag('span',__('Select your company name', true)).$html->image('icons/titA.png'));
         
         echo '<div class="compSel">';
            echo $form->input('User.company_id', array('label' => '', 'type' => 'text', 'div' => 'input text compSelInp'));
            echo '<div class="findB">';
               echo $form->input('User.find', array('label' => '', 'div' => 'input text find', 'default' => __('Type in your company name ...', true), 'after' => $html->image('icons/loupe.png', array('class' => 'loupe'))));  
            echo '</div>';
         echo '</div>';         
         
         echo '<br />';
         echo $html->div('tit', $html->tag('span',__('Found companies', true)).$html->image('icons/titA.png'));
         
         echo '<div id="results" class="results">';
            echo $this->element('companies');  
         echo '</div>'; 
         
         echo '</div>';  
         
         //echo '<div class="blk usrBlk none">';
         if (isset($user_card))
            echo '<div class="blk swiBlk" id="user_data">';
         else 
            echo '<div class="blk swiBlk none" id="user_data">';
         
         echo $html->div('tit', $html->tag('span',__('Enter your personal information', true)).$html->image('icons/titA.png'));
         
         $write_time = encrypt(time());
         echo $form->input('User.time', array('type' => 'hidden', 'value' => $write_time));                   
         echo $form->input('User.e_name', array('label' => '', 'type' => 'text', 'div' => 'input text none'));
         echo $form->input('User.user_card', array('type' => 'hidden'));
         
       ///  echo $form->input('User.title', array('label' => __('Title', true).':', 'div' => 'input text short80'));     
         echo '<div class="req">';
         echo $form->input('User.first_name', array('label' => __('First name', true).':'));
         echo '</div>';
        /// echo $form->input('User.middle_name', array('label' => __('Middle name', true).':'));
  	      echo '<div class="req">';
         echo $form->input('User.last_name', array('label' => __('Family name', true).':'));
         ///echo $form->input('User.job_title', array('label' => __('Job title', true).':'));
         echo $form->input('User.email', array('label' => __('E-mail', true).':'));
         echo '</div>';
         echo '<br />';
         ///Must save email.log as true
         ///echo $form->input('User.email_log', array('label' => __('Use my email as my login', true), 'type' => 'checkbox'));
         echo '<div class="req">';
         //echo $form->input('User.login', array('label' => __('Login', true).':'));
         echo $form->input('User.psword', array('label' => __('Password', true).':'));
  	      echo $form->input('User.passwd', array('label' => __('Re-enter password', true).':')); 
         echo '</div>';
         echo $html->div(null, '', array('id' => 'passStrong'));
         echo '<br />';
         /*
         echo '<div class="double">';
            echo $form->input('User.phone1', array('label' => __('TEL', true).':', 'div' => 'input text code'));
            echo $form->input('User.phone2', array('label' => '', 'div' => 'input text phone'));
         echo '</div>';
         echo '<div class="double">';
            echo $form->input('User.fax1', array('label' => __('FAX', true).':', 'div' => 'input text code'));
            echo $form->input('User.fax2', array('label' => '', 'div' => 'input text phone'));
         echo '</div>';
         
         echo $form->input('User.sex_id', array('label' => __('Gender', true).':', 'type' => 'select', 'empty' => ' - '.__('Select your gender', true).' - '));  
         */
         echo '<div class="req">';
         if ($lang == 'ja')
            $terms = $html->link(__('ご利用規約', true), array('controller' => 'sites', 'action' => 'view', 'terms-and-conditions-of-use'), array('target' => '_blank')).' '.$html->tag('span', __('に同意する', true));         
         else
            $terms = $html->tag('span', __('I understand and accept the', true)).' '.$html->link(__('Terms and Conditions of Use', true), array('controller' => 'sites', 'action' => 'view', 'terms-and-conditions-of-use'), array('target' => '_blank'));        
         echo $form->input('User.terms', array('label' => $terms, 'type' => 'checkbox')); 
         echo '</div>';
         
         echo '</div>';
         
         //registration info                                                                 
         echo '<div class="regInf">';
            echo '<div class="bubble rad3">';               
               
               if ($lang == 'en')
                  echo $this->element('register_en');
               if ($lang == 'ja')
                  echo $this->element('register_ja');
                              
               echo $html->image('bg/regBubArr.png', array('class' => 'bubA'));
               echo $html->image('bg/bubiMascotR.png', array('class' => 'bubM'));
            echo '</div>';   
         echo '</div>';
                 
         echo '</div>';
         
         if (isset($user_card)) {
            echo '<div class="bblk" id="regt">';
               echo $form->submit(__('register now', true));
            echo '</div>';
            echo '<div class="bblk none" id="next">';
               echo '<div class="submit">';
                  echo $html->div('butt blB', $html->tag('span', '', array('class' => 'l')).$html->image('bg/buttA.png').$html->tag('span', __('proceed with the registration', true), array('class' => 't')).$html->tag('span', '', array('class' => 'r')));
               echo '</div>';
            echo '</div>'; 
         }
         else {
            echo '<div class="bblk none" id="regt">';
               echo $form->submit(__('register now', true));
            echo '</div>';
            echo '<div class="bblk" id="next">';
               echo '<div class="submit">';
                  echo $html->div('butt blB', $html->tag('span', '', array('class' => 'l')).$html->image('bg/buttA.png').$html->tag('span', __('proceed with the registration', true), array('class' => 't')).$html->tag('span', '', array('class' => 'r')));
               echo '</div>';
            echo '</div>';      
         }
                  
      echo $form->end();  
   echo '</div>';

echo '</div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
   
   var compSizeHov = false;
   
   setRegionSel();
   publicSecCheck();

   //region selector
   $('#CompanyCountryId').change(function(){
      setRegionSel();
   });

   //public sector checkbox
   $('#CompanyPublicSector').change(function() {
      publicSecCheck();   
   });
   
   $('#CompanySectorText').click(function() {
      $(this).val('');
      $('#secSel').show();   
   });
            
   $('#UserFind').click(function() {
         $(this).val('');
   });
   
   //company size------>
   if($('#CompanySize').val() != '')
      $('#CompanyTextSize').val($('#CompanySize').val());
   if($('#CompanySize').val() < 0)
      $('#CompanyTextSize').val($('.compSize .options').find('.'+$('#CompanySize').val()).html());
   
   $('.compSize .options').bind('mouseenter', function(){
      compSizeHov = true;   
   }).bind('mouseleave', function(){
      compSizeHov = false;   
   });
   //show or hide options          
   $('#CompanyTextSize, .compSize .arr').bind('click', function(){
      $(this).val('');
      $('.compSize .options').toggle();      
   }).bind('focusout', function(){
      if(!compSizeHov)
         $('.compSize .options').hide();
   });
   //write size
   $('#CompanyTextSize').bind('change', function(){
      $('#CompanySize').val($(this).val());
   }) 
   //select size
   $('.compSize .options div').bind('click', function(){
      $('#CompanyTextSize').val($(this).html());
      $('.compSize .options').hide();
      $('#CompanySize').val($(this).attr('class'));
   });
          
   //<------
   
   //onload register type
   var load_reg_type = '<? if (isset($user_card)) echo '1'; else echo '0'; ?>';
   $('#UserUserCard').val(load_reg_type);
   //onload if exist error
   var load_reg_error = '<? if (!empty($this->validationErrors)) echo '1'; else echo '0'; ?>'; 
   if (load_reg_error == '1') {
      $('#next').hide();
      $('#regt, #user_data').show();
    }    
   
   //register form switch
   $('#belt .register .swi').bind('click', function(){
      if(!$(this).hasClass('act')) {
        $('#belt .register .swi').removeClass('act');
        $(this).addClass('act');
        $('#main .swiBlk').toggle();
        if($(this).hasClass('usr')) { 
           $('#UserUserCard').val('1');
           $('#next').hide();
           
           $('#regt, #user_data').show();          
        } 
        else { 
           $('#UserUserCard').val('0');
           $('#next').show();
           $('#regt, #user_data').hide();
        }
        
        $('.regInf').find('.elm:visible').hide();
        $('#welcome_text_bub').show();
          
      }     
   });
   
   //next
   $('#next').bind('click', function(){ 
      $(this).hide();
      $('#regt, #user_data').show(); 
   });
   
   //info icons click
   $('.infoIcon, .infoLink').bind('click', function(){ 
      $('.regInf').find('.elm:visible').hide();
      $('.regForm').find('#' + $(this).attr('id') + '_bub').show();   
   });
   
   //password strength
   $('#UserPsword').password_strength({container: '#passStrong', minLength: 6});
   
   //use email as login
   $('#UserEmailLog').change(function() {
      if ($('#UserEmailLog:checked').val() == '1')    
         $('#UserLogin').val($('#UserEmail').val());
      else
         $('#UserLogin').val('');  
   });
   
   //bussines sector select
   $('#secSel .cat').click(function() {
      var $parCat = $(this).parent();
      var path = '';
      //if exists child
      if($parCat.find('> .cats').exists()){
         $parCat.find('> .cats').toggle();   
      }
      else {
         if ($(this).hasClass('sec')) {
            $('#CompanySectorId').val($(this).find('.id').html());
            while ($parCat.parent().find('> .cat').exists()) {
               path = $parCat.parent().find('> .cat .name').html().replace("&amp;", "&")+' > ' + path;
               $parCat = $parCat.parent();               
            }
            $('#CompanySectorText').val(path + $(this).find('.name').html().replace("&amp;", "&"));
            $('#secSel').hide();
         }   
      }   
   });

   $('#UserFind').keypress(function(event) {
      $.ajax({url: '<?php echo $html->url(array('controller' => 'companies', 'action' => 'search')); ?>/'+$(this).val()+String.fromCharCode(event.which)+'/companies',
           success: function(data){$('#results').html(data);}});
   });
});

function setRegionSel(refresh) {
   var cntId = $('#CompanyCountryId option:selected').attr('value');
   var regId = <?echo $reg_id;?>; 
   if (cntId != "" && $('#CompanyRegionIdTemp optgroup[label="'+cntId+'"]').exists()) {
      $('#CompanyRegionId').html('<option value><? echo ' - '.__('Select your county', true).' - '; ?></option>'+$('#CompanyRegionIdTemp optgroup[label="'+cntId+'"]').html());        
      if(regId != 0)
         $('#CompanyRegionId option[value="'+regId+'"]').attr('selected', 'selected');   
      $('#CompanyRegionId').removeAttr('disabled');
   }
   else {
      $('#CompanyRegionId').html('<option value><? echo ' - '.__('Select your county', true).' - '; ?></option>');
      $('#CompanyRegionId').attr('disabled', 'disabled');
   }  
}

function publicSecCheck() {
   if ($('#CompanyPublicSector:checked').val() == '1') {     
      $('#CompanySectorText, #CompanyRegnum').attr('disabled', 'disabled');
      $('#secSel').hide();
   }        
   else  
      $('#CompanySectorText, #CompanyRegnum').removeAttr('disabled');       
}
</script>