<?php
echo $this->element('belt_setting');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('setting_my');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Update my personal information', true));
      
      echo '<div class="form">';
      echo $form->create('User', array('action' => 'edit'));

      echo '<div>'; 
         echo '<div class="blk">';         
            echo $form->input('User.title', array('label' => __('Title', true).':', 'div' => 'input text short80'));     
            echo '<div class="req">';
            echo $form->input('User.first_name', array('label' => __('First name', true).':'));
            echo '</div>';
            echo $form->input('User.middle_name', array('label' => __('Middle name', true).':'));
  	         echo '<div class="req">';
            echo $form->input('User.last_name', array('label' => __('Last name', true).':'));
            echo '<br />';
            echo $form->input('User.job_title', array('label' => __('Job title', true).':'));
            echo $form->input('User.email', array('label' => __('E-mail', true).':'));
            echo '</div>';
            echo '<br />';
            echo $form->input('User.email_log', array('label' => __('Use my email as my login', true), 'type' => 'checkbox'));
            echo '<div class="req">';
            echo $form->input('User.login', array('label' => __('Login', true).':'));
            echo $form->input('User.psword', array('label' => __('Password', true).':'));
  	         echo $form->input('User.passwd', array('label' => __('Re-enter password', true).':')); 
            echo '</div>';
            echo $html->div(null, '', array('id' => 'passStrongEdit'));
            echo $form->input('User.email_ban', array('label' => __('Don\'t send me e-mails from Ezbizi', true), 'type' => 'checkbox'));
         echo '</div>';
         echo '<div class="blk">';
            echo '<div class="double">';
               echo $form->input('User.phone1', array('label' => __('TEL', true).':', 'div' => 'input text code'));
               echo $form->input('User.phone2', array('label' => '', 'div' => 'input text phone'));
            echo '</div>';
            echo '<div class="double">';
               echo $form->input('User.fax1', array('label' => __('FAX', true).':', 'div' => 'input text code'));
               echo $form->input('User.fax2', array('label' => '', 'div' => 'input text phone'));
            echo '</div>';
            echo $form->input('User.sex_id', array('label' => __('Gender', true).':', 'type' => 'select', 'empty' => ' - '.__('Select your gender', true).' - '));           
            echo '<br />';
            echo $form->input('User.birthday', array('label' => __('Birthday', true).':', 'dateFormat' => 'DMY', 'id' => 'bday', 'minYear' => '1950', 'maxYear' => date('Y'), 'empty' => ' - ', 'separator' => null));
            
            echo $form->input('User.web', array('label' => __('Website', true).':'));
            echo $form->input('User.skype', array('label' => __('Skype', true).':', 'before' => $this->Html->image('socmedia/small/skype.png', array('class' => 'soci')), 'div' => 'input text socl'));
            
            // Napevno alebo cez foreach? 
            //$socials = array('facebook' => 'Facebook', 'linkedin' => 'LinkedIn', 'twitter' => 'Twitter', 'google+' => 'Google+', 'myspace' => 'Myspace', 'Foursquare' => 'foursquare', 'reddit' => 'Reddit', 'stumble' => 'StumbleUpon', 'quora' => 'Quora', 'tumblr' => 'Tumblr', 'flickr' => 'Flickr', 'picasa' => 'Picasa', 'etsy' => 'Etsy', 'rss' => 'RSS');
            
            echo $form->input('User.facebook', array('label' => __('Facebook', true).':', 'before' => $this->Html->image('socmedia/small/facebook.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.linkedin', array('label' => __('LinkedIn', true).':', 'before' => $this->Html->image('socmedia/small/linkedin.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.twitter', array('label' => __('Twitter', true).':', 'before' => $this->Html->image('socmedia/small/twitter.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.google', array('label' => __('Google+', true).':', 'before' => $this->Html->image('socmedia/small/google.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.myspace', array('label' => __('Myspace', true).':', 'before' => $this->Html->image('socmedia/small/myspace.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.foursquare', array('label' => __('Foursquare', true).':', 'before' => $this->Html->image('socmedia/small/foursquare.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.reddit', array('label' => __('Reddit', true).':', 'before' => $this->Html->image('socmedia/small/reddit.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.stumble', array('label' => __('StumbleUpon', true).':', 'before' => $this->Html->image('socmedia/small/stumble.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.quora', array('label' => __('Quora', true).':', 'before' => $this->Html->image('socmedia/small/quora.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.tumblr', array('label' => __('Tumblr', true).':', 'before' => $this->Html->image('socmedia/small/tumblr.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.flickr', array('label' => __('Flickr', true).':', 'before' => $this->Html->image('socmedia/small/flickr.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.picasa', array('label' => __('Picasa', true).':', 'before' => $this->Html->image('socmedia/small/picasa.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.etsy', array('label' => __('Etsy', true).':', 'before' => $this->Html->image('socmedia/small/etsy.png', array('class' => 'soci')), 'div' => 'input text socl'));
            echo $form->input('User.rss', array('label' => __('RSS', true).':', 'before' => $this->Html->image('socmedia/small/rss.png', array('class' => 'soci')), 'div' => 'input text socl'));
            
            echo $form->input('User.state_id', array('label' => __('Marital status', true).':', 'type' => 'select', 'empty' => ' - '.__('Select your status', true).' - '));   
         echo '</div>';
      echo '</div>';
         
      echo '<div class="bblk">';
         echo $form->submit(__('update', true));
      echo '</div>';
                  
      echo $form->end(); 
      echo '</div>';
      
   echo '</div>';

echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   

   //password strength
   $('#UserPsword').password_strength({container: '#passStrongEdit', minLength: 6});
   
   //use email as login
   $('#UserEmailLog').change(function() {
      if ($('#UserEmailLog:checked').val() == '1')    
         $('#UserLogin').val($('#UserEmail').val());
      else
         $('#UserLogin').val('');  
   });
   
});
</script>