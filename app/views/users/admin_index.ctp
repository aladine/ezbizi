<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Overview of registered users', true));
      
      echo '<div class="form">';
         if (!empty($users)) {
            foreach ($users as $user) {
               echo '<div class="list rad3">';
                  //50x50  
                  if (!empty($user['User']['avatar']))
                     echo $html->div('img', $image->resize('users/'.$user['User']['id'].'/'.$user['User']['avatar'], 50, 50));
                  else        
                     echo $html->div('img', $html->image('noImage50.png'));
                  echo '<div class="body">';
                     echo '<div class="top">';
                        $name = '';
                        if (!empty($user['User']['title']))
                           $name .= $user['User']['title'].' ';
                        $name .= $user['User']['first_name'];
                        if (!empty($user['User']['middle_name']))
                           $name .= ' '.$user['User']['middle_name'];
                        $name .= ' '.$user['User']['last_name'];
                        echo $html->link($name, array('controller' => 'users', 'action' => 'view', $user['User']['id']), array('class' => 'link'));                 
                        if ($user['User']['group_id'] == 2)
                           echo $html->tag('span', __('company admin', true), array('class' => 'ind rad3 iR10 geBg'));
                     echo '</div>';
                     echo '<div class="col">';
                        echo $html->div('it', $html->div('at', __('E-mail', true).':').$html->div('vl', $user['User']['email']));
                        echo $html->div('it', $html->div('at', __('Job title', true).':').$html->div('vl', $user['User']['job_title']));
                     echo '</div>';
                     echo '<div class="col">';
                        if (!empty($user['User']['phone2']))
                           echo $html->div('it', $html->div('at', __('Phone', true).':').$html->div('vl', $user['User']['phone1'].' '.$user['User']['phone2']));
                        if (!empty($user['User']['fax2']))
                           echo $html->div('it', $html->div('at', __('Fax', true).':').$html->div('vl', $user['User']['fax1'].' '.$user['User']['fax2']));
                        if (!empty($user['Sex']['name_'.$lang]))
                           echo $html->div('it', $html->div('at', __('Gender', true).':').$html->div('vl', $user['Sex']['name_'.$lang]));
                     echo '</div>';
                     echo '<div class="col btns">';
                        echo $html->div('it', $html->link(__('show company', true), array('controller' => 'companies', 'action' => 'view', $user['User']['company_id']), array('class' => 'bt')));
                     echo '</div>';
                  echo '</div>';
                  echo $html->link(__('remove', true), array('controller' => 'users', 'action' => 'delete', $user['User']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->div('noData', __('Users list is empty', true));   

      echo '</div>';
      
      if (!empty($users)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      } 
      
   echo '</div>';

echo '</div></div>';
?>