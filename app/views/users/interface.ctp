<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface"><div class="cen">';
   echo '<div class="iCen">';
      
      echo '<div class="top">'; 
      
      //---> STATUS
      
      echo '<div class="rB g_B w480 float">';
         echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
         echo '<div class="mid">';
            if(!empty($user['status']))
               echo $html->div('status', $html->tag('span', $user['status']));
            echo '<div class="statAdd">';         
               echo $form->create('User', array('action' => 'home'));
                  echo $form->input('User.status', array('label' => ''));
                  echo $html->div('def', __('Type in your new status ...', true), array('id' => 'UserStatusDef'));
                  echo $form->submit(__('submit', true));
               echo $form->end();
            echo '</div>';
         echo '</div>';
         echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
      echo '</div>';
      
      //---> ADD DEMAND

      if (in_array($company['Company']['type_id'], array(1, 3))) {
         echo '<div class="rB z_B w200 float ml20 h55 bs_b">';
            echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
            echo '<div class="mid">';
               echo $html->link(__('Click here', true).'<br />'.__('to post a new demand', true), array('controller' => 'demands', 'action' => 'add'), array('escape' => false, 'class' => 'btext'));
            echo '</div>';
            echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
         echo '</div>'; 
      }
      else {
         echo '<div class="rB z_B w200 float ml20 h55 bs_b">';
            echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
            echo '<div class="mid">';
               echo $html->link(__('Search', true).'<br />'.__('other demands', true), array('controller' => 'demands', 'action' => 'index'), array('escape' => false, 'class' => 'btext'));
            echo '</div>';
            echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
         echo '</div>'; 
      }
      
      echo '</div>';
      
      //---> LATEST DEMANDS
      
      if (in_array($company['Company']['type_id'], array(2, 3))) {
         echo '<div class="feeds latDems">'; 
         
            echo '<div class="modP rad5">';
            echo $html->div('headline hlB hlIn', __('Latest demands of your business interest', true));
            if (!empty($demands)) {
               foreach ($demands as $demand) {
                  echo '<div class="feed">';
                     echo '<div class="body">';
                        echo $html->image('icons/f_dem.png', array('class' => 'iL'));
                        echo $html->tag('span', $demand['Sector']['name_'.$lang].':', array('class' => 'sector')).'&nbsp;';
                        if (empty($demand['Demand']['priceper_id'])) {
                           $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                           if ($lang == 'ja')
                              $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                        }
                        else {
                           $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                           if ($lang == 'ja')
                              $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                        }   
                        echo $html->link($dtitle, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'], $dtitle), array('class' => 'link', 'escape' => false));               
                        echo $html->tag('span', rel_time($demand['Demand']['created']), array('class' => 'date'));
                     echo '</div>';
                  echo '</div>';
               }
               echo $html->div('edtL', $html->link(__('more demands', true), array('controller' => 'demands', 'action' => 'index')));
            }
            else {
               echo $html->div('noData', __('Unfortunately, at the moment there aren\'t any demands in our database matching your desired criteria.', true));
               if (in_array($session->read('Auth.User.group_id'), array(1, 2))) {
                  echo $html->div('edtL', $html->link(__('select your business interest', true), array('controller' => 'companies', 'action' => 'edit', 'business-area')));
               }
            }
            echo '</div>';
         echo '</div>';
      }
      
      //---> FEEDS
      
      echo '<div class="feeds modP rad5">'; 
      
         echo $html->div('headline hlB hlIn', __('Newsfeed', true));
         
         if (!empty($notifications)) {
            echo $this->element('feeds');
            echo $html->div('edtL', $html->link(__('more feeds', true), array('controller' => 'notifications', 'action' => 'index')));
         }
         else
            echo $html->div('noData', __('Currently there are no news to display', true));
         
      echo '</div>';
           
   echo '</div>';
   echo '<div class="iRight">';
      echo $this->element('company_interface_right');
   echo '</div>';
   
echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
   
   $('#UserStatus').val('');
   
   //status input
   if($('#UserStatus').val() != '')
      $('#UserStatusDef').hide();   
      
   $('#UserStatus').bind('focus', function(){
      $('#UserStatusDef').hide();   
   }).bind('focusout', function() {
      if($('#UserStatus').val() == '')
         $('#UserStatusDef').show();
   });
   
   $('#UserStatusDef').bind('click', function(){
      $(this).hide();
   });   

});

</script>