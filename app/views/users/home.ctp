<?php
echo $this->element('belt_home');
echo '<div id="promo"><div class="cen home">';
      
      echo '<div class="buble rad5">';
         echo $html->div('tit', __('Why to sign up?', true));      
         echo $html->div('txt', '<b>Ezbizi</b>&nbsp;'.__('is an international deal-making platform with social networking features. It is designed for small-medium sized companies to build lasting business relationships more effectively. Ezbizi will help you generate targeted business leads and connect you to a bustling community of buyers, sellers and traders. Find this and more in one neat, trustworthy place!', true));
      echo '</div>';
      echo $html->image('bg/bubbleA.png', array('class' => 'bubA'));
      
      echo $html->image('bg/mascot.png', array('class' => 'mascot'));
      
      echo $html->link(__('Learn more', true), array('controller' => 'pages', 'action' => 'display', 'learn_more'), array('class' => 'butt2 geB2 rad5 more')); 
      echo $html->link(__('Sign up', true), array('controller' => 'pages', 'action' => 'display', 'register'), array('class' => 'butt2 blB2 rad5 sign')); 

echo '</div></div>';

echo '<div id="main" class="home"><div class="cen">';
   if (!empty($categories)) {
      echo $html->div('tit', $html->tag('span',__('Browse demands by business category', true)).$html->image('icons/titA.png'));   
      
      echo '<div class="path none" id="hPath">';
         echo $html->div('it rad3 geBg', __('Home', true), array('id' => 'pHome'));
         echo $html->div('it none', $html->image('icons/cpSep.png'), array('id' => 'pLev2s'));
         echo $html->div('it rad3 blBg none', '', array('id' => 'pLev2'));
         echo $html->div('it none', $html->image('icons/cpSep.png'), array('id' => 'pLev3s'));
         echo $html->div('it rad3 viBg none', '', array('id' => 'pLev3'));
      echo '</div>';
      
      echo '<div class="cats" id="hCat">';
       
      $count = count($categories);
      $maxcount = ceil($count/3);
      $new_box = true;
      $counter = 0;
      $color = 0;
      
      foreach ($categories as $category) {
         $counter++;
         if ($new_box) {
            echo '<div class="catb">';
            $new_box = false;
            $color ++;
         }
         switch ($color) {
            case 1: $clr_cls = 'cat1'; break;
            case 2: $clr_cls = 'cat2'; break;
            case 3: $clr_cls = 'cat3'; break;
         }       
         echo '<div class="ct">';                                    //tu namiesto indexu radsej id categorie potom
            echo $html->div('cat rad5 '.$clr_cls, $html->image('cats/'.$category['Category']['id'].'.png', array('class' => 'ico')).$html->tag('span', $category['Category']['name_'.$lang], array('class' => 'name')));
            
            //--> LEVEL 2 START
            if (!empty($category['children'])) {
            echo '<div class="pan rad5 none">';

               $count2 = count($category['children']);
               $maxcount2 = ceil($count2/2);
               $new_box2 = true;
               $counter2 = 0;
               $color2 = 0;
               
               foreach ($category['children'] as $children) {
                  
                  $counter2++;  
                  if ($new_box2) {
                     echo '<div class="catb2">';
                     $new_box2 = false;
                  }
                  
                  $color2++;
                  if ($color2 == 3)
                     $color2 = 1;    
                  switch ($color2) {
                     case 1: $clr_cls2 = 'bl'; break;
                     case 2: $clr_cls2 = 'dbl'; break;
                  }
                  
                  echo '<div class="ct">';
                     echo $html->div('scat '.$clr_cls2, $children['Category']['name_'.$lang]);
                     
                     //--> LEVEL 3 START
                     if (!empty($children['Sector'])) {
                     echo '<div class="tPan none">';

                        $count3 = count($children['Sector']);
                        $maxcount3 = ceil($count3/2);
                        $new_box3 = true;
                        $counter3 = 0;
                        $color3 = 0;
               
                        foreach ($children['Sector'] as $sector) {
                  
                           $counter3++;  
                           if ($new_box3) {
                              echo '<div class="catb2">';
                              $new_box3 = false;
                           }
                  
                           $color3++;
                           if ($color3 == 3)
                              $color3 = 1;    
                           switch ($color3) {
                              case 1: $clr_cls3 = 'vi'; break;
                              case 2: $clr_cls3 = 'dvi'; break;
                           }
                  
                           echo $html->div(null, $html->link($sector['name_'.$lang], array('controller' => 'demands', 'action' => 'display', normalize($category['Category']['name_'.$lang]), normalize($children['Category']['name_'.$lang]), normalize($sector['name_'.$lang]), 's', $sector['id']), array('class' => 'sec '.$clr_cls3)));                     
                  
                           if ($counter3 == $maxcount3) {
                              echo '</div>';
                              $counter3 = 0;
                              $new_box3 = true;
                           }
                        }
                        if (!$new_box3)
                           echo '</div>';

                     echo '</div>';
                     }
                     //--> LEVEL 3 END
                     
                  echo '</div>';
                  
                  if ($counter2 == $maxcount2) {
                     echo '</div>';
                     $counter2 = 0;
                     $new_box2 = true;
                  }
               }
               if (!$new_box2)
                  echo '</div>';
                  
            echo '</div>';
            }
            
            //--> LEVEL 2 END
            
         echo '</div>';
         if ($counter == $maxcount) {
            echo '</div>';
            $counter = 0;
            $new_box = true;
         }          
      }

      if (!$new_box)
         echo '</div>';
      echo '</div>';
   }
   
   if (!empty($demands)) {
      echo $html->div('tit', $html->tag('span',__('Latest demands', true)).$html->image('icons/titA.png'));
   
      echo '<div class="latest rad3">';
         echo '<div class="row head rad3">';
            echo $html->div('col off', __('Demand', true));
            echo $html->div('col grp', __('Industry group', true));
            echo $html->div('col ent', __('Entered', true));
            echo $html->div('col loc', __('Location', true));
            echo $html->div('col val', __('Validity', true));
         echo '</div>';
      
         foreach ($demands as $demand) {
            echo '<div class="row body">';
               if (empty($demand['Demand']['priceper_id'])) {
                  $demandt = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $demandt = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                  echo $html->div('col off', $html->link($demandt, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'], $demandt), array('class' => 'bl', 'escape' => false)));
               }
               else {
                  $demandt = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang].'&nbsp;'.__('is needed.', true);
                  if ($lang == 'ja')
                     $demandt = number_format($demand['Demand']['quantity']).' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
                  echo $html->div('col off', $html->link($demandt, array('controller' => 'demands', 'action' => 'view', $demand['Demand']['id'], $demandt), array('class' => 'bl', 'escape' => false)));
               }
               echo $html->div('col grp', $html->tag('span', $demand['Sector']['name_'.$lang]));
               echo $html->div('col ent', $html->tag('span', rel_time($demand['Demand']['created']), array('class' => 'gr')));
               echo $html->div('col loc', $html->image('flags/'.$demand['Country']['id'].'.png').$html->tag('span', $demand['Country']['name_'.$lang], array('class' => 'gr')));
               //100px == 100%
               $px = round(((time() - strtotime($demand['Demand']['valid_from']) + 86400) / (strtotime($demand['Demand']['valid_to']) - strtotime($demand['Demand']['valid_from']) + 86400))*100);
               $cls = '';
               if($px > 33) $cls = 'or_ind';
               if($px > 66) $cls = 'red_ind';
               echo $html->div('col val '.$cls, $html->div('ind rad3', $html->div('rat rad3', '', array('style' => 'width: '.$px.'px;')).$html->div('mask', '')).$html->div('date', date('d.m.Y', strtotime($demand['Demand']['valid_from'])).'<br />'.date('d.m.Y', strtotime($demand['Demand']['valid_to']))));
            echo '</div>';
         }                                                                                                      
   echo '</div>';
   }
      
echo '</div></div>';
?>
<script type="text/javascript">
var ftime = 500;
var vtime = 500;

$(document).ready(function(){   
      
   $path = $('#hPath');
   $category = $('#hCat .cat');
   $subcat = $('#hCat .scat');
   $tpanel = $('#hCat .tPan');
   $panel = $('#hCat .pan');
 
   //main category click
   $category.bind('click', function(){
      $ths = $(this);
      $('#pLev2').html($ths.find('.name').html());
      $category.fadeOut(ftime, function(){$ths.parent().find('.pan').fadeIn(ftime); $path.fadeIn(ftime, function() {$('#pLev2, #pLev2s').fadeIn(ftime);});});
   });
   
   //main sub category click
   $subcat.bind('click', function(){
      $ths = $(this);
      $('#pLev3').html($ths.html());
      $subcat.hide(function(){$ths.parent().find('.tPan').fadeIn(ftime, function(){$('#pLev3, #pLev3s').fadeIn(ftime)});});
   });
   
   //home click
   $('#pHome').bind('click', function(){
      $path.hide(function(){
         $panel.fadeOut(ftime, function(){
            $('#pLev2, #pLev2s').hide();
            $('#pLev3, #pLev3s').hide();
            $tpanel.hide();
            $subcat.show();
            setTimeout('$category.fadeIn(ftime);', vtime);
         });
      });
   });
   
   //plev2 click
   $('#pLev2').bind('click', function(){
       $('#pLev3, #pLev3s').fadeOut(ftime);
       $('#hCat .pan:visible').find('.tPan').hide(function(){$('#hCat .pan:visible .scat').fadeIn(ftime);});
   }); 

});
</script>