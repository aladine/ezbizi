<?php
echo $this->element('belt_interface');
echo '<div id="main" class="iface"><div class="cen">';
   $cen_cls = 'iCen';

   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'iCen iCenS';
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   echo '<div class="'.$cen_cls.'">';
      echo '<div class="list">';
         if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id'))
            echo $html->div('headline hlB', __('Colleagues', true));
         else
            echo $html->div('headline hlB', __('Staff', true));
            
         if (!empty($staff)) {
            foreach ($staff as $colleague) {              
               if (!$colleague['User']['active'])
                  $act_cls = 'unact';
               else
                  $act_cls = '';   
               echo '<div class="bconn rad3 '.$act_cls.'">';
                  
                  if (empty($colleague['User']['avatar']))
                     $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                  else
                     $foto = $image->resize('users/'.$colleague['User']['id'].'/'.$colleague['User']['avatar'], 130, 173, true, true);
                  echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                  
                  if ($colleague['User']['online']) {
                        $last_activity = strtotime($colleague['User']['last_activity']);
                        $now = time();
                        $last_activity = $now - $last_activity;
                        if ($last_activity < 1800)
                           echo $html->image('icons/online.png', array('class' => 'onf'));
                        else
                           echo $html->image('icons/offline.png', array('class' => 'onf'));
                  }
                  else
                     echo $html->image('icons/offline.png', array('class' => 'onf'));
                  
                  echo '<div class="hov">';                     
                     $name = '';
                     if (!empty($colleague['User']['title']))
                        $name .= $colleague['User']['title'].' ';
                     $name .= $colleague['User']['first_name'];
                     if (!empty($colleague['User']['middle_name']))
                        $name .= $colleague['User']['middle_name'];
                     $name .= ' '.$colleague['User']['last_name'];
                     $job = $colleague['User']['job_title'];
                     if (!empty($colleague['Sex']['name_'.$lang]))
                        $job .= ', '.$colleague['Sex']['name_'.$lang];
                     
                     echo $html->tag('span', $name, array('class' => 'name'));
                     echo '<br />';
                     echo $html->tag('span', $job, array('class' => 'job'));
                  echo '</div>';
                  
                  echo $html->link('', array('controller' => 'users', 'action' => 'view', $colleague['User']['id']), array('class' => 'tLink'));
                  if ($colleague['User']['active'])
                     echo $html->div('bt', $html->link(__('send a message', true), array('controller' => 'messages', 'action' => 'add',  $colleague['User']['id']), array('class' => 'rad3 geBg sm')));
                  else {
                     echo $html->div('ia red', __('inactive user', true));
                     echo $html->div('bt', $html->link(__('approve', true), array('controller' => 'users', 'action' => 'approve', $colleague['User']['id']), array('class' => 'rad3 redBg sm')));
                  }
                  if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id'))
                     echo $html->link('x ', array('controller' => 'users', 'action' => 'delete', $colleague['User']['id']), array('class' => 'del', 'escape' => false), __('Are you sure?',true)); 
                  
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('No colleagues registered', true));
         
      echo '</div>';
      
      if (!empty($staff)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   
echo '</div></div>';
?>
