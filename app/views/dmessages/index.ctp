<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      echo $html->div('headline hlB', __('Demand messages', true));
      
      echo '<div class="modP rad5">';
      echo $html->div('headline hlG hlIn', __('Business negotiations', true));
      echo '<div class="list conv">';
      
      if (!empty($dmessages)) {
         foreach ($dmessages as $dmessage) {
            if ($dmessage['Demand']['company_id'] != $company['Company']['id']) {
               $cls = 'itmD3';
               $own = array('id' => __('Your Bid', true), 'bg' => 'viBg');
            }               
            else {
               $cls = '';
               $own = array('id' => __('Your Demand', true), 'bg' => 'blBg');
            }
            echo '<div class="itm itmD '.$cls.'">';
               $new = '';
               if (!empty($dmessage['NEW']))
                  $new = $html->tag('span', __('new', true), array('class' => 'new'));
               $sm = '';
               if ($dmessage['Demand']['user_id'] == $session->read('Auth.User.id'))
                  $sm = $html->image('icons/bl_home.png', array('class' => 'iL'));
               $link = array('controller' => 'dmessages', 'action' => 'index', $dmessage['Demand']['id']);
               if ($dmessage['Demand']['company_id'] != $company['Company']['id']) {
                  $link = array('controller' => 'dmessages', 'action' => 'view', $dmessage['Demand']['id'], $dmessage['Demand']['user_id']);
               }
               echo $html->div('topDate', $html->tag('span', rel_time($dmessage['Dmessage']['created']), array('class' => 'date')).$html->tag('span', $own['id'], array('class' => 'own rad3 '.$own['bg'])));
               
               $dtitle = $dmessage['Demand']['quantity'].' '.$dmessage['Demand']['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$dmessage['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$dmessage['Demand']['price'].' '.$dmessage['Demand']['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
               if ($lang == 'ja')
                  $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
               echo $html->div('top', $new.$sm.$html->link($dtitle, $link, array('class' => 'name', 'escape' => false)));
               if ($dmessage['Demand']['company_id'] != $company['Company']['id'])
                  echo $html->link('x | '.__('remove', true), array('controller' => 'dmessages', 'action' => 'delete', $dmessage['Demand']['id'], $dmessage['Demand']['company_id']), array('class' => 'del del3', 'escape' => false), __('Are you sure?',true));    
            echo '</div>';
         }
      }
      else
         echo $html->div('noData', __('Currently you have no demand messages', true));
      
      echo '</div>';
      echo '</div>';
      
   echo '</div>';
   echo '<div class="mRight iface">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>