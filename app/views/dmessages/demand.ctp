<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag mes"><div class="cen">';
 
   echo '<div class="mCen">';
      $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      if ($lang == 'ja')
         $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
      echo $html->div('headline hlB', __('Demand messages', true).' >> '.$dtitle);
      
      //echo '<div class="modP rad5">';
      echo '<div class="list conv">';
      
      if (!empty($dmessages)) {
         $thr = 0;
         foreach ($dmessages as $dmessage) {
            
            $thr++; if($thr > 3) $thr = 1;
            echo '<div class="itm thr thr'.$thr.'">';
               $new = '';
               if (!$dmessage['Dmessage']['read'] && $dmessage['Dmessage']['sender_id'] != $session->read('Auth.User.id'))
                  $new = $html->tag('span', __('new', true), array('class' => 'new'));
               $user_id = $dmessage['Dmessage']['sender_id'];
               if ($user_id == $session->read('Auth.User.id'))
                  $user_id = $dmessage['Dmessage']['recipient_id'];
               echo $html->tag('top', $new.$html->link(__('Demand negotiation - latest response', true).': ', array('controller' => 'dmessages', 'action' => 'view', $demand['Demand']['id'], $user_id), array('class' => 'name')).$html->tag('span', rel_time($dmessage['Dmessage']['created']), array('class' => 'date')));
            
               $acpt = '';
               if($dmessage['Dmessage']['accepted'] == 1)
                  $acpt = $html->image('icons/accept.png', array('class' => 'iL'));
               if($dmessage['Dmessage']['accepted'] == -2)
                  $acpt = $html->image('icons/no_accept.png', array('class' => 'iL'));
               if($dmessage['Dmessage']['accepted'] == 2)
                  $acpt = $html->image('icons/deal.png', array('class' => 'iL'));
               
               $mes_text = $dmessage['Dmessage']['text'];            
               if(strlen($mes_text) <= 200)
                  echo $html->div('mess', $acpt.$html->tag('span', $mes_text));
               else {
                  $lst = strrpos(substr(strip_tags($mes_text), 0, 200), ' ');
                  echo $html->div('mess', $acpt.$html->tag('span', substr(strip_tags($mes_text), 0, $lst).' ...'));
               }
                   
               echo $html->link('x | '.__('remove', true), array('controller' => 'dmessages', 'action' => 'delete', $demand['Demand']['id'], $user_id), array('class' => 'del del3', 'escape' => false), __('Are you sure?',true));    
            echo '</div>';
         }
      }
      
      echo '</div>';
      //echo '</div>';
      
   echo '</div>';
   echo '<div class="mRight iface">';      
      echo $this->element('messages_menu');     
   echo '</div>';
      
echo '</div></div>';
?>