<?php
echo $html->css('calendar');
echo $html->script(array('jquery.ui.core', 'jquery.ui.datepicker'));

echo $this->element('belt_interface');
echo '<div id="main" class="pag dem"><div class="cen">';
 
   echo '<div class="dCen">';
      $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$lang].'&nbsp;'.__('of', true).'&nbsp;'.$demand['Demand']['commodity'].'&nbsp;'.__('for', true).'&nbsp;'.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'&nbsp;'.__('is needed.', true);
      if ($lang == 'ja')
         $dtitle = $demand['Demand']['quantity'].' '.$demand['Unit']['name_ja'].'  の '.$demand['Demand']['commodity'].' を 1 '.$demand['Unit']['name_ja'].' '.$demand['Demand']['price'].' '.$demand['Currency']['name_ja'].' で  探しています';
      echo $html->div('headline hlB', $dtitle);

      echo '<div class="detail mTop rad5t">';
         echo $html->div('cat', '('.$demand['Sector']['name_'.$lang].')');
         echo '<div class="info">';
            echo $html->div('blk dat', $html->tag('span', __('Date', true).': ', array('class' => 'gr')).$html->tag('span', rel_time($demand['Demand']['created']), array('class' => 'gr')));
            echo $html->div('blk loc', $html->tag('span', __('Location', true).': ', array('class' => 'gr')).$html->image('flags/'.$demand['Country']['id'].'.png', array('class' => 'iL')).$html->tag('span', $demand['Country']['name_'.$lang], array('class' => 'ge')));
            if (empty($demand['Demand']['priceper_id']))
               echo $html->div('blk prc', $html->tag('span', __('Price', true).': ', array('class' => 'gr')).$html->tag('span', $demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang], array('class' => 'bl')));
            else
               echo $html->div('blk prc', $html->tag('span', __('Price', true).': ', array('class' => 'gr')).$html->tag('span', $demand['Demand']['price'].' '.$demand['Currency']['name_'.$lang].'/'.$demand['Priceper']['name_'.$lang], array('class' => 'bl')));
         echo '</div>';
         echo '<div class="info">';
            echo $html->div('tot rad3', $html->tag('span', __('Total demand value', true).': ').$html->tag('span', number_format($demand['Demand']['norm_price']).' '.$demand['Currency']['name_'.$lang], array('class' => 'sum blBg rad3')));         
         echo '</div>';
         echo $html->div('sep', '');
         echo $html->para('dsc', str_replace(PHP_EOL, '<br />', $demand['Demand']['text']));
         //100px == 100%
         $px = round(((time() - strtotime($demand['Demand']['valid_from']) + 86400) / (strtotime($demand['Demand']['valid_to']) - strtotime($demand['Demand']['valid_from']) + 86400))*100);
         $cls = '';
         if($px > 33) $cls = 'or_ind';
         if($px > 66) $cls = 'red_ind';
         echo '<div class="val '.$cls.'">';
            echo $html->div('gr float', __('Validity', true).': ');
            echo $html->div('ind rad3', $html->div('rat rad3', '', array('style' => 'width: '.$px.'px;')).$html->div('mask', ''));
            echo $html->div('dat none', date('d.m.Y', strtotime($demand['Demand']['valid_from'])).' - '.date('d.m.Y', strtotime($demand['Demand']['valid_to'])));
         echo '</div>';             
      echo '</div>';
            
      if (!empty($dmessages)) {
      echo '<div class="reaBlk rad5b">';
         
         foreach ($dmessages as $dmessage) {       
            
            $user_id = $session->read('Auth.User.id');
            if ($dmessage['Dmessage']['sender_id'] == $user_id) {
               $det_cls = 'rea1';
               $mes_ind = $html->image('icons/w_mes_out.png', array('class' => 'mInd'));
               $arr_ind = $html->image('bg/demBlA.png', array('class' => 'reaA'));
            }
            else {
               $det_cls = 'rea2';
               $mes_ind = $html->image('icons/w_mes_in.png', array('class' => 'mInd'));
               $arr_ind = $html->image('bg/demGeA.png', array('class' => 'reaA'));
            }
               
            if ($dmessage['Dmessage']['accepted'] == -2) {
               echo '<div class="rea '.$det_cls.'">';
                  echo '<div class="info">';
                     echo $html->div('blk', $html->tag('span', rel_time($dmessage['Dmessage']['created']), array('class' => 'date')));
                  echo '</div>';
                  echo $html->para('dsc', $html->image('icons/no_accept.png', array('class' => 'iL')).$html->tag('span', $dmessage['Dmessage']['text']));                              
                  echo $mes_ind;
                  echo $arr_ind;
               echo '</div>';
            }
            else if ($dmessage['Dmessage']['accepted'] == 1) {
               echo '<div class="rea '.$det_cls.'">';
                  echo '<div class="info">';
                     echo $html->div('blk', $html->tag('span', rel_time($dmessage['Dmessage']['created']), array('class' => 'date')));
                  echo '</div>';
                  echo $html->para('dsc', $html->image('icons/accept.png', array('class' => 'iL')).$html->tag('span', $dmessage['Dmessage']['text']));                              
                  echo $mes_ind;
                  echo $arr_ind;
               echo '</div>';
               if ($dmessage['Dmessage']['recipient_id'] == $session->read('Auth.User.id')) {
                  echo '<div class="form uForm detForm">';
                     echo '<div class="btns">';
                        echo $html->link(__('accept', true), array('controller' => 'dmessages', 'action' => 'accept', $dmessage['Dmessage']['id']), array('class' => 'btn'));
                        echo $html->link(__('reject', true), array('controller' => 'dmessages', 'action' => 'no_accept', $dmessage['Dmessage']['id']), array('class' => 'btn'));
                     echo '</div>';
                  echo '</div>';
               }
            }
            else if ($dmessage['Dmessage']['accepted'] == 2) {      
               echo '<div class="rea '.$det_cls.'">';
                  echo '<div class="info">';
                     echo $html->div('blk', $html->tag('span', rel_time($dmessage['Dmessage']['created']), array('class' => 'date')));
                  echo '</div>';
                  echo $html->para('dsc', $html->image('icons/deal.png', array('class' => 'iL')).$html->tag('span', $dmessage['Dmessage']['text']));                                
                  echo $mes_ind;
                  echo $arr_ind;
               echo '</div>';
            }
            else {
               echo '<div class="rea '.$det_cls.'">';
                  echo '<div class="info">';
                     echo $html->div('blk', $html->tag('span', rel_time($dmessage['Dmessage']['created']), array('class' => 'date')));
                     if (!empty($dmessage['Dmessage']['valid_to']))
                        echo $html->div('blk', $html->tag('span', __('Valid till', true).': ', array('class' => 'at')).$html->tag('span', date('d.m.Y', strtotime($dmessage['Dmessage']['valid_to'])), array('class' => 'vl')));
                     $ppu = '';
                     if (!empty($dmessage['Dmessage']['priceper_id']))
                       $ppu = '/'.$dmessage['Priceper']['name_'.$lang];
                     echo $html->div('blk', $html->tag('span', __('Bid price', true).': ', array('class' => 'at')).$html->tag('span', $dmessage['Dmessage']['price'].' '.$dmessage['Currency']['name_'.$lang], array('class' => 'vl')));
                     $price = $dmessage['Dmessage']['price'] * $demand['Demand']['quantity'];
                  echo '</div>';

                  echo $html->para('dsc', $dmessage['Dmessage']['text']);                              
                  echo $html->div('tot', $html->tag('span', __('Total bid value', true).': ', array('class' => 'at')).$html->tag('span', number_format($price).' '.$dmessage['Currency']['name_'.$lang], array('class' => 'vl rad3')));                  
                  
                  echo $mes_ind;
                  echo $arr_ind;
               echo '</div>';
            }
            
            if (empty($dmessage['Dmessage']['accepted']) && $dmessage['Dmessage']['recipient_id'] == $session->read('Auth.User.id')) { 
               echo $form->create('Dmessage', array('url' => array('controller' => 'dmessages', 'action' => 'view', $demand['Demand']['id'], $dmessage['Dmessage']['sender_id'])));
               echo $form->input('Dmessage.valid_to', array('type' => 'hidden', 'value' => $dmessage['Dmessage']['valid_to']));
               echo $form->input('Dmessage.message_id', array('type' => 'hidden', 'value' => $dmessage['Dmessage']['id']));
               
               echo '<div class="form uForm detForm mdetForm">';
                  echo '<div class="btns">';
                     echo $html->link(__('accept', true), array('controller' => 'dmessages', 'action' => 'accept', $dmessage['Dmessage']['id']), array('class' => 'btn'));
                     echo $html->div('btn', __('counter offer', true), array('id' => 'cOfferB'));
                     echo $html->link(__('do not accept', true), array('controller' => 'dmessages', 'action' => 'no_accept', $dmessage['Dmessage']['id']), array('class' => 'btn'));
                  echo '</div>';
                  echo '<div class="none" id="cOffer">';
                     //echo $html->div('title', __('Counter offer', true).':');
                     echo '<div class="line">'; 
                        echo $form->input('Dmessage.text', array('label' => __('Text', true).':', 'type' => 'textarea', 'div' => 'input textarea'));
                     echo '</div>';
                     echo '<div class="line double last">';
                        echo $form->input('Dmessage.price', array('label' => __('Price', true).':', 'default' => $dmessage['Dmessage']['price'], 'div' => 'input text quan')).'  '.$demand['Currency']['name_'.$lang].' / '.$demand['Priceper']['name_'.$lang];
                     echo '</div>';
                     echo $form->submit(__('send counter offer', true));
                  echo '</div>';
               echo '</div>';
               
               echo $form->end();
            }
         }      
      echo '</div>';
      
      echo '<div class="pgn">';
         echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
      echo '</div>';
      }
      
   
   echo '</div>';
   echo '<div class="dRight">';     
      if (!empty($vcompany)) {  
         
         echo '<div class="demCon iface rad5">';
            echo $html->link($vcompany['Company']['name'], array('controller' => 'companies', 'action' => 'view', $vcompany['Company']['id']), array('class' => 'comName rad3'));  
   
            echo '<br />';
            echo $html->div('headlineP', __('Contact information', true).':'); 
            
            echo '<div class="conBlk">';
               echo $html->div('it', $html->image('icons/addr.png', array('class' => 'iL')).$html->tag('span', $vcompany['Company']['address1']));
               if (!empty($vcompany['Company']['address2']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $vcompany['Company']['address2']));
               if (!empty($vcompany['Company']['address3']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $vcompany['Company']['address3']));
               if (!empty($vcompany['Company']['zip']))
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $vcompany['Company']['zip'].' '.$vcompany['Company']['city']));
               else
                  echo $html->div('it', $html->image('icons/emp.png', array('class' => 'iL')).$html->tag('span', $vcompany['Company']['city']));               
               echo $html->div('it', $html->image('icons/phone.png', array('class' => 'iL')).$html->tag('span', '+'.$vcompany['Company']['phone1'].' '.$vcompany['Company']['phone2']));
               if (!empty($vcompany['Company']['fax2']))
                  echo $html->div('it', $html->image('icons/fax.png', array('class' => 'iL')).$html->tag('span', '+'.$vcompany['Company']['fax1'].' '.$vcompany['Company']['fax2']));
               if (!empty($vcompany['Company']['web']))
                  echo $html->div('it', $html->image('icons/web.png', array('class' => 'iL')).$html->tag('span', $html->link($vcompany['Company']['web'], http_sanitize($vcompany['Company']['web']))));
                  
               $con_name = '';
               if (!empty($contact['User']['title']))
                  $con_name .= $contact['User']['title'].' ';
               if (!empty($contact['User']['first_name']))
                  $con_name .= $contact['User']['first_name'].' ';
               if (!empty($contact['User']['middle_name']))
                  $con_name .= $contact['User']['middle_name'].' ';
               if (!empty($contact['User']['last_name']))
                  $con_name .= $contact['User']['last_name'];
               echo $html->div('it',  $html->image('icons/pers.png', array('class' => 'iL')).$html->tag('span', $html->link($con_name, array('controller' => 'users', 'action' => 'view', $contact['User']['id']))));
               //echo $html->div('it', $html->div('at', __('Job title', true).':').$html->div('vl', $vcompany['User']['job_title']));
               echo $html->div('it', $html->image('icons/mail.png', array('class' => 'iL')).$html->tag('span', $contact['User']['email']));
                  
            echo '</div>';
   
            echo '<br />';
            echo $html->div('headlineP', __('Company rating', true).':'); 
         
            echo '<div class="rating">';    
            echo '<div class="rB z_B w200">';
               echo $html->div('tl', '').$html->div('t', '').$html->div('tr', '');
               echo '<div class="mid">';
                  echo '<div class="ratingBlk">';
                     echo '<div class="ratBot">';
                        $px = round(($vcompany['Company']['rating'] / 5) * 100);
                        echo '<div class="line" style="width: '.$px.'px;"></div>'; //100% = 100px
                        echo '<div class="mask"></div>';                                                                                                            
                     echo '</div>';
                     echo $html->div('value', number_format($vcompany['Company']['rating'], 2));
                  echo '</div>';
               echo '</div>';
               echo $html->div('bl', '').$html->div('b', '').$html->div('br', '');
            echo '</div>';
         
            if ($vcompany['Company']['neg_rating'])
               echo $html->div('negativeInd', '! '.__('Warning: Negative rating by other users!', true)); 
      
         echo '</div>';
      }
      else if ($deal_is_on) {
         echo '<div class="demInf">';           
            if ($demand['Demand']['company_id'] == $company['Company']['id']) {
               $bub_text = __('Congratulations! Both of the sides accepted the negotiated terms and the deal is on. Please wait for the buyer to contact you or click here and upgrade to a premium account, to see the buyer\'s contact details now!', true);
            }
            else {
               $bub_text = __('Total value of this demand is higher than the maximum bidding value available for basic subscribers. In order to see the contact details of your counter party, please upgrade to a premium account!', true).$html->link(__('here', true), array('controller' => 'companies', 'action' => 'edit', 'account'), array('class' => 'lnk'));
               //echo '<div>Price : '.$package['Package']['price'].' $ </div>';
               //echo $html->link($html->image('https://www.sandbox.paypal.com/en_GB/i/btn/btn_buynow_LG.gif', array('alt' => __('PayPal - Secure and easy way to pay online!', true))), array('controller' => 'orders', 'action' => 'add', $package['Package']['id']), array('escape' => false, 'title' => __('PayPal — The safer, easier way to pay online.', true)));
            }
            echo $html->div('bub rad5', $bub_text.$html->image('bg/demBubA.png', array('class' => 'bubA nrm')).$html->image('bg/demBubAH.png', array('class' => 'bubA hov none')));
            echo $html->image('bg/bubiMascotL.png', array('class' => 'bubM'));
         echo '</div>';
      }
      
      echo '</div>';
      
   echo '</div>';
      
echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){   
    
    //counter offer button click
    $('#cOfferB').click(function (){
      $('#cOffer').toggle();  
    });
       
   //dates
   var dates = $('#DemandValidTo').datepicker({
      dateFormat: 'dd.mm.yy',
      minDate: 0,
      //defaultDate: "+1w",
		showOn: 'button',
		buttonImage: '/ezbizi/img/bg/dateMask.png',
		buttonImageOnly: true,
		buttonText: '<? echo __('select a date', true) ?>',
		onSelect: function(selectedDate) {
			var option = this.id == "DemandValidFrom" ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
   
});
</script>