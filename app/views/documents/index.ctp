<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag iface"><div class="cen">';
   $cen_cls = 'iCen';

   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'iCen iCenS';
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   echo '<div class="'.$cen_cls.'">';
      
      if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id')) {
         echo '<div class="top">';
            echo $html->div('headline hlB', __('Upload new document', true));
            
            echo $form->create('Document', array('url' => array('controller' => 'documents', 'action' => 'index'), 'enctype' => 'multipart/form-data'));
            
               echo '<div class="form uForm adForm">';
                  echo '<div class="line">'; 
                     echo $form->input('Document.document', array('label' => __('Select a document', true).':', 'type' => 'file')); 
                  echo '</div>';
                  echo '<div class="line">'; 
                     echo $form->input('Document.name', array('label' => __('Name', true).':')); 
                  echo '</div>';
                  echo '<div class="line last">';                  
                     echo $form->input('Document.description', array('label' => __('Description', true).':' , 'type' => 'textarea', 'div' => 'input textarea long'));              
                  echo '</div>';
               
               echo $form->submit(__('Upload new document', true));
               
               echo '</div>';
            
            echo $form->end();      
         echo '</div>';
      }
         
      echo '<div class="list">';
      
         echo '<div class="modP rad5">';
         echo $html->div('headline hlB hlIn', __('Documents', true));
         
         if(!empty($documents)) {
            foreach ($documents as $document) {
               echo '<div class="itm ovrf rad5">';
                  echo $html->link($html->image('icons/doc.png', array('class' => 'iL')).$html->tag('span', $document['Document']['name']), array('controller' => 'documents', 'action' => 'view', $document['Document']['id']), array('class' => 'docName', 'escape' => false, 'title' => __('download document', true)));
                  echo $html->div('dsc', str_replace(PHP_EOL, '<br />', linked_text($document['Document']['description'])));
                  if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id') && in_array($session->read('Auth.User.group_id'), array(1, 2))) {
                     echo $html->link('x | '.__('remove', true), array('controller' => 'documents', 'action' => 'delete', $document['Document']['id']), array('class' => 'del del3', 'escape' => false));
                     
                     //PRIVACY
                     echo '<div class="priv">';
                       echo $html->div('privLnk rad3', 'v | '.__('privacy settings', true), array('id' => 'priv'));
                       echo $html->div('none privID', $document['Document']['privacy']);
                       echo '<div class="privWin none" id="privWin">';
                          echo $html->link(__('public', true), array('controller' => 'documents', 'action' => 'privacy', $document['Document']['id'], 1), array('class' => 'p1'));
                          echo $html->link(__('connections only', true), array('controller' => 'documents', 'action' => 'privacy', $document['Document']['id'], 2), array('class' => 'p2'));
                          echo $html->link(__('company only', true), array('controller' => 'documents', 'action' => 'privacy', $document['Document']['id'], 3), array('class' => 'p3'));
                       echo '</div>';
                     echo '</div>';
                  }
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('No document added', true));
         
         echo '</div>';
         
         if(!empty($documents)) {
            echo '<div class="pgn">';
               echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
            echo '</div>';
         }
      echo '</div>';
      
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   
echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){     
   $("input[type=file]").filestyle({
      image: "/img/bg/fileBg.png",
      inputheight : 18,
      inputwidth : 162,
      imageheight : 20,
      imagewidth : 38
   });
   
   $('.priv').each(function() {
      $(this).find('.p'+$(this).find('.privID').html()).addClass('act');
   });                                                             
});
</script>
