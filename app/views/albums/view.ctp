<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag iface"><div class="cen">';
   $cen_cls = 'iCen';

   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'iCen iCenS';
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   echo '<div class="'.$cen_cls.'">';
      
      if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id')) {
         echo '<div class="top">';     
            echo $html->div('headline hlB', __('Upload new photo', true));
            
            echo $form->create('Album', array('url' => array('controller' => 'albums', 'action' => 'view', $album['Album']['id']), 'enctype' => 'multipart/form-data'));
            
               echo '<div class="form uForm adForm">';
                  echo '<div class="line">'; 
                     echo $form->input('Photo.name', array('label' => __('Name', true).':'));
                  echo '</div>';
                  echo '<div class="line last">';   
                     echo $form->input('Photo.image', array('label' => __('Select a photo', true).':', 'type' => 'file'));
                  echo '</div>';
                  echo '<div>';
                     echo $form->submit(__('Upload new photo', true));
                  echo '</div>';
               
               echo '</div>';
            
            echo $form->end();
         echo '</div>';
      }
      
      echo '<div class="modP rad5">';
      echo $html->div('headline hlB hlIn', $album['Album']['name']);
         
      echo '<div class="albums">';
         
         if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id') &&  in_array($session->read('Auth.User.group_id'), array(1, 2))) {
            echo '<div class="delA">'; 
            
            echo $html->link('x | '.__('Delete album', true), array('controller' => 'albums', 'action' => 'delete', $album['Album']['id']), array('class' => 'del', 'escape' => false), __('Are you sure?',true));
            
            //PRIVACY
            echo '<div class="priv">';
              echo $html->div('privLnk rad3', 'v | '.__('privacy settings', true), array('id' => 'priv'));
              echo '<div class="privWin none" id="privWin">';
                 echo $html->link(__('public', true), array('controller' => 'albums', 'action' => 'privacy', $album['Album']['id'], 1), array('class' => 'p1'));
                 echo $html->link(__('connections only', true), array('controller' => 'albums', 'action' => 'privacy', $album['Album']['id'], 2), array('class' => 'p2'));
                 echo $html->link(__('company only', true), array('controller' => 'albums', 'action' => 'privacy', $album['Album']['id'], 3), array('class' => 'p3'));
              echo '</div>';
            echo '</div>';
            
            echo '</div>';
         }
         //stan - 160 x 160, big - 800 x 800 
         if (!empty($album['Photo'])) {
            foreach ($album['Photo'] as $photo) {
               echo '<div class="itm">';
                  echo '<div class="pht rad3">';

                     $foto = $image->resize('comps/'.$album['Album']['company_id'].'/'.$album['Album']['id'].'/'.$photo['Photo']['file'], 200, 200, true, true);
                     echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                     
                     echo $html->link('', '../img/comps/'.$album['Album']['company_id'].'/'.$album['Album']['id'].'/'.$photo['Photo']['file'], array('class' => 'tLink gal', 'title' => $photo['Photo']['name']));
                     if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id'))
                        echo $html->link('x | '.__('remove', true), array('controller' => 'albums', 'action' => 'photo_delete', $photo['Photo']['id']), array('class' => 'del', 'escape' => false));     
                  echo '</div>';
                  echo '<div class="inf">';
                     if (!empty($photo['Photo']['name']))
                        echo $html->div('nm', $photo['Photo']['name']);
                  echo '</div>';
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('No photos added', true));
         
         echo $html->div('edtL', $html->link(__('more albums', true), array('controller' => 'albums', 'action' => 'index', $pcompany['Company']['id'])));
         
      echo '</div>';
      echo '</div>';
      
      if (!empty($album['Photo'])) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   
echo '</div></div>';
?>

<script type="text/javascript">
$(document).ready(function(){     
   $("input[type=file]").filestyle({
      image: "/img/bg/fileBg.png",
      inputheight : 18,
      inputwidth : 162,
      imageheight : 20,
      imagewidth : 38
   });
   
   $('#privWin').find('.p'+<? echo $album['Album']['privacy']?>).addClass('act');
   
});
</script>