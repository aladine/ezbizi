<?php
echo $this->element('belt_interface');
echo '<div id="main" class="pag iface"><div class="cen">';
   $cen_cls = 'iCen';

   if ($company['Company']['id'] != $pcompany['Company']['id']) {
      $cen_cls = 'iCen iCenS';
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   echo '<div class="'.$cen_cls.'">';
      
      if ($pcompany['Company']['id'] == $session->read('Auth.User.company_id')) {
         echo '<div class="top">';      
            echo $html->div('headline hlB', __('Create a new album', true));
      
            echo $form->create('Album', array('url' => array('controller' => 'albums', 'action' => 'index')));
            
               echo '<div class="form uForm adForm">';
                  echo '<div class="line last">'; 
                     echo $form->input('Album.name', array('label' => __('Album name', true).':'));
                  echo '</div>';
               
               echo $form->submit(__('Create a new album', true));
               
               echo '</div>';
            
            echo $form->end();              
         echo '</div>';
      }
      
      echo '<div class="modP rad5">'; 
      echo $html->div('headline hlB hlIn', __('Albums', true));
        
      echo '<div class="albums">';
      
         if(!empty($albums)) {   
           //big - 160 x 160 (first photo from album), mini - 60 x 60 (random photo from album if exist :) )
            foreach ($albums as $album) {
               echo '<div class="itm">';
                  echo '<div class="pht rad3">';
                     
                     if (empty($album['Photo'][0]['file']))
                        $foto = $this->webroot.IMAGES_URL.'noImage50.png';
                     else
                        $foto = $image->resize('comps/'.$album['Album']['company_id'].'/'.$album['Album']['id'].'/'.$album['Photo'][0]['file'], 200, 200, true, true);
                     echo $html->div('foto rad3', '', array('style' => 'background-image: url('.$foto.');'));
                     
                     $count = count($album['Photo']);
                     if ($count > 1) {
                        $ind = rand(1, $count - 1);
                        
                        $mini = $image->resize('comps/'.$album['Album']['company_id'].'/'.$album['Album']['id'].'/'.$album['Photo'][$ind]['file'], 67, 67, true, true);
                        echo '<div class="mini rad3">';
                           echo $html->div('ft', '', array('style' => 'background-image: url('.$mini.');'));
                        echo '</div>';
                     }
                     echo $html->link('', array('controller' => 'albums', 'action' => 'view', $album['Album']['id']), array('class' => 'tLink'));        
                  echo '</div>';
                  echo '<div class="inf">';
                     echo $html->div('name', $album['Album']['name']);
                     if ($album['Album']['photo_count'] == 0)
                        echo $html->div('cnt', __('empty', true));
                     else if ($album['Album']['photo_count'] == 1)
                        echo $html->div('cnt', $album['Album']['photo_count'].' '.__('photo', true));
                     else
                        echo $html->div('cnt', $album['Album']['photo_count'].' '.__('photos', true));
                  echo '</div>';
               echo '</div>';
            }
         }
         else
            echo $html->div('noData', __('No albums added', true));
         
      echo '</div>';
      echo '</div>';
     
      if(!empty($albums)) {  
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      }
      
   echo '</div>';
   if ($company['Company']['id'] == $pcompany['Company']['id']) {
      echo '<div class="iRight">';
         echo $this->element('company_interface_right');
      echo '</div>';
   }
   
echo '</div></div>';
?>
