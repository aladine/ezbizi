<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Sites', true));
      
      echo '<div class="form">';
         
         echo $html->link($html->image('icons/ad_add.png', array('class' => 'iL')).$html->tag('span',__('Add new site', true)), array('controller' => 'sites', 'action' => 'add'), array('class' => 'rad3 listA geB', 'escape' => false));
         
         if (!empty($sites)) {
            foreach ($sites as $site) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';                    
                     echo '<div class="top">';
                        echo $html->link($site['Site']['name_'.$lang], array('controller' => 'sites', 'action' => 'edit', $site['Site']['id']), array('class' => 'link'));                 
                     echo '</div>';                   
                  echo '</div>';
                  
                  echo $html->link(__('display', true), array('controller' => 'sites', 'action' => 'view', $site['Site']['url_name_en']), array('class' => 'dsp'));
                  echo $html->link(__('remove', true), array('controller' => 'sites', 'action' => 'delete', $site['Site']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->div('noData', __('List is empty', true));   

      echo '</div>';
      
      if (!empty($sites)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      } 
      
   echo '</div>';

echo '</div></div>';
?>