<?php
echo $html->script(array('cleditor.min', 'cleditor.init'));

echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Add new site', true));
      
      echo '<div class="form">'; 
      echo $form->create('Site', array('action' => 'add'));
         echo $form->input('Site.name_en', array('label' => __('Name En', true).':'));
         echo $form->input('Site.name_ja', array('label' => __('Name Ja', true).':'));
         
         echo $form->input('User.bott_menu', array('label' => __('Placed on the bottom menu of the page', true), 'type' => 'checkbox'));
                  
         echo $form->input('Site.text_en', array('label' => __('Text En', true).':', 'type' => 'textarea', 'div' => 'input textarea cleditor'));
         echo $form->input('Site.text_ja', array('label' => __('Text Ja', true).':', 'type' => 'textarea', 'div' => 'input textarea cleditor'));
   
         echo '<div class="bblk">';
            echo $form->submit(__('add', true));
         echo '</div>';
         
      echo $form->end();

      echo '</div>';
            
   echo '</div>';

echo '</div></div>';
?>