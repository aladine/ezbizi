<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Add new region', true));
      
      echo '<div class="form">';
         
      echo $form->create('Region', array('url' => array('controller' => 'regions', 'action' => 'add', $country['Country']['id'])));
         echo $form->input('Region.name_en', array('label' => __('Name En', true).':'));
         echo $form->input('Region.name_ja', array('label' => __('Name Ja', true).':'));
      
         echo '<div class="bblk">';
            echo $form->submit(__('add', true));
         echo '</div>';
         
      echo $form->end();

      echo '</div>';
            
   echo '</div>';

echo '</div></div>';
?>