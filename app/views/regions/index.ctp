<?php
echo $this->element('belt_admin');               
echo '<div id="main" class="setting"><div class="cen">';
   
   echo '<div class="sLeft">';
      echo $this->element('admin_menu');
   echo '</div>';
   
   echo '<div class="sRight">';
      echo $html->div('headline', __('Regions', true));
      
      echo '<div class="path">';
         echo $html->link($country['Country']['name_'.$lang], array('controller' => 'countries', 'action' => 'index'), array('class' => 'it'));
      echo '</div>';  
      
      echo '<div class="form">';
         
         echo $html->link($html->image('icons/ad_add.png', array('class' => 'iL')).$html->tag('span',__('Add new region', true)), array('controller' => 'regions', 'action' => 'add', $country['Country']['id']), array('class' => 'rad3 listA geB', 'escape' => false));
         
         if (!empty($regions)) {
            foreach ($regions as $region) {
               echo '<div class="list rad3">';

                  echo '<div class="body">';                    
                     echo '<div class="top">';
                        echo $html->tag('span', $region['Region']['name_'.$lang], array('class' => 'name'));                 
                     echo '</div>';                   
                  echo '</div>';
                  
                  echo $html->link(__('remove', true), array('controller' => 'regions', 'action' => 'delete', $region['Region']['id']), array('class' => 'del'), __('Are you sure?', true));
               echo '</div>'; 
            }
         }
         else
            echo $html->div('noData', __('List is empty', true));   

      echo '</div>';
      
      if (!empty($regions)) {
         echo '<div class="pgn">';
            echo $paginator->first('<<', array('tag' => 'div')).$paginator->numbers(array('separator' => '', 'modulus' => '10', 'tag' => 'div')).$paginator->last('>>', array('tag' => 'div'));   
         echo '</div>';
      } 
      
   echo '</div>';

echo '</div></div>';
?>