<?php
class AppController extends Controller {
   
   var $components = array('Session', 'Cookie', 'Acl', 'Auth');
   var $helpers = array('Js' => array('Jquery'), 'Session');
   var $uses = array('Language', 'User', 'Site');
   var $lang = 'en';
   
   var $sections = null; 
   var $company = null;
   
   function beforeFilter(){
      $this->_setCookie();
      $this->_setLanguage();  
      parent::beforeFilter();
      $this->Auth->authorize = 'actions';
      $this->Auth->actionPath = 'controllers/';
      $this->Auth->allowedActions = array('display');
      $this->Auth->loginAction = array('controller'=>'users', 'action'=>'login', 'language' => $this->lang);
		$this->Auth->logoutRedirect = array('controller'=>'users', 'action'=>'home', 'language' => $this->lang);
		$this->Auth->loginRedirect = array('controller'=>'users', 'action'=>'home', 'language' => $this->lang);
		$this->Auth->loginError = __('Login failed. You entered invalid login name or password.', true);
		$this->Auth->authError = __('Please login or register.', true);
      $this->Auth->flashElement = 'flash';
      $this->Auth->fields = array('username' => 'login', 'password' => 'password');
      $this->Auth->autoRedirect = false;
      $this->set('languages', $this->Language->find('all', array('conditions' => array('active' => true))));
      $foot_menu = $this->Site->find('list', array('fields' => array('Site.url_name_en', 'Site.name_'.$this->lang)));
      $this->set('foot_menu', $foot_menu);
      $this->_onlineIndicator();
   }
   
   function _onlineIndicator() {
      if ($this->Session->check('Auth.User.id')) {
         $this->User->id = $this->Session->read('Auth.User.id');
         $this->User->saveField('last_log', date('Y-m-d H:i:s'));
         $this->User->saveField('online', true);
         $this->User->saveField('last_activity', date('Y-m-d H:i:s'));
         $company_id = $this->Session->read('Auth.User.company_id');
         if (!empty($company_id)) {
            $company = $this->User->Company->find('first', array('conditions' => array('Company.id' => $company_id), 'recursive' => -1));
            if (empty($company)) {
               $this->Auth->logout();
               $this->redirect('/');
            }
            $this->User->Company->id = $company_id;
            $this->User->Company->saveField('online', true);
            $this->User->Company->saveField('last_activity', date('Y-m-d H:i:s'));
            $account = $this->User->Company->Account->find('first', array('conditions' => array('Account.id' => $company['Company']['account_id']), 'recursive' => -1));
            $this->set('account', $account);
            $this->set('company', $company);
            $this->company = $company;
            $connr = $this->User->Company->Connection->find('count', array('conditions' => array('Connection.com2_id' => $company_id, 'Connection.approved' => false)));
            $this->set('new_con', $connr);
            if ($this->Session->read('Auth.User.group_id') != 3) {
               $new_usrs = $this->User->find('count', array('conditions' => array('User.company_id' => $company_id, 'User.active' => 0, 'User.key' => null)));
               if (!empty($new_usrs))
                  $this->set('new_users', $new_usrs);
            }
            $ints = $this->User->Company->Interest->find('count', array('conditions' => array('Interest.recipient_id' => $company_id)));
            if (!empty($ints))
               $this->set('ints_count', $ints); 
         }
         $new_mm = $this->User->field('unread');
         $new_dm = $this->User->field('unreadd');
         $this->set('new_mm', $new_mm);
         $this->set('new_dm', $new_dm);
         $this->set('new_mess', $new_mm + $new_dm);
      }
      else {
         $cookie = $this->Cookie->read('EzbiziAuthRemember');
         if (!is_null($cookie)) {
            if ($this->Auth->login($cookie)) {
               $this->redirect($this->Auth->redirect());
            }
         }
      }
   }
   
   function _setCookie() {
      if ($this->Session->check('Auth.User.id'))
         $this->Cookie->name = 'EzbiziUser'.$this->Session->read('Auth.User.id');
      else
         $this->Cookie->name = 'EzbiziUser0';
      $this->Cookie->time =  '1 month';
      $this->Cookie->key = 'a3KcRka94k2l0';
   }
   
   function _setLanguage() {
      if (isset($this->params['language'])) {
         if ($this->Session->check('Config.language')) {
            if ($this->params['language'] !=  $this->Session->read('Config.language')) {
               $this->Session->write('Config.language', $this->params['language']);
               $this->Cookie->write('lang', $this->params['language'], false, '20 days');
            }
         }
         else {
            $this->Session->write('Config.language', $this->params['language']);
            $this->Cookie->write('lang', $this->params['language'], false, '20 days');
         }
      }
      else {      
         if ($this->Cookie->read('lang') && !$this->Session->check('Config.language')) {
            $this->Session->write('Config.language', $this->Cookie->read('lang'));
         }
         else
            $this->Session->write('Config.language', 'en');
         $this->params['language'] = $this->Session->read('Config.language');
      }
      $this->lang = $this->Session->read('Config.language');
      if ($this->lang == 'en')
         Configure::write('Config.language', 'en-gb');
      if ($this->lang == 'ja')
         Configure::write('Config.language', 'jpn');
      if ($this->lang == 'zh')
         Configure::write('Config.language', 'chi');
      $this->set('lang', $this->lang);
      /*
      $locale = Configure::read('Config.language');    
      if ($locale && file_exists(VIEWS . $locale . DS . $this->viewPath))        
         $this->viewPath = $locale . DS . $this->viewPath;  */
   }
       
   function redirect($url, $status = NULL, $exit = true) {
      if (!isset($url['language']) && $this->Session->check('Config.language')) {
         $url['language'] = $this->Session->read('Config.language');
      }
      parent::redirect($url,$status,$exit);
   }
   
   function flash($message, $url, $pause = 1) {
      if (!isset($url['language']) && $this->Session->check('Config.language')) {
         $url['language'] = $this->Session->read('Config.language');
      }
      parent::flash($message,$url,$pause);
   }
   
   function build_acl() {        
      if (!Configure::read('debug')) {            
         return $this->_stop();        
      }        
      $log = array();        
      $aco =& $this->Acl->Aco;        
      $root = $aco->node('controllers');        
      if (!$root) {            
         $aco->create(array('parent_id' => null, 'model' => null, 'alias' => 'controllers'));            
         $root = $aco->save();            
         $root['Aco']['id'] = $aco->id;             
         $log[] = 'Created Aco node for controllers';        
      } else {            
         $root = $root[0];              
      }           
      App::import('Core', 'File');        
      $Controllers = Configure::listObjects('controller');        
      $appIndex = array_search('App', $Controllers);        
      if ($appIndex !== false ) {            
         unset($Controllers[$appIndex]);        
      }        
      $baseMethods = get_class_methods('Controller');        
      $baseMethods[] = 'buildAcl';        
      $Plugins = $this->_getPluginControllerNames();        
      $Controllers = array_merge($Controllers, $Plugins);        
      // look at each controller in app/controllers        
      foreach ($Controllers as $ctrlName) {            
         $methods = $this->_getClassMethods($this->_getPluginControllerPath($ctrlName));            
         // Do all Plugins First            
         if ($this->_isPlugin($ctrlName)){                
            $pluginNode = $aco->node('controllers/'.$this->_getPluginName($ctrlName));                
            if (!$pluginNode) {                    
               $aco->create(array('parent_id' => $root['Aco']['id'], 'model' => null, 'alias' => $this->_getPluginName($ctrlName)));                    
               $pluginNode = $aco->save();                    
               $pluginNode['Aco']['id'] = $aco->id;                    
               $log[] = 'Created Aco node for ' . $this->_getPluginName($ctrlName) . ' Plugin';                
            }            
         }            
         // find / make controller node            
         $controllerNode = $aco->node('controllers/'.$ctrlName);            
         if (!$controllerNode) {               
            if ($this->_isPlugin($ctrlName)){                    
               $pluginNode = $aco->node('controllers/' . $this->_getPluginName($ctrlName));                    
               $aco->create(array('parent_id' => $pluginNode['0']['Aco']['id'], 'model' => null, 'alias' => $this->_getPluginControllerName($ctrlName)));                    
               $controllerNode = $aco->save();                    
               $controllerNode['Aco']['id'] = $aco->id;                    
               $log[] = 'Created Aco node for ' . $this->_getPluginControllerName($ctrlName) . ' ' . $this->_getPluginName($ctrlName) . ' Plugin Controller';                
            } else {                    
               $aco->create(array('parent_id' => $root['Aco']['id'], 'model' => null, 'alias' => $ctrlName));                    
               $controllerNode = $aco->save();                    
               $controllerNode['Aco']['id'] = $aco->id;                    
               $log[] = 'Created Aco node for ' . $ctrlName;                
            }            
         } else {                
            $controllerNode = $controllerNode[0];            
         }            
         //clean the methods. to remove those in Controller and private actions.            
         foreach ($methods as $k => $method) {                
            if (strpos($method, '_', 0) === 0) {                    
               unset($methods[$k]);                    
               continue;                
            }                
            if (in_array($method, $baseMethods)) {                    
               unset($methods[$k]);                    
               continue;                
            }                
            $methodNode = $aco->node('controllers/'.$ctrlName.'/'.$method);                
            if (!$methodNode) {                    
               $aco->create(array('parent_id' => $controllerNode['Aco']['id'], 'model' => null, 'alias' => $method));                    
               $methodNode = $aco->save();                    
               $log[] = 'Created Aco node for '. $method;                
            }            
         }        
      }        
      if(count($log)>0) {            
         debug($log);        
      }    
   }    
   
   function _getClassMethods($ctrlName = null) {        
      App::import('Controller', $ctrlName);        
      if (strlen(strstr($ctrlName, '.')) > 0) {            
         // plugin's controller            
         $num = strpos($ctrlName, '.');            
         $ctrlName = substr($ctrlName, $num+1);        
      }        
      $ctrlclass = $ctrlName . 'Controller';        
      $methods = get_class_methods($ctrlclass);       
      // Add scaffold defaults if scaffolds are being used        
      $properties = get_class_vars($ctrlclass);        
      if (array_key_exists('scaffold',$properties)) {            
         if($properties['scaffold'] == 'admin') {                
            $methods = array_merge($methods, array('admin_add', 'admin_edit', 'admin_index', 'admin_view', 'admin_delete'));            
         } else {                
            $methods = array_merge($methods, array('add', 'edit', 'index', 'view', 'delete'));            
         }        
      }        
      return $methods;    
   }    
   
   function _isPlugin($ctrlName = null) {        
      $arr = String::tokenize($ctrlName, '/');        
      if (count($arr) > 1) {            
         return true;        
      } else {            
         return false;        
      }    
   }    
   
   function _getPluginControllerPath($ctrlName = null) {        
      $arr = String::tokenize($ctrlName, '/');        
      if (count($arr) == 2) {            
         return $arr[0] . '.' . $arr[1];        
      } else {            
         return $arr[0];        
      }    
   }    
   
   function _getPluginName($ctrlName = null) {        
      $arr = String::tokenize($ctrlName, '/');        
      if (count($arr) == 2) {            
         return $arr[0];        
      } else {            
         return false;        
      }    
   }    
   
   function _getPluginControllerName($ctrlName = null) {        
      $arr = String::tokenize($ctrlName, '/');        
      if (count($arr) == 2) {            
         return $arr[1];        
      } else {            
         return false;        
      }    
   }
   
   /** * Get the names of the plugin controllers ... *  * This function will get an array of the plugin controller names, and * also makes sure the controllers are available for us to get the  * method names by doing an App::import for each plugin controller. * * @return array of plugin names. * */    
   function _getPluginControllerNames() {        
      App::import('Core', 'File', 'Folder');        
      $paths = Configure::getInstance();        
      $folder =& new Folder();        
      $folder->cd(APP . 'plugins');        
      // Get the list of plugins        
      $Plugins = $folder->read();       
      $Plugins = $Plugins[0];        
      $arr = array();        
      // Loop through the plugins        
      foreach($Plugins as $pluginName) {            
         // Change directory to the plugin            
         $didCD = $folder->cd(APP . 'plugins'. DS . $pluginName . DS . 'controllers');            
         // Get a list of the files that have a file name that ends            
         // with controller.php            
         $files = $folder->findRecursive('.*_controller\.php');            
         // Loop through the controllers we found in the plugins directory            
         foreach($files as $fileName) {                
            // Get the base file name                
            $file = basename($fileName);                
            // Get the controller name                
            $file = Inflector::camelize(substr($file, 0, strlen($file)-strlen('_controller.php')));                
            if (!preg_match('/^'. Inflector::humanize($pluginName). 'App/', $file)) {                    
               if (!App::import('Controller', $pluginName.'.'.$file)) {                        
                  debug('Error importing '.$file.' for plugin '.$pluginName);                    
               } else {                        
                  /// Now prepend the Plugin name ...                        
                  // This is required to allow us to fetch the method names.                        
                  $arr[] = Inflector::humanize($pluginName) . "/" . $file;                    
               }                
            }            
         }        
      }        
      return $arr;    
   }
}
?>
