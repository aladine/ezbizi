<?php

App::import('Vendor','S3',array('file' => 'S3.php'));

class AmazonS3 extends Object {
	var $s3 = null;
	var $s3bucket = null;
	
	var $_error = '';
	var $_key = '';
	var $_secret_key = '';
	
	function __construct () {
		$this->_key = AWS_AXS_KEY;
		$this->_secret_key = AWS_SECRET_AXS_KEY;
		$this->s3 = new S3($this->_key,$this->_secret_key,false);
		
		$this->bucket(AWS_S3_BUCKET); // Set the default bucket
	}
	
	function auth ($path,$expires = 3000) {
		list($bucket,$path) = $this->_parsePath($path);
		return $this->s3->getAuthenticatedURL($bucket,$path,$expires);
	}
	
	function bucket ($bucket) {
		$this->s3bucket = $bucket;
	}
	
	function copy ($src,$dest,$copy_req_hdrs = array(),$meta_hdrs = array()) {
		list($src_bkt,$src_pth) = $this->_parsePath($src);
		list($dest_bkt,$dest_pth) = $this->_parsePath($dest);
		
		if ($this->s3->copyObject($src_bkt,$src_pth,$dest_bkt,$dest_pth,null,$meta_hdrs,$copy_req_hdrs)) {
			return true;
		}
		$this->_error = $this->s3->getError();
		return false;
	}
	
  function get ($path,$dst_path) {
  	list($bucket,$path) = $this->_parsePath($path);
  	return $this->s3->getObject($bucket,$path,$dst_path);
  }
  
	function getError () {
		return $this->_error;
	}
	
  function getInfo ($path) {
  	list($bucket,$path) = $this->_parsePath($path);
  	return $this->s3->getObjectInfo($bucket,$path);
  }
  
  function postPolicy ($conditions,$bucket = null) {
  	if (!isset($bucket)) {
  		$bucket = $this->s3bucket;
  		foreach ($conditions as $condition) {
  			if ($condition[1] == 'bucket') {
  				$bucket = $condition[2];
  				break;
  			}
  		}
		}
  	
		$s3policy = new S3PostPolicy($this->_key,$this->_secret_key,$bucket);
		$s3policy->addCondition($conditions);
		return $s3policy;
  }
  
  function put ($lcl_path,$path) {
  	list($bucket,$path) = $this->_parsePath($path);
  	$file_attrs = $this->s3->inputFile($lcl_path);
  	if ($this->s3->putObject($file_attrs,$bucket,$path)) {
  		return true;
  	}
		$this->_error = $this->s3->getError();
  	return false;
  }
  
  function rm ($path) {
  	list($bucket,$path) = $this->_parsePath($path);
  	if ($this->s3->deleteObject($bucket,$path)) {
  		return true;
  	}
  	return false;
  }
  
  function url ($path) {
  	list($bucket,$path) = $this->_parsePath($path);
  	return 'http://'.$bucket.'.s3.amazonaws.com/'.$path;
  }
  
  /**
   * This S3 library silently fails when uploading to a location like xxx//yyy/zzz.jpg
   * Strips out the redundant '/'
   */
  function _clean ($path) { 
		return ltrim($path,'/'); 
	}
	
	function _parsePath ($path) {
		$pieces = parse_url($path);
		if (isset($pieces['scheme']) && strtolower($pieces['scheme']) == 's3') {
			return array($pieces['host'],$this->_clean($pieces['path']));
		} else {
			return array($this->s3bucket,$this->_clean($path));
		}
	}
}

?>