<?php
class Category extends AppModel {

   var $name = 'Category';
   
   var $belongsTo = array('Parent' => array('className' => 'Category',
                                            'foreignKey' => 'parent_id',
                                            'counterCache' => true));
   
   var $hasMany = array('Subcategory' => array('className' => 'Category',
                                               'foreignKey' => 'parent_id',
                                               'order' => array('name_en'),
                                               'dependent' => true));
   
   var $hasAndBelongsToMany = array('Sector' => array('className' => 'Sector',
                                                      'joinTable' => 'categories_sectors',
                                                      'foreignKey' => 'category_id',
                                                      'associationForeignKey' => 'sector_id',
                                                      'unique' => true));
   
   var $validate = array('name_en' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')),
                         'name_ja' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
}
?>