<?php
class Invitation extends AppModel {

   var $name = 'Invitation';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id',
                                             'counterCache' => true,
                                             'counterScope' => array('Invitation.paid' => false)),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'user_id'));
   
   var $validate = array('email' => array('rule1' => array('rule' => array('email', true),
														                 'message' => 'Please provide a valid email address.'),
                                          'rule2' => array('rule' => 'isUnique',
														                 'message' => 'Sorry, this e-mail has been taken already.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
                                          
}
?>