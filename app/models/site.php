<?php
class Site extends AppModel {
   
   var $name = 'Site';
   
   var $validate = array('name_en' => array('rule1' => array('rule' => array('maxLength', 50),
										       		                   'message' => 'You have exceeded the maximum length of 50 characters.'),
										              'rule2' => array('rule' => 'isUnique',
													    	                'message' => 'A page with this name exists already.'),
                                            'rule3' => array('rule' => 'notEmpty',
                                                             'message' => 'Enter the name of the page.')),
                         'name_ja' => array('rule1' => array('rule' => array('maxLength', 50),
										       		                   'message' => 'You have exceeded the maximum length of 50 characters.'),
										              'rule2' => array('rule' => 'isUnique',
													    	                'message' => 'A page with this name exists already.'),
                                            'rule3' => array('rule' => 'notEmpty',
                                                             'message' => 'Enter the name of the page.')),
                         'url_name_en' => array('rule1' => array('rule' => 'isUnique',
													    	                    'message' => 'A page with this name exists already.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
                                                                                                
}
?>