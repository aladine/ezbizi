<?php
class User extends AppModel {

   var $name = 'User';
   
   var $belongsTo = array('Group' => array('className' => 'Group',
                                           'foreignKey' => 'group_id',
                                           'counterCache' => true),
                          'Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id',
                                             'counterCache' => true),
                          'State' => array('className' => 'State',
                                           'foreignKey' => 'state_id',
                                           'counterCache' => true),
                          'Sex' => array('className' => 'Sex',
                                         'foreignKey' => 'sex_id',
                                         'counterCache' => true));
   
   var $hasMany = array('Notification' => array('className' => 'Notification',
                                                'foreignKey' => 'user_id',
                                                'order' => 'created DESC',
                                                'conditions' => array('Notification.type' => array('user_status', 'new_demand')),
                                                'dependent' => true),
                        'ReceivedMessage' => array('className' => 'Message',
                                                   'foreignKey' => 'recipient_id',
                                                   'dependent' => true),
                        'SentMessage' => array('className' => 'Message',
                                               'foreignKey' => 'sender_id',
                                               'dependent' => true),
                        'Demand' => array('className' => 'Demand',
                                          'foreignKey' => 'user_id',
                                          'dependent' => true),
                        'Offer' => array('className' => 'Offer',
                                          'foreignKey' => 'user_id',
                                          'dependent' => true),
                        'Ban' => array('className' => 'Ban',
                                       'foreignKey' => 'recipient_id',
                                       'dependent' => true),
                        'SentBan' => array('className' => 'Ban',
                                           'foreignKey' => 'sender_id',
                                           'dependent' => true),
                        'Recommendation' => array('className' => 'Recommendation',
                                                  'foreignKey' => 'user_id'));
   
   var $actsAs = array('Acl' => array('type' => 'requester'));
 
   var $validate = array('title' => array('rule1' => array('rule' => array('maxLength', 10),
                                                           'message' => 'You have exceeded the maximum length of 10 characters.')),  
                         'first_name' => array('rule1' => array('rule' => array('maxLength', 100),
                                                                'message' => 'You have exceeded the maximum length of 100 characters.'),
                                               'rule2' => array('rule' => 'notEmpty',        
                                                                'message' => 'This field cannot be left blank.')),
                         'middle_name' => array('rule1' => array('rule' => array('maxLength', 100),
                                                               'message' => 'You have exceeded the maximum length of 100 characters.')),
                         'last_name' => array('rule1' => array('rule' => array('maxLength', 100),
                                                               'message' => 'You have exceeded the maximum length of 100 characters.'),
                                              'rule2' => array('rule' => 'notEmpty',        
                                                               'message' => 'This field cannot be left blank.')),
                         'job_title' => array('rule1' => array('rule' => array('maxLength', 150),
                                                               'message' => 'You have exceeded the maximum length of 150 characters.'),
                                              'rule2' => array('rule' => 'notEmpty',        
                                                               'message' => 'This field cannot be left blank.')),  
                         'email' => array('rule1' => array('rule' => array('email', true),
														                 'message' => 'Please provide a valid email address.'),
                                          'rule2' => array('rule' => 'isUnique',
														                 'message' => 'Sorry, this e-mail has been taken already.')), 
                         'for_email' => array('rule1' => array('rule' => array('email', true),
									      				                  'message' => 'Please provide a valid email address.')),                                   
                         'login' => array('rule1' => array('rule' => 'isUnique',
														                 'message' => 'Sorry, this login name is in use already.'),
										            'rule2' => array('rule' => array('between', 5, 50),
														                 'message' => 'Login must be between 5 and 50 characters long.')),
                         'psword' => array('rule2' => array('rule' => array('between', 5, 20),
														                  'message' => 'Password must be between 5 and 20 characters long.'),
											          'rule3' => array('rule' => array('passwordCompare', 'passwd'),
														                  'message' => 'Passwords do not match.')),
						       'passwd' => array('rule1' => array('rule' => array('passwordCompare', 'psword'),
											                           'message' => 'Passwords do not match.')),
                         'phone1' => array('rule1' => array('rule' => array('maxLength', 5),
                                                            'message' => 'You have exceeded the maximum length of 5 characters.')),
                         'phone2' => array('rule1' => array('rule' => array('maxLength', 25),
                                                            'message' => 'You have exceeded the maximum length of 25 characters.')),
                         'fax1' => array('rule1' => array('rule' => array('maxLength', 5),
                                                          'message' => 'You have exceeded the maximum length of 5 characters.')),
                         'fax2' => array('rule1' => array('rule' => array('maxLength', 25),
                                                          'message' => 'You have exceeded the maximum length of 25 characters.')),
                         'status' => array('rule1' => array('rule' => array('between', 5, 500),
														                  'message' => 'Status must be between 5 and 500 characters long.')),                                
                         'email_ban' => array('rule' => array('boolean'),
											             'message' => 'Incorrect value'),                                
                         'newsletter' => array('rule' => array('boolean'),
												           'message' => 'Incorrect value'),
                         'oldpass' => array('rule1' => array('rule' => array('okPassword'),
															                'message' => 'Your entry does not match the old password.'),
											           'rule2' => array('rule' => array('editPassword', 'newpass1'),
															                'message' => 'Enter the old password.')),
						       'newpass1' => array('rule1' => array('rule' => array('passwordCompare', 'newpass2'),
														  	                 'message' => 'Passwords do not match.'),
											            'rule2' => array('rule' => array('editPassword', 'oldpass'),
																              'message' => 'Enter the new password.'),
											            'rule3' => array('rule' => array('between', 5, 20),
														                    'message' => 'Password must be between 5 and 20 characters long.')),
						       'newpass2' => array('rule' => array('passwordCompare', 'newpass1'),
											            'message' => 'Passwords do not match.'),
								 'company_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                                'message' => 'Select a company.')),
                         'terms' => array('rule' => array('equalTo', '1'),
                                          'message' => 'In order to proceed you must agree to the Terms and Conditions of Use.'),
                         'image' => array('rule1' => array('rule' => 'imageSize',
                                                           'message' => 'Please upload a photo with a lower resolution.'),
                                          'rule2' => array('rule' => 'imageVal',
                                                           'message' => 'Please upload a JPEG file format.')),
                         'interest' => array('rule1' => array('rule' => array('maxLength', 2000),
                                                              'message' => 'You have exceeded the maximum length of 2000 characters.')),
                         'web' => array('rule1' => array('rule' => 'url',
                                                         'allowEmpty' => true,
                                                         'message' => 'Enter url address.')),
                         'facebook' => array('rule1' => array('rule' => 'url',
                                                              'allowEmpty' => true,
                                                              'message' => 'Enter url address.')),
                         'linkedin' => array('rule1' => array('rule' => 'url',
                                                              'allowEmpty' => true,
                                                              'message' => 'Enter url address.')),
                         'twitter' => array('rule1' => array('rule' => 'url',
                                                             'allowEmpty' => true,
                                                             'message' => 'Enter url address.')),
                         'google' => array('rule1' => array('rule' => 'url',
                                                            'allowEmpty' => true,
                                                            'message' => 'Enter url address.')),
                         'myspace' => array('rule1' => array('rule' => 'url',
                                                             'allowEmpty' => true,
                                                             'message' => 'Enter url address.')),
                         'foursquare' => array('rule1' => array('rule' => 'url',
                                                                'allowEmpty' => true,
                                                                'message' => 'Enter url address.')),
                         'reddit' => array('rule1' => array('rule' => 'url',
                                                            'allowEmpty' => true,
                                                            'message' => 'Enter url address.')),
                         'stumble' => array('rule1' => array('rule' => 'url',
                                                             'allowEmpty' => true,
                                                             'message' => 'Enter url address.')),
                         'quora' => array('rule1' => array('rule' => 'url',
                                                           'allowEmpty' => true,
                                                           'message' => 'Enter url address.')),
                         'tumblr' => array('rule1' => array('rule' => 'url',
                                                            'allowEmpty' => true,
                                                            'message' => 'Enter url address.')),
                         'flickr' => array('rule1' => array('rule' => 'url',
                                                            'allowEmpty' => true,
                                                            'message' => 'Enter url address.')),
                         'picasa' => array('rule1' => array('rule' => 'url',
                                                            'allowEmpty' => true,
                                                            'message' => 'Enter url address.')),
                         'etsy' => array('rule1' => array('rule' => 'url',
                                                          'allowEmpty' => true,
                                                          'message' => 'Enter url address.')),
                         'rss' => array('rule1' => array('rule' => 'url',
                                                         'allowEmpty' => true,
                                                         'message' => 'Enter url address.')),
                         'text' => array('r1' => array('rule' => 'notEmpty',
                                                       'message' => 'This field cannot be left blank.'),
                                        'r2' => array('rule' => array('maxLength', 2000),
                                                      'message' => 'Message must be no larger than 2000 characters long.')));
 
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   function passwordCompare($data = array(), $compare_field = null) {
		$value = array_values($data);
		return $value[0] === $this->data[$this->name][$compare_field];
	}
	
	function editPassword($data=array(), $compare_field=null) {
		$value = array_values($data);
		if ($value[0] == '' && $this->data['User'][$compare_field] != '')
			return false;
		else
			return true;
	}
	
	function okPassword($data=array()) {
		$value = array_values($data);
		if ($value[0] != '')
			return Security::hash($value[0], 'sha1', true) === $this->data['User']['password'];
		else
			return true;
	}
 
   function parentNode() {
      if (!$this->id && empty($this->data)) {
         return null;
      }
      if (isset($this->data['User']['group_id'])) {
	   $groupId = $this->data['User']['group_id'];
      } else {
    	   $groupId = $this->field('group_id');
      }
      if (!$groupId) {
	      return null;
      } else {
         return array('Group' => array('id' => $groupId));
      }
   }
   
   function imageVal($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $type = exif_imagetype($value['tmp_name']);
         if (in_array($type, array(IMAGETYPE_JPEG)))
            return true;
      }
      return false;
   }
   
   function imageSize($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $size = getimagesize($value['tmp_name']);
         if (in_array($size[2], array(IMAGETYPE_JPEG)))
            if (($size[0] * $size[1]) < 12000000)
               return true;
      }
      return false;
   }
   
   var $temp_user_data = null;
   
   function beforeDelete() {
      $this->temp_user_data = $this->find('first', array('conditions' => array('User.id' => $this->id), 'recursive' => -1));
      if (empty($this->temp_user_data))
         return false;
      return true;
   }
   
   function afterDelete() {
      if (!empty($this->temp_user_data)) {
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'users'.DS.$this->temp_user_data['User']['id'].DS.$this->temp_user_data['User']['avatar'];
         if (file_exists($file))
            unlink($file);
      }
   }
}
?>