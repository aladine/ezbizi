<?php
class Ban extends AppModel {

   var $name = 'Ban';
   
   var $belongsTo = array('Sender' => array('className' => 'User',
                                            'foreignKey' => 'sender_id'),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'recipient_id'));

}
?>