<?php
class Visit extends AppModel {

   var $name = 'Visit';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id',
                                             'counterCache' => true),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'user_id'));
                                          
}
?>