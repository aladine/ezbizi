<?php
class Document extends AppModel {
   
   var $name = 'Document';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id'));
                                             
   var $validate = array('name' => array('rule1' => array('rule' => array('maxLength', 250),
                                                          'message' => 'You have exceeded the maximum length of 250 characters.'),
                                         'rule2' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')),
                         'description' => array('rule1' => array('rule' => array('maxLength', 500),
                                                                 'message' => 'You have exceeded the maximum length of 500 characters.'),
                                                'rule2' => array('rule' => 'notEmpty',        
                                                                 'message' => 'This field cannot be left blank.')),
                         'document' => array('rule1' => array('rule' => 'fileNotEmpty',
                                                              'message' => 'Attach a file.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   function fileNotEmpty($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK)
         return true;
      return false;
   }
   
   var $temp_doc_data = null;

   function beforeDelete() {
      $this->temp_doc_data = $this->find('first', array('conditions' => array('Document.id' => $this->id), 'recursive' => -1));
      if (empty($this->temp_doc_data))
         return false;
      return true;
   }
   
   function afterDelete() {
      if (!empty($this->temp_doc_data)) {
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'files'.DS.'comps'.DS.$this->temp_doc_data['Document']['company_id'].DS.$this->temp_doc_data['Document']['file'];
         if (file_exists($file))
            unlink($file);
      }
   }
}
?>