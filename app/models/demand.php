<?php
class Demand extends AppModel {
   
   var $name = 'Demand';
   
   var $actsAs = array('Searchable');
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id'),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'user_id'),
                          'Sector' => array('className' => 'Sector',
                                            'foreignKey' => 'sector_id',
                                            'countreCache' => true),
                          'Country' => array('className' => 'Country',
                                             'foreingKey' => 'country_id',
                                             'counterCache' => true),
                          'Currency' => array('className' => 'Currency',
                                              'foreingKey' => 'currency_id',
                                              'counterCache' => true),
                          'Dimension' => array('className' => 'Dimension',
                                               'foreingKey' => 'dimension_id',
                                               'counterCache' => true),
                          'Unit' => array('className' => 'Unit',
                                          'foreingKey' => 'unit_id',
                                          'counterCache' => true),
                          'Priceper' => array('className' => 'Unit',
                                              'foreingKey' => 'priceper_id'));
                                              
   var $hasMany = array('Dmessage' => array('className' => 'Dmessage',
                                            'foreignKey' => 'demand_id',
                                            'dependent' => true));
      
   var $validate = array('text' => array('rule1' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')),
                         'valid_from' => array('rule1' => array('rule' => array('date', 'ymd'),        
                                                                'message' => 'Enter a valid date.')),
                         'valid_to' => array('rule1' => array('rule' => array('date', 'ymd'),        
                                                              'message' => 'Enter a valid date.')),
                         'sector_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                               'message' => 'Select a demand category.')),
                         'quantity' => array('rule1' => array('rule'=> 'numeric',        
                                                               'message' => 'Enter a value.')),
                         'commodity' => array('rule1' => array('rule'=> 'notEmpty',        
                                                               'message' => 'This field cannot be left blank.')),
                         'country_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                                'message' => 'Select a country.')),
                         'unit_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                             'message' => 'Select a unit.')),                                       
                         'price' => array('rule1' => array('rule'=> 'numeric',        
                                                           'message' => 'Enter a value.')),
                         'currency_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                                 'message' => 'Select a currency.')));
                                                          
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   function indexData() {
		$index = array();
		$data = $this->data[$this->name];

    	foreach ($data as $key => $value) {
    		$columns = $this->getColumnTypes();
    		if ($key != $this->primaryKey && isset($columns[$key]) && in_array($columns[$key],array('text','varchar','char','string'))) {
    			$index []= strip_tags(html_entity_decode($value,ENT_COMPAT,'UTF-8'));
    		}
    	}
		$index = join('. ',$index);
		$index = iconv('UTF-8', 'UTF-8', $index);
		$index = preg_replace('/[\ ]+/',' ',$index);
		
		return $index;
	}
   
}
?>