<?php
class Country extends AppModel {

   var $name = 'Country';
   
   var $hasMany = array('Region' => array('className' => 'Region',
                                          'foreignKey' => 'country_id',
                                          'dependent' => true),
                        'Company' => array('className' => 'Company',
                                           'foreignKey' => 'country_id',
                                           'dependent' => true),
                        'Demand' => array('className' => 'Demand',
                                          'foreignKey' => 'country_id',
                                          'dependent' => true),
                        'Offer' => array('className' => 'Offer',
                                          'foreignKey' => 'country_id',
                                          'dependent' => true));

   var $validate = array('name_en' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')),
                         'name_ja' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')),
                         'image' => array('rule1' => array('rule' => 'imageSize',
                                                           'message' => 'Please upload a photo with a lower resolution.'),
                                          'rule2' => array('rule' => 'imageVal',
                                                           'message' => 'Please upload a PNG file format.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   function imageVal($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $type = exif_imagetype($value['tmp_name']);
         if (in_array($type, array(IMAGETYPE_PNG)))
            return true;
      }
      return false;
   }
   
   function imageSize($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $size = getimagesize($value['tmp_name']);
         if (in_array($size[2], array(IMAGETYPE_PNG)))
            if (($size[0] * $size[1]) < 120000)
               return true;
      }
      return false;
   }
   
   var $temp_country_data = null;

   function beforeDelete() {
      $this->temp_country_data = $this->find('first', array('conditions' => array('Country.id' => $this->id), 'recursive' => -1));
      if (empty($this->temp_country_data))
         return false;
      return true;
   }
   
   function afterDelete() {
      if (!empty($this->temp_country_data)) {
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'flags'.DS.$this->temp_country_data['Country']['id'].'.png';
         if (file_exists($file))
            unlink($file);
      }
   }
}
?>