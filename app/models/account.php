<?php
class Account extends AppModel {

   var $name = 'Account';
   
   var $hasMany = array('Company' => array('className' => 'Company',
                                           'foreignKey' => 'account_id'));

}
?>