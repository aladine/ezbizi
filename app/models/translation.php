<?php
class Translation extends AppModel {

   var $name = 'Translation';

   var $validate = array('text_en' => array('rule1' => array('rule' => array('maxLength', 500),
                                                             'message' => 'You have exceeded the maximum length of 500 characters.'),
                                            'rule2' => array('rule' => 'isUnique',
													    	                'message' => 'Phrase exists already.'),
                                            'rule3' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')));
                                                             
}
?>