<?php
class Sector extends AppModel {

   var $name = 'Sector';
   
   var $hasMany = array('Company' => array('className' => 'Company',
                                           'foreignKey' => 'sector_id',
                                           'dependent' => true),
                        'Demand' => array('className' => 'Demand',
                                          'foreignKey' => 'sector_id',
                                          'dependent' => true),
                        'Offer' => array('className' => 'Offer',
                                          'foreignKey' => 'sector_id',
                                          'dependent' => true));
   
   var $hasAndBelongsToMany = array('Category' => array('className' => 'Category',
                                                        'joinTable' => 'categories_sectors',
                                                        'foreignKey' => 'sector_id',
                                                        'associationForeignKey' => 'category_id',
                                                        'unique' => false));
   
   var $validate = array('name_en' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')),
                         'name_ja' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
                                                      
}
?>