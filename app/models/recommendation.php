<?php
class Recommendation extends AppModel {

   var $name = 'Recommendation';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id',
                                             'counterCache' => true),
                          'Senderc' => array('className' => 'Company',
                                             'foreignKey' => 'senderc_id'),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'user_id',
                                          'counterCache' => true));
   
   var $validate = array('text' => array('rule1' => array('rule' => 'notEmpty',        
                                                          'message' => 'Please write your recommendation.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
                                          
}
?>