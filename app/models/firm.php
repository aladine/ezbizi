<?php 
class Firm extends AppModel {

   var $name = 'Firm';
   
   var $belongsTo = array('Region' => array('className' => 'Region',
                                            'foreignKey' => 'region_id',
                                            'counterCache' => true),
                          'Country' => array('className' => 'Country',
                                             'foreignKey' => 'country_id',
                                             'counterCache' => true),
                          'Sector' => array('className' => 'Sector',
                                            'foreignKey' => 'sector_id',
                                            'counterCache' => true));
   
}
?>