<?php
class Region extends AppModel {

   var $name = 'Region';

   var $belongsTo = array('Country' => array('className' => 'Country',
                                             'foreignKey' => 'country_id',
                                             'counterCache' => true));
                                             
   var $validate = array('name_en' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')),
                         'name_ja' => array('rule1' => array('rule' => array('maxLength', 200),
                                                             'message' => 'You have exceeded the maximum length of 200 characters.'),
                                            'rule2' => array('rule' => 'notEmpty',        
                                                             'message' => 'This field cannot be left blank.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }                                         

}
?>