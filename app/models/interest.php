<?php
class Interest extends AppModel {

   var $name = 'Interest';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'recipient_id',
                                             'counterCache' => true),
                          'Interester' => array('className' => 'Company',
                                                'foreignKey' => 'sender_id'),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'user_id'));
                                          
}
?>