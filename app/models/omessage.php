<?php
class Omessage extends AppModel {

   var $name = 'Omessage';
   
   var $belongsTo = array('Offer' => array('className' => 'Offer',
                                            'foreingKey' => 'offer_id'),
                          'Currency' => array('className' => 'Currency',
                                              'foreingKey' => 'currency_id'),
                          'Priceper' => array('className' => 'Unit',
                                              'foreingKey' => 'priceper_id'));
   
   var $validate = array('text' => array('rule1' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')),
                         'price' => array('rule1' => array('rule'=> 'numeric',        
                                                           'message' => 'Enter a value.')),
                         'valid_to' => array('rule1' => array('rule' => array('date', 'ymd'),        
                                                              'message' => 'Enter a valid date.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
}
?>