<?php
class Photo extends AppModel {

   var $name = 'Photo';
   
   var $belongsTo = array('Album' => array('className' => 'Album',
                                           'foreignKey' => 'album_id',
                                           'counterCache' => true));
   var $validate = array('image' => array('rule1' => array('rule' => 'imageSize',
                                                           'message' => 'Please upload a photo with a lower resolution.'),
                                          'rule2' => array('rule' => 'imageVal',
                                                           'message' => 'Please upload a JPEG file format.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   function imageVal($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $type = exif_imagetype($value['tmp_name']);
         if (in_array($type, array(IMAGETYPE_JPEG)))
            return true;
      }
      return false;
   }
   
   function imageSize($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $size = getimagesize($value['tmp_name']);
         if (in_array($size[2], array(IMAGETYPE_JPEG)))
            if (($size[0] * $size[1]) < 12000000)
               return true;
      }
      return false;
   }
   
   var $temp_photo_data = null;

   function beforeDelete() {
      $this->temp_photo_data = $this->find('first', array('conditions' => array('Photo.id' => $this->id), 'recursive' => 1));
      if (empty($this->temp_photo_data))
         return false;
      return true;
   }
   
   function afterDelete() {
      if (!empty($this->temp_photo_data)) {
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'comps'.DS.$this->temp_photo_data['Album']['company_id'].DS.$this->temp_photo_data['Photo']['album_id'].DS.$this->temp_photo_data['Photo']['file'];
         if (file_exists($file))
            unlink($file);
      }
   }
                                          
}
?>