<?php
class Group extends AppModel {

   var $name = 'Group';
   
   var $actsAs = array('Acl' => array('type' => 'requester'));
   
   var $hasMany = array('User' => array('className' => 'User',
                                        'foreignKey' => 'group_id',
                                        'dependent' => true));
                                        
   function parentNode() {
      return null;
   }

}
?>