<?php
class Company extends AppModel {

   var $name = 'Company';
   
   //var $actsAs = array('Searchable');
   
   var $hasMany = array('User' => array('className' => 'User',
                                        'foreignKey' => 'company_id'),
                        'Interest' => array('className' => 'Interest',
                                            'foreignKey' => 'recipient_id',
                                            'dependent' => true),
                        'SentInterest' => array('className' => 'Interest',
                                                'foreignKey' => 'sender_id',
                                                'dependent' => true),
                        'Connection' => array('className' => 'Connection',
                                              'foreignKey' => 'com1_id',
                                              'dependent' => true),
                        'Connection2' => array('className' => 'Connection',
                                               'foreignKey' => 'com2_id',
                                               'dependent' => true),
                        'Recommendation' => array('className' => 'Recommendation',
                                                  'foreignKey' => 'company_id',
                                                  'dependent' => true),
                        'SendRecomm' => array('className' => 'Recommendation',
                                              'foreignKey' => 'senderc_id',
                                              'dependent' => true),
                        'Document' => array('className' => 'Document',
                                            'foreignKey' => 'company_id',
                                            'limit' => 5,
                                            'order' => 'Document.created DESC',
                                            'dependent' => true),
                        'Album' => array('className' => 'Album',
                                         'foreignKey' => 'company_id',
                                         'limit' => 4,
                                         'order' => 'Album.created DESC',
                                         'dependent' => true),
                        'Demand' => array('className' => 'Demand',
                                          'foreignKey' => 'company_id',
                                          'dependent' => true),
                         'Offer' => array('className' => 'Offer',
                                          'foreignKey' => 'company_id',
                                          'dependent' => true),
                        'Visit' => array('className' => 'Visit',
                                         'foreignKey' => 'company_id',
                                         'dependent' => true));
                                        
   var $belongsTo = array('Region' => array('className' => 'Region',
                                            'foreignKey' => 'region_id',
                                            'counterCache' => true),
                          'Country' => array('className' => 'Country',
                                             'foreignKey' => 'country_id',
                                             'counterCache' => true),
                          'Sector' => array('className' => 'Sector',
                                            'foreignKey' => 'sector_id',
                                            'counterCache' => true),
                          'Type' => array('className' => 'Type',
                                          'foreignKey' => 'type_id',
                                          'counterCache' => true),
                          'Account' => array('className' => 'Account',
                                             'foreignKey' => 'account_id',
                                             'counterCache' => true));
                                             
   var $validate = array('name' => array('rule1' => array('rule' => array('maxLength', 100),
                                                          'message' => 'You have exceeded the maximum length of 100 characters.'),
                                         'rule2' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')),
                         'address1' => array('rule1' => array('rule' => array('maxLength', 150),
                                                              'message' => 'You have exceeded the maximum length of 150 characters.'),
                                             'rule2' => array('rule' => 'notEmpty',        
                                                              'message' => 'This field cannot be left blank.')),
                         'address2' => array('rule1' => array('rule' => array('maxLength', 150),
                                                              'message' => 'You have exceeded the maximum length of 150 characters.')),
                         'address3' => array('rule1' => array('rule' => array('maxLength', 150),
                                                              'message' => 'You have exceeded the maximum length of 150 characters.')),
                         'zip' => array('rule1' => array('rule' => array('maxLength', 10),
                                                         'message' => 'You have exceeded the maximum length of 10 characters.'),
                                        /*'rule2' => array('rule' => 'notEmpty',        
                                                         'message' => 'This field cannot be left blank.')*/),
                         'city' => array('rule1' => array('rule' => array('maxLength', 150),
                                                          'message' => 'You have exceeded the maximum length of 150 characters.'),
                                         'rule2' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')),
                         'country_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                                'message' => 'Select a country.')),
                         /*'region_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                               'message' => 'Select a region.')),*/
                         'establish' => array('rule1' => array('rule' => array('date','ymd'),
                                                               'allowEmpty' => true,
                                                               'message' => 'Enter a valid date.')),
                         'sector_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                               'message' => 'Select a business category.')),
                         'phone1' => array('rule1' => array('rule' => array('maxLength', 5),
                                                            'message' => 'You have exceeded the maximum length of 5 characters.'),
                                           'rule2' => array('rule' => 'notEmpty',        
                                                            'message' => 'This field cannot be left blank.')),
                         'phone2' => array('rule1' => array('rule' => array('maxLength', 25),
                                                            'message' => 'You have exceeded the maximum length of 25 characters.'),
                                           'rule2' => array('rule' => 'notEmpty',        
                                                            'message' => 'This field cannot be left blank.')),
                         'regnum' => array('rule1' => array('rule' => array('maxLength', 25),
                                                            'message' => 'You have exceeded the maximum length of 25 characters.'),
                                           /*'rule2' => array('rule' => 'notEmpty',        
                                                            'message' => 'This field cannot be left blank.')*/),
                         'size' => array('rule1' => array('rule' => 'numeric',
                                                          'message' => 'Select a size or type a numeric value')),
                         'public_sector' => array('rule1' => array('rule' => 'boolean',
                                                                   'message' => 'Incorrect value')),
                         'fax1' => array('rule1' => array('rule' => array('maxLength', 5),
                                                          'message' => 'You have exceeded the maximum length of 5 characters.')),
                         'fax2' => array('rule1' => array('rule' => array('maxLength', 25),
                                                          'message' => 'You have exceeded the maximum length of 25 characters.')),
                         'web' => array('rule1' => array('rule' => 'url',
                                                         'allowEmpty' => true,
                                                         'message' => 'Enter url address.')),
                         'type_id' => array('rule1' => array('rule'=> 'notEmpty',        
                                                             'message' => 'Select a type.')),
                         'image' => array('rule1' => array('rule' => 'imageSize',
                                                           'message' => 'Please upload a photo with a lower resolution.'),
                                          'rule2' => array('rule' => 'imageVal',
                                                           'message' => 'Please upload a JPEG file format.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   function imageVal($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $type = exif_imagetype($value['tmp_name']);
         if (in_array($type, array(IMAGETYPE_JPEG)))
            return true;
      }
      return false;
   }
   
   function imageSize($check) {
      $value = array_values($check);
      $value = $value[0];
      if ($value['error'] == UPLOAD_ERR_OK) {
         $size = getimagesize($value['tmp_name']);
         if (in_array($size[2], array(IMAGETYPE_JPEG)))
            if (($size[0] * $size[1]) < 12000000)
               return true;
      }
      return false;
   }
   
   function indexData() {
		$index = array();
		$data = $this->data[$this->name];

    	foreach ($data as $key => $value) {
    		$columns = $this->getColumnTypes();
    		if ($key != $this->primaryKey && isset($columns[$key]) && in_array($columns[$key],array('text','varchar','char','string'))) {
    			$index []= strip_tags(html_entity_decode($value,ENT_COMPAT,'UTF-8'));
    		}
    	}
		$index = join('. ',$index);
		$index = iconv('UTF-8', 'UTF-8', $index);
		$index = preg_replace('/[\ ]+/',' ',$index);
		
		return $index;
	}
	
	var $temp_company_data = null;
   
   function beforeDelete() {
      $this->temp_company_data = $this->find('first', array('conditions' => array('Company.id' => $this->id), 'recursive' => -1));
      if (empty($this->temp_company_data))
         return false;
      return true;
   }
   
   function afterDelete() {
      if (!empty($this->temp_company_data)) {
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'comps'.DS.$this->temp_company_data['Company']['id'].DS.$this->temp_company_data['Company']['avatar'];
         if (file_exists($file))
            unlink($file);
      }
   }
   
}
?>