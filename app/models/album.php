<?php
class Album extends AppModel {
   
   var $name = 'Album';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id'));
                                             
   var $hasMany = array('Photo' => array('className' => 'Photo',
                                         'foreignKey' => 'album_id',
                                         'order' => 'created DESC',
                                         'dependent' => true));
                                         
   var $validate = array('name' => array('rule1' => array('rule' => array('maxLength', 100),
                                                          'message' => 'You have exceeded the maximum length of 100 characters.'),
                                         'rule2' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
   var $temp_album_data = null;

   function beforeDelete() {
      $this->temp_album_data = $this->find('first', array('conditions' => array('Album.id' => $this->id)));
      if (empty($this->temp_album_data))
         return false;
      return true;
   }
   
   function afterDelete() {
      if (!empty($this->temp_album_data)) {
         $dir = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'comps'.DS.$this->temp_album_data['Album']['company_id'].DS.$this->temp_album_data['Album']['id'];
         if (is_dir($dir))
            rmdir($dir);
      }
   }
   
}
?>