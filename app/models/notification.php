<?php
class Notification extends AppModel {

   var $name = 'Notification';
   
   var $belongsTo = array('Company' => array('className' => 'Company',
                                             'foreignKey' => 'company_id'),
                          'User' => array('className' => 'User',
                                          'foreignKey' => 'user_id'));
                                          
}
?>