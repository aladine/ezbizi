<?php
class Message extends AppModel {

   var $name = 'Message';
    
   var $belongsTo = array('Recipient' => array('className' => 'User',
                                               'foreignKey' => 'recipient_id'),
                          'Sender' => array('className' => 'User',
                                            'foreignKey' => 'sender_id'));
                                            
   var $validate = array('text' => array('rule1' => array('rule' => 'notEmpty',        
                                                          'message' => 'This field cannot be left blank.')),
                         'to' => array('rule1' => array('rule' => 'notEmpty',        
                                                        'message' => 'Select recipient.')));
   
   function invalidate($field, $value = true) {
      return parent::invalidate($field, __($value, true));
   }
   
}
?>