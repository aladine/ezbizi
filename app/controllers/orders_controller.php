<?php
class OrdersController extends AppController {

   var $helpers = array('Image');
   var $uses = array('Order', 'Invitation');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('add', 'index', 'paid', 'delete');
   }
   
   function paid($key = null, $pack_id = null) {
      if ($key == 'invitations' && !empty($pack_id)) {
         $invs = $this->Invitation->find('all', array('conditions' => array('Invitation.company_id' => $this->company['Company']['id'], 'Invitation.paid' => false), 'recursive' => -1));
         $package = $this->Order->Package->find('first', array('conditions' => array('Package.id' => $pack_id), 'recursive' => -1));
         if (!empty($package) && !empty($package['Package']['invitations']) && count($invs) >= $package['Package']['invitations']) {                                                                   
            $counter = 0;
            foreach ($invs as $inv) {
               $counter++;
               $this->Invitation->id = $inv['Invitation']['id'];
               $this->Invitation->saveField('paid', true);
               if ($counter >= $package['Package']['invitations'])
                  break;
            }
            $this->Order->Company->id = $this->company['Company']['id'];
            $this->Order->Company->saveField('account_id', $package['Package']['account_id']);
            $date = date('Y-m-d', strtotime('+'.$package['Package']['validity'].' month'));
            $this->Order->Company->saveField('valid', $date);
            $this->Session->setFlash(__('Congratulations! You have successfully upgraded your account.', true), 'flash');
         }
      }
      else if (!$this->Session->check('PaidCommandUsed') || $this->Session->read('Auth.User.group_id') == 1) {
         $this->Session->write('PaidCommandUsed', true);
         $order = $this->Order->find('first', array('conditions' => array('Order.key' => $key)/*, 'recursive' => -1*/));
         if (empty($order))
            $this->redirect($this->referer());
         $this->Session->delete('PaidCommandUsed');
         if (!empty($_POST['payment_status']) && $_POST['payment_status'] != 'Completed')
            die;
         $this->Order->id = $order['Order']['id'];
         $this->Order->saveField('paid', true);
         $this->Order->saveField('key', null);
         if (!empty($order['Package']['account_id'])) {
            $this->Order->Company->id = $order['Order']['company_id'];
            $this->Order->Company->saveField('account_id', $order['Package']['account_id']);
            $date = date('Y-m-d', strtotime('+'.$order['Package']['validity'].' month'));
            $this->Order->Company->saveField('valid', $date);
         }
         $this->Session->setFlash(__('Payment has been recorded.', true), 'flash');
         if ($this->Session->read('Auth.User.group_id') != 1)
            die;
      }
      $this->redirect($this->referer());
   }
   
   function delete($id = null) {
     $order = $this->Order->find('first', array('conditions' => array('Order.id' => $id), 'recursive' => -1));
     if (!empty($order) && $this->Order->delete($id)) {
        $this->Session->setFlash(__('Order has been removed.', true), 'flash');
     }
     $this->redirect($this->referer());
   }
   
   function index() {
      $this->paginate['Order'] = array('limit' => 10, 'order' => array('Order.paid', 'Order.created DESC'));
      $orders = $this->paginate('Order');
      $this->set('orders', $orders);
   }
   
   function add($id = null) {
      $package = $this->Order->Package->find('first', array('conditions' => array('Package.id' => $id)));
      if (empty($package))
         $this->redirect($this->referer());
      if ($package['Package']['account_id'] == -1) {
         $this->Session->setFlash('Uhrada poplatku', 'flash');
         $this->redirect($this->referer());
      }
      if ($this->company['Company']['account_id'] != 1)
         $this->redirect($this->referer());
      $order = $this->Order->find('first', array('conditions' => array('Order.company_id' => $this->company['Company']['id'], 'Order.paid' => false), 'recursive' => -1));
      if (!empty($order))
         $this->redirect($this->referer());
      $data['Order']['company_id'] = $this->company['Company']['id'];
      $data['Order']['package_id'] = $id;
      $data['Order']['key'] = $this->_activateKeyGenerator(64);
      if ($this->Order->save($data)) {
         $amount = $package['Package']['price'];
         $item_name = substr(strtr($package['Package']['name_en'], array(' ' => '+')), 0, 127);     //max 127 chars
         $item_number = $id;              //max 127 chars
         $custom = $data['Order']['key'];
         $invoice = $this->Order->id;
         $business = 'dusan._1318411856_biz@gmail.com';
         $page_style = 'primary';
         $return = 'http://www.aitechnology.sk/ezbizi/'.$this->lang.'/company-settings/account';
         $notify_url = 'http://www.aitechnology.sk/ezbizi/'.$this->lang.'/order-paid/'.$data['Order']['key'];
         $this->redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_xclick&bn=Ezbizi_ByuNow_WPS_SG&business='.$business.'&amount='.$amount.'&item_name='.$item_name.'&item_number='.$item_number.'&custom='.$custom.'&invoice='.$invoice.'&page_style='.$page_style.'&no_note=1&no_shipping=1&return='.$return.'&notify_url='.$notify_url);
      }
      $this->redirect($this->referer());
   }
   
   function _activateKeyGenerator($length) {
      $done = true;
      do {
         $key = $this->_passwordGenerator($length);
         if (!$this->Order->find('first', array('conditions' => array('Order.key' => $key), 'recursive' => -1))) {
            $done = false;
            $this->data['Order']['key'] = $key;
         }
      } while ($done);
      return $key;
   }
   
   function _passwordGenerator($length) {
		 $chars = 'akuFmqTs0HAc9NwBe2Q3Go1gLZijDhM8OSYn4W6UPzJERI5rXpfdV7ytKvCxlb';
		 $password = "";
		 for ($i = 0; $i < $length; $i++) {
				$password .= $chars[(rand() % strlen($chars))];
		 }
		 return $password;
	}

}
?>