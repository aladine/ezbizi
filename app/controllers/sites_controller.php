<?php
class SitesController extends AppController {
   
   var $helpers = array('Image');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('add', 'view', 'delete', 'edit');
   }
   
   function delete($id = null) {
      if ($this->Site->delete($id)) 
         $this->Session->setFlash(__('Site was deleted.', true), 'flash');
      $this->redirect($this->referer());
   }
   
   function edit($id = null) {
      if (!empty($this->data)) {
         $this->Site->id = $id;
         $this->data['Site']['url_name_en'] = normalize($this->data['Site']['name_en']);
         if ($this->Site->save($this->data)) {
            $this->Session->setFlash(__('Site was successfully updated.', true), 'flash');
            $this->redirect(array('controller' => 'sites', 'action' => 'index'));
         }
      }
      else
         $this->data = $this->Site->find('first', array('conditions' => array('Site.id' => $id)));
   }
   
   function index() {
      $this->paginate['Site'] = array('limit' => 10, 'order' => array('name_'.$this->lang => 'asc'));
      $sites = $this->paginate('Site');
      $this->set('sites', $sites);
   }

   function add() {
      $this->set('title_for_layout', __('Adding pages', true));
      if (!empty($this->data)) {
         $this->data['Site']['url_name_en'] = normalize($this->data['Site']['name_en']);
         if ($this->Site->save($this->data)) {
            $this->Session->setFlash(__('Site was successfully added.', true), 'flash');
            $this->redirect(array('controller' => 'sites', 'action' => 'index'));
         }
      }
   }
   
   function view($url_name = null) {
      $site = $this->Site->find('first', array('conditions' => array('Site.url_name_en' => $url_name)));
      if (empty($site))
         $this->redirect(array('controller' => 'users', 'action' => 'home'));

      $this->set('title_for_layout', $site['Site']['name_en']);
      $this->set('site', $site);
   }
}
?>