<?php
class CountriesController extends AppController {
   
   var $helpers = array('Image');
     
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('index', 'delete');
   }
   
   function delete($id = null) {
      if ($this->Country->delete($id)) {
         $this->Session->setFlash(__('Country has been successfully removed.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function index() {
      $this->paginate['Country'] = array('limit' => 10, 'order' => array('name_'.$this->lang => 'asc'));
      $countries = $this->paginate('Country');
      $this->set('countries', $countries);
   }
   
   function add() {
      if (!empty($this->data)) {
         if ($this->Country->save($this->data)) {
            __fileSave($this->data['Country']['image'], 'img'.DS.'flags'.DS.$this->Country->id.'.png', 24, 24);
            
            $this->Session->setFlash(__('Country has been successfully added.', true), 'flash');
            $this->redirect(array('controller' => 'countries', 'action' => 'index'));
         }
      }
   }

}
?>