<?php
class NotificationsController extends AppController {
   
   var $helpers = array('Image');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function index($id = null) {
      if (empty($id)) {
         $id = $this->Session->read('Auth.User.company_id');
         $this->set('pcompany', $this->company);
      }
      else {
         $pcompany = $this->Notification->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
         if (empty($pcompany))
            $this->redirect($this->referer());
         $this->set('pcompany', $pcompany);
      }
      $this->set('title_for_layout', __('Newsfeeds', true));

      $comp_ids = array();
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com2_id' => $this->Session->read('Auth.User.company_id'))), 'recursive' => -1));
         if (!empty($conns)) {
            foreach ($conns as $conn) {
               if ($conn['Connection']['com1_id'] == $this->Session->read('Auth.User.company_id'))
                  $comp_ids []= $conn['Connection']['com2_id'];
               else
                  $comp_ids []= $conn['Connection']['com1_id'];
            }
         }
         $comp_ids []= $this->Session->read('Auth.User.company_id');
      }
      else
         $comp_ids []= $id;
      
      $this->paginate['Notification'] = array('limit' => 10, 'order' => array('Notification.created DESC'));
      
      $conn = $this->User->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));      
      $notifications = null;
      $conditions = array('Notification.company_id' => $comp_ids, 'Notification.user_id !=' => $this->User->id);
      if ($id != $this->company['Company']['id'] && $this->company['Company']['account_id'] != 3 && empty($conn))
         $conditions['Notification.type NOT'] = array('new_demand', 'user_status');
      else if ($id != $this->company['Company']['id'])
         $conditions['Notification.type !='] = 'user_status';      
      $notifications = $this->paginate('Notification', $conditions);
      
      if (!empty($notifications)) {
         foreach ($notifications as $ind => $notification) {
            if ($notification['Notification']['type'] == 'new_photos') {
               $data = explode('|', $notification['Notification']['text']);
               $album = $this->User->Company->Album->find('first', array('conditions' => array('Album.id' => $data[0]), 'recursive' => -1));
               if (empty($album)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               
               if ($this->company['Company']['id'] != $album['Album']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                  if (!in_array($album['Album']['company_id'], $comp_ids) && $album['Album']['privacy'] != 1) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  if (in_array($album['Album']['company_id'], $comp_ids) && !in_array($album['Album']['privacy'], array(1, 2))) {
                     unset($notifications[$ind]);
                     continue;
                  }
               }
               
               $notifications[$ind]['Album'] = $album['Album'];
               unset($data[0]);
               $photos = $this->User->Company->Album->Photo->find('list', array('conditions' => array('Photo.id' => $data), 'fields' => array('Photo.id', 'Photo.file')));
               $notifications[$ind]['Photo'] = $photos;
            }
            else if ($notification['Notification']['type'] == 'new_document') {
               $document = $this->User->Company->Document->find('first', array('conditions' => array('Document.id' => $notification['Notification']['text']), 'recursive' => -1));
               if (empty($document)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               
               if ($this->company['Company']['id'] != $document['Document']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                  if (!in_array($document['Document']['company_id'], $comp_ids) && $document['Document']['privacy'] != 1) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  if (in_array($document['Document']['company_id'], $comp_ids) && !in_array($document['Document']['privacy'], array(1, 2))) {
                     unset($notifications[$ind]);
                     continue;
                  }
               }
                  
               $notifications[$ind]['Document'] = $document['Document'];
            } 
            else if ($notification['Notification']['type'] == 'new_demand') {
               $demand = $this->User->Company->Demand->find('first', array('conditions' => array('Demand.id' => $notification['Notification']['text'])));
               if (empty($demand)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               $notifications[$ind]['Demand'] = $demand;
            }
            else if ($notification['Notification']['type'] == 'connection') {
               $connection = $this->User->Company->find('first', array('conditions' => array('Company.id' => $notification['Notification']['text'])));
               if (empty($connection)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               $notifications[$ind]['Connection'] = $connection;
            }                                                                                                               
         }
      }
      $this->set('notifications', $notifications);
      
      $types = $this->User->Company->Type->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('types', $types);
      
      $this->_companyPanel($id);
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids); 
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }
   
}
?>