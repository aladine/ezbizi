<?php
class BansController extends AppController {

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function delete($id = null) {
      $user = $this->Ban->User->find('first', array('conditions' => array('User.id' => $id), 'recursive' => -1));
      if (!empty($user)) {
         $ban = $this->Ban->find('first', array('conditions' => array('Ban.sender_id' => $id, 'Ban.recipient_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
         if (!empty($ban) && $this->Ban->delete($ban['Ban']['id']))
            $this->Session->setFlash(__('Messages from this user have been unblocked succesfully.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function add($id = null) {
      $user = $this->Ban->User->find('first', array('conditions' => array('User.id' => $id), 'recursive' => -1));
      if (!empty($user) && $this->Session->read('Auth.User.company_id') != $id) {
         $ban = $this->Ban->find('first', array('conditions' => array('Ban.sender_id' => $id, 'Ban.recipient_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
         if (empty($ban)) {
            $data['Ban']['sender_id'] = $id;
            $data['Ban']['recipient_id'] = $this->Session->read('Auth.User.id');
            if ($this->Ban->save($data))
               $this->Session->setFlash(__('Messages from this user have been blocked succesfully.', true), 'flash');
         }
         else
            $this->Session->setFlash(__('Messages from this user are already blocked.', true), 'flash');
      } 
      $this->redirect($this->referer());
   }

}
?>