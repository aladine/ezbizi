<?php
class OffersController extends AppController {
   
   var $helpers = array('Image');
   var $uses = array('Offer', 'Notification', 'SearchIndex');
   var $components = array('Email');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('display', 'search', 'view','index');
   }
   
   function resave() {
      $offers = $this->Offer->find('all', array('recursive' => -1));
		foreach ($offers as $offer) {
			$this->Offer->id = $offer['Offer']['id'];
			$this->Offer->save($offer, false);
	   }
	   die;
   }
   
   function search($query = null) {
      $categories = $this->Offer->Company->Sector->Category->find('threaded');
      $this->set('categories', $categories);
      $countries = $this->User->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('countries', $countries);
      $currencies = $this->Offer->Currency->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('currencies', $currencies);
      $units = $this->Offer->Unit->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('units', $units);
      if (!empty($this->data['Search']['query']) || !empty($query)) {
         if (!empty($this->data['Search']['type_id']) && $this->data['Search']['type_id'] == 2)
            $this->redirect(array('controller' => 'companies', 'action' => 'search', $this->data['Search']['query']));
         if (!empty($this->data['Search']['query']))
            $this->redirect(array('controller' => 'offers', 'action' => 'search', $this->data['Search']['query']));
         $string = $query;
         App::import('Core', 'Sanitize');
		   $string = Sanitize::escape($string);
		   if (empty($string))
            $this->redirect($this->referer());
	      $this->SearchIndex->searchModels(array('Offer'));

    	 	$this->paginate = array('SearchIndex' => array('limit' => 10, 'recursive' => 2,
    	 	                        'fields' => array('Offer.id'),
			                        'conditions' =>  "(MATCH(SearchIndex.data) AGAINST('$string' IN BOOLEAN MODE) OR SearchIndex.data LIKE '%$string%') AND (`Offer`.`valid_from` <= CURDATE()) AND (`Offer`.`valid_to` >= CURDATE())"));
		   $ids = $this->paginate('SearchIndex');
		   $idss = array();
		   foreach ($ids as $id) {
            $idss []= $id['Offer']['id'];
         }
		   $offers = $this->Offer->find('all', array('conditions' => array('Offer.id' => $idss), 'order' => array('Company.account_id DESC', 'Offer.created DESC'), 'recursive' => 2));
		   //$this->set('options_url', array('controller' => 'products', 'action' => 'search', $string));
	      $this->set('offers', $offers);  
         return;
      }
      if (!empty($this->data)) {
         $conditions = null;        
         if (!empty($this->data['Offer']['commodity']))
            $conditions['Offer.commodity LIKE'] = '%'.$this->data['Offer']['commodity'].'%';
         if (!empty($this->data['Offer']['country_id']) && is_numeric($this->data['Offer']['country_id']))
            $conditions['Offer.country_id'] = $this->data['Offer']['country_id'];
         if (!empty($this->data['Offer']['sector_id']) && is_numeric($this->data['Offer']['sector_id']))
            $conditions['Offer.sector_id'] = $this->data['Offer']['sector_id'];
         if (!empty($this->data['Offer']['currency_id']) && is_numeric($this->data['Offer']['currency_id'])) {            
            $conditions['Offer.currency_id'] = $this->data['Offer']['currency_id'];
            if (!empty($this->data['Offer']['price_min']) && is_numeric($this->data['Offer']['price_min']))
               $conditions['Offer.norm_price >='] = $this->data['Offer']['price_min'];
            if (!empty($this->data['Offer']['price_max']) && is_numeric($this->data['Offer']['price_max']))
               $conditions['Offer.norm_price <='] = $this->data['Offer']['price_max'];
         }
         if (!empty($this->data['Offer']['unit_id']) && is_numeric($this->data['Offer']['unit_id'])) {
            $unit = $this->Offer->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Offer']['unit_id'])));
            if (!empty($unit)) {            
               $units = $this->Offer->Unit->find('list', array('conditions' => array('Unit.dimension_id' => $unit['Unit']['dimension_id']), 'fields' => array('Unit.id')));
               $conditions['Offer.unit_id'] = $units;
               if (!empty($this->data['Offer']['quantity_min']) && is_numeric($this->data['Offer']['quantity_min']))
                  $conditions['Offer.norm_quantity >='] = $this->data['Offer']['quantity_min'] * $unit['Unit']['multiplier'];
               if (!empty($this->data['Offer']['quantity_max']) && is_numeric($this->data['Offer']['quantity_max']))
                  $conditions['Offer.norm_quantity <='] = $this->data['Offer']['quantity_max'] * $unit['Unit']['multiplier'];             
            }
         }
         $this->Session->write('offersearchSettings', $conditions);
         $this->Session->write('offersearchSettingsData', $this->data);
      }
      if (!$this->Session->check('offersearchSettings'))
         $this->redirect($this->referer());
      //debug($this->Session->read('offersearchSettings'));
      $conditions = $this->Session->read('offersearchSettings');
      if (empty($this->data) && $this->Session->check('offersearchSettingsData')) {
         $this->data = $this->Session->read('offersearchSettingsData');
      }
      $this->paginate['Offer'] = array('order' => array('Company.account_id DESC', 'OfferDate DESC'), 'fields' => array('Offer.id', 'Offer.company_id', 'Offer.quantity', 'Offer.valid_from', 'Offer.valid_to', 'Offer.commodity', 'Offer.price', 'Offer.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Offer.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Offer`.`created`) AS OfferDate'), 'limit' => 10, 'recursive' => 2);
      $offers = $this->paginate('Offer', $conditions);
      $this->set('offers', $offers);
      
   }
   
   function _catsec($id) {
      $list = $this->Offer->Sector->CategoriesSector->find('list', array('conditions' => array('category_id' => $id), 'fields' => array('sector_id')));
      $cats = $this->Offer->Sector->Category->find('list', array('conditions' => array('Category.parent_id' => $id), 'fields' => array('id')));
      if (!empty($cats)) {
         foreach ($cats as $cat) {
            $secs = $this->_catsec($cat);
            $list = array_merge($list, $secs);
         }
      }
      return $list;
   }
   
   function display() {
      $args = func_get_args();
      $id = end($args);
      $type = prev($args);
      if (!in_array($type, array('c', 's')))
         $this->redirect($this->referer());
      
      $sectors = array();   
      if ($type == 's') {
         $sector = $this->Offer->Sector->find('first', array('conditions' => array('Sector.id' => $id), 'recursive' => -1));
         if (empty($sector))
            $this->redirect($this->referer());
         $sectors []= $sector['Sector']['id'];
         
      }     
      $category = null;
      if ($type == 'c') {
         $category = $this->Offer->Sector->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
         if (empty($category))
            $this->redirect($this->referer());
         $category['last'] = true;
         $sectors = $this->_catsec($category['Category']['id']);
         $this->set('model', array('Category', 'c'));
         $subs = $this->Offer->Sector->Category->find('all', array('conditions' => array('Category.parent_id' => $category['Category']['id']), 'recursive' => -1));
         if (empty($subs)) {
            $this->set('model', array('Sector', 's'));
            $secs = $this->_catsec($category['Category']['id']);
            $subs = $this->Offer->Sector->find('all', array('conditions' => array('Sector.id' => $secs), 'recursive' => -1));
         }
         $this->set('subs', $subs);
      }
      
      if (!empty($sector)) {
         $this->set('sector', $sector);
         $catid = $this->Offer->Sector->CategoriesSector->field('category_id', array('sector_id' => $sector['Sector']['id']));
         $category = $this->Offer->Sector->Category->find('first', array('conditions' => array('Category.id' => $catid), 'recursive' => -1));
         $secs = $this->_catsec($catid);
         $secs = $this->Offer->Sector->find('all', array('conditions' => array('Sector.id' => $secs), 'recursive' => -1));
         $this->set('model', array('Sector', 's'));
         $this->set('subs', $secs);
      }
      
      $this->set('title_for_layout', __('Public view of offers', true));
      
      $path = array($category);         
      while (!empty($category['Category']['parent_id'])) {
         $category = $this->Offer->Sector->Category->find('first', array('conditions' => array('Category.id' => $category['Category']['parent_id']), 'recursive' => -1));
         $path []= $category;
      }
      $path = array_reverse($path);
      $this->set('path', $path);

      $this->paginate['Offer'] = array('order' => array('OfferDate DESC', 'Company.account_id DESC'), 'fields' => array('Offer.id', 'Offer.company_id', 'Offer.quantity', 'Offer.commodity', 'Offer.price', 'Offer.valid_from', 'Offer.valid_to', 'Offer.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Offer.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Offer`.`created`) AS OfferDate'), 'limit' => 10, 'recursive' => 2);
      $offers = $this->paginate('Offer', array('Offer.sector_id' => $sectors, 'Offer.valid_from <=' => date('Y-m-d'), 'Offer.valid_to >=' => date('Y-m-d')));
      $this->set('offers', $offers);
   }
   
   function delete($id = null) {
      $offer = $this->Offer->find('first', array('conditions' => array('Offer.id' => $id), 'recursive' => -1));
      if (!empty($offer) && ($offer['Offer']['company_id'] == $this->Session->read('Auth.User.company_id') || $this->Session->read('Auth.User.group_id') == 1) && $this->Offer->delete($id)) {
         $this->Session->setFlash(__('Offer has been successfully removed.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function view($id = null) {
      $offer = $this->Offer->find('first', array('conditions' => array('Offer.id' => $id), 'recursive' => 2));
      if (empty($offer))
         $this->redirect($this->referer());
      if ($offer['Offer']['valid_from'] > date('Y-m-d') || $offer['Offer']['valid_to'] < date('Y-m-d')) {
         $this->Session->setFlash(__('This offer has not yet started or is out of date. Please double-check the validity period of this Offer.', true), 'flash');
         if ($this->Session->read('Auth.User.company_id') != $offer['Offer']['company_id'])
            $this->redirect($this->referer());
      }
      if ($this->Session->check('Auth.User.id') && !empty($this->data)) {
         $last_m = $this->Offer->Omessage->find('first', array('conditions' => array('offer_id' => $offer['Offer']['id'], 'OR' => array(array('recipient_id' => $offer['Offer']['user_id'], 'sender_id' => $this->Session->read('Auth.User.id')), array('recipient_id' => $this->Session->read('Auth.User.id'), 'sender_id' => $offer['Offer']['user_id']))), 'order' => 'created DESC', 'recursive' => -1));
         if (!empty($last_m))
            $this->redirect($this->referer());
         if ($this->company['Company']['account_id'] == 1 && $this->company['Company']['reaction_count'] >= 30) {
            $this->Session->setFlash(__('Sorry! You have reached your limit for sending offers for this month. In order to send more offers this month, please upgrade your account.', true), 'flash');
            $this->redirect($this->referer());
         }
         $this->data['Omessage']['offer_id'] = $offer['Offer']['id'];
         $this->data['Omessage']['recipient_id'] = $offer['Offer']['user_id'];
         $this->data['Omessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Omessage']['currency_id'] = $offer['Offer']['currency_id'];
         $this->data['Omessage']['priceper_id'] = $offer['Offer']['priceper_id'];
         $this->data['Omessage']['accepted'] = 0;
         if (!empty($this->data['Omessage']['valid_to']))
            $this->data['Omessage']['valid_to'] = date('Y-m-d', strtotime($this->data['Omessage']['valid_to']));
         if ($this->Offer->Omessage->save($this->data)) {
            $this->User->id = $offer['Offer']['user_id'];
            $unread = $this->Offer->Omessage->find('count', array('conditions' => array('Omessage.recipient_id' => $this->User->id, 'Omessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
             
            $this->Offer->Company->id = $this->company['Company']['id'];
            $rcount = $this->Offer->Company->field('reaction_count') + 1;
            $this->Offer->Company->saveField('reaction_count', $rcount);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $this->data['Omessage']['recipient_id']), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $data['Offer'] = $offer;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New offer message', true), 'omessage', $data);
            }
            
            $this->Session->setFlash(__('Your offer has been sent successfully.', true), 'flash');
            $this->redirect(array('controller' => 'offers', 'action' => 'view', $id));
         }
      }
      
      if ($this->Session->check('Auth.User.id')) {
         $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com2_id' => $this->Session->read('Auth.User.company_id'))), 'recursive' => -1));
         $comp_ids = array();
         if (!empty($conns)) {
            foreach ($conns as $conn) {
               if ($conn['Connection']['com1_id'] == $this->Session->read('Auth.User.company_id'))
                  $comp_ids[$conn['Connection']['com2_id']] = $conn['Connection']['com2_id'];
               else
                  $comp_ids[$conn['Connection']['com1_id']] = $conn['Connection']['com1_id'];
            }
         }
         if (isset($comp_ids[$offer['Offer']['company_id']]))
            $offer['Connected'] = true;
      }
      
      $this->set('offer', $offer);
      $this->set('title_for_layout', $offer['Offer']['quantity'].' '.$offer['Unit']['name_'.$this->lang].' '.__('of', true).' '.$offer['Offer']['commodity'].' '.__('for', true).' '.$offer['Offer']['price'].' '.$offer['Currency']['name_'.$this->lang].' '.__('is needed.', true));
   }
   
   function index($task = null) {
      $this->set('title_for_layout', __('offers', true));
      $user = $this->Session->read('Auth.User');
      $this->set('user', $user);
      
      if (empty($task)) {
         if ($this->company['Company']['type_id'] == 1)
            $task = 'added';
         else
            $task = 'latests';
      }
      
      switch ($task) {
         case 'latests':
            $this->paginate['Offer'] = array('order' => array('Company.account_id DESC', 'OfferDate DESC'), 'fields' => array('Offer.id', 'Offer.company_id', 'Offer.quantity', 'Offer.valid_from', 'Offer.valid_to', 'Offer.commodity', 'Offer.price', 'Offer.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Offer.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Offer`.`created`) AS OfferDate'), 'limit' => 10, 'recursive' => 2);
            $conditions = array('Offer.valid_from <=' => date('Y-m-d'), 'Offer.valid_to >=' => date('Y-m-d'), 'Offer.company_id !=' => $user['company_id']);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Offer.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Offer.sector_id'] = $this->company['Company']['i_sector_id'];
            $offers = $this->paginate('Offer', $conditions); 
                                                                                                                                  
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids[$conn['Connection']['com2_id']] = $conn['Connection']['com2_id'];
                  else
                     $comp_ids[$conn['Connection']['com1_id']] = $conn['Connection']['com1_id'];
               }
            }
            if (!empty($offers)) {
               foreach ($offers as $ind => $offer) {
                  if (isset($comp_ids[$offer['Offer']['company_id']]))
                     $offers[$ind]['Connected'] = true;
               }
            }
            
            $this->set('offers', $offers);
            $this->set('latests', true);
            break;
         case 'added':
            $this->paginate['Offer'] = array('order' => array('Offer.created DESC'), 'limit' => 10, 'recursive' => 2);
            $offers = $this->paginate('Offer', array('Offer.company_id' => $user['company_id']));
            //$offers = $this->Offer->find('all', array('conditions' => array('Offer.company_id' => $user['company_id']), 'order' => array('Offer.created DESC'), 'limit' => 10, 'recursive' => 2));
            $this->set('offers', $offers);
            $this->set('added', true);
            break;
         case 'all':
            $this->paginate['Offer'] = array('order' => array('Company.account_id DESC', 'OfferDate DESC'), 'fields' => array('Offer.id', 'Offer.company_id', 'Offer.quantity', 'Offer.valid_from', 'Offer.valid_to', 'Offer.commodity', 'Offer.price', 'Offer.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Offer.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Offer`.`created`) AS OfferDate'), 'limit' => 10, 'recursive' => 2);
            $conditions = array('Offer.valid_from <=' => date('Y-m-d'), 'Offer.valid_to >=' => date('Y-m-d'));
            $offers = $this->paginate('Offer', $conditions); 
            $this->set('offers', $offers);
            $this->set('all', true);
            break;
         default:
            $this->redirect($this->referer());
            break;
      }
      
      $categories = $this->Offer->Company->Sector->Category->find('threaded');
      $this->set('categories', $categories);
      $countries = $this->User->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('countries', $countries);
      $currencies = $this->Offer->Currency->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('currencies', $currencies);
      $units = $this->Offer->Unit->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('units', $units);
      
   }
   
   function add() {
      if (!$this->Session->check('Auth.User.id')) {
         $this->Session->setFlash(__('In order to access this information, please login or register your account with Ezbizi!', true), 'flash');
         $this->redirect(array('controller' => 'users', 'action' => 'register'));
      }
      if ($this->company['Company']['type_id'] == 2) {
         $this->Session->setFlash(__('Sorry! Your account is set to "seller" type, therefore you are not able to post a Offer. In order to post your offer, please change the type of your account to "buyer" or "trader".', true), 'flash');
         $this->redirect($this->referer());
      }                                                   
      $this->set('title_for_layout', __('Post offer', true));
      $categories = $this->Offer->Company->Sector->Category->find('threaded');
      $this->set('categories', $categories);
      $sectors = $this->Offer->Company->Sector->find('list', array('fields' => array('id', 'name_'.$this->lang), 'order' => array('name_'.$this->lang)));
      $this->set('sectors', $sectors);
      $countries = $this->Offer->Country->find('list', array('fields' => array('id', 'name_'.$this->lang), 'order' => array('Country.name_'.$this->lang)));
      $this->set('countries', $countries);
      $currencies = $this->Offer->Currency->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('currencies', $currencies);
      $units = $this->Offer->Unit->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('units', $units);
      if (!empty($this->data)) {
         if ($this->company['Company']['account_id'] == 1 && $this->company['Company']['offer_count'] >= 15) {
            $this->Session->setFlash(__('Sorry! You have reached your limit for posting offers for this month. In order to post more offers this month, please upgrade your account.', true), 'flash');
            $this->redirect($this->referer());
         }
         if (!empty($this->data['Offer']['valid_from']))
            $this->data['Offer']['valid_from'] = date('Y-m-d', strtotime($this->data['Offer']['valid_from']));
         if (!empty($this->data['Offer']['valid_to']))
            $this->data['Offer']['valid_to'] = date('Y-m-d', strtotime($this->data['Offer']['valid_to']));
         $this->data['Offer']['company_id'] = $this->Session->read('Auth.User.company_id');
         $this->data['Offer']['user_id'] = $this->Session->read('Auth.User.id');
         $this->Offer->set($this->data);
         if ($this->Offer->validates()) {
            $unit = $this->Offer->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Offer']['unit_id']), 'recursive' => -1));
            $this->data['Offer']['dimension_id'] = $unit['Unit']['dimension_id'];
            $this->data['Offer']['norm_quantity'] = $unit['Unit']['multiplier'] * $this->data['Offer']['quantity'];
            $price = 0;
            // Zaplata zmeny podmienok vyradenim vyznamu priceper_id (priceper_id = unit_id) - zachovanie kompatibility
            $this->data['Offer']['priceper_id'] = $this->data['Offer']['unit_id'];
            if (empty($this->data['Offer']['priceper_id']))
               $price = $this->data['Offer']['price'];
            else {
               $unit = $this->Offer->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Offer']['priceper_id']), 'recursive' => -1));
               $price = ($this->data['Offer']['price'] / $unit['Unit']['multiplier']) * $this->data['Offer']['norm_quantity'];
            }
            $this->data['Offer']['norm_price'] = $price; 
            if ($this->Offer->save($this->data)) {
               $this->Session->setFlash(__('Your offer has been posted successfully.', true), 'flash');
               
               $this->Offer->Company->id = $this->company['Company']['id'];
               $dcount = $this->Offer->Company->field('offer_count') + 1;
               $this->Offer->Company->saveField('offer_count', $dcount);
               
               $notice['Notification']['company_id'] = $this->Session->read('Auth.User.company_id');
               $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
               $notice['Notification']['type'] = 'new_offer';
               $notice['Notification']['text'] = $this->Offer->id;
               $this->Notification->save($notice);
               
               $this->redirect(array('controller' => 'users', 'action' => 'home'));
            }
         }
         else {         
            $errors = $this->Offer->invalidFields();
            if (!empty($errors['sector_id']))
               $this->Session->setFlash($errors['sector_id'], 'flash');
         }
      }
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}
   
}
?>