<?php
class CompaniesController extends AppController {

   var $components = array('RequestHandler', 'Email');
   var $helpers = array('Image');
   var $uses = array('Company', 'Size', 'Notification', 'SearchIndex', 'Order', 'Package', 'Invitation', 'Firm', 'People');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('invitation', 'search', 'acc_verified','view');
   }
   
   function delete_avatar() {
      $this->Company->id = $this->Session->read('Auth.User.company_id');
      $avatar = $this->Company->field('avatar', array('Company.id' => $this->Company->id));
      if (!empty($avatar)) {
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'comps'.DS.$this->Company->id.DS.$avatar;
         if (file_exists($file))
            unlink($file);
         $this->Company->saveField('avatar', null);
      }
      $this->redirect($this->referer());
   }
   
   function invitation() {
      $user = $this->Company->User->find('first', array('conditions' => array('User.id' => 3), 'recursive' => -1));
      $this->_userMail($user, __('One time only invitation to join Ezbizi beta', true), 'invitation', $user);
      die;
   }
   
   function set_account($id = null) {
      $comp = $this->Company->find('first', array('conditions' => array('Company.id' => $id)));
      $this->set('comp', $comp);
      $accounts = $this->Company->Account->find('list', array('fields' => array('Account.id', 'Account.name_'.$this->lang)));
      $this->set('accounts', $accounts);
      if (!empty($this->data)) {
         if (!empty($this->data['Company']['validity_to'])) 
            $this->data['Company']['valid'] = $this->data['Company']['validity_to']['year'].'-'.$this->data['Company']['validity_to']['month'].'-'.$this->data['Company']['validity_to']['day'];
         $this->Company->id = $id;
         if ($this->Company->save($this->data)) {
            $this->Session->setFlash(__('Type of the account has been changed.', true), 'flash');
         }
      }
      else {
         $this->data['Company']['account_id'] = $comp['Company']['account_id'];
         if (!empty($comp['Company']['valid']))
            $this->data['Company']['validity_to'] = $comp['Company']['valid']; 
      }
   }
   
   function acc_verified($id = null) {
      $regnum_verified = $this->Company->field('regnum_verified', array('Company.id' => $id));
      $this->Company->id = $id;
      if ($regnum_verified) {
         $this->Company->saveField('regnum_verified', false);
         $this->Session->setFlash(__('Account verified flag has been removed.', true), 'flash');
      }
      else {
         $this->Company->saveField('regnum_verified', true);         
         $this->Session->setFlash(__('Account verified flag has been addded.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function neg_rating($id = null) {
      $rating = $this->Company->field('neg_rating', array('Company.id' => $id));
      $this->Company->id = $id;
      if ($rating) {
         $this->Company->saveField('neg_rating', false);
         $this->Session->setFlash(__('Negative rating has been removed.', true), 'flash');
      }
      else {
         $this->Company->saveField('neg_rating', true);
         
         $notice['Notification']['company_id'] = $id;
         $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
         $notice['Notification']['type'] = 'neg_rating';
         $this->Notification->save($notice);
         
         $this->Session->setFlash(__('Negative rating has been addded.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function admin_index() {
      $this->paginate['Company'] = array('limit' => 10, 'order' => array('Company.name asc'));
      $companies = $this->paginate('Company');
      $this->set('companies', $companies);
   }
   
   function delete($id = null) {
      $dcompany = $this->Company->find('first', array('conditions' => array('Company.id' => $id)));
      //debug($dcompany); die;
      if (!empty($dcompany) && ($this->Session->read('Auth.User.group_id') == 1 || ($this->Session->read('Auth.User.group_id') == 2 && $this->Session->read('Auth.User.company_id') == $id))) {
         foreach ($dcompany['User'] as $user) {
            if ($user['group_id'] == 1) {
               $this->Session->setFlash('Superadmin protection.', 'flash');
               $this->redirect($this->referer());
            }
         }
         $data['Firm']['name'] = $dcompany['Company']['name'];
         $data['Firm']['address'] = $dcompany['Company']['address1'].' '.$dcompany['Company']['address2'].' '.$dcompany['Company']['address3'];
         $data['Firm']['zip'] = $dcompany['Company']['zip'];
         $data['Firm']['city'] = $dcompany['Company']['city'];
         $data['Firm']['region_id'] = $dcompany['Company']['region_id'];
         $data['Firm']['country_id'] = $dcompany['Company']['country_id'];
         $data['Firm']['establish'] = $dcompany['Company']['establish'];
         $data['Firm']['sector_id'] = $dcompany['Company']['sector_id'];
         $data['Firm']['phone'] = $dcompany['Company']['phone1'].' '.$dcompany['Company']['phone2'];
         $data['Firm']['regnum'] = $dcompany['Company']['regnum'];
         $data['Firm']['size'] = $dcompany['Company']['size'];
         $data['Firm']['public_sector'] = $dcompany['Company']['public_sector'];
         $data['Firm']['fax'] = $dcompany['Company']['fax1'].' '.$dcompany['Company']['fax2'];
         $data['Firm']['web'] = $dcompany['Company']['web'];
         if ($this->Firm->save($data)) {
            foreach ($dcompany['User'] as $user) {
               $this->People->id = null;
               $people['People'] = $user;
               $people['People']['firm_id'] = $this->Firm->id;
               $people['People']['name'] = $user['title'].' '.$user['first_name'].' '.$user['middle_name'].' '.$user['last_name'];
               $people['People']['phone'] = $user['phone1'].' '.$user['phone2'];
               $people['People']['fax'] = $user['fax1'].' '.$user['fax2'];
               if ($this->People->save($people))
                  $this->Company->User->delete($user['id']);
            }
            if ($this->Company->delete($id)) {
               $peoples = $this->People->find('all', array('conditions' => array('People.company_id' => $dcompany['Company']['id']), 'recursive' => -1));
               foreach ($peoples as $people) {
                  $this->People->id = $people['People']['id'];
                  $this->People->saveField('company_id', null);
                  $this->People->saveField('firm_id', $this->Firm->id);
               }
               $this->Session->setFlash(__('Company account has been removed.', true), 'flash');
               
               if ($this->Session->read('Auth.User.company_id') == $id) {
                  $this->Auth->logout();
                  $this->redirect(array('controller' => 'users', 'action' => 'home'));
               }
               else
                  $this->redirect($this->referer());
            }
         }
      }
   }
   
   function resave() {
      $companies = $this->Company->find('all', array('recursive' => -1));
		foreach ($companies as $company) {
			$this->Company->id = $company['Company']['id'];
			$this->Company->save($company, false);
	   }
	   die;
   }
   
   function edit($task = 'info') {
      $this->Company->id = $this->Session->read('Auth.User.company_id');
      unset($this->company['Company']['id']);
      switch ($task) {
         case 'info' :
            $this->set('title_for_layout', __('Company settings', true).' : '.__('Information', true));
            $countries = $this->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
            $regions = $this->Company->Region->find('list', array('fields' => array('id', 'name_'.$this->lang, 'country_id')));
            $sizes = $this->Size->find('list', array('fields' => array('num', 'name_'.$this->lang)));
            $sectors = $this->Company->Sector->find('list', array('fields' => array('id', 'name_'.$this->lang), 'order' => array('name_'.$this->lang)));
            $this->set('countries', $countries);
            $this->set('regions', $regions);
            $this->set('sizes', $sizes);
            $this->set('sectors', $sectors);
            $categories = $this->User->Company->Sector->Category->find('threaded');
            $this->set('categories', $categories);
            if (!empty($this->data)) {
               if (!empty($this->data['Company']['est_date']))
                  $this->data['Company']['establish'] = $this->data['Company']['est_date']['year'].'-'.$this->data['Company']['est_date']['month'].'-'.$this->data['Company']['est_date']['day'];
               if (empty($this->data['Company']['country_id']) || (!empty($this->data['Company']['country_id']) && empty($regions[$this->data['Company']['country_id']])))
                  unset($this->data['Company']['region_id']);
               if (!empty($this->data['Company']['public_sector'])) {
                  unset($this->data['Company']['regnum']);
                  $this->data['Company']['sector_id'] = 0;
               }
               $data = $this->data;   
               if ($this->Company->save($data)) {
                  $this->Session->setFlash(__('Company information has been updated successfully.', true), 'flash');
                  $this->redirect(array('controller' => 'companies', 'action' => 'edit'));
               }
            }
            else {
               $this->data = $this->company;
               $this->data['Company']['est_date']['day'] = date('j', strtotime($this->company['Company']['establish']));
               $this->data['Company']['est_date']['month'] = date('n', strtotime($this->company['Company']['establish']));
               $this->data['Company']['est_date']['year'] =  date('Y', strtotime($this->company['Company']['establish']));
               if (!$this->company['Company']['public_sector'])
                  $this->data['Company']['sector_text'] = $sectors[$this->data['Company']['sector_id']];
               else
                  unset($this->data['Company']['sector_id']);
            }
            break;
         case 'logo' :
            $this->set('title_for_layout', __('Company settings', true).' : '.__('Logo', true));
            if (!empty($this->data)) {
               $this->data['Company']['avatar'] = date('YmdHis').'.jpg';
               if ($this->Company->save($this->data)) { 
                  __fileSave($this->data['Company']['image'], 'img'.DS.'comps'.DS.$this->Company->id.DS.$this->data['Company']['avatar'], 680, 680);
                  if (!empty($this->company['Company']['avatar']))
                     unlink(ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'comps'.DS.$this->Company->id.DS.$this->company['Company']['avatar']);
                  $this->Session->setFlash(__('Logo has been added successfully.', true), 'flash');
                  $this->redirect(array('controller' => 'companies', 'action' => 'edit', 'logo'));
               }
            }
            $this->render('logo');
            break;
         case 'type' :
            $this->set('title_for_layout', __('Company settings', true).' : '.__('Account type', true));
            if (!empty($this->data['Company']['type_id'])) {
               if ($this->Company->saveField('type_id', $this->data['Company']['type_id'], true)) {
                  $notice['Notification']['company_id'] = $this->Company->id;
                  $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
                  $notice['Notification']['type'] = 'type_change';
                  $notice['Notification']['text'] = $this->data['Company']['type_id'];
                  $this->Notification->save($notice);
                  $this->Session->setFlash(__('Account type has been changed successfully.', true), 'flash');
               }
            }
            else
               $this->data = $this->company;
            $this->render('type');
            break;
         case 'business-area' :
            $countries = $this->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
            $this->set('countries', $countries);
            $categories = $this->User->Company->Sector->Category->find('threaded');
            $this->set('categories', $categories);
            if (!empty($this->data)) {
               if ($this->Company->save($this->data)) {
                  $this->Session->setFlash(__('Area of your business interest has been updated successfully.', true), 'flash');
               }
            }
            else {
               if (!empty($this->company['Company']['i_locality_id']))
                  $this->data['Company']['i_locality_id'] = $this->company['Company']['i_locality_id'];
               if (!empty($this->company['Company']['i_sector_id']))
                  $this->data['Company']['i_sector_id'] = $this->company['Company']['i_sector_id']; 
            }
            $this->render('business_area');
            break;
         case 'account' :
            $order = $this->Order->find('first', array('conditions' => array('Package.account_id !=' => -1, 'Order.company_id' => $this->Company->id, 'Order.paid' => false)/*, 'recursive' => -1*/));
            if (!empty($order)) {
               $this->set('order', $order);
            }
            else if ($this->company['Company']['account_id'] == 1) {
               $packages = $this->Package->find('all', array('conditions' => array('Package.active' => true), 'recursive' => -1, 'order' => array('Package.account_id ASC', 'Package.price DESC')));
               $this->set('packages', $packages);
               if (!empty($this->data['Invitation']['email'])) {
                  $this->data['Invitation']['user_id'] = $this->Session->read('Auth.User.id');
                  $this->data['Invitation']['company_id'] = $this->Company->id;
                  $email = $this->Company->User->find('count', array('conditions' => array('User.email' => $this->data['Invitation']['email'])));
                  if (!empty($email)) {
                     $this->Session->setFlash(__('Sorry, this email is already registered.', true), 'flash');
                     $this->render('order_account');
                     break;
                  }
                  if ($this->Invitation->save($this->data)) {
                     $user['User']['email'] = $this->data['Invitation']['email'];  
                     $this->data['User'] = $this->Session->read('Auth.User');                   
                     $this->_userMail($user, __('one time only invitation to join ezbizi beta', true), 'invitation', $this->data);
                     $this->Session->setFlash(__('Invitation has been sent.', true), 'flash');
                     $this->redirect($this->referer());
                  }
               }
            }
            $this->render('order_account');
            break;
         case 'newsletter' :
            if (!empty($this->data['Newsletter']['subject']) && !empty($this->data['Newsletter']['text'])) {
               $users = $this->User->find('all', array('conditions' => array('User.company_id' => $this->Company->id, 'User.active' => true), 'recursive' => -1));
               if (!empty($users)) {
                  foreach ($users as $user) {
                     $this->_userMail($user, $this->data['Newsletter']['subject'], 'newsletter', $this->data);
                  }
                  $this->Session->setFlash(__('E-mail has been sent.', true), 'flash');
                  $this->data = null;
               }
            }
            $this->render('newsletter');
            break;
         default :
            $this->redirect($this->referer());
      }
   }
   
   function view($id = null) {
      $v_company = $this->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true)));
      if (empty($v_company))
         $this->redirect($this->referer());
         
      if (!empty($this->data['Company']['status']) && $this->Session->read('Auth.User.company_id') == $v_company['Company']['id'] && $this->Session->read('Auth.User.group_id') < 3 && $this->data['Company']['status'] != $v_company['Company']['status']) {
         if ($this->Company->saveField('status', $this->data['Company']['status'], true)) {
            $notice['Notification']['company_id'] = $this->Session->read('Auth.User.company_id');
            $notice['Notification']['user_id'] = $this->User->id;
            $notice['Notification']['type'] = 'company_status';
            $notice['Notification']['text'] = $this->data['Company']['status'];
            $this->Notification->save($notice);
            $this->Session->setFlash(__('Company status has been updated.', true), 'flash');
            $v_company['Company']['status'] = $this->data['Company']['status'];
            $this->data = null;
         }
      }  
         
      $this->set('title_for_layout', __('Company profile', true).' : '.$v_company['Company']['name']);
      $this->set('vcompany', $v_company);
      $recommendations = $this->Company->Recommendation->find('all', array('conditions' => array('Recommendation.company_id' => $id, 'Recommendation.approved' => true), 'limit' => 5, 'order' => array('Recommendation.created DESC')));
      $this->set('recommendations', $recommendations);
      
      $recom = $this->Company->Recommendation->find('count', array('conditions' => array('Recommendation.company_id' => $id, 'Recommendation.senderc_id' => $this->company['Company']['id'])));
      if (!empty($recom))
         $this->set('recommended', true);
      
      $colleagues = $this->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
      $this->set('colleagues', $colleagues);
      
      $conns = $this->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $id, 'Connection.com2_id' => $id)), 'order' => 'RAND()', 'limit' => 5, 'recursive' => -1));
      $comp_ids = array();
      if (!empty($conns)) {
         foreach ($conns as $conn) {
            if ($conn['Connection']['com1_id'] == $id)
               $comp_ids []= $conn['Connection']['com2_id'];
            else
               $comp_ids []= $conn['Connection']['com1_id'];
         }
      }
      $connections = $this->Company->find('all', array('conditions' => array('Company.id' => $comp_ids)));
      $this->set('connections', $connections);
      
      $sizes = $this->Size->find('list', array('fields' => array('num', 'name_'.$this->lang)));
      $this->set('sizes', $sizes);
      $demands = $this->Company->Demand->find('all', array('conditions' => array('Demand.company_id' => $id, 'Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d')), 'order' => array('Demand.created DESC')));
      $this->set('demands', $demands);
                                        
      $conn = $this->Company->Connection->find('first', array('conditions' => array('OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
      if (!empty($conn))
         $this->set('connect', true);
      if (!empty($conn['Connection']['approved']))
         $this->set('connected', true);
         
      $conditions = array('Album.company_id' => $id, 'Album.photo_count >' => 0);
      if ($id != $this->company['Company']['id']  && $this->Session->read('Auth.User.group_id') != 1) {
         if (!empty($conn['Connection']['approved']))
            $conditions['Album.privacy'] = array(1, 2); 
         else
            $conditions['Album.privacy'] = 1;
      }
      $albums = $this->Company->Album->find('all', array('conditions' => $conditions, 'limit' => 3, 'order' => 'RAND()'));
      $this->set('albums', $albums);
      
      $conditions = array('Document.company_id' => $id);
      if ($id != $this->company['Company']['id']  && $this->Session->read('Auth.User.group_id') != 1) {
         if (!empty($conn['Connection']['approved']))
            $conditions['Document.privacy'] = array(1, 2); 
         else
            $conditions['Document.privacy'] = 1;
      }
      $documents = $this->Company->Document->find('all', array('conditions' => $conditions, 'limit' => 5, 'order' => 'Document.created DESC'));
      $this->set('documents', $documents);
         
      $com1 = $this->Company->User->find('list', array('conditions' => array('User.company_id' => $id), 'fields' => array('User.id', 'User.id')));
      $com2 = $this->Company->User->find('list', array('conditions' => array('User.company_id' => $this->company['Company']['id']), 'fields' => array('User.id', 'User.id')));
      $dealcount = $this->Company->Demand->Dmessage->find('count', array('conditions' => array('Dmessage.done' => true, 'OR' => array(array('Dmessage.recipient_id' => $com1, 'Dmessage.sender_id' => $com2), array('Dmessage.recipient_id' => $com2, 'Dmessage.sender_id' => $com1)))));
      if (!empty($dealcount))
         $this->set('deal', true);
      
      $int = $this->Company->Interest->find('count', array('conditions' => array('Interest.recipient_id' => $id, 'Interest.sender_id' => $this->company['Company']['id'])));
      if (!empty($int))
         $this->set('interested', true); 
             
      $conditions = array('Notification.company_id' => $id, 'Notification.type !=' => 'new_demand');
      if ($id != $this->company['Company']['id'])
         $conditions['Notification.type NOT'] = array('user_status', 'new_demand');   
      $notifications = $this->Notification->find('all', array('conditions' => $conditions, 'order' => array('Notification.created DESC'), 'limit' => 10));
      if (!empty($notifications)) {
         foreach ($notifications as $ind => $notification) {
            if ($notification['Notification']['type'] == 'new_photos') {
               $data = explode('|', $notification['Notification']['text']);
               $album = $this->Company->Album->find('first', array('conditions' => array('Album.id' => $data[0]), 'recursive' => -1));
               if (empty($album)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               
               if ($this->company['Company']['id'] != $album['Album']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                  if (empty($conn['Connection']['approved']) && $album['Album']['privacy'] != 1) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  if (!empty($conn['Connection']['approved']) && !in_array($album['Album']['privacy'], array(1, 2))) {
                     unset($notifications[$ind]);
                     continue;
                  }
               }   
                  
               $notifications[$ind]['Album'] = $album['Album'];
               unset($data[0]);
               $photos = $this->Company->Album->Photo->find('list', array('conditions' => array('Photo.id' => $data), 'fields' => array('Photo.id', 'Photo.file')));
               $notifications[$ind]['Photo'] = $photos;
            }
            else if ($notification['Notification']['type'] == 'new_document') {
               $document = $this->Company->Document->find('first', array('conditions' => array('Document.id' => $notification['Notification']['text']), 'recursive' => -1));
               if (empty($document)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               
               if ($this->company['Company']['id'] != $document['Document']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                  if (empty($conn['Connection']['approved']) && $document['Document']['privacy'] != 1) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  if (!empty($conn['Connection']['approved']) && !in_array($document['Document']['privacy'], array(1, 2))) {
                     unset($notifications[$ind]);
                     continue;
                  }
               }
               
               $notifications[$ind]['Document'] = $document['Document'];
            }
            else if ($notification['Notification']['type'] == 'new_demand') {
               $demand = $this->Company->Demand->find('first', array('conditions' => array('Demand.id' => $notification['Notification']['text'])));
               if (empty($demand)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               $notifications[$ind]['Demand'] = $demand;
            }
            else if ($notification['Notification']['type'] == 'connection') {
               $connection = $this->User->Company->find('first', array('conditions' => array('Company.id' => $notification['Notification']['text'])));
               if (empty($connection)) {
                  unset($notifications[$ind]);
                  continue; 
               }
               $notifications[$ind]['Connection'] = $connection;
            }                                                                                                                 
         }
      }
      $this->set('notifications', $notifications);
      
      $types = $this->Company->Type->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('types', $types);
      
      if ($this->Session->read('Auth.User.company_id') != $id) {
         $visit = $this->Company->Visit->find('first', array('conditions' => array('Visit.company_id' => $id, 'Visit.user_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
         if (empty($visit)) {
            $data['Visit']['company_id'] = $id;
            $data['Visit']['user_id'] = $this->Session->read('Auth.User.id');
            $this->Company->Visit->save($data);
         }
      }      
   }
   
   function search($query = null, $target = 'search') {
      if (!in_array($target, array('companies', 'companies_interface', 'search')))
         die;
      $companies = null;
      if (!empty($query)) {
         $conditions = array('Company.name LIKE' => $query.'%', 'active' => true);
         if (!empty($this->company)) {
            $conditions['Company.id !='] = $this->company['Company']['id'];
         }
         if ($target != 'search')
            $companies = $this->Company->find('all', array('conditions' => $conditions, 'limit' => 5));
         else {
            $this->paginate['Company'] = array('limit' => 15);
            $companies = $this->paginate('Company', $conditions);
         }
      }
      $this->set('companies', $companies);
      if ($target != 'search')
         $this->render('/elements/'.$target);
      else {
         $this->data['Search']['type_id'] = 2;
         $this->set('pcompany', $this->company);
         $this->_companyPanel($this->company['Company']['id']);
      }
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }
}
?>