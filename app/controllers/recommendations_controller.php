<?php
class RecommendationsController extends AppController {
   
   var $helpers = array('Image');
   var $uses = array('Recommendation', 'Notification');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function index($id = null) {
      if (empty($id)) {
         $id = $this->Session->read('Auth.User.company_id');
         $this->set('pcompany', $this->company);
      }
      else {
         $pcompany = $this->Recommendation->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
         if (empty($pcompany))
            $this->redirect($this->referer());
         $this->set('pcompany', $pcompany);
      }
      
      $this->set('title_for_layout', __('Recommendations', true));
      
      $user = $this->Session->read('Auth.User');
      $this->set('user', $user);
      
      $this->_companyPanel($id);
      
      $this->paginate['Recommendation'] = array('limit' => 10, 'recursive' => 1, 'order' => array('Recommendation.created DESC'));
      $recommendations = $this->paginate('Recommendation', array('Recommendation.company_id' => $id, 'Recommendation.approved' => true));
      $this->set('recommendations', $recommendations);
   }
   
   function add() {      
      if (!empty($this->data) && $this->data['Recommendation']['company_id'] != $this->Session->read('Auth.User.company_id')) {
         $recom = $this->Recommendation->find('first', array('conditions' => array('Recommendation.company_id' => $this->data['Recommendation']['company_id']), 'recursive' => -1));
         if (!empty($recom)) {
            $this->Session->setFlash(__('You already recommended this company.', true), 'flash');
            $this->redirect($this->referer());
         }
            
         $conn = $this->User->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $this->data['Recommendation']['company_id'], 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $this->data['Recommendation']['company_id']))), 'recursive' => -1));
         $com1 = $this->User->find('list', array('conditions' => array('User.company_id' => $this->data['Recommendation']['company_id']), 'fields' => array('User.id', 'User.id')));
         $com2 = $this->User->find('list', array('conditions' => array('User.company_id' => $this->company['Company']['id']), 'fields' => array('User.id', 'User.id')));
         $dealcount = $this->User->Company->Demand->Dmessage->find('count', array('conditions' => array('Dmessage.done' => true, 'OR' => array(array('Dmessage.recipient_id' => $com1, 'Dmessage.sender_id' => $com2), array('Dmessage.recipient_id' => $com2, 'Dmessage.sender_id' => $com1)))));
         if (empty($conn) && empty($dealcount)) {
            $this->Session->setFlash(__('Sorry! You not able to recommend this company.', true), 'flash');
            $this->redirect($this->referer());
         }
         
         $this->data['Recommendation']['senderc_id'] = $this->Session->read('Auth.User.company_id');
         $this->data['Recommendation']['user_id'] = $this->Session->read('Auth.User.id');
         $this->data['Recommendation']['approved'] = true;
         if ($this->Recommendation->save($this->data)) {
            $notice['Notification']['company_id'] = $this->data['Recommendation']['company_id'];
            $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
            $notice['Notification']['type'] = 'recommendation';
            $notice['Notification']['text'] = $this->data['Recommendation']['text'];
            $this->Notification->save($notice);
            $this->Session->setFlash(__('Recommendation has been sent succesfully.', true), 'flash');
         }
         else {
            $errors = $this->Recommendation->invalidFields();
            if (!empty($errors['text']))
               $this->Session->setFlash($errors['text'], 'flash');
         }
      }
      $this->redirect($this->referer());
   }
   
   function delete($id = null) {
   
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }
   
}
?>