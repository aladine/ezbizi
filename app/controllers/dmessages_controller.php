<?php
class DmessagesController extends AppController {
   
   var $helpers = array('Image');
   var $components = array('Email');
   var $uses = array('Dmessage', 'Package');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function accept($id = null) {
      $dmessage = $this->Dmessage->find('first', array('conditions' => array('Dmessage.id' => $id, 'Dmessage.accepted' => array(0, 1), 'Dmessage.recipient_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
      if (!empty($dmessage)) {
         $this->Dmessage->id = $id;
         $this->Dmessage->saveField('accepted', -1);
         $this->Dmessage->id = null;
         $this->data['Dmessage']['demand_id'] = $dmessage['Dmessage']['demand_id'];
         $this->data['Dmessage']['recipient_id'] = $dmessage['Dmessage']['sender_id'];
         $this->data['Dmessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Dmessage']['currency_id'] = $dmessage['Dmessage']['currency_id'];
         $this->data['Dmessage']['priceper_id'] = $dmessage['Dmessage']['priceper_id'];
         $this->data['Dmessage']['price'] = $dmessage['Dmessage']['price'];
         if ($dmessage['Dmessage']['accepted'] == 0) {
            $this->data['Dmessage']['accepted'] = 1;
            $this->data['Dmessage']['text'] = __('Accepted.', true);
         }
         else {
            $this->data['Dmessage']['accepted'] = 2;
            $this->data['Dmessage']['text'] = __('Congratulations, The deal is on!', true);

            $price = 0;
            $demand = $this->Dmessage->Demand->find('first', array('conditions' => array('Demand.id' => $dmessage['Dmessage']['demand_id']), 'recursive' => -1));
            if (empty($this->data['Dmessage']['priceper_id']))
               $price = $this->data['Dmessage']['price'];
            else {
               $unit = $this->Dmessage->Demand->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Dmessage']['priceper_id']), 'recursive' => -1));
               $price = ($this->data['Dmessage']['price'] / $unit['Unit']['multiplier']) * $demand['Demand']['norm_quantity'];
            }
            $comid = $this->User->field('company_id', array('id' => $dmessage['Dmessage']['sender_id']));
            $connect = $this->Dmessage->Demand->Company->Connection->find('count', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $comid, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $comid)))));
            $p_limit = $this->Dmessage->Demand->Currency->field('p_limit', array('id' => $demand['Demand']['currency_id']));
            if ($price < $p_limit || $this->company['Company']['account_id'] != 1 || !empty($connect)) {
               $this->data['Dmessage']['done'] = true;
            } 
         }
         if ($this->Dmessage->save($this->data)) {
            $this->User->id = $dmessage['Dmessage']['sender_id'];;
            $unread = $this->Dmessage->find('count', array('conditions' => array('Dmessage.recipient_id' => $this->User->id, 'Dmessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $dmessage['Dmessage']['sender_id']), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $demand = $this->Dmessage->Demand->find('first', array('conditions' => array('Demand.id' =>  $dmessage['Dmessage']['demand_id']), 'recursive' => 1));
               $data['Demand'] = $demand;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New demand message', true), 'dmessage', $data);
            }
            
            $this->Session->setFlash($this->data['Dmessage']['text'], 'flash');
         }
      } 
      $this->redirect($this->referer());
   }
   
   function no_accept($id = null) {
      $dmessage = $this->Dmessage->find('first', array('conditions' => array('Dmessage.id' => $id, 'Dmessage.accepted' => array(0, 1), 'Dmessage.recipient_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
      if (!empty($dmessage)) {
         $this->Dmessage->id = $id;
         $this->Dmessage->saveField('accepted', -1);
         $this->Dmessage->id = null;
         $this->data['Dmessage']['demand_id'] = $dmessage['Dmessage']['demand_id'];
         $this->data['Dmessage']['recipient_id'] = $dmessage['Dmessage']['sender_id'];
         $this->data['Dmessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Dmessage']['accepted'] = -2;
         $this->data['Dmessage']['text'] = __('Not accepted.', true);
         if ($this->Dmessage->save($this->data)) {
            $this->User->id = $dmessage['Dmessage']['sender_id'];
            $unread = $this->Dmessage->find('count', array('conditions' => array('Dmessage.recipient_id' => $this->User->id , 'Dmessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $dmessage['Dmessage']['sender_id']), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $demand = $this->Dmessage->Demand->find('first', array('conditions' => array('Demand.id' =>  $dmessage['Dmessage']['demand_id']), 'recursive' => 1));
               $data['Demand'] = $demand;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New demand message', true), 'dmessage', $data);
            }
            
            $this->Session->setFlash(__('Not accepted.', true), 'flash');
         }
      }
      $this->redirect($this->referer());
   }
   
   function delete($demand_id = null, $user_id = null) {
      $user = $this->User->find('first', array('conditions' => array('User.id' => $user_id, 'User.active' => true), 'recursive' => -1));
      $demand = $this->Dmessage->Demand->find('first', array('conditions' => array('Demand.id' => $demand_id), 'recursive' => -1));
      if (empty($user) || empty($demand))
         $this->redirect(array('controller' => 'dmessages', 'action' => 'index'));
      $this->Dmessage->updateAll(array('Dmessage.rec_del' => true, 'Dmessage.read' => true), array('Dmessage.demand_id' => $demand_id, 'Dmessage.sender_id' => $user_id, 'Dmessage.recipient_id' => $this->Session->read('Auth.User.id')));
      $this->Dmessage->updateAll(array('Dmessage.sen_del' => true), array('Dmessage.demand_id' => $demand_id, 'Dmessage.sender_id' => $this->Session->read('Auth.User.id'), 'Dmessage.recipient_id' => $user_id));
      $this->User->id = $this->Session->read('Auth.User.id');
      $unreadd = $this->Dmessage->find('count', array('conditions' => array('Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.read' => false)));
      $this->User->saveField('unreadd', $unreadd);
      $this->Session->setFlash(__('Conversation has been removed successfully.', true), 'flash');
      if ($demand['Demand']['user_id'] != $this->Session->read('Auth.User.id'))
         $this->redirect(array('controller' => 'dmessages', 'action' => 'index'));
      else
         $this->redirect(array('controller' => 'dmessages', 'action' => 'index', $demand['Demand']['id']));
   }
   
   function view($demand_id = null, $user_id = null) {
      $demand = $this->Dmessage->Demand->find('first', array('conditions' => array('Demand.id' => $demand_id), 'recursive' => 1));
      if (empty($demand))
         $this->redirect(array('controller' => 'dmessages', 'action' => 'index'));
      
      if ($demand['Demand']['valid_from'] > date('Y-m-d') || $demand['Demand']['valid_to'] < date('Y-m-d'))
         $this->Session->setFlash(__('This demand has not yet started or is out of date. Please double-check the validity period for this demand.', true), 'flash');
           
      $this->set('title_for_layout', __('Demand messages', true).' : '.$demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$this->lang].__(' of ', true).$demand['Demand']['commodity'].__(' for ', true).$demand['Demand']['price'].' '.$demand['Currency']['name_'.$this->lang].__(' is needed.', true));
      
      $last_m = $this->Dmessage->find('first', array('conditions' => array('demand_id' => $demand['Demand']['id'], 'OR' => array(array('recipient_id' => $user_id, 'sender_id' => $this->Session->read('Auth.User.id')), array('recipient_id' => $this->Session->read('Auth.User.id'), 'sender_id' => $user_id))), 'order' => 'created DESC', 'recursive' => -1));
      if (!empty($this->data) && !empty($last_m) && $last_m['Dmessage']['sender_id'] != $this->Session->read('Auth.User.id') && empty($last_m['Dmessage']['accepted'])) {
         $this->data['Dmessage']['demand_id'] = $demand['Demand']['id'];
         $this->data['Dmessage']['recipient_id'] = $user_id;
         $this->data['Dmessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Dmessage']['currency_id'] = $demand['Demand']['currency_id'];
         $this->data['Dmessage']['priceper_id'] = $demand['Demand']['priceper_id'];
         $this->data['Dmessage']['accepted'] = 0;
         //if (!empty($this->data['Dmessage']['valid_to']))
         //   $this->data['Dmessage']['valid_to'] = date('Y-m-d', strtotime($this->data['Dmessage']['valid_to']));
         if ($this->Dmessage->save($this->data)) {
            $this->User->id = $user_id;
            $unread = $this->Dmessage->find('count', array('conditions' => array('Dmessage.recipient_id' => $user_id, 'Dmessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            $this->Dmessage->id = $this->data['Dmessage']['message_id'];
            $this->Dmessage->saveField('accepted', -1);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $user_id), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $data['Demand'] = $demand;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New demand message', true), 'dmessage', $data);
            }
            
            $this->Session->setFlash(__('Your counter offer has been sent successfully.', true), 'flash');
            $this->data = null;
         }
      }
      $this->paginate['Dmessage'] = array('limit' => 5, 'recursive' => 1, 'order' => array('Dmessage.created DESC'));
      $dmessages = $this->paginate('Dmessage', array('Dmessage.demand_id' => $demand_id, 'OR' => array(array('Dmessage.sender_id' => $user_id, 'Dmessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Dmessage.rec_del' => false), array('Dmessage.recipient_id' => $user_id, 'Dmessage.sender_id' => $this->Session->read('Auth.User.id'), 'Dmessage.sen_del' => false))));
      $done = false;
      $deal_is_on = false;
      
      if (!empty($dmessages)) {
         $counter = 0;
         foreach ($dmessages as $dmessage) {
            if ((!$dmessage['Dmessage']['read']) && $dmessage['Dmessage']['sender_id'] == $user_id) {
               $this->Dmessage->id = $dmessage['Dmessage']['id'];
               $this->Dmessage->saveField('read', true);
               $counter++;
            }
            if ($dmessage['Dmessage']['done'])
               $done = true;
            if ($dmessage['Dmessage']['accepted'] == 2)
               $deal_is_on = true;
         }
         if ($counter > 0) {
            $this->User->id = $this->Session->read('Auth.User.id');
            $unreadd = $this->Dmessage->find('count', array('conditions' => array('Dmessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Dmessage.read' => false)));
            $this->User->saveField('unreadd', $unreadd);
            $this->set('new_dm', $unreadd);
            $unread = $this->User->field('unread');
            $this->set('new_mess', $unread + $unreadd);
         }
      }
      else
         $this->redirect($this->referer());
      $this->set('demand', $demand);
      $this->set('dmessages', $dmessages);
      
      
      $id = $demand['Company']['id'];
      if ($id == $this->Session->read('Auth.User.company_id'))
         $id = $this->User->field('company_id', array('id' => $user_id));
      $conn = $this->User->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
      
      if ($this->company['Company']['account_id'] == 3 || !empty($conn) || $done || ($deal_is_on && $this->company['Company']['account_id'] == 2)) {
         $vcompany = $this->User->Company->find('first', array('conditions' => array('Company.id' => $id)));
         $this->set('vcompany', $vcompany);
      }
      $this->set('deal_is_on', $deal_is_on);
      
      $contact = $this->User->find('first', array('conditions' => array('User.id' => $user_id), 'recursive' => -1));
      $this->set('contact', $contact);
      
      //else {
         //$package = $this->Package->find('first', array('conditions' => array('Package.account_id' => -1), 'recursive' => -1));
         //$this->set('package', $package);
      //}
   }
   
   function index($id = null) {
      if (!empty($id)) {
         $demand = $this->Dmessage->Demand->find('first', array('conditions' => array('Demand.id' => $id, 'Demand.user_id' => $this->Session->read('Auth.User.id'))));
         if (empty($demand))
            $this->redirect($this->referer());
         $dmessages = $this->Dmessage->find('all', array('conditions' => array('Dmessage.demand_id' => $id, 'OR' => array(array('Dmessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Dmessage.rec_del' => false), array('Dmessage.sender_id' => $this->Session->read('Auth.User.id'), 'Dmessage.sen_del' => false))), 'recursive' => 2, 'order' => array('Dmessage.created DESC')));
         $sorted = array();                                      
         if (!empty($dmessages)) {
            foreach ($dmessages as $dmessage) {
               if ($dmessage['Dmessage']['sender_id'] == $this->Session->read('Auth.User.id') && !isset($sorted[$dmessage['Dmessage']['recipient_id']])) {
                  $sorted[$dmessage['Dmessage']['recipient_id']] = $dmessage;
               }
               else if ($dmessage['Dmessage']['sender_id'] != $this->Session->read('Auth.User.id') && !isset($sorted[$dmessage['Dmessage']['sender_id']]))
                  $sorted[$dmessage['Dmessage']['sender_id']] = $dmessage;
            }
         }
         else
            $this->redirect($this->referer());
         $this->set('dmessages', $sorted);
         $this->set('demand', $demand);
         $this->set('title_for_layout', __('Demand messages', true).' : '.$demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$this->lang].__(' of ', true).$demand['Demand']['commodity'].__(' for ', true).$demand['Demand']['price'].' '.$demand['Currency']['name_'.$this->lang].__(' is needed.', true));
         $this->render('demand');
      }
      else {
         $this->set('title_for_layout', __('Demand messages', true));
         $dmessages = $this->Dmessage->find('all', array('conditions' => array('OR' => array(array('Dmessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Dmessage.rec_del' => false), array('Dmessage.sender_id' => $this->Session->read('Auth.User.id'), 'Dmessage.sen_del' => false))), 'recursive' => 2, 'order' => array('Dmessage.created DESC')));
         $sorted = array();
         if (!empty($dmessages)) {
            foreach ($dmessages as $dmessage) {
               if (!isset($sorted[$dmessage['Dmessage']['demand_id']])) {
                  $sorted[$dmessage['Dmessage']['demand_id']] = $dmessage;
               }
               if (empty($sorted[$dmessage['Dmessage']['demand_id']]['NEW']) && ($dmessage['Dmessage']['recipient_id'] == $this->Session->read('Auth.User.id')) && (!$dmessage['Dmessage']['read']))
                  $sorted[$dmessage['Dmessage']['demand_id']]['NEW'] = true;
            }
         }
         $this->set('dmessages', $sorted);
      }
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}

}
?>