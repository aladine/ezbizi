<?php
class UsersController extends AppController {

   var $components = array('Email', 'Cookie');
   var $helpers = array('Image');
   var $uses = array('User', 'Size', 'Notification', 'People');
   //var $layout = 'user';

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('contact', 'build_acl', 'init_db', 'login', 'register', 'activate', 'forgot', 'home', 'group','home2');
   }
   
   function delete_avatar() {
      $avatar = $this->Session->read('Auth.User.avatar');
      if (!empty($avatar)) {
         $this->User->id = $this->Session->read('Auth.User.id');
         $file = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'users'.DS.$this->User->id.DS.$avatar;
         if (file_exists($file))
            unlink($file);
         $this->User->saveField('avatar', null);
         $this->Session->write('Auth.User.avatar', null);
      }
      $this->redirect($this->referer());
   }
   
   function contact() {
      $this->set('title_for_layout', __('Feedback', true));
      if (!empty($this->data) && empty($this->data['User']['e_name']) && check_spam($this->data['User']['time'])) {
         $this->User->set($this->data);
         if ($this->User->validates()) {
            $this->_adminMail('contact', $this->data, 'Feedback message');
            $this->Session->setFlash(__('Your message has been sent.', true), 'flash');
            $this->data = null;
         }
      }
   }
   
   function init_db() {
      $group =& $this->User->Group;

		$group->id = 1;
		$this->Acl->allow($group, 'controllers');
		 
		$group->id = 2;
		$this->Acl->allow($group, 'users/set_admin');
		$this->Acl->allow($group, 'users/approve');
		$this->Acl->allow($group, 'users/delete');
		$this->Acl->allow($group, 'users/delete_avatar');
		$this->Acl->allow($group, 'users/logout');
		$this->Acl->allow($group, 'users/view');
		$this->Acl->allow($group, 'users/edit');
		$this->Acl->allow($group, 'users/colleagues');
		$this->Acl->allow($group, 'recommendations/index');
		$this->Acl->allow($group, 'recommendations/add');
		$this->Acl->allow($group, 'ratings/add');
		$this->Acl->allow($group, 'notifications/index');
		$this->Acl->allow($group, 'messages/add');
		$this->Acl->allow($group, 'messages/index');
		$this->Acl->allow($group, 'messages/view');
		$this->Acl->allow($group, 'messages/delete');
		$this->Acl->allow($group, 'interests/add');
		$this->Acl->allow($group, 'interests/index');
		$this->Acl->allow($group, 'interests/delete');
		$this->Acl->allow($group, 'documents/index');
		$this->Acl->allow($group, 'documents/delete');
		$this->Acl->allow($group, 'documents/view');
		$this->Acl->allow($group, 'dmessages/index');
		$this->Acl->allow($group, 'dmessages/delete');
		$this->Acl->allow($group, 'dmessages/view');
		$this->Acl->allow($group, 'dmessages/accept');
		$this->Acl->allow($group, 'dmessages/no_accept');
		$this->Acl->allow($group, 'demands/index');
		$this->Acl->allow($group, 'demands/delete');
		$this->Acl->allow($group, 'demands/view');
		$this->Acl->allow($group, 'demands/add');
		$this->Acl->allow($group, 'connections/delete');
		$this->Acl->allow($group, 'connections/index');
		$this->Acl->allow($group, 'connections/add');
		$this->Acl->allow($group, 'companies/view');
		$this->Acl->allow($group, 'companies/edit');
		$this->Acl->allow($group, 'companies/delete');
		$this->Acl->allow($group, 'bans/add');
		$this->Acl->allow($group, 'bans/delete');
		$this->Acl->allow($group, 'albums/index');
		$this->Acl->allow($group, 'albums/view');
		$this->Acl->allow($group, 'albums/delete');
		$this->Acl->allow($group, 'albums/photo_delete');
		
		$group->id = 3;
		$this->Acl->allow($group, 'users/delete');
		$this->Acl->allow($group, 'users/delete_avatar');
		$this->Acl->allow($group, 'users/logout');
		$this->Acl->allow($group, 'users/view');
		$this->Acl->allow($group, 'users/edit');
		$this->Acl->allow($group, 'users/delete');
		$this->Acl->allow($group, 'users/colleagues');
		$this->Acl->allow($group, 'recommendations/index');
		$this->Acl->allow($group, 'recommendations/add');
		$this->Acl->allow($group, 'ratings/add');
		$this->Acl->allow($group, 'notifications/index');
		$this->Acl->allow($group, 'messages/add');
		$this->Acl->allow($group, 'messages/index');
		$this->Acl->allow($group, 'messages/view');
		$this->Acl->allow($group, 'messages/delete');
		$this->Acl->allow($group, 'interests/index');
		$this->Acl->allow($group, 'documents/index');
		$this->Acl->allow($group, 'documents/view');
		$this->Acl->allow($group, 'dmessages/index');
		$this->Acl->allow($group, 'dmessages/delete');
		$this->Acl->allow($group, 'dmessages/view');
		$this->Acl->allow($group, 'dmessages/accept');
		$this->Acl->allow($group, 'dmessages/no_accept');
		$this->Acl->allow($group, 'demands/index');
		$this->Acl->allow($group, 'demands/delete');
		$this->Acl->allow($group, 'demands/view');
		$this->Acl->allow($group, 'demands/add');
		$this->Acl->allow($group, 'connections/index');
		$this->Acl->allow($group, 'companies/view');
		$this->Acl->allow($group, 'bans/add');
		$this->Acl->allow($group, 'bans/delete');
		$this->Acl->allow($group, 'albums/index');
		$this->Acl->allow($group, 'albums/view');
		$this->Acl->allow($group, 'albums/delete');
		$this->Acl->allow($group, 'albums/photo_delete');
		
		$this->Session->setFlash(__('Prístupové práva boli úspešné nastavené.', true), 'flash');
      
      $this->redirect(array('controller' => 'users', 'action' => 'home'));
   }
   
   function admin_index($id = null) {
      $this->paginate['User'] = array('limit' => 10, 'order' => array('User.first_name asc', 'User.last_name asc'));
      if (!empty($id))
         $this->paginate['User']['conditions']['User.company_id'] = $id;
      $users = $this->paginate('User');
      $this->set('users', $users);
   }
   
   function delete($id = null) {
      $conditions = array('User.id' => $id, 'User.company_id' => $this->company['Company']['id']);
      if ($id != $this->Session->read('Auth.User.id'))
         $conditions['User.group_id'] = 3;
      if ($this->Session->read('Auth.User.group_id') == 1)
         $conditions = array('User.id' => $id);
      $user = $this->User->find('first', array('conditions' => $conditions, 'recursive' => -1));
      if (!empty($user['User']['id']) && $user['User']['group_id'] != 1) {
         $admins = $this->User->find('count', array('conditions' => array('User.company_id' => $user['User']['company_id'], 'User.group_id <' => 3, 'User.id !=' => $id)));
         if ((empty($admins) || $admins < 2) && $user['User']['group_id'] < 3) {
            $this->Session->setFlash(__('Your company must have at least one admin.', true), 'flash');
            $this->redirect($this->referer());
         } 
         $data['People'] = $user['User'];
         $data['People']['name'] = $user['User']['title'].' '.$user['User']['first_name'].' '.$user['User']['middle_name'].' '.$user['User']['last_name'];
         $data['People']['phone'] = $user['User']['phone1'].' '.$user['User']['phone2'];
         $data['People']['fax'] = $user['User']['fax1'].' '.$user['User']['fax2'];
         if ($this->People->save($data)) {
            if ($this->User->delete($id))
               $this->Session->setFlash(__('User account has been removed.', true), 'flash');
            if ($this->Session->read('Auth.User.id') == $id) {
               $this->Auth->logout();
               $this->redirect(array('controller' => 'users', 'action' => 'home'));
            }
         }
      }
      $this->redirect($this->referer());
   }
   
   function approve($id = null) {
      $user = $this->User->find('first', array('conditions' => array('User.id' => $id, 'User.company_id' => $this->company['Company']['id'], 'User.group_id' => 3, 'User.active' => false), 'recursive' => -1));
      if (!empty($user['User']['id'])) {
         $this->User->id = $id;
         $this->User->saveField('active', true);
         $this->_userMail($user, __('Your Ezbizi account has been approved', true), 'user_approve', $user);
         $this->Session->setFlash(__('User has been approved.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function set_admin($id = null) {
      $user_id = $this->User->field('id', array('User.id' => $id, 'User.company_id' => $this->company['Company']['id'], 'User.group_id' => 3, 'User.active' => true));
      if (!empty($user_id)) {
         $this->User->id = $user_id;
         $this->User->saveField('group_id', 2);
         $this->Session->setFlash(__('User has been set as an administrator of company account.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function colleagues($id = null) {
      if (empty($id)) {
         $id = $this->Session->read('Auth.User.company_id');
         $this->set('pcompany', $this->company);
      }
      else {
         $pcompany = $this->User->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
         if (empty($pcompany))
            $this->redirect($this->referer());
         $this->set('pcompany', $pcompany);
      }
      
      $this->_companyPanel($id);
      
      if ($id == $this->Session->read('Auth.User.company_id'))
         $this->set('title_for_layout', __('Colleagues', true));
      else
         $this->set('title_for_layout', __('Staff', true));
      
      $this->paginate['User'] = array('limit' => 10, 'order' => array('User.active', 'User.first_name'), 'recursive' => -1);
      $conditions = array('User.company_id' => $id, 'User.id !=' => $this->Session->read('Auth.User.id'), 'User.key' => null);
      if (!($id == $this->Session->read('Auth.User.company_id') && $this->Session->read('Auth.User.group_id') != 3))
         $conditions['User.active'] = true;
      $staff = $this->paginate('User', $conditions);
      
      $this->set('staff', $staff); 
   }
   
   function edit($task = 'info') {
      $states = $this->User->State->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('states', $states);
      $user = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
      $this->set('user', $user); 
      $this->User->id = $user['User']['id'];
      unset($user['User']['id']);
      switch ($task) {
         case 'info' :
            $this->set('title_for_layout', __('Settings', true).' : '.__('Personal data', true));
            if (!empty($this->data)) {
               $data = $this->data;
               if (empty($data['User']['psword'])) {
                  unset($data['User']['passwd']);
                  unset($data['User']['psword']);
               }
               else {
                  $data['User']['password'] = Security::hash($data['User']['psword'], 'sha1', true);
               }
               if ($data['User']['email'] == $user['User']['email'])
                  unset($data['User']['email']);
               else {
                  $data['User']['key'] = $this->_activateKeyGenerator(20);
               }   
               if ($this->User->save($data)) {
                  $user = $this->User->find('first', array('conditions' => array('User.id' => $this->User->id), 'recursive' => -1));
      		      $this->Session->write('Auth.User', $user['User']);
      		      if (!empty($this->data['User']['key']))
      		         $this->Session->setFlash(__('The information about how to change your registered emailed address has been sent to the new email address you provided.', true).'<br />'.__('To your new email address has been sent informations necessary for succesfull email change.', true), 'flash');
                  else
                     $this->Session->setFlash(__('Your information has been updated successfully.', true), 'flash');
                  $this->redirect(array('controller' => 'users', 'action' => 'edit'));
               }
            }
            else
               $this->data = $user;
            $sexes = $this->User->Sex->find('list', array('fields' => array('id', 'name_'.$this->lang)));
            $this->set('sexes', $sexes);
            break;
         case 'avatar' :
            $this->set('title_for_layout', __('Settings', true).' : '.__('Profile photo', true));
            if (!empty($this->data)) {
               $this->data['User']['avatar'] = date('YmdHis').'.jpg';
               if ($this->User->save($this->data)) { 
                  __fileSave($this->data['User']['image'], 'img'.DS.'users'.DS.$this->User->id.DS.$this->data['User']['avatar'], 680, 680);
                  if (!empty($user['User']['avatar']))
                     unlink(ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.'img'.DS.'users'.DS.$this->User->id.DS.$user['User']['avatar']);
                  $this->Session->write('Auth.User.avatar', $this->data['User']['avatar']);
                  $this->Session->setFlash(__('Photo has been uploaded successfully.', true), 'flash');
                  $this->redirect(array('controller' => 'users', 'action' => 'edit', 'avatar'));
               }
            }
            $this->render('avatar');
            break;
         case 'interests' : 
            $this->set('title_for_layout', __('Settings', true).' : '.__('Interests', true));
            if (!empty($this->data)) {
               if ($this->User->save($this->data)) { 
                  $this->Session->write('Auth.User.interest', $this->data['User']['interest']);
                  $this->Session->setFlash(__('Your interests has been succesfully updated.', true), 'flash');
               }
            }
            else
               $this->data = $user;
            $this->render('interests');
            break;
      }
   }
   
   function view($id = null) {
      $conditions = array('User.id' => $id);         
      $user = $this->User->find('first', array('conditions' => $conditions));
      if (empty($user) || ($user['User']['active'] == false && $this->Session->read('Auth.User.group_id') == 3 && $this->Session->read('Auth.User.company_id') != $user['User']['company_id']))
         $this->redirect($this->referer());
      
      $conn = $this->User->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $user['User']['company_id'], 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $user['User']['company_id']))), 'recursive' => -1));
         
      if ($user['User']['company_id'] != $this->company['Company']['id'] && $this->company['Company']['account_id'] == 1 && empty($conn)) {
         $this->Session->setFlash(__('Only premium account subscribers are able to view personal profiles of other users.', true), 'flash');
         $this->redirect($this->referer());
      }
      
      if ($user['User']['company_id'] != $this->company['Company']['id'] && $this->company['Company']['account_id'] == 2) {
         if (empty($conn)) {
            $this->Session->setFlash(__('Company is not in your connections.', true), 'flash');
            $this->redirect($this->referer());
         }
      }  
         
      $name = '';
      if (!empty($user['User']['title']))
         $name .= $user['User']['title'].' ';
      if (!empty($user['User']['first_name']))
         $name .= $user['User']['first_name'].' ';
      if (!empty($user['User']['middle_name']))
         $name .= $user['User']['middle_name'].' ';
      if (!empty($user['User']['last_name']))
         $name .= $user['User']['last_name'];
         
      if ($user['User']['id'] != $this->Session->read('Auth.User.id'))
         $this->set('can_message', true);
      
      $ban = $this->User->Ban->find('first', array('conditions' => array('Ban.recipient_id' => $this->Session->read('Auth.User.id'), 'Ban.sender_id' => $id), 'recursive' => -1));
      if (!empty($ban))
         $this->set('blocked', true);
         
      $this->set('title_for_layout', __('User profile', true).' : '.$name);
      
      $this->set('user', $user);
      
      $pcompany['Company'] = $user['Company']; 
      $this->set('pcompany', $pcompany);
      
      $this->_companyPanel($user['Company']['id'], $user);
   }
   
   function home() {
      if ($this->Session->check('Auth.User')) {
         $this->User->id = $this->Session->read('Auth.User.id');
         if (!empty($this->data['User']['status']) && $this->data['User']['status'] != $this->Session->read('Auth.User.status')) {
            if ($this->User->saveField('status', $this->data['User']['status'], true)) {
               $this->Session->write('Auth.User.status', $this->data['User']['status']);
               $notice['Notification']['company_id'] = $this->Session->read('Auth.User.company_id');
               $notice['Notification']['user_id'] = $this->User->id;
               $notice['Notification']['type'] = 'user_status';
               $notice['Notification']['text'] = $this->data['User']['status'];
               $this->Notification->save($notice);
               $this->data = null;
               $this->Session->setFlash(__('Your status has been updated.', true), 'flash');
            }
         }
         $this->set('title_for_layout', __('Home', true));
         $user = $this->Session->read('Auth.User');
         $this->set('user', $user);
         $colleagues = $this->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
         
         $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
         $comp_ids = array();
         if (!empty($conns)) {
            foreach ($conns as $conn) {
               if ($conn['Connection']['com1_id'] == $user['company_id'])
                  $comp_ids []= $conn['Connection']['com2_id'];
               else
                  $comp_ids []= $conn['Connection']['com1_id'];
            }
         }
         $comp_ids []= $user['company_id'];
         
         $comcon = 'Company.id !=';
         if (count($comp_ids) > 1)
            $comcon = 'Company.id NOT';
         else
            $comp_ids = current($comp_ids);     
         
         if ($this->company['Company']['account_id'] != 1) {
            $this->_companyPanel($this->company['Company']['id']);/*
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];      
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended); */
         }
         
         $notifications = $this->Notification->find('all', array('conditions' => array('Notification.company_id' => $comp_ids, 'Notification.user_id !=' => $this->User->id), 'order' => array('Notification.created DESC'), 'limit' => 10));
         if (!empty($notifications)) {
            foreach ($notifications as $ind => $notification) {
               if ($notification['Notification']['type'] == 'new_photos') {
                  $data = explode('|', $notification['Notification']['text']);
                  $album = $this->User->Company->Album->find('first', array('conditions' => array('Album.id' => $data[0]), 'recursive' => -1));
                  if (empty($album)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  
                  if ($this->company['Company']['id'] != $album['Album']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                     if (!in_array($album['Album']['company_id'], $comp_ids) && $album['Album']['privacy'] != 1) {
                        unset($notifications[$ind]);
                        continue; 
                     }
                     if (in_array($album['Album']['company_id'], $comp_ids) && !in_array($album['Album']['privacy'], array(1, 2))) {
                        unset($notifications[$ind]);
                        continue;
                     }
                  }
                     
                  $notifications[$ind]['Album'] = $album['Album'];
                  unset($data[0]);
                  $photos = $this->User->Company->Album->Photo->find('list', array('conditions' => array('Photo.id' => $data), 'fields' => array('Photo.id', 'Photo.file')));
                  $notifications[$ind]['Photo'] = $photos;
               }
               else if ($notification['Notification']['type'] == 'new_document') {
                  $document = $this->User->Company->Document->find('first', array('conditions' => array('Document.id' => $notification['Notification']['text']), 'recursive' => -1));
                  if (empty($document)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  
                  if ($this->company['Company']['id'] != $document['Document']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                     if (!in_array($document['Document']['company_id'], $comp_ids) && $document['Document']['privacy'] != 1) {
                        unset($notifications[$ind]);
                        continue; 
                     }
                     if (in_array($document['Document']['company_id'], $comp_ids) && !in_array($document['Document']['privacy'], array(1, 2))) {
                        unset($notifications[$ind]);
                        continue;
                     }
                  }
                  
                  $notifications[$ind]['Document'] = $document['Document'];
               } 
               else if ($notification['Notification']['type'] == 'new_demand') {
                  $demand = $this->User->Company->Demand->find('first', array('conditions' => array('Demand.id' => $notification['Notification']['text'])));
                  if (empty($demand)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  $notifications[$ind]['Demand'] = $demand;
               }
               else if ($notification['Notification']['type'] == 'connection') {
                  $connection = $this->User->Company->find('first', array('conditions' => array('Company.id' => $notification['Notification']['text'])));
                  if (empty($connection)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  $notifications[$ind]['Connection'] = $connection;
               }                                                                                                               
            }
         }
         $this->set('notifications', $notifications);

         $types = $this->User->Company->Type->find('list', array('fields' => array('id', 'name_'.$this->lang)));
         $this->set('types', $types);
         
         if (in_array($this->company['Company']['type_id'], array(2, 3))) {
            $conditions = array('Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d'), 'Demand.company_id !=' => $this->company['Company']['id']);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Demand.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Demand.sector_id'] = $this->company['Company']['i_sector_id'];
            $demands = $this->User->Company->Demand->find('all', array('conditions' => $conditions, 'order' => array('DemandDate DESC', 'Company.account_id DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 5));
            $this->set('demands', $demands);
         }
         
         $this->set('pcompany', $this->company);
         
         $this->render('interface');
      }
      else {
         $this->set('title_for_layout', __('Home', true));
         //$categories = $this->User->Company->Sector->Category->find('list', array('conditions' => array('Category.parent_id' => null), 'fields' => array('id', 'name_'.$this->lang)));
         $categories = $this->User->Company->Sector->Category->find('threaded');
         //debug($categories);
         $this->set('categories', $categories);
         $demands = $this->User->Company->Demand->find('all', array('conditions' => array('Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d')), 'order' => array('DemandDate DESC', 'Company.account_id DESC', 'Demand.created DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Demand.valid_from', 'Demand.valid_to', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 5));
         $this->set('demands', $demands);
      }
   }
   //new design
   function home2() {
      $this->layout='user';
      if ($this->Session->check('Auth.User')) {
         $this->User->id = $this->Session->read('Auth.User.id');
         if (!empty($this->data['User']['status']) && $this->data['User']['status'] != $this->Session->read('Auth.User.status')) {
            if ($this->User->saveField('status', $this->data['User']['status'], true)) {
               $this->Session->write('Auth.User.status', $this->data['User']['status']);
               $notice['Notification']['company_id'] = $this->Session->read('Auth.User.company_id');
               $notice['Notification']['user_id'] = $this->User->id;
               $notice['Notification']['type'] = 'user_status';
               $notice['Notification']['text'] = $this->data['User']['status'];
               $this->Notification->save($notice);
               $this->data = null;
               $this->Session->setFlash(__('Your status has been updated.', true), 'flash');
            }
         }
         $this->set('title_for_layout', __('Home', true));
         $user = $this->Session->read('Auth.User');
         $this->set('user', $user);
         $colleagues = $this->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
         
         $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
         $comp_ids = array();
         if (!empty($conns)) {
            foreach ($conns as $conn) {
               if ($conn['Connection']['com1_id'] == $user['company_id'])
                  $comp_ids []= $conn['Connection']['com2_id'];
               else
                  $comp_ids []= $conn['Connection']['com1_id'];
            }
         }
         $comp_ids []= $user['company_id'];
         
         $comcon = 'Company.id !=';
         if (count($comp_ids) > 1)
            $comcon = 'Company.id NOT';
         else
            $comp_ids = current($comp_ids);     
         
         if ($this->company['Company']['account_id'] != 1) {
            $this->_companyPanel($this->company['Company']['id']);/*
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];      
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended); */
         }
         
         $notifications = $this->Notification->find('all', array('conditions' => array('Notification.company_id' => $comp_ids, 'Notification.user_id !=' => $this->User->id), 'order' => array('Notification.created DESC'), 'limit' => 10));
         if (!empty($notifications)) {
            foreach ($notifications as $ind => $notification) {
               if ($notification['Notification']['type'] == 'new_photos') {
                  $data = explode('|', $notification['Notification']['text']);
                  $album = $this->User->Company->Album->find('first', array('conditions' => array('Album.id' => $data[0]), 'recursive' => -1));
                  if (empty($album)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  
                  if ($this->company['Company']['id'] != $album['Album']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                     if (!in_array($album['Album']['company_id'], $comp_ids) && $album['Album']['privacy'] != 1) {
                        unset($notifications[$ind]);
                        continue; 
                     }
                     if (in_array($album['Album']['company_id'], $comp_ids) && !in_array($album['Album']['privacy'], array(1, 2))) {
                        unset($notifications[$ind]);
                        continue;
                     }
                  }
                     
                  $notifications[$ind]['Album'] = $album['Album'];
                  unset($data[0]);
                  $photos = $this->User->Company->Album->Photo->find('list', array('conditions' => array('Photo.id' => $data), 'fields' => array('Photo.id', 'Photo.file')));
                  $notifications[$ind]['Photo'] = $photos;
               }
               else if ($notification['Notification']['type'] == 'new_document') {
                  $document = $this->User->Company->Document->find('first', array('conditions' => array('Document.id' => $notification['Notification']['text']), 'recursive' => -1));
                  if (empty($document)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  
                  if ($this->company['Company']['id'] != $document['Document']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
                     if (!in_array($document['Document']['company_id'], $comp_ids) && $document['Document']['privacy'] != 1) {
                        unset($notifications[$ind]);
                        continue; 
                     }
                     if (in_array($document['Document']['company_id'], $comp_ids) && !in_array($document['Document']['privacy'], array(1, 2))) {
                        unset($notifications[$ind]);
                        continue;
                     }
                  }
                  
                  $notifications[$ind]['Document'] = $document['Document'];
               } 
               else if ($notification['Notification']['type'] == 'new_demand') {
                  $demand = $this->User->Company->Demand->find('first', array('conditions' => array('Demand.id' => $notification['Notification']['text'])));
                  if (empty($demand)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  $notifications[$ind]['Demand'] = $demand;
               }
               else if ($notification['Notification']['type'] == 'connection') {
                  $connection = $this->User->Company->find('first', array('conditions' => array('Company.id' => $notification['Notification']['text'])));
                  if (empty($connection)) {
                     unset($notifications[$ind]);
                     continue; 
                  }
                  $notifications[$ind]['Connection'] = $connection;
               }                                                                                                               
            }
         }
         $this->set('notifications', $notifications);

         $types = $this->User->Company->Type->find('list', array('fields' => array('id', 'name_'.$this->lang)));
         $this->set('types', $types);
         
         if (in_array($this->company['Company']['type_id'], array(2, 3))) {
            $conditions = array('Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d'), 'Demand.company_id !=' => $this->company['Company']['id']);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Demand.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Demand.sector_id'] = $this->company['Company']['i_sector_id'];
            $demands = $this->User->Company->Demand->find('all', array('conditions' => $conditions, 'order' => array('DemandDate DESC', 'Company.account_id DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 5));
            $this->set('demands', $demands);
         }
         
         $this->set('pcompany', $this->company);
         
         //$this->render('interface');
         $this->render('interface2');
      }
      else {
         $this->set('title_for_layout', __('Home', true));
         //$categories = $this->User->Company->Sector->Category->find('list', array('conditions' => array('Category.parent_id' => null), 'fields' => array('id', 'name_'.$this->lang)));
         $categories = $this->User->Company->Sector->Category->find('threaded');
         //debug($categories);
         $this->set('categories', $categories);
         $demands = $this->User->Company->Demand->find('all', array('conditions' => array('Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d')), 'order' => array('DemandDate DESC', 'Company.account_id DESC', 'Demand.created DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Demand.valid_from', 'Demand.valid_to', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 5));
         $this->set('demands', $demands);
      }
   }

   function group() {
      if (!empty($this->data)) {
         if ($this->User->save($this->data))
            $this->Session->setFlash('succesfully done!', 'flash');
      }
   }
   
   function add_group() {
      if (!empty($this->data)) {
         if ($this->User->Group->save($this->data))
            $this->Session->setFlash('Group added', 'flash');
      }
   }
   
   function register($type = null) {
      $this->set('title_for_layout', __('Registration', true));
      $countries = $this->User->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $regions = $this->User->Company->Region->find('list', array('fields' => array('id', 'name_'.$this->lang, 'country_id')));
      $sexes = $this->User->Sex->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $sizes = $this->Size->find('list', array('fields' => array('num', 'name_'.$this->lang)));
      $types = $this->User->Company->Type->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $categories = $this->User->Company->Sector->Category->find('threaded');
      $sectors = $this->User->Company->Sector->find('list', array('fields' => array('id', 'name_'.$this->lang), 'order' => array('name_'.$this->lang)));
      $this->set('categories', $categories);
      $this->set('countries', $countries);
      $this->set('regions', $regions);  
      $this->set('sexes', $sexes);   
      $this->set('sizes', $sizes);
      $this->set('types', $types);
      $this->set('sectors', $sectors);
      if ($type == 'user')
         $this->set('user_card', true);
      ///if POST request
      if (!empty($this->data)) {
         if (!empty($this->data['User']['user_card'])) {
            unset($this->data['Company']);
            $this->set('user_card', true);
         }
         else
            unset($this->data['User']['company_id']);
        /// if ($this->data['User']['email_log'])
            $this->data['User']['login'] = $this->data['User']['email'];
         $this->User->set($this->data);
         if (!empty($this->data['Company']['est_date']))
            $this->data['Company']['establish'] = $this->data['Company']['est_date']['year'].'-'.$this->data['Company']['est_date']['month'].'-'.$this->data['Company']['est_date']['day'];
         if (empty($this->data['Company']['country_id']) || (!empty($this->data['Company']['country_id']) && empty($regions[$this->data['Company']['country_id']])))
            unset($this->data['Company']['region_id']);
         if (!empty($this->data['Company']['public_sector'])) {
            unset($this->data['Company']['regnum']);
            $this->data['Company']['sector_id'] = 0;
         }
         $this->User->Company->set($this->data);
         $this->User->Company->validates();
         if ($this->User->validates() && empty($this->data['User']['e_name']) && check_spam($this->data['User']['time'])) {
            $this->data['User']['password'] = Security::hash($this->data['User']['psword'], 'sha1', true);
            $this->data['User']['key'] = $this->_activateKeyGenerator(20);
            $this->data['User']['active'] = false;
            if (!$this->data['User']['user_card']) {
               $this->data['Company']['account_id'] = 1;
               $this->data['Company']['active'] = false;
               if ($this->User->Company->save($this->data)) {
                  $this->data['User']['group_id'] = $this->User->Group->field('id', array('name' => 'company admin'));
                  $this->data['User']['company_id'] = $this->User->Company->id;
                  if ($this->User->save($this->data)) {
                     $this->_userMail($this->data, __('Welcome to Ezbizi', true), 'reg_message', $this->data);
                     $this->_adminMail('reg_admin', $this->data, 'New user registration.');
                     $this->Session->setFlash(__('Thank you for registering! The information about how to activate your account has been sent to your email.', true), 'flash');
                     $this->redirect(array('controller' => 'users', 'action' => 'home'));
                  }
               }
            }
            else {
               $companies = $this->User->Company->find('list', array('conditions' => array('Company.active' => true), 'fields' => array('id', 'name')));
               $this->data['User']['group_id'] = $this->User->Group->field('id', array('name' => 'client'));
               if (!isset($companies[$this->data['User']['company_id']])) {
                  $this->Session->setFlash(__('Select a company.', true), 'flash');
                  return;
               }
               $reg_company = $this->User->Company->find('first', array('conditions' => array('Company.id' => $this->data['User']['company_id'])));
               if (!empty($reg_company['Account']['user_limit']) && $reg_company['Company']['user_count'] >= $reg_company['Account']['user_limit']) {
                  $this->Session->setFlash(__('You have reached the limit of registered users with your company account. In order to allow more users to register with your company account, please upgrade to premium account.', true), 'flash');
                  return;
               }
               if ($this->User->save($this->data)) {
                  $this->data['Company'] = $reg_company['Company'];
                  $this->_userMail($this->data, __('Welcome to Ezbizi', true), 'reg_message', $this->data);
                  $this->_adminMail('reg_admin', $this->data, 'New Registration.');
                  $this->Session->setFlash(__('Thank you for registering! The information on how to activate your account has been send to your email address.', true), 'flash');
                  $this->redirect(array('controller' => 'users', 'action' => 'home'));
               }
            }
         }
      }
   }
   
   function login() {
      $this->set('title_for_layout', __('Login', true));
      $user = $this->Auth->user();
      if ($user && !$user['User']['active']) {
         $this->Session->setFlash(__('Your account has not yet been activated.', true), 'flash');
         $this->logout();
      }
      if ($user) {
         $this->User->id = $user['User']['id'];
         $this->User->saveField('last_log', date('Y-m-d H:i:s'));
         $this->User->saveField('online', true);
         $this->User->saveField('last_activity', date('Y-m-d H:i:s'));
         if (!empty($user['User']['company_id'])) {
            $this->User->Company->id = $user['User']['company_id'];
            $this->User->Company->saveField('online', true);
            $this->User->Company->saveField('last_activity', date('Y-m-d H:i:s'));
         }
         if (!empty($this->data['User']['remember'])) {
            $cookie = array();
            $this->Cookie->name = 'EzbiziUser0';
            $cookie['login'] = $this->data['User']['login'];
            $cookie['password'] = $this->data['User']['password']; 
            $this->Cookie->write('EzbiziAuthRemember', $cookie, true, '+2 weeks');
            unset($this->data['User']['remember']);
         }
         $this->redirect($this->Auth->redirect());
      }
      $this->redirect(array('controller' => 'users', 'action' => 'home'));
   }
   
   function logout() {
      $this->set('title_for_layout', __('Logout', true));
      if (!$this->Session->check('Message.flash'))
         $this->Session->setFlash(__('Thank you for using Ezbizi.', true), 'flash');
      $company_id = $this->Session->read('Auth.User.company_id');
      $last = date('Y-m-d H:i:s', time() - 1800);
      $online = $this->User->find('count', array('conditions' => array('User.company_id' => $company_id, 'User.id !=' => $this->Session->read('Auth.User.id'), 'User.online' => true, 'User.last_activity >=' => $last)));
      if (!empty($company_id) && empty($online)) {
         $this->User->Company->id = $company_id;
         $this->User->Company->saveField('online', false);
      }
      $this->User->id = $this->Session->read('Auth.User.id');
      $this->User->saveField('online', false);
      $this->Cookie->name = 'EzbiziUser0';
      $this->Cookie->delete('EzbiziAuthRemember');
      $this->Auth->logout();
      $this->redirect(array('controller' => 'users', 'action' => 'home'));
   }
   
   function forgot() {
      $this->set('title_for_layout', __('Forgotten Password', true));
      if (!empty($this->data)) {
         $this->User->set($this->data);
         if ($this->User->validates()) {
            $user = $this->User->findByEmail($this->data['User']['for_email']);
            if (!empty($user)) {
               $this->User->set($user);
               $user['User']['passwd'] = $this->_passwordGenerator(8);
               if (!$this->User->saveField('password', Security::hash($user['User']['passwd'], 'sha1', true)))
                  $this->Session->setFlash(__('Unable to establish connection with the database.', true), 'flash');
               else {
                  $this->_userMail($user, __('Request to retrieve your Ezbizi login details', true), 'for_message', $user);
                  $this->Session->setFlash(__('The new password has been sent to your email address:', true).$user['User']['email'], 'flash');
               }
               $this->data = null;
            }
         }
      }
   }
   
   function activate($key = null) {
      $this->set('title_for_layout', __('Account activation', true));
      $user = $this->User->find('first', array('conditions' => array('User.key' => $key, 'User.active' => false), 'recursive' => -1));
      if (!empty($user)) {
         $this->User->id = $user['User']['id'];
         $this->User->saveField('key', null);
         
         $admin_group = $this->User->Group->field('id', array('name' => 'company admin'));
		   if ($user['User']['group_id'] == $admin_group) {
		      $this->User->saveField('active', true);
            $this->User->Company->id = $user['User']['company_id'];
            $this->User->Company->saveField('active', true);
            $this->Session->setFlash(__('Your account has been succesfully activated.', true), 'flash');
            $user = $this->User->find('first', array('conditions' => array('User.id' => $this->User->id), 'recursive' => -1));
		      $this->Session->write('Auth.User', $user['User']);
         }
         else {          
		      $this->Session->setFlash(__('Your email address has been successfully verified.', true).'<br />'.__('Your request is pending. Please wait for your company account administrator\'s approval.', true), 'flash');
		   }
      }
      $this->redirect(array('controller' => 'users', 'action' => 'home'));
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}
   
   function _adminMail($template, $data, $subject) {
      
      $emails = array();
      if ($template == 'reg_admin')
         $emails = array('newusers@ezbizi.com', 'info@aitechnology.sk');
      else if ($template == 'contact')
         $emails = array('feedback@ezbizi.com', 'info@aitechnology.sk');
      else
         $emails = $this->User->find('list', array('conditions' => array('User.group_id' => 1), 'fields' => array('User.email')));
      
      $this->set('mes_data', $data);

      $this->Email->subject = $subject;
      $this->Email->template = $template;
         
      foreach($emails as $email) {
         $this->Email->to = $email;
         $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
         $this->Email->sendAs = 'text';
   	   $this->Email->delivery = 'mail';
   	   $this->Email->send();
      }
   }
   
   function _passwordGenerator($length) {
		 $chars = 'akuFmqTs0HAc9NwBe2Q3Go1gLZijDhM8OSYn4W6UPzJERI5rXpfdV7ytKvCxlb';
		 $password = "";
		 for ($i = 0; $i < $length; $i++) {
				$password .= $chars[(rand() % strlen($chars))];
		 }
		 return $password;
	}
   
   function _activateKeyGenerator($length) {
      $chars = 'akuFmqTs0HAc9NwBe2Q3Go1gLZijDhM8OSYn4W6UPzJERI5rXpfdV7ytKvCxlb';
      $done = true;
      do {
         $key = $this->_passwordGenerator($length);
         if (!$this->User->find('first', array('conditions' => array('User.key' => $key), 'recursive' => -1))) {
            $done = false;
            $this->data['User']['key'] = $key;
         }
      } while ($done);
      return $key;
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }
   
}
?>