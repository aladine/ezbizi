<?php
class TranslationsController extends AppController {
   
   var $helpers = array('Image');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('index', 'add', 'delete', 'edit', 'translate');
   }
   
   function translate() {
      $trans = $this->Translation->find('all', array('conditions' => array('Translation.edit' => false, 'Translation.text_ja !=' => null)));
      
      if (!empty($trans)) {
         $content = '';
         
         foreach ($trans as $tran) {
            if (!empty($tran['Translation']['text_ja'])) {
               $content .= 'msgid "'.$tran['Translation']['text_en'].'"'.chr(13).chr(10).'msgstr "'.$tran['Translation']['text_ja'].'"'.chr(13).chr(10).chr(13).chr(10);
            }
         }
         
         $file = ROOT.DS.APP_DIR.DS.'locale'.DS.'jpn'.DS.'LC_MESSAGES'.DS.'default.po';
         $handle = fopen($file, 'w');
         fwrite($handle, $content);
         fclose($handle);
      }
   
      $this->Session->setFlash('Translated', 'flash');
      $this->redirect($this->referer());
   }
   
   function index() {
      if (!empty($this->data['Translation']['text_ja'])) {
         if (!empty($this->data['Translation']['repair'])) {
            $this->Translation->id = $this->data['Translation']['id'];
            $this->Translation->saveField('text_en', $this->data['Translation']['text_ja']);
            $this->Translation->saveField('text_ja', null);
            $this->Translation->saveField('edit', false);
            $this->redirect($this->referer());
         }
         else if ($this->Translation->save($this->data)) {
            $this->Session->setFlash('Translated.', 'flash');
            $this->data = null;
            $this->redirect($this->referer());
         }
      }
      $this->paginate['Translation'] = array('limit' => 20, 'order' => array('Translation.edit' => 'DESC', 'Translation.text_ja' => 'asc'));
      $translations = $this->paginate('Translation');
      $this->set('phrases', $translations);
   }
   
   function add() {
      if (!empty($this->data)) {
         if ($this->Translation->save($this->data)) {
            $this->Session->setFlash('Successfully added.', 'flash');
            $this->data = null;
         }
      }
   }
   
   function delete($id = null) {
      $this->Translation->delete($id);
      $this->redirect($this->referer()); 
   }
   
   function edit($id = null) {
   
   }

}
?>