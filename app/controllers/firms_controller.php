<?php
class FirmsController extends AppController {

   var $helpers = array('Image');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function index() {
      $this->paginate['Firm'] = array('limit' => 10, 'order' => array('Firm.name asc'));
      $firms = $this->paginate('Firm');
      $this->set('firms', $firms);
   }
   
   function delete($id = null) {
      if ($this->Firm->delete($id))
         $this->Session->setFlash(__('Company has been removed from archive.', true), 'flash');
      $this->redirect($this->referer());
   }

}
?>