<?php
class MessagesController extends AppController {
   
   var $components = array('Email');
   var $helpers = array('Image');
   var $uses = array('Message', 'User');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('mailtest');
   }
   
   function mailtest($template) {
      $data = array('User' => array('title' => 'Ing.', 'first_name' => 'Dusan', 'middle_name' => '', 'last_name' => 'Holodak', 'login' => 'dusan.holodak', 'passwd' => 'heslo', 'key' => 'asdsdsadfsadfsadfasdfasd'),
                    'Demand' => array('User' => array('title' => 'Ing.', 'first_name' => 'Dusan', 'middle_name' => '', 'last_name' => 'Holodak', 'login' => 'dusan.holodak', 'passwd' => 'heslo', 'key' => 'asdsdsadfsadfsadfasdfasd'),
                                      'Demand' => array('quantity' => '5', 'commodity' => 'Iron', 'price' => '10', 'id' => '4'),
                                      'Unit' => array('name_en' => 'kg', 'name_ja' => 'kg'),
                                      'Currency' => array('name_en' => '€', 'name_ja' => '€')),
                    'Sender' => array('id' => '2', 'title' => 'Ing.', 'first_name' => 'Tomas', 'middle_name' => '', 'last_name' => 'Kocis'));
      $this->Email->to = 'tomas.kocis@gmail.com';
	   $this->Email->subject = 'Test';
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	   echo 'DONE<br />';
	   echo $this->viewPath;
	   die;
   }
   
   function index() {
      $messages = $this->Message->find('all', array('conditions' => array('OR' => array(array('Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.rec_del' => false), array('Message.sender_id' => $this->Session->read('Auth.User.id'), 'Message.sen_del' => false))), 'order' => array('Message.created DESC')));
      $sorted = array();
      $bans = $this->User->Ban->find('list', array('conditions' => array('Ban.recipient_id' => $this->Session->read('Auth.User.id')), 'fields' => array('Ban.sender_id', 'Ban.sender_id'), 'recursive' => -1));
      if (!empty($messages)) {
         foreach ($messages as $message) {
            if ($message['Message']['sender_id'] == $this->Session->read('Auth.User.id') && !isset($sorted[$message['Message']['recipient_id']])) {
               $message['Sender'] = $message['Recipient'];
               $sorted[$message['Message']['recipient_id']] = $message;
               if (isset($bans[$message['Message']['recipient_id']]))
                  $sorted[$message['Message']['recipient_id']]['blocked'] = true;
            }
            else if ($message['Message']['sender_id'] != $this->Session->read('Auth.User.id') && !isset($sorted[$message['Message']['sender_id']])) {
               $sorted[$message['Message']['sender_id']] = $message;
               if (isset($bans[$message['Message']['sender_id']]))
                  $sorted[$message['Message']['sender_id']]['blocked'] = true;
            }
         }
      }
      $this->set('messages', $sorted);
      $this->set('title_for_layout', __('Messages', true));
   }
   
   function add($id = null) {
      if (!empty($this->data['Message']['to']))
         $id = $this->data['Message']['to'];
      
      if (!empty($id)) {
         $user = $this->User->find('first', array('conditions' => array('User.id' => $id, 'User.active' => true), 'recursive' => -1));
         if (empty($user) || $user['User']['id'] == $this->Session->read('Auth.User.id'))
            $this->redirect($this->referer());
         $this->set('user', $user);
      }
      
      $this->set('title_for_layout', __('Write a new message', true));
      $colleagues = $this->User->find('all', array('conditions' => array('User.active' => true, 'User.id !=' => $this->Session->read('Auth.User.id'), 'User.company_id' => $this->Session->read('Auth.User.company_id')), 'recursive' => -1, 'order' => array('User.first_name')));
      $this->set('colleagues', $colleagues);
      
      $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com2_id' => $this->Session->read('Auth.User.company_id'))), 'recursive' => -1));
      $comp_ids = array();
      if (!empty($conns)) {
         foreach ($conns as $conn) {
            if ($conn['Connection']['com1_id'] == $this->Session->read('Auth.User.company_id'))
               $comp_ids []= $conn['Connection']['com2_id'];
            else
               $comp_ids []= $conn['Connection']['com1_id'];
         }
      }
      $connections = $this->User->Company->find('all', array('conditions' => array('Company.id' => $comp_ids), 'order' => array('Company.name')));
      $this->set('connections', $connections);
      
      if (!empty($this->data)) {
         $this->data['Message']['recipient_type'] = 'user';
         if (empty($this->data['Message']['to'])) {
            $this->Session->setFlash(__('Select a recipient.', true), 'flash');
            return;
         }
         $this->data['Message']['sender_id'] = $this->Session->read('Auth.User.id');
         if ($this->data['Message']['recipient_type'] == 'user') {
            $this->data['Message']['recipient_id'] = $this->data['Message']['to'];
            
            if ($this->company['Company']['account_id'] == 1) {
               $comp = $this->User->field('company_id', array('id' => $id));
               $conn = $this->User->Company->Connection->find('count', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com2_id' => $comp), array('Connection.com2_id' => $comp, 'Connection.com2_id' => $this->Session->read('Auth.User.company_id')))), 'recursive' => -1)); 
               if (empty($conn)) {
                  $this->Session->setFlash(__('Company is not in your connections.', true), 'flash');
                  $this->redirect($this->referer());
               }
            }            
            
            $ban = $this->User->Ban->find('first', array('conditions' => array('Ban.recipient_id' => $this->data['Message']['recipient_id'], 'Ban.sender_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
            if (!empty($ban))
               $this->redirect($this->referer());
            if ($this->Message->save($this->data)) {
               $this->User->id = $this->data['Message']['recipient_id'];
               $unread = $this->Message->find('count', array('conditions' => array('Message.recipient_id' => $this->User->id, 'Message.read' => false)));
               $this->User->saveField('unread', $unread);
               
               $user = $this->User->find('first', array('conditions' => array('User.id' => $id), 'recursive' => -1));
               if (!empty($user) && empty($user['User']['email_ban'])) {
                  $data = $this->data;
                  $data['Sender'] = $this->Session->read('Auth.User');
                  $this->_userMail($user, __('New message', true), 'message', $data);
               }
               
               $this->Session->setFlash(__('Message has been sent successfully.', true), 'flash');
               $this->redirect(array('controller' => 'messages', 'action' => 'index'));
            }
         }
         
         /*else if ($this->data['Message']['recipient_type'] == 'company') {
            $users = $this->User->find('all', array('conditions' => array('User.active' => true, 'User.id !=' => $this->Session->read('Auth.User.id'), 'User.company_id' => $this->data['Message']['to']), 'recursive' => -1));
            if (!empty($users)) {
               foreach ($users as $user) {
                  $this->Message->id = null;
                  $this->data['Message']['recipient_id'] = $user['User']['id'];
                  $this->Message->save($this->data);
               }
            }
         } */
      }
   }
   
   function view($id = null) {
      $user = $this->User->find('first', array('conditions' => array('User.id' => $id, 'User.active' => true), 'recursive' => -1));
      if (empty($user))
         $this->redirect(array('controller' => 'messages', 'action' => 'index'));
      $this->set('sender', $user);
      if (!empty($this->data)) {
         $this->data['Message']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Message']['recipient_id'] = $id;
         if ($this->Message->save($this->data)) {
            $this->User->id = $this->data['Message']['recipient_id'];
            $unread = $this->Message->find('count', array('conditions' => array('Message.recipient_id' => $id, 'Message.read' => false)));
            $this->User->saveField('unread', $unread);
            
            if (empty($user['User']['email_ban'])) {
               $data = $this->data;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New message', true), 'message', $data);
            }
            
            $this->Session->setFlash(__('Message has been sent successfully.', true), 'flash');
            $this->data = null;
         }
      }
      $this->paginate['Message'] = array('limit' => 10, 'recursive' => -1, 'order' => array('Message.created DESC'));
      $messages = $this->paginate('Message', array('OR' => array(array('Message.sender_id' => $id, 'Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.rec_del' => false), array('Message.sender_id' => $this->Session->read('Auth.User.id'), 'Message.recipient_id' => $id, 'Message.sen_del' => false))));
      //$messages = $this->Message->find('all', array('conditions' => array('OR' => array(array('Message.sender_id' => $id, 'Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.rec_del' => false), array('Message.sender_id' => $this->Session->read('Auth.User.id'), 'Message.recipient_id' => $id, 'Message.sen_del' => false))), 'recursive' => -1, 'order' => array('Message.created DESC')));
      if (!empty($messages)) {
         $counter = 0;
         foreach ($messages as $message) {
            if ((!$message['Message']['read']) && $message['Message']['sender_id'] == $id) {
               $this->Message->id = $message['Message']['id'];
               $this->Message->saveField('read', true);
               $counter++;
            }
         }
         if ($counter > 0) {
            $this->User->id = $this->Session->read('Auth.User.id');
            $unread = $this->Message->find('count', array('conditions' => array('Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.read' => false)));
            $this->User->saveField('unread', $unread);
            $this->set('new_mm', $unread);
            $unreadd = $this->User->field('unreadd');
            $this->set('new_mess', $unread + $unreadd);
         }
      }
      $this->set('messages', $messages);
      $this->set('title_for_layout', __('Conversation with', true).' '.$user['User']['title'].' '.$user['User']['first_name'].' '.$user['User']['middle_name'].' '.$user['User']['last_name']);
   } 
   
   function delete($id = null) {
      $user = $this->User->find('first', array('conditions' => array('User.id' => $id, 'User.active' => true), 'recursive' => -1));
      if (empty($user))
         $this->redirect(array('controller' => 'messages', 'action' => 'index'));
      $this->Message->updateAll(array('Message.rec_del' => true, 'Message.read' => true), array('Message.sender_id' => $id, 'Message.recipient_id' => $this->Session->read('Auth.User.id')));
      $this->Message->updateAll(array('Message.sen_del' => true), array('Message.sender_id' => $this->Session->read('Auth.User.id'), 'Message.recipient_id' => $id));
      $this->User->id = $this->Session->read('Auth.User.id');
      $unread = $this->Message->find('count', array('conditions' => array('Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.read' => false)));
      $this->User->saveField('unread', $unread);
      $this->Session->setFlash(__('Conversation has been removed successfully.', true), 'flash');
      $this->redirect(array('controller' => 'messages', 'action' => 'index'));
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}

}
?>