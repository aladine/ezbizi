<?php
class RatingsController extends AppController {
   
   var $uses = array('Rating', 'Company', 'Notification');
    
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function add($id = null, $points = null) {
      $r_company = $this->Company->find('first', array('conditions' => array('Company.id' => $id), 'recursive' => -1));
      if (empty($r_company) || !in_array($points, array(1, 2, 3, 4, 5)))
         $this->redirect($this->referer());
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $this->Session->setFlash(__('Sorry! You are not able to rate your own company.', true), 'flash');
         $this->redirect($this->referer());
      }
      $rating = $this->Rating->find('first', array('conditions' => array('Rating.company_id' => $id, 'Rating.rater_id' => $this->Session->read('Auth.User.company_id'))));
      if (!empty($rating)) {
         $this->Session->setFlash(__('You have already rated this company.', true), 'flash');
         $this->redirect($this->referer());
      }
      $conn = $this->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
      $com1 = $this->Company->User->find('list', array('conditions' => array('User.company_id' => $id), 'fields' => array('User.id', 'User.id')));
      $com2 = $this->Company->User->find('list', array('conditions' => array('User.company_id' => $this->company['Company']['id']), 'fields' => array('User.id', 'User.id')));
      $dealcount = $this->Company->Demand->Dmessage->find('count', array('conditions' => array('Dmessage.done' => true, 'OR' => array(array('Dmessage.recipient_id' => $com1, 'Dmessage.sender_id' => $com2), array('Dmessage.recipient_id' => $com2, 'Dmessage.sender_id' => $com1)))));
      if (empty($conn) && empty($dealcount)) {
         $this->Session->setFlash(__('Sorry! You are not able to rate this company.', true), 'flash');
         $this->redirect($this->referer());
      }
      
      $data['Rating']['company_id'] = $id;
      $data['Rating']['rater_id'] = $this->Session->read('Auth.User.company_id');
      $data['Rating']['user_id'] = $this->Session->read('Auth.User.id');
      $this->Rating->save($data);
      $this->Company->id = $id;
      $data['Company']['rating_sum'] = $r_company['Company']['rating_sum'] + $points;
      $data['Company']['rating_count'] = $r_company['Company']['rating_count'] + 1;
      $data['Company']['rating'] = $data['Company']['rating_sum'] / $data['Company']['rating_count']; 
      $this->Company->save($data);
      
      $notice['Notification']['company_id'] = $id;
      $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
      $notice['Notification']['type'] = 'rating_change';
      $notice['Notification']['text'] = $data['Company']['rating'];
      $this->Notification->save($notice);
      
      $this->Session->setFlash(__('Thank you for rating this company.', true), 'flash');
      $this->redirect($this->referer());
   }
   
}
?>