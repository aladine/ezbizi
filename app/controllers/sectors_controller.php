<?php
class SectorsController extends AppController {

   var $helpers = array('Image');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('add', 'edit', 'delete');
   }
   
   function delete($id = null) {
      if ($this->Sector->delete($id))
         $this->Session->setFlash(__('Sector was deleted.', true), 'flash');
      $this->redirect($this->referer());
      //$this->redirect(array('controller' => 'categories', 'action' => 'index'));
   }
   
   function add($id = null) {
      if (!empty($this->data)) {
         $this->data['Category'] = array($id => $id);
         if ($this->Sector->save($this->data)) {
            $this->Session->setFlash(__('Sector has been successfuly saved.', true), 'flash');
            $this->redirect(array('controller' => 'categories', 'action' => 'index', $id));
         }  
      }
      $this->set('category_id', $id);
   }
   
   function edit($id = null) {
      if (!empty($this->data)) {
         if ($this->Sector->save($this->data)) {
            $this->Session->setFlash(__('Sector has been successfuly updated.', true), 'flash');
            //$this->redirect(array('controller' => 'categories', 'action' => 'index', $id));
         }
      }
      else
         $this->data = $this->Sector->find('first', array('conditions' => array('Sector.id' => $id)));
   }

}
?>