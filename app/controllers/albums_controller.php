<?php
class AlbumsController extends AppController {
   
   var $uses = array('Album', 'Notification');
   var $helpers = array('Image');
   ///public $components = array('AmazonS3'); 
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('privacy');
   }
   /*
         $s3_pth = "users/{$student['User']['userid']}.{$student['User']['id']}/profile_img.jpg";
         if (AWS_S3_STORAGE) {
                        $students_data['photo']= $this->AmazonS3->auth($s3_pth);
                     }else{
                        if (!file_exists(IMG_DIR . $s3_pth)) {                         
                                 $s3_pth = 'shared/pict_default_109x112.jpg';                               
                        }
                        $students_data['photo']='http://'.$_SERVER['SERVER_NAME'].'/img/'.$s3_pth;
                     }
   */
   function privacy($id = null, $value = null) {
      $album = $this->Album->find('first', array('conditions' => array('Album.id' => $id, 'Album.company_id' => $this->Session->read('Auth.User.company_id')), 'recursive' => -1));
      if (empty($album))
         $this->redirect($this->referer());
      if (in_array($value, array(1, 2, 3))) {
         $this->Album->id = $id;
         $this->Album->saveField('privacy', $value);
         $this->Session->setFlash(__('Privacy settings were updated.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function delete($id = null) {
      $album = $this->Album->find('first', array('conditions' => array('Album.id' => $id, 'Album.company_id' => $this->Session->read('Auth.User.company_id')), 'recursive' => -1));
      if (!empty($album) && $this->Album->delete($id)) {
         $this->Session->setFlash(__('Album has been removed successfully.', true), 'flash');
      }
      $this->redirect(array('controller' => 'albums', 'action' => 'index'));
   }
   
   function photo_delete($id = null) {
      $photo = $this->Album->Photo->find('first', array('conditions' => array('Photo.id' => $id, 'Album.company_id' => $this->Session->read('Auth.User.company_id'))));
      if (!empty($photo) && $this->Album->Photo->delete($id)) {
         $this->Session->setFlash(__('Photo has been removed successfully.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function view($id = null) {         
      $album = $this->Album->find('first', array('conditions' => array('Album.id' => $id), 'recursive' => -1));
      if (empty($album))
         $this->redirect($this->referer());
      if ($this->company['Company']['id'] != $album['Album']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
         $conn = $this->Album->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $album['Album']['company_id'], 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $album['Album']['company_id']))), 'recursive' => -1));
         if (empty($conn) && $album['Album']['privacy'] != 1)
            $this->redirect($this->referer());
         if (!empty($conn) && !in_array($album['Album']['privacy'], array(1, 2)))     
            $this->redirect($this->referer());
      }
         
      $pcompany = $this->Album->Company->find('first', array('conditions' => array('Company.id' => $album['Album']['company_id'], 'Company.active' => true), 'recursive' => -1));
      $this->set('pcompany', $pcompany);
      
      $this->_companyPanel($pcompany['Company']['id']);
      
      $this->paginate['Photo'] = array('limit' => 12, 'order' => array('Photo.created'), 'recursive' => -1);
      $album['Photo'] = $this->paginate('Photo', array('Photo.album_id' => $id));
      $this->set('album', $album);
      
      $this->set('title_for_layout', __('Album', true).':'.$album['Album']['name']);
      ///SAVE DATA 
      if (!empty($this->data) && $album['Album']['company_id'] == $this->company['Company']['id']) {
         $this->data['Photo']['album_id'] = $id;
         $this->data['Photo']['file'] = date('YmdHis').$id.'.jpg';
         if ($this->Album->Photo->save($this->data)) {
            __fileSave($this->data['Photo']['image'], 'img'.DS.'comps'.DS.$album['Album']['company_id'].DS.$id.DS.$this->data['Photo']['file'], 680, 680);
           ///TEST AMAZON S3 
           /// __amazonSave($this->data['Photo']['image']['tmp_name'],$album['Album']['company_id']);
            if ($this->Session->check('PhotoNotification'.$id)) {   
               $this->Notification->id = $this->Session->read('PhotoNotification'.$id);
               $text = $this->Session->read('PhotoNotification'.$id.'photos').'|'.$this->Album->Photo->id;
               $this->Notification->saveField('text', $text);
               $this->Session->write('PhotoNotification'.$id.'photos', $text);
            }
            else {
               $notice['Notification']['company_id'] = $this->Session->read('Auth.User.company_id');
               $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
               $notice['Notification']['type'] = 'new_photos';
               $notice['Notification']['text'] = $album['Album']['id'];
               if ($this->Notification->save($notice)) {
                  $this->Session->write('PhotoNotification'.$id, $this->Notification->id);
                  $this->Session->write('PhotoNotification'.$id.'photos', $id.'|'.$this->Album->Photo->id);
                  $this->Notification->saveField('text', $id.'|'.$this->Album->Photo->id);
               }
            }
            $this->Session->setFlash(__('Photo has been uploaded successfully.', true), 'flash');
            $this->redirect(array('controller' => 'albums', 'action' => 'view', $id));
         }
      }
   }
   
   function index($id = null) {
      if (empty($id)) {
         $id = $this->Session->read('Auth.User.company_id');
         $this->set('pcompany', $this->company);
      }
      else {
         $pcompany = $this->Album->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
         if (empty($pcompany))
            $this->redirect($this->referer());
         $this->set('pcompany', $pcompany);
      }
      $this->set('title_for_layout', __('Albums', true));
      
      $this->_companyPanel($id);
      
      $this->paginate['Album'] = array('limit' => '12', 'order' => array('Album.created DESC'));
      $conditions = array('Album.company_id' => $id);
      if ($this->company['Company']['id'] != $id && $this->Session->read('Auth.User.group_id') != 1) {
         $conn = $this->Album->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
         if (!empty($conn))
            $conditions['Album.privacy'] = array(1, 2);
         else     
            $conditions['Album.privacy'] = 1;
      }
      
      $albums = $this->paginate('Album', $conditions);
      $this->set('albums', $albums);
      
      if (!empty($this->data)) {
         $this->data['Album']['company_id'] = $this->company['Company']['id'];
         if ($this->Album->save($this->data)) {
            $this->Session->setFlash(__('New album has been added successfully.', true), 'flash');
            $this->redirect(array('controller' => 'albums', 'action' => 'index'));
         }
      }
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $this->set('user', $user);
         $colleagues = $this->Album->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->Album->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->Album->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }

}
?>