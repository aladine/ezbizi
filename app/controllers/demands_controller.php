<?php
class DemandsController extends AppController {
   
   var $helpers = array('Image');
   var $uses = array('Demand', 'Notification', 'SearchIndex');
   var $components = array('Email');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('display', 'search', 'view');
   }
   
   function resave() {
      $demands = $this->Demand->find('all', array('recursive' => -1));
		foreach ($demands as $demand) {
			$this->Demand->id = $demand['Demand']['id'];
			$this->Demand->save($demand, false);
	   }
	   die;
   }
   
   function search($query = null) {
      $categories = $this->Demand->Company->Sector->Category->find('threaded');
      $this->set('categories', $categories);
      $countries = $this->User->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('countries', $countries);
      $currencies = $this->Demand->Currency->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('currencies', $currencies);
      $units = $this->Demand->Unit->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('units', $units);
      if (!empty($this->data['Search']['query']) || !empty($query)) {
         if (!empty($this->data['Search']['type_id']) && $this->data['Search']['type_id'] == 2)
            $this->redirect(array('controller' => 'companies', 'action' => 'search', $this->data['Search']['query']));
         if (!empty($this->data['Search']['query']))
            $this->redirect(array('controller' => 'demands', 'action' => 'search', $this->data['Search']['query']));
         $string = $query;
         App::import('Core', 'Sanitize');
		   $string = Sanitize::escape($string);
		   if (empty($string))
            $this->redirect($this->referer());
	      $this->SearchIndex->searchModels(array('Demand'));

    	 	$this->paginate = array('SearchIndex' => array('limit' => 10, 'recursive' => 2,
    	 	                        'fields' => array('Demand.id'),
			                        'conditions' =>  "(MATCH(SearchIndex.data) AGAINST('$string' IN BOOLEAN MODE) OR SearchIndex.data LIKE '%$string%') AND (`Demand`.`valid_from` <= CURDATE()) AND (`Demand`.`valid_to` >= CURDATE())"));
		   $ids = $this->paginate('SearchIndex');
		   $idss = array();
		   foreach ($ids as $id) {
            $idss []= $id['Demand']['id'];
         }
		   $demands = $this->Demand->find('all', array('conditions' => array('Demand.id' => $idss), 'order' => array('Company.account_id DESC', 'Demand.created DESC'), 'recursive' => 2));
		   //$this->set('options_url', array('controller' => 'products', 'action' => 'search', $string));
	      $this->set('demands', $demands);  
         return;
      }
      if (!empty($this->data)) {
         $conditions = null;        
         if (!empty($this->data['Demand']['commodity']))
            $conditions['Demand.commodity LIKE'] = '%'.$this->data['Demand']['commodity'].'%';
         if (!empty($this->data['Demand']['country_id']) && is_numeric($this->data['Demand']['country_id']))
            $conditions['Demand.country_id'] = $this->data['Demand']['country_id'];
         if (!empty($this->data['Demand']['sector_id']) && is_numeric($this->data['Demand']['sector_id']))
            $conditions['Demand.sector_id'] = $this->data['Demand']['sector_id'];
         if (!empty($this->data['Demand']['currency_id']) && is_numeric($this->data['Demand']['currency_id'])) {            
            $conditions['Demand.currency_id'] = $this->data['Demand']['currency_id'];
            if (!empty($this->data['Demand']['price_min']) && is_numeric($this->data['Demand']['price_min']))
               $conditions['Demand.norm_price >='] = $this->data['Demand']['price_min'];
            if (!empty($this->data['Demand']['price_max']) && is_numeric($this->data['Demand']['price_max']))
               $conditions['Demand.norm_price <='] = $this->data['Demand']['price_max'];
         }
         if (!empty($this->data['Demand']['unit_id']) && is_numeric($this->data['Demand']['unit_id'])) {
            $unit = $this->Demand->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Demand']['unit_id'])));
            if (!empty($unit)) {            
               $units = $this->Demand->Unit->find('list', array('conditions' => array('Unit.dimension_id' => $unit['Unit']['dimension_id']), 'fields' => array('Unit.id')));
               $conditions['Demand.unit_id'] = $units;
               if (!empty($this->data['Demand']['quantity_min']) && is_numeric($this->data['Demand']['quantity_min']))
                  $conditions['Demand.norm_quantity >='] = $this->data['Demand']['quantity_min'] * $unit['Unit']['multiplier'];
               if (!empty($this->data['Demand']['quantity_max']) && is_numeric($this->data['Demand']['quantity_max']))
                  $conditions['Demand.norm_quantity <='] = $this->data['Demand']['quantity_max'] * $unit['Unit']['multiplier'];             
            }
         }
         $this->Session->write('DemandSearchSettings', $conditions);
         $this->Session->write('DemandSearchSettingsData', $this->data);
      }
      if (!$this->Session->check('DemandSearchSettings'))
         $this->redirect($this->referer());
      //debug($this->Session->read('DemandSearchSettings'));
      $conditions = $this->Session->read('DemandSearchSettings');
      if (empty($this->data) && $this->Session->check('DemandSearchSettingsData')) {
         $this->data = $this->Session->read('DemandSearchSettingsData');
      }
      $this->paginate['Demand'] = array('order' => array('Company.account_id DESC', 'DemandDate DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.valid_from', 'Demand.valid_to', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 10, 'recursive' => 2);
      $demands = $this->paginate('Demand', $conditions);
      $this->set('demands', $demands);
      
   }
   
   function _catsec($id) {
      $list = $this->Demand->Sector->CategoriesSector->find('list', array('conditions' => array('category_id' => $id), 'fields' => array('sector_id')));
      $cats = $this->Demand->Sector->Category->find('list', array('conditions' => array('Category.parent_id' => $id), 'fields' => array('id')));
      if (!empty($cats)) {
         foreach ($cats as $cat) {
            $secs = $this->_catsec($cat);
            $list = array_merge($list, $secs);
         }
      }
      return $list;
   }
   
   function display() {
      $args = func_get_args();
      $id = end($args);
      $type = prev($args);
      if (!in_array($type, array('c', 's')))
         $this->redirect($this->referer());
      
      $sectors = array();   
      if ($type == 's') {
         $sector = $this->Demand->Sector->find('first', array('conditions' => array('Sector.id' => $id), 'recursive' => -1));
         if (empty($sector))
            $this->redirect($this->referer());
         $sectors []= $sector['Sector']['id'];
         
      }     
      $category = null;
      if ($type == 'c') {
         $category = $this->Demand->Sector->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
         if (empty($category))
            $this->redirect($this->referer());
         $category['last'] = true;
         $sectors = $this->_catsec($category['Category']['id']);
         $this->set('model', array('Category', 'c'));
         $subs = $this->Demand->Sector->Category->find('all', array('conditions' => array('Category.parent_id' => $category['Category']['id']), 'recursive' => -1));
         if (empty($subs)) {
            $this->set('model', array('Sector', 's'));
            $secs = $this->_catsec($category['Category']['id']);
            $subs = $this->Demand->Sector->find('all', array('conditions' => array('Sector.id' => $secs), 'recursive' => -1));
         }
         $this->set('subs', $subs);
      }
      
      if (!empty($sector)) {
         $this->set('sector', $sector);
         $catid = $this->Demand->Sector->CategoriesSector->field('category_id', array('sector_id' => $sector['Sector']['id']));
         $category = $this->Demand->Sector->Category->find('first', array('conditions' => array('Category.id' => $catid), 'recursive' => -1));
         $secs = $this->_catsec($catid);
         $secs = $this->Demand->Sector->find('all', array('conditions' => array('Sector.id' => $secs), 'recursive' => -1));
         $this->set('model', array('Sector', 's'));
         $this->set('subs', $secs);
      }
      
      $this->set('title_for_layout', __('Public view of demands', true));
      
      $path = array($category);         
      while (!empty($category['Category']['parent_id'])) {
         $category = $this->Demand->Sector->Category->find('first', array('conditions' => array('Category.id' => $category['Category']['parent_id']), 'recursive' => -1));
         $path []= $category;
      }
      $path = array_reverse($path);
      $this->set('path', $path);

      $this->paginate['Demand'] = array('order' => array('DemandDate DESC', 'Company.account_id DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.commodity', 'Demand.price', 'Demand.valid_from', 'Demand.valid_to', 'Demand.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 10, 'recursive' => 2);
      $demands = $this->paginate('Demand', array('Demand.sector_id' => $sectors, 'Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d')));
      $this->set('demands', $demands);
   }
   
   function delete($id = null) {
      $demand = $this->Demand->find('first', array('conditions' => array('Demand.id' => $id), 'recursive' => -1));
      if (!empty($demand) && ($demand['Demand']['company_id'] == $this->Session->read('Auth.User.company_id') || $this->Session->read('Auth.User.group_id') == 1) && $this->Demand->delete($id)) {
         $this->Session->setFlash(__('Demand has been successfully removed.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function view($id = null) {
      $demand = $this->Demand->find('first', array('conditions' => array('Demand.id' => $id), 'recursive' => 2));
      if (empty($demand))
         $this->redirect($this->referer());
      if ($demand['Demand']['valid_from'] > date('Y-m-d') || $demand['Demand']['valid_to'] < date('Y-m-d')) {
         $this->Session->setFlash(__('This demand has not yet started or is out of date. Please double-check the validity period of this demand.', true), 'flash');
         if ($this->Session->read('Auth.User.company_id') != $demand['Demand']['company_id'])
            $this->redirect($this->referer());
      }
      if ($this->Session->check('Auth.User.id') && !empty($this->data)) {
         $last_m = $this->Demand->Dmessage->find('first', array('conditions' => array('demand_id' => $demand['Demand']['id'], 'OR' => array(array('recipient_id' => $demand['Demand']['user_id'], 'sender_id' => $this->Session->read('Auth.User.id')), array('recipient_id' => $this->Session->read('Auth.User.id'), 'sender_id' => $demand['Demand']['user_id']))), 'order' => 'created DESC', 'recursive' => -1));
         if (!empty($last_m))
            $this->redirect($this->referer());
         if ($this->company['Company']['account_id'] == 1 && $this->company['Company']['reaction_count'] >= 30) {
            $this->Session->setFlash(__('Sorry! You have reached your limit for sending offers for this month. In order to send more offers this month, please upgrade your account.', true), 'flash');
            $this->redirect($this->referer());
         }
         $this->data['Dmessage']['demand_id'] = $demand['Demand']['id'];
         $this->data['Dmessage']['recipient_id'] = $demand['Demand']['user_id'];
         $this->data['Dmessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Dmessage']['currency_id'] = $demand['Demand']['currency_id'];
         $this->data['Dmessage']['priceper_id'] = $demand['Demand']['priceper_id'];
         $this->data['Dmessage']['accepted'] = 0;
         if (!empty($this->data['Dmessage']['valid_to']))
            $this->data['Dmessage']['valid_to'] = date('Y-m-d', strtotime($this->data['Dmessage']['valid_to']));
         if ($this->Demand->Dmessage->save($this->data)) {
            $this->User->id = $demand['Demand']['user_id'];
            $unread = $this->Demand->Dmessage->find('count', array('conditions' => array('Dmessage.recipient_id' => $this->User->id, 'Dmessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            
            $this->Demand->Company->id = $this->company['Company']['id'];
            $rcount = $this->Demand->Company->field('reaction_count') + 1;
            $this->Demand->Company->saveField('reaction_count', $rcount);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $this->data['Dmessage']['recipient_id']), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $data['Demand'] = $demand;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New demand message', true), 'dmessage', $data);
            }
            
            $this->Session->setFlash(__('Your offer has been sent successfully.', true), 'flash');
            $this->redirect(array('controller' => 'demands', 'action' => 'view', $id));
         }
      }
      
      if ($this->Session->check('Auth.User.id')) {
         $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com2_id' => $this->Session->read('Auth.User.company_id'))), 'recursive' => -1));
         $comp_ids = array();
         if (!empty($conns)) {
            foreach ($conns as $conn) {
               if ($conn['Connection']['com1_id'] == $this->Session->read('Auth.User.company_id'))
                  $comp_ids[$conn['Connection']['com2_id']] = $conn['Connection']['com2_id'];
               else
                  $comp_ids[$conn['Connection']['com1_id']] = $conn['Connection']['com1_id'];
            }
         }
         if (isset($comp_ids[$demand['Demand']['company_id']]))
            $demand['Connected'] = true;
      }
      
      $this->set('demand', $demand);
      $this->set('title_for_layout', $demand['Demand']['quantity'].' '.$demand['Unit']['name_'.$this->lang].' '.__('of', true).' '.$demand['Demand']['commodity'].' '.__('for', true).' '.$demand['Demand']['price'].' '.$demand['Currency']['name_'.$this->lang].' '.__('is needed.', true));
   }
   
   function index($task = null) {
      $this->set('title_for_layout', __('Demands', true));
      $user = $this->Session->read('Auth.User');
      $this->set('user', $user);
      
      if (empty($task)) {
         if ($this->company['Company']['type_id'] == 1)
            $task = 'added';
         else
            $task = 'latests';
      }
      
      switch ($task) {
         case 'latests':
            $this->paginate['Demand'] = array('order' => array('Company.account_id DESC', 'DemandDate DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.valid_from', 'Demand.valid_to', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 10, 'recursive' => 2);
            $conditions = array('Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d'), 'Demand.company_id !=' => $user['company_id']);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Demand.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Demand.sector_id'] = $this->company['Company']['i_sector_id'];
            $demands = $this->paginate('Demand', $conditions); 
                                                                                                                                  
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids[$conn['Connection']['com2_id']] = $conn['Connection']['com2_id'];
                  else
                     $comp_ids[$conn['Connection']['com1_id']] = $conn['Connection']['com1_id'];
               }
            }
            if (!empty($demands)) {
               foreach ($demands as $ind => $demand) {
                  if (isset($comp_ids[$demand['Demand']['company_id']]))
                     $demands[$ind]['Connected'] = true;
               }
            }
            
            $this->set('demands', $demands);
            $this->set('latests', true);
            break;
         case 'added':
            $this->paginate['Demand'] = array('order' => array('Demand.created DESC'), 'limit' => 10, 'recursive' => 2);
            $demands = $this->paginate('Demand', array('Demand.company_id' => $user['company_id']));
            //$demands = $this->Demand->find('all', array('conditions' => array('Demand.company_id' => $user['company_id']), 'order' => array('Demand.created DESC'), 'limit' => 10, 'recursive' => 2));
            $this->set('demands', $demands);
            $this->set('added', true);
            break;
         case 'all':
            $this->paginate['Demand'] = array('order' => array('Company.account_id DESC', 'DemandDate DESC'), 'fields' => array('Demand.id', 'Demand.company_id', 'Demand.quantity', 'Demand.valid_from', 'Demand.valid_to', 'Demand.commodity', 'Demand.price', 'Demand.priceper_id', 'Unit.name_'.$this->lang, 'Currency.name_'.$this->lang, 'Priceper.name_'.$this->lang, 'Sector.name_'.$this->lang, 'Country.id', 'Country.name_'.$this->lang, 'Demand.created', 'Company.account_id', 'Company.name', 'Company.id', 'DATE(`Demand`.`created`) AS DemandDate'), 'limit' => 10, 'recursive' => 2);
            $conditions = array('Demand.valid_from <=' => date('Y-m-d'), 'Demand.valid_to >=' => date('Y-m-d'));
            $demands = $this->paginate('Demand', $conditions); 
            $this->set('demands', $demands);
            $this->set('all', true);
            break;
         default:
            $this->redirect($this->referer());
            break;
      }
      
      $categories = $this->Demand->Company->Sector->Category->find('threaded');
      $this->set('categories', $categories);
      $countries = $this->User->Company->Country->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('countries', $countries);
      $currencies = $this->Demand->Currency->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('currencies', $currencies);
      $units = $this->Demand->Unit->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('units', $units);
      
   }
   
   function add() {
      if (!$this->Session->check('Auth.User.id')) {
         $this->Session->setFlash(__('In order to access this information, please login or register your account with Ezbizi!', true), 'flash');
         $this->redirect(array('controller' => 'users', 'action' => 'register'));
      }
      if ($this->company['Company']['type_id'] == 2) {
         $this->Session->setFlash(__('Sorry! Your account is set to "seller" type, therefore you are not able to post a demand. In order to post your demand, please change the type of your account to "buyer" or "trader".', true), 'flash');
         $this->redirect($this->referer());
      }                                                   
      $this->set('title_for_layout', __('Post demand', true));
      $categories = $this->Demand->Company->Sector->Category->find('threaded');
      $this->set('categories', $categories);
      $sectors = $this->Demand->Company->Sector->find('list', array('fields' => array('id', 'name_'.$this->lang), 'order' => array('name_'.$this->lang)));
      $this->set('sectors', $sectors);
      $countries = $this->Demand->Country->find('list', array('fields' => array('id', 'name_'.$this->lang), 'order' => array('Country.name_'.$this->lang)));
      $this->set('countries', $countries);
      $currencies = $this->Demand->Currency->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('currencies', $currencies);
      $units = $this->Demand->Unit->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('units', $units);
      if (!empty($this->data)) {
         if ($this->company['Company']['account_id'] == 1 && $this->company['Company']['demand_count'] >= 15) {
            $this->Session->setFlash(__('Sorry! You have reached your limit for posting demands for this month. In order to post more demands this month, please upgrade your account.', true), 'flash');
            $this->redirect($this->referer());
         }
         if (!empty($this->data['Demand']['valid_from']))
            $this->data['Demand']['valid_from'] = date('Y-m-d', strtotime($this->data['Demand']['valid_from']));
         if (!empty($this->data['Demand']['valid_to']))
            $this->data['Demand']['valid_to'] = date('Y-m-d', strtotime($this->data['Demand']['valid_to']));
         $this->data['Demand']['company_id'] = $this->Session->read('Auth.User.company_id');
         $this->data['Demand']['user_id'] = $this->Session->read('Auth.User.id');
         $this->Demand->set($this->data);
         if ($this->Demand->validates()) {
            $unit = $this->Demand->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Demand']['unit_id']), 'recursive' => -1));
            $this->data['Demand']['dimension_id'] = $unit['Unit']['dimension_id'];
            $this->data['Demand']['norm_quantity'] = $unit['Unit']['multiplier'] * $this->data['Demand']['quantity'];
            $price = 0;
            // Zaplata zmeny podmienok vyradenim vyznamu priceper_id (priceper_id = unit_id) - zachovanie kompatibility
            $this->data['Demand']['priceper_id'] = $this->data['Demand']['unit_id'];
            if (empty($this->data['Demand']['priceper_id']))
               $price = $this->data['Demand']['price'];
            else {
               $unit = $this->Demand->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Demand']['priceper_id']), 'recursive' => -1));
               $price = ($this->data['Demand']['price'] / $unit['Unit']['multiplier']) * $this->data['Demand']['norm_quantity'];
            }
            $this->data['Demand']['norm_price'] = $price; 
            if ($this->Demand->save($this->data)) {
               $this->Session->setFlash(__('Your demand has been posted successfully.', true), 'flash');
               
               $this->Demand->Company->id = $this->company['Company']['id'];
               $dcount = $this->Demand->Company->field('demand_count') + 1;
               $this->Demand->Company->saveField('demand_count', $dcount);
               
               $notice['Notification']['company_id'] = $this->Session->read('Auth.User.company_id');
               $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
               $notice['Notification']['type'] = 'new_demand';
               $notice['Notification']['text'] = $this->Demand->id;
               $this->Notification->save($notice);
               
               $this->redirect(array('controller' => 'users', 'action' => 'home'));
            }
         }
         else {         
            $errors = $this->Demand->invalidFields();
            if (!empty($errors['sector_id']))
               $this->Session->setFlash($errors['sector_id'], 'flash');
         }
      }
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}
   
}
?>