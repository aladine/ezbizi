<?php
class ConnectionsController extends AppController {
   
   var $helpers = array('Image');
   var $uses = array('Connection', 'Notification');
    
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function add($id = null) {
      $company = $this->Connection->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
      if (empty($company) || $id == $this->Session->read('Auth.User.company_id'))
         $this->redirect($this->referer());
      $conn = $this->Connection->find('first', array('conditions' => array('OR'  => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->Session->read('Auth.User.company_id'), 'Connection.approved' => true), array('Connection.com2_id' => $id, 'Connection.com1_id' => $this->Session->read('Auth.User.company_id')))), 'recursive' => -1));
      if (!empty($conn))
         $this->redirect($this->referer());
      $connection = $this->Connection->find('first', array('conditions' => array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->Session->read('Auth.User.company_id'), 'Connection.approved' => false), 'recursive' => -1));
      if (!empty($connection)) {
         $this->Connection->id = $connection['Connection']['id'];
         $this->Connection->saveField('approved', true);
         $this->Session->setFlash(__('Connection has been approved.', true), 'flash');
         
         $notice['Notification']['company_id'] = $id;
         $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
         $notice['Notification']['type'] = 'connection';
         $notice['Notification']['text'] = $this->Session->read('Auth.User.company_id');
         $this->Notification->save($notice);
         $this->redirect(array('controller' => 'connections', 'action' => 'index'));       
      }
      else {
         if ($this->company['Company']['account_id'] == 1) {
            $com1 = $this->Connection->Company->User->find('list', array('conditions' => array('User.company_id' => $id), 'fields' => array('User.id', 'User.id')));
            $com2 = $this->Connection->Company->User->find('list', array('conditions' => array('User.company_id' => $this->company['Company']['id']), 'fields' => array('User.id', 'User.id')));
            $dealcount = $this->Connection->Company->Demand->Dmessage->find('count', array('conditions' => array('Dmessage.done' => true, 'OR' => array(array('Dmessage.recipient_id' => $com1, 'Dmessage.sender_id' => $com2), array('Dmessage.recipient_id' => $com2, 'Dmessage.sender_id' => $com1)))));
            if ($dealcount < 1) {
               $this->Session->setFlash(__('Sorry! Only parties who entered into business negotiation are able to recommend each other.', true), 'flash'); 
               $this->redirect($this->referer());
            }
         }
         if ($this->company['Company']['account_id'] == 2 && $this->company['Company']['connection_limit'] > 2) {
            $this->Session->setFlash(__('Sorry! Your you have reached your connection request limit for this month. In order to send more connection requests this month, please upgrade your account.', true), 'flash');
            $this->redirect($this->referer()); 
         }
         $data['Connection']['com1_id'] = $this->Session->read('Auth.User.company_id');
         $data['Connection']['com2_id'] = $id;
         $this->Connection->save($data);
         $this->Connection->Company->id = $this->Session->read('Auth.User.company_id');
         $conlim = $this->Connection->Company->field('connection_limit');
         $this->Connection->Company->saveField('connection_limit', $conlim + 1);
         $this->Session->setFlash(__('Request for connection has been sent successfully.', true), 'flash');
      }
      $this->redirect($this->referer());
   }                                                  
   
   function delete($id = null) {
      $conn = $this->Connection->find('first', array('conditions' => array('OR' => array(array('Connection.com1_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com2_id' => $id), array('Connection.com2_id' => $this->Session->read('Auth.User.company_id'), 'Connection.com1_id' => $id))), 'recursive' => -1));
      if (!empty($conn)) {
         $this->Connection->delete($conn['Connection']['id']);
         $this->Session->setFlash(__('Connection has been successfully removed.', true), 'flash');
      }
      $this->redirect(array('controller' => 'connections', 'action' => 'index'));
   }
   
   function index($task = null) {
      $id = null;
      if (empty($task) || $task == 'requests') {
         $id = $this->Session->read('Auth.User.company_id');
         $this->set('pcompany', $this->company);
      }
      else if ($task != 'requests') {
         $id = $task;     
         $pcompany = $this->Connection->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
         if (empty($pcompany))
            $this->redirect($this->referer());
         $this->set('pcompany', $pcompany);
      }
     
      $this->set('title_for_layout', __('Connections', true));
      $user = $this->Session->read('Auth.User');
      $this->set('user', $user);
      
      $this->_companyPanel($id); 
      
      if ($task == 'requests') {
         $new_cons = $this->Connection->find('list', array('conditions' => array('Connection.com2_id' => $user['company_id'], 'Connection.approved' => false), 'fields' => array('Connection.com1_id', 'Connection.com1_id')));
         $this->paginate['Company'] = array('limit' => 10, 'order' => array('Company.name'));
         $new_connections = $this->paginate('Company', array('Company.id' => $new_cons));
         $this->set('new_connections', $new_connections);
         $this->render('requests');
      }     
      
      $conns = $this->Connection->find('all', array('conditions' => array('Connection.approved' => true, 'OR' => array('Connection.com1_id' => $id, 'Connection.com2_id' => $id)), 'recursive' => -1));
      $comp_ids = array();
      if (!empty($conns)) {
         foreach ($conns as $conn) {
            if ($conn['Connection']['com1_id'] == $id)
               $comp_ids []= $conn['Connection']['com2_id'];
            else
               $comp_ids []= $conn['Connection']['com1_id'];
         }
      }
      
      $this->paginte['Company'] = array('limit' => 10, 'order' => array('Company.name'));
      $connections = $this->paginate('Company', array('Company.id' => $comp_ids));
      
      $this->set('connections', $connections);
      
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }
   
}
?>