<?php
class InterestsController extends AppController {

   var $helpers = array('Image');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function delete($id = null) {
      $interest = $this->Interest->find('first', array('conditions' => array('Interest.recipient_id' => $this->company['Company']['id'], 'Interest.sender_id' => $id), 'recursive' => -1));
      if (!empty($interest) && $this->Interest->delete($interest['Interest']['id']))
         $this->Session->setFlash(__('Interest has been removed succesfully.', true), 'flash');
      $this->redirect($this->referer());
   }
   
   function index() {
      $this->_companyPanel($this->company['Company']['id']);
      $this->set('title_for_layout', __('Interests', true));
      if ($this->company['Company']['account_id'] != 1) {
         $this->paginate['Interest'] = array('limit' => '12', 'order' => array('Interest.created DESC'), 'recursive' => -1);
         $interests = $this->paginate('Interest', array('Interest.recipient_id' => $this->company['Company']['id']));
         if (!empty($interests)) {
            $ids = array();
            foreach ($interests as $interest) {
               $ids []= $interest['Interest']['sender_id'];
            }
            $companies = $this->Interest->Company->find('all', array('conditions' => array('Company.id' => $ids)));
            $this->set('interests', $companies);
         }
      }
   }
   
   function add($id = null) {
      $comp = $this->Interest->Company->find('first', array('conditions' => array('Company.id' => $id/*, 'Company.account_id' => 1*/), 'recursive' => -1));
      if (!empty($comp) && $id != $this->company['Company']['id'] && $this->company['Company']['account_id'] == 1) {
         $conn = $this->Interest->Company->Connection->find('first', array('conditions' => array('OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
         if (empty($conn)) {
            $interest = $this->Interest->find('first', array('conditions' => array('Interest.recipient_id' => $id, 'Interest.sender_id' => $this->company['Company']['id']), 'recursive' => -1));
            if (empty($interest)) {           
               $data['Interest']['recipient_id'] = $id;                                
               $data['Interest']['sender_id'] = $this->company['Company']['id'];
               $data['Interest']['user_id'] = $this->Session->read('Auth.User.id');
               if ($this->Interest->save($data))
                  $this->Session->setFlash(__('Interest has been send succesfully.', true), 'flash');
            }
            else
              $this->Session->setFlash(__('This company already received your interest.', true), 'flash');
         } 
      }
      $this->redirect($this->referer());
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $this->set('pcompany', $this->company);
         
         $user = $this->Session->read('Auth.User');
         $this->set('user', $user);
         $colleagues = $this->Interest->Company->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->Interest->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->Interest->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }

}