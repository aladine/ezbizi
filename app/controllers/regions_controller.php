<?php
class RegionsController extends AppController {
   
   var $helpers = array('Image');
     
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('index', 'delete', 'add');
   }
   
   function index($id = null) {
      $country = $this->Region->Country->find('first', array('conditions' => array('Country.id' => $id), 'recursive' => -1));
      if (empty($country))
         $this->redirect($this-referer());
      $this->set('country', $country);
      $this->paginate['Region'] = array('limit' => 10, 'order' => array('Region.name_'.$this->lang => 'asc'));
      $regions = $this->paginate('Region', array('Region.country_id' => $id));
      $this->set('regions', $regions);
   }
   
   function add($id = null) {
      $country = $this->Region->Country->find('first', array('conditions' => array('Country.id' => $id), 'recursive' => -1));
      if (empty($country))
         $this->redirect($this-referer());
      if (!empty($this->data)) {
         $this->data['Region']['country_id'] = $id;
         if ($this->Region->save($this->data)) {
            $this->Session->setFlash(__('Region has been added.', true), 'flash');
            $this->redirect(array('controller' => 'regions', 'action' => 'index', $id));
         }
      }
      $this->set('country', $country);
   }
   
   function delete($id = null) {
      if ($this->Region->delete($id)) 
         $this->Session->setFlash(__('Region was deleted.', true), 'flash');
      $this->redirect($this->referer());
   }

}
?>