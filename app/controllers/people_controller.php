<?php
class PeopleController extends AppController {

   var $helpers = array('Image');
   var $uses = array('People');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function index($id = null) {
      $this->paginate['People'] = array('limit' => 10, 'order' => array('People.name asc'));
      if (!empty($id))
         $this->paginate['People']['conditions']['People.firm_id'] = $id;
      $peoples = $this->paginate('People');
      $this->set('peoples', $peoples);
   }
   
   function delete($id = null) {
      if ($this->People->delete($id))
         $this->Session->setFlash(__('User has been removed from the archive.', true), 'flash');
      $this->redirect($this->referer());
   }

}
?>