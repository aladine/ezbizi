<?php
class CategoriesController extends AppController {

   var $helpers = array('Image');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('index', 'delete', 'remove_sector');
   }
   
   function remove_sector($cat = null, $sec = null) {
      $node = $this->Category->CategoriesSector->find('first', array('conditions' => array('category_id' => $cat, 'sector_id' => $sec), 'recursive' => -1));
      if (!empty($node) && $this->Category->CategoriesSector->delete($node['CategoriesSector']['id']))
         $this->Session->setFlash(__('Sector has been removed.', true), 'flash');
      $this->redirect($this->referer());
   }
   
   function delete($id = null) {
     if ($this->Category->delete($id))
        $this->Session->setFlash(__('Category has been removed.', true), 'flash');
     $this->redirect($this->referer());
   }
   
   function index($id = null) {
      $parent = null;
      if (!empty($id))
         $parent = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
      $this->set('parent', $parent);
      if (!empty($parent)) {
         $path = array($id => $parent['Category']['name_'.$this->lang]);
         while (!empty($parent['Category']['parent_id'])) {
            $parent = $this->Category->find('first', array('conditions' => array('Category.id' => $parent['Category']['parent_id']), 'recursive' => -1));
            $path[$parent['Category']['id']] = $parent['Category']['name_'.$this->lang];
         }
         $path = array_reverse($path, true);
         $this->set('path', $path);
      }
      $categories = $this->Category->find('all', array('conditions' => array('Category.parent_id' => $id), 'recursive' => -1, 'order' => array('Category.name_'.$this->lang)));
      $this->set('categories', $categories);
      $ids = $this->Category->CategoriesSector->find('list', array('conditions' => array('category_id' => $id), 'fields' => array('sector_id')));
      $sectors = $this->Category->Sector->find('list', array('conditions' => array('Sector.id' => $ids), 'fields' => array('id', 'name_'.$this->lang), 'order' => array('name_'.$this->lang => 'asc')));
      $this->set('sectors', $sectors);
   }
   
   function add($id = null) {
      if (!empty($this->data)) {
         if ($this->Category->save($this->data)) {
            $this->Session->setFlash(__('Category has been successfuly saved.', true), 'flash');
            $this->redirect(array('controller' => 'categories', 'action' => 'index', $this->data['Category']['parent_id']));
         }  
      }
      else
         $this->data['Category']['parent_id'] = $id;
      $categories = $this->Category->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('categories', $categories);
   }                                                       

   function edit($id = null) {
      if (!empty($this->data)) {
         //debug($this->data); die;
         if ($this->Category->save($this->data))
            $this->Session->setFlash(__('Category has been successfuly updated.', true), 'flash');
      }
      else {
         $this->data = $this->Category->find('first', array('conditions' => array('Category.id' => $id)));
      }
      $categories = $this->Category->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('categories', $categories);
      $sectors = $this->Category->Sector->find('list', array('fields' => array('id', 'name_'.$this->lang)));
      $this->set('sectors', $sectors);
   }
}
?>