<?php
class OmessagesController extends AppController {
   
   var $helpers = array('Image');
   var $components = array('Email');
   var $uses = array('Omessage', 'Package');
   
   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array();
   }
   
   function accept($id = null) {
      $Omessage = $this->Omessage->find('first', array('conditions' => array('Omessage.id' => $id, 'Omessage.accepted' => array(0, 1), 'Omessage.recipient_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
      if (!empty($Omessage)) {
         $this->Omessage->id = $id;
         $this->Omessage->saveField('accepted', -1);
         $this->Omessage->id = null;
         $this->data['Omessage']['offer_id'] = $Omessage['Omessage']['offer_id'];
         $this->data['Omessage']['recipient_id'] = $Omessage['Omessage']['sender_id'];
         $this->data['Omessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Omessage']['currency_id'] = $Omessage['Omessage']['currency_id'];
         $this->data['Omessage']['priceper_id'] = $Omessage['Omessage']['priceper_id'];
         $this->data['Omessage']['price'] = $Omessage['Omessage']['price'];
         if ($Omessage['Omessage']['accepted'] == 0) {
            $this->data['Omessage']['accepted'] = 1;
            $this->data['Omessage']['text'] = __('Accepted.', true);
         }
         else {
            $this->data['Omessage']['accepted'] = 2;
            $this->data['Omessage']['text'] = __('Congratulations, The deal is on!', true);

            $price = 0;
            $offer = $this->Omessage->Offer->find('first', array('conditions' => array('Offer.id' => $Omessage['Omessage']['offer_id']), 'recursive' => -1));
            if (empty($this->data['Omessage']['priceper_id']))
               $price = $this->data['Omessage']['price'];
            else {
               $unit = $this->Omessage->Offer->Unit->find('first', array('conditions' => array('Unit.id' => $this->data['Omessage']['priceper_id']), 'recursive' => -1));
               $price = ($this->data['Omessage']['price'] / $unit['Unit']['multiplier']) * $offer['Offer']['norm_quantity'];
            }
            $comid = $this->User->field('company_id', array('id' => $Omessage['Omessage']['sender_id']));
            $connect = $this->Omessage->Offer->Company->Connection->find('count', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $comid, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $comid)))));
            $p_limit = $this->Omessage->Offer->Currency->field('p_limit', array('id' => $offer['Offer']['currency_id']));
            if ($price < $p_limit || $this->company['Company']['account_id'] != 1 || !empty($connect)) {
               $this->data['Omessage']['done'] = true;
            } 
         }
         if ($this->Omessage->save($this->data)) {
            $this->User->id = $Omessage['Omessage']['sender_id'];;
            $unread = $this->Omessage->find('count', array('conditions' => array('Omessage.recipient_id' => $this->User->id, 'Omessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $Omessage['Omessage']['sender_id']), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $offer = $this->Omessage->Offer->find('first', array('conditions' => array('Offer.id' =>  $Omessage['Omessage']['offer_id']), 'recursive' => 1));
               $data['Offer'] = $offer;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New Offer message', true), 'Omessage', $data);
            }
            
            $this->Session->setFlash($this->data['Omessage']['text'], 'flash');
         }
      } 
      $this->redirect($this->referer());
   }
   
   function no_accept($id = null) {
      $Omessage = $this->Omessage->find('first', array('conditions' => array('Omessage.id' => $id, 'Omessage.accepted' => array(0, 1), 'Omessage.recipient_id' => $this->Session->read('Auth.User.id')), 'recursive' => -1));
      if (!empty($Omessage)) {
         $this->Omessage->id = $id;
         $this->Omessage->saveField('accepted', -1);
         $this->Omessage->id = null;
         $this->data['Omessage']['offer_id'] = $Omessage['Omessage']['offer_id'];
         $this->data['Omessage']['recipient_id'] = $Omessage['Omessage']['sender_id'];
         $this->data['Omessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Omessage']['accepted'] = -2;
         $this->data['Omessage']['text'] = __('Not accepted.', true);
         if ($this->Omessage->save($this->data)) {
            $this->User->id = $Omessage['Omessage']['sender_id'];
            $unread = $this->Omessage->find('count', array('conditions' => array('Omessage.recipient_id' => $this->User->id , 'Omessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $Omessage['Omessage']['sender_id']), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $offer = $this->Omessage->Offer->find('first', array('conditions' => array('Offer.id' =>  $Omessage['Omessage']['offer_id']), 'recursive' => 1));
               $data['Offer'] = $offer;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New Offer message', true), 'Omessage', $data);
            }
            
            $this->Session->setFlash(__('Not accepted.', true), 'flash');
         }
      }
      $this->redirect($this->referer());
   }
   
   function delete($offer_id = null, $user_id = null) {
      $user = $this->User->find('first', array('conditions' => array('User.id' => $user_id, 'User.active' => true), 'recursive' => -1));
      $offer = $this->Omessage->Offer->find('first', array('conditions' => array('Offer.id' => $offer_id), 'recursive' => -1));
      if (empty($user) || empty($offer))
         $this->redirect(array('controller' => 'Omessages', 'action' => 'index'));
      $this->Omessage->updateAll(array('Omessage.rec_del' => true, 'Omessage.read' => true), array('Omessage.offer_id' => $offer_id, 'Omessage.sender_id' => $user_id, 'Omessage.recipient_id' => $this->Session->read('Auth.User.id')));
      $this->Omessage->updateAll(array('Omessage.sen_del' => true), array('Omessage.offer_id' => $offer_id, 'Omessage.sender_id' => $this->Session->read('Auth.User.id'), 'Omessage.recipient_id' => $user_id));
      $this->User->id = $this->Session->read('Auth.User.id');
      $unreadd = $this->Omessage->find('count', array('conditions' => array('Message.recipient_id' => $this->Session->read('Auth.User.id'), 'Message.read' => false)));
      $this->User->saveField('unreadd', $unreadd);
      $this->Session->setFlash(__('Conversation has been removed successfully.', true), 'flash');
      if ($offer['Offer']['user_id'] != $this->Session->read('Auth.User.id'))
         $this->redirect(array('controller' => 'Omessages', 'action' => 'index'));
      else
         $this->redirect(array('controller' => 'Omessages', 'action' => 'index', $offer['Offer']['id']));
   }
   
   function view($offer_id = null, $user_id = null) {
      $offer = $this->Omessage->Offer->find('first', array('conditions' => array('Offer.id' => $offer_id), 'recursive' => 1));
      if (empty($offer))
         $this->redirect(array('controller' => 'Omessages', 'action' => 'index'));
      
      if ($offer['Offer']['valid_from'] > date('Y-m-d') || $offer['Offer']['valid_to'] < date('Y-m-d'))
         $this->Session->setFlash(__('This Offer has not yet started or is out of date. Please double-check the validity period for this Offer.', true), 'flash');
           
      $this->set('title_for_layout', __('Offer messages', true).' : '.$offer['Offer']['quantity'].' '.$offer['Unit']['name_'.$this->lang].__(' of ', true).$offer['Offer']['commodity'].__(' for ', true).$offer['Offer']['price'].' '.$offer['Currency']['name_'.$this->lang].__(' is needed.', true));
      
      $last_m = $this->Omessage->find('first', array('conditions' => array('offer_id' => $offer['Offer']['id'], 'OR' => array(array('recipient_id' => $user_id, 'sender_id' => $this->Session->read('Auth.User.id')), array('recipient_id' => $this->Session->read('Auth.User.id'), 'sender_id' => $user_id))), 'order' => 'created DESC', 'recursive' => -1));
      if (!empty($this->data) && !empty($last_m) && $last_m['Omessage']['sender_id'] != $this->Session->read('Auth.User.id') && empty($last_m['Omessage']['accepted'])) {
         $this->data['Omessage']['offer_id'] = $offer['Offer']['id'];
         $this->data['Omessage']['recipient_id'] = $user_id;
         $this->data['Omessage']['sender_id'] = $this->Session->read('Auth.User.id');
         $this->data['Omessage']['currency_id'] = $offer['Offer']['currency_id'];
         $this->data['Omessage']['priceper_id'] = $offer['Offer']['priceper_id'];
         $this->data['Omessage']['accepted'] = 0;
         //if (!empty($this->data['Omessage']['valid_to']))
         //   $this->data['Omessage']['valid_to'] = date('Y-m-d', strtotime($this->data['Omessage']['valid_to']));
         if ($this->Omessage->save($this->data)) {
            $this->User->id = $user_id;
            $unread = $this->Omessage->find('count', array('conditions' => array('Omessage.recipient_id' => $user_id, 'Omessage.read' => false)));
            $this->User->saveField('unreadd', $unread);
            $this->Omessage->id = $this->data['Omessage']['message_id'];
            $this->Omessage->saveField('accepted', -1);
            
            $user = $this->User->find('first', array('conditions' => array('User.id' => $user_id), 'recursive' => -1));
            if (!empty($user) && empty($user['User']['email_ban'])) {
               $data = $this->data;
               $data['Offer'] = $offer;
               $data['Sender'] = $this->Session->read('Auth.User');
               $this->_userMail($user, __('New Offer message', true), 'Omessage', $data);
            }
            
            $this->Session->setFlash(__('Your counter offer has been sent successfully.', true), 'flash');
            $this->data = null;
         }
      }
      $this->paginate['Omessage'] = array('limit' => 5, 'recursive' => 1, 'order' => array('Omessage.created DESC'));
      $Omessages = $this->paginate('Omessage', array('Omessage.offer_id' => $offer_id, 'OR' => array(array('Omessage.sender_id' => $user_id, 'Omessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Omessage.rec_del' => false), array('Omessage.recipient_id' => $user_id, 'Omessage.sender_id' => $this->Session->read('Auth.User.id'), 'Omessage.sen_del' => false))));
      $done = false;
      $deal_is_on = false;
      
      if (!empty($Omessages)) {
         $counter = 0;
         foreach ($Omessages as $Omessage) {
            if ((!$Omessage['Omessage']['read']) && $Omessage['Omessage']['sender_id'] == $user_id) {
               $this->Omessage->id = $Omessage['Omessage']['id'];
               $this->Omessage->saveField('read', true);
               $counter++;
            }
            if ($Omessage['Omessage']['done'])
               $done = true;
            if ($Omessage['Omessage']['accepted'] == 2)
               $deal_is_on = true;
         }
         if ($counter > 0) {
            $this->User->id = $this->Session->read('Auth.User.id');
            $unreadd = $this->Omessage->find('count', array('conditions' => array('Omessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Omessage.read' => false)));
            $this->User->saveField('unreadd', $unreadd);
            $this->set('new_dm', $unreadd);
            $unread = $this->User->field('unread');
            $this->set('new_mess', $unread + $unreadd);
         }
      }
      else
         $this->redirect($this->referer());
      $this->set('Offer', $offer);
      $this->set('Omessages', $Omessages);
      
      
      $id = $offer['Company']['id'];
      if ($id == $this->Session->read('Auth.User.company_id'))
         $id = $this->User->field('company_id', array('id' => $user_id));
      $conn = $this->User->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
      
      if ($this->company['Company']['account_id'] == 3 || !empty($conn) || $done || ($deal_is_on && $this->company['Company']['account_id'] == 2)) {
         $vcompany = $this->User->Company->find('first', array('conditions' => array('Company.id' => $id)));
         $this->set('vcompany', $vcompany);
      }
      $this->set('deal_is_on', $deal_is_on);
      
      $contact = $this->User->find('first', array('conditions' => array('User.id' => $user_id), 'recursive' => -1));
      $this->set('contact', $contact);
      
      //else {
         //$package = $this->Package->find('first', array('conditions' => array('Package.account_id' => -1), 'recursive' => -1));
         //$this->set('package', $package);
      //}
   }
   
   function index($id = null) {
      if (!empty($id)) {
         $offer = $this->Omessage->Offer->find('first', array('conditions' => array('Offer.id' => $id, 'Offer.user_id' => $this->Session->read('Auth.User.id'))));
         if (empty($offer))
            $this->redirect($this->referer());
         $Omessages = $this->Omessage->find('all', array('conditions' => array('Omessage.offer_id' => $id, 'OR' => array(array('Omessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Omessage.rec_del' => false), array('Omessage.sender_id' => $this->Session->read('Auth.User.id'), 'Omessage.sen_del' => false))), 'recursive' => 2, 'order' => array('Omessage.created DESC')));
         $sorted = array();                                      
         if (!empty($Omessages)) {
            foreach ($Omessages as $Omessage) {
               if ($Omessage['Omessage']['sender_id'] == $this->Session->read('Auth.User.id') && !isset($sorted[$Omessage['Omessage']['recipient_id']])) {
                  $sorted[$Omessage['Omessage']['recipient_id']] = $Omessage;
               }
               else if ($Omessage['Omessage']['sender_id'] != $this->Session->read('Auth.User.id') && !isset($sorted[$Omessage['Omessage']['sender_id']]))
                  $sorted[$Omessage['Omessage']['sender_id']] = $Omessage;
            }
         }
         else
            $this->redirect($this->referer());
         $this->set('Omessages', $sorted);
         $this->set('Offer', $offer);
         $this->set('title_for_layout', __('Offer messages', true).' : '.$offer['Offer']['quantity'].' '.$offer['Unit']['name_'.$this->lang].__(' of ', true).$offer['Offer']['commodity'].__(' for ', true).$offer['Offer']['price'].' '.$offer['Currency']['name_'.$this->lang].__(' is needed.', true));
         $this->render('Offer');
      }
      else {
         $this->set('title_for_layout', __('Offer messages', true));
         $Omessages = $this->Omessage->find('all', array('conditions' => array('OR' => array(array('Omessage.recipient_id' => $this->Session->read('Auth.User.id'), 'Omessage.rec_del' => false), array('Omessage.sender_id' => $this->Session->read('Auth.User.id'), 'Omessage.sen_del' => false))), 'recursive' => 2, 'order' => array('Omessage.created DESC')));
         $sorted = array();
         if (!empty($Omessages)) {
            foreach ($Omessages as $Omessage) {
               if (!isset($sorted[$Omessage['Omessage']['offer_id']])) {
                  $sorted[$Omessage['Omessage']['offer_id']] = $Omessage;
               }
               if (empty($sorted[$Omessage['Omessage']['offer_id']]['NEW']) && ($Omessage['Omessage']['recipient_id'] == $this->Session->read('Auth.User.id')) && (!$Omessage['Omessage']['read']))
                  $sorted[$Omessage['Omessage']['offer_id']]['NEW'] = true;
            }
         }
         $this->set('Omessages', $sorted);
      }
   }
   
   function _userMail($user, $subject, $template, $data = null, $attachments = null) {
	   if (!empty($attachments))
         $this->Email->attachments = $attachments;
      $this->Email->to = $user['User']['email'];
	   $this->Email->subject = $subject;
	   $this->Email->from = __('www.ezbizi.com <info@ezbizi.com>', true);
	   $this->Email->template = $template;
	   $this->Email->sendAs = 'html';
	   $this->Email->delivery = 'mail';
	   $this->set('mes_data', $data);
	   $this->Email->send();
	}

}
?>