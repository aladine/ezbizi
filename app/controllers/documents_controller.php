<?php
class DocumentsController extends AppController {

   var $helpers = array('Image');
   var $uses = array('Document', 'Notification');

   function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allowedActions = array('privacy');
   }
   
   function privacy($id = null, $value = null) {
      $document = $this->Document->find('first', array('conditions' => array('Document.id' => $id, 'Document.company_id' => $this->Session->read('Auth.User.company_id')), 'recursive' => -1));
      if (empty($document))
         $this->redirect($this->referer());
      if (in_array($value, array(1, 2, 3))) {
         $this->Document->id = $id;
         $this->Document->saveField('privacy', $value);
         $this->Session->setFlash(__('Privacy settings were updated.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function view($id = null) {
      $document = $this->Document->find('first', array('conditions' => array('Document.id' => $id), 'recursive' => -1));  
      if (empty($document))
         $this->redirect($this->referer());
      
      $conn = null;   
      if ($this->company['Company']['account_id'] != 3 && $this->company['Company']['id'] != $document['Document']['company_id']) {
         $conn = $this->User->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $document['Company']['id'], 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $document['Company']['id']))), 'recursive' => -1));
         if (empty($conn)) {
            $this->Session->setFlash(__('Your company must be in connection with this company.', true), 'flash');
            $this->redirect($this->referer());
         }
      }
      
      if ($this->company['Company']['id'] != $document['Document']['company_id'] && $this->Session->read('Auth.User.group_id') != 1) {
         if (empty($conn) && $document['Document']['privacy'] != 1)
            $this->redirect($this->referer());
         if (!empty($conn) && !in_array($document['Document']['privacy'], array(1, 2)))     
            $this->redirect($this->referer());
      }
         
      $path = APP.'webroot'.DS.'files'.DS.'comps'.DS.$document['Document']['company_id'].DS;

      Configure::write('debug', 0);
      
      $this->view = 'Media';
      $info = pathinfo($document['Document']['file']);
      $pos = strlen(strrchr($document['Document']['file'], '.'));    
      $name = substr($document['Document']['file'], 0, -$pos);       
      $params = array('id' => $info['basename'],              
                      'name' => $name,              
                      'download' => true,              
                      'extension' => strtolower($info['extension']),              
                      'path' => $path);       
      $this->set($params);
   }
   
   function delete($id = null) {
      $document = $this->Document->find('first', array('conditions' => array('Document.id' => $id, 'Document.company_id' => $this->Session->read('Auth.User.company_id')), 'recursive' => -1));
      if (!empty($document) && $this->Document->delete($id)) {
         $this->Session->setFlash(__('Document has been successfully removed.', true), 'flash');
      }
      $this->redirect($this->referer());
   }
   
   function index($id = null) {
      if (empty($id)) {
         $id = $this->Session->read('Auth.User.company_id');
         $this->set('pcompany', $this->company);
      }
      else {
         $pcompany = $this->Document->Company->find('first', array('conditions' => array('Company.id' => $id, 'Company.active' => true), 'recursive' => -1));
         if (empty($pcompany))
            $this->redirect($this->referer());
         $this->set('pcompany', $pcompany);
      }
   
      $this->set('title_for_layout', __('Documents', true));
      
      $user = $this->Session->read('Auth.User');
      $this->set('user', $user);

      $this->_companyPanel($id);
      
      $this->paginate['Document'] = array('limit' => 10, 'recursive' => -1, 'order' => array('Document.created DESC'));
      
      $conditions = array('Document.company_id' => $id);
      if ($this->company['Company']['id'] != $id && $this->Session->read('Auth.User.group_id') != 1) {
         $conn = $this->Document->Company->Connection->find('first', array('conditions' => array('Connection.approved' => true, 'OR' => array(array('Connection.com1_id' => $id, 'Connection.com2_id' => $this->company['Company']['id']), array('Connection.com1_id' => $this->company['Company']['id'], 'Connection.com2_id' => $id))), 'recursive' => -1));
         if (!empty($conn))
            $conditions['Document.privacy'] = array(1, 2);
         else     
            $conditions['Document.privacy'] = 1;
      }
      
      $documents = $this->paginate('Document', $conditions);
      $this->set('documents', $documents);
      
      if (!empty($this->data) && $id == $this->company['Company']['id']) {
         $this->data['Document']['company_id'] = $this->company['Company']['id'];
         if (!empty($this->data['Document']['document']['name']))
            $this->data['Document']['file'] = date('YmdHis').'_'.$this->data['Document']['document']['name'];
         if ($this->Document->save($this->data)) {
            $dir = WWW_ROOT.'files'.DS.'comps'.DS.$this->company['Company']['id'];
            if (!file_exists($dir))
               mkdir($dir, 0755, true);
            move_uploaded_file($this->data['Document']['document']['tmp_name'], $dir.DS.$this->data['Document']['file']);
            
            $notice['Notification']['company_id'] = $this->company['Company']['id'];
            $notice['Notification']['user_id'] = $this->Session->read('Auth.User.id');
            $notice['Notification']['type'] = 'new_document';
            $notice['Notification']['text'] = $this->Document->id;
            $this->Notification->save($notice);
            
            $this->Session->setFlash(__('Document has been successfully added.', true), 'flash');
            $this->redirect(array('controller' => 'documents', 'action' => 'index'));
         }
      }      
   }
   
   function _companyPanel($id) {
      if ($id == $this->Session->read('Auth.User.company_id')) {
         $user = $this->Session->read('Auth.User');
         $colleagues = $this->User->find('all', array('conditions' => array('User.company_id' => $user['company_id'], 'User.active' => true, 'User.id !=' => $user['id']), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      
         if ($this->company['Company']['account_id'] != 1) {
            $conns = $this->User->Company->Connection->find('all', array('conditions' => array(/*'Connection.approved' => true, */'OR' => array('Connection.com1_id' => $user['company_id'], 'Connection.com2_id' => $user['company_id'])), 'recursive' => -1));
            $comp_ids = array();
            if (!empty($conns)) {
               foreach ($conns as $conn) {
                  if ($conn['Connection']['com1_id'] == $user['company_id'])
                     $comp_ids []= $conn['Connection']['com2_id'];
                  else
                     $comp_ids []= $conn['Connection']['com1_id'];
               }
            }
            $comp_ids []= $user['company_id'];
            
            $comcon = 'Company.id !=';       
            if (count($comp_ids) > 1)
               $comcon = 'Company.id NOT';
            else
               $comp_ids = current($comp_ids);
            
            $conditions = array($comcon => $comp_ids, 'Company.active' => true);
            if (!empty($this->company['Company']['i_locality_id']))
               $conditions['Company.country_id'] = $this->company['Company']['i_locality_id'];
            if (!empty($this->company['Company']['i_sector_id']))
               $conditions['Company.sector_id'] = $this->company['Company']['i_sector_id'];  
            $recommended = $this->User->Company->find('all', array('conditions' => $conditions, 'order' => array('RAND()'), 'limit' => 5));
            $this->set('recommended', $recommended);
         }
      }
      else {
         $colleagues = $this->User->Company->User->find('all', array('conditions' => array('User.company_id' => $id, 'User.active' => true), 'limit' => 6, 'order' => 'RAND()', 'recursive' => -1));
         $this->set('colleagues', $colleagues);
      }
   }
   
}
?>