﻿
--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `quantity` float NOT NULL,
  `unit_id` int(11) NOT NULL,
  `norm_quantity` float NOT NULL,
  `dimension_id` int(11) NOT NULL,
  `sector_id` int(11) NOT NULL,
  `commodity` varchar(100) NOT NULL,
  `country_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `norm_price` float NOT NULL,
  `priceper_id` int(11) DEFAULT NULL,
  `currency_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;




--
-- Table structure for table `dmessages`
--

CREATE TABLE IF NOT EXISTS `omessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `price` float DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `priceper_id` int(11) DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `rec_del` tinyint(1) NOT NULL DEFAULT '0',
  `sen_del` tinyint(1) NOT NULL DEFAULT '0',
  `accepted` smallint(6) NOT NULL DEFAULT '0',
  `done` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

#Add offer_count in companies
ALTER TABLE  `companies` ADD  `offer_count` INT( 11 ) NOT NULL DEFAULT  '0' AFTER  `demand_count`;

